<?php
namespace PC\UI\Plugin;
class PluginMenu extends \Pecee\UI\Menu\Menu {
	protected static $instance;
	/**
	 * Get instance
	 * @return \PC\UI\Plugin\PluginMenu
	 */
	public static function Instance() {
		if(is_null(self::$instance)) {
			self::$instance=new self();
		}
		return self::$instance;
	}
}