<?php
namespace PC\UI\Form\Validate;
class ValidateDate extends \PC\UI\Form\Validate\ValidateInput {
	protected $allowEmpty;
	protected $msg;
	public function __construct($allowEmpty=false, $msg='Error/InvalidDate') {
		$this->allowEmpty=$allowEmpty;
		$this->msg=$msg;
	}

	public function validate() {
		return ($this->allowEmpty && empty($this->value) || \Pecee\Date::IsValid($this->value));
	}
	public function getErrorMessage() {
		return $this->_($this->msg, $this->name);
	}
}