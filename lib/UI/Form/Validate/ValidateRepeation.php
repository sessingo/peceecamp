<?php
namespace PC\UI\Form\Validate;
class ValidateRepeation extends \PC\UI\Form\Validate\ValidateInput {
	protected $compareName;
	protected $compareValue;
	protected $caseSensitive;
	public function __construct( $compareName, $compareValue, $caseSensitive = true ) {
		$this->compareName = $compareName;
		$this->compareValue = $compareValue;
		$this->caseSensitive = $caseSensitive;
	}
	public function validate() {
		if(!$this->caseSensitive) {
			return ((bool)strtolower($this->compareValue) == strtolower($this->value));
		}
		return ((bool)$this->compareValue == $this->value);
	}
	public function getErrorMessage() {
		return $this->_('Error/InvalidRepeation', $this->compareName, $this->name);
	}
}