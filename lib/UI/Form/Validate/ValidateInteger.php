<?php
namespace PC\UI\Form\Validate;
class ValidateInteger extends \PC\UI\Form\Validate\ValidateInput {
	protected $msg;
	public function __construct($msg=NULL) {
		$this->msg=$msg;
	}

	public function validate() {
		return (\Pecee\Integer::is_int($this->value));
	}
	public function getErrorMessage() {
		return ($this->msg) ? $this->msg : $this->_('Error/InvalidNumber', $this->name);
	}
}