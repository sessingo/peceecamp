<?php
namespace PC\UI\Form\Validate\File;
class FileAllowedExtension extends \PC\UI\Form\Validate\ValidateInput {
	protected $file;
	protected $extensions;
	public function __construct($file, array $extensions) {
		$this->file = $file;
		$this->extensions=$extensions;
	}


	public function validate() {
		$ext=\Pecee\IO\File::GetExtension($this->file->name);
		return (in_array($ext, $this->extensions));
	}

	public function getErrorMessage() {
		return $this->_('Error/InvalidFormat', $this->name);
	}
}