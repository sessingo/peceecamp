<?php
namespace PC\UI\Form\Validate\File;
class FileMimeType extends FileAbstract {
	protected $mimeTypes;
	protected $msg;
	public function __construct(array $mimeTypes, $msg=NULL) {
		$this->msg = $msg;
		$this->mimeTypes=$mimeTypes;
	}

	public function validate() {
		return (in_array(strtolower($this->fileType), $this->mimeTypes));
	}

	public function getErrorMessage() {
		return ($this->msg) ? $this->msg :  $this->_('Error/InvalidFormat', array($this->name));
	}
}