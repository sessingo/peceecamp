<?php
namespace PC\UI\Form\Validate\File;
class FileNotNullOrEmpty extends \PC\UI\Form\Validate\ValidateInput {
	protected $file;
	public function __construct($file) {
		$this->file = $file;
	}
	public function validate() {
		return (!empty($this->file->name) && $this->file->size > 0 && $this->file->error == 0);
	}
	public function getErrorMessage() {
		return $this->_('Error/CannotBeEmpty', $this->name);
	}
}