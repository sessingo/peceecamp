<?php
namespace PC\UI\Form\Validate;
class ValidateEmail extends \PC\UI\Form\Validate\ValidateInput {
	public function validate() {
		return (\Pecee\Util::is_email($this->value));
	}
	public function getErrorMessage() {
		return $this->_('Error/InvalidEmail', $this->name);
	}
}