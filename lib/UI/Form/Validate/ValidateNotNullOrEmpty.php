<?php
namespace PC\UI\Form\Validate;
class ValidateNotNullOrEmpty extends \PC\UI\Form\Validate\ValidateInput {
	protected $msg;
	public function __construct($msg=NULL) {
		$this->msg=$msg;
	}

	public function validate() {
		return (!empty($this->value));
	}
	public function getErrorMessage() {
		return ($this->msg) ? $this->msg : $this->_('Error/Required', $this->name);
	}
}