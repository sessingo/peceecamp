<?php
namespace PC\Controller\Site;
class ControllerJs extends \PC\Controller\File\ControllerAbstract {
	public function __construct() {
		parent::__construct(self::TYPE_JAVASCRIPT);
	}
}