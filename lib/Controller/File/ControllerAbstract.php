<?php
namespace PC\Controller\File;
use Pecee\Str;

abstract class ControllerAbstract extends \Pecee\Controller\File\ControllerFile {
	protected $isAdmin = FALSE;
	public function wrapView($files = NULL) {
		set_time_limit(60);

		$this->files = $files;
		$this->cacheDate = $this->getParam('_', '');
		header('Content-type: '.$this->getHeader());
		header('Charset: ' . \Pecee\UI\Site::GetInstance()->getCharset());
		header('Cache-Control: must-revalidate');
		header('Expires: ' . gmdate("D, d M Y H:i:s", time() + 9600) . ' GMT');

		if(!in_array('ob_gzhandler', ob_list_handlers())) {
			ob_start ("ob_gzhandler");
		}
		if(isset($_GET['clearcache']) && \Pecee\Auth::GetInstance()->hasAdminIP() && is_dir(self::TEMP_DIR)) {
			$handle = opendir(self::TEMP_DIR);
			while (false !== ($file = readdir($handle))) {
				if($file == (md5(\PC\Router::GetCurrentRoute(FALSE)) . '.' . $this->type)) {
					unlink(self::TEMP_DIR . DIRECTORY_SEPARATOR . $file);
				}
			}
			closedir($handle);
		}
		if($this->isAdmin || !file_exists($this->getTempFile()) || \Pecee\Registry::GetInstance()->get('DisableFileWrapperCache', FALSE)) {
			$this->saveTempFile();
		}
		echo file_get_contents($this->getTempFile(), FILE_USE_INCLUDE_PATH);
	}
	protected function saveTempFile() {
		if($this->files) {
			$files = (strpos($this->files, ',')) ? @explode(',', $this->files) : array($this->files);
			if(count($files) > 0) {
				/* Begin wrapping */
				$dir=$_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . self::TEMP_DIR;
				if(!is_dir($dir)) {
					\Pecee\IO\File::CreatePath($dir);
				}
				$handle = fopen($this->getTempFile(), 'w+', FILE_USE_INCLUDE_PATH);
				if($handle) {
					foreach($files as $index=>$file) {
						$content = file_get_contents('www/' . $this->getFile($file), FILE_USE_INCLUDE_PATH);
						if($content) {
							if($this->type==self::TYPE_JAVASCRIPT && !$this->debugMode()) {
								$content=\Pecee\Web\Minify\JSMin\JSMin::minify($content);
							}

							if($this->type==self::TYPE_CSS && !$this->debugMode()) {
								$content=\Pecee\Web\Minify\CSSMin::process($content);
							}

							$buffer = '/* '.strtoupper($this->type).': ' . $file . ' */';
							$buffer.= ($this->debugMode()) ? $content : Str::removeTabs($content);

							if( $index < count($files)-1 ) {
								$buffer .= str_repeat(chr(10),2);
							}
							fwrite($handle, $buffer);
						}
					}
					fclose($handle);
					chmod($this->getTempFile(), 0777);
				}
			}
		}
	}

	protected function getFile($file) {
		switch($this->type) {
			case self::TYPE_JAVASCRIPT:
				if(\PC\Plugin::Instance()->getPlugin()) {
					$path = sprintf('plugin/%s/theme/%s/js/%s',
									\PC\Plugin::Instance()->getPlugin()->name,
									\PC\Plugin::Instance()->getPlugin()->application->Theme,
									$file);

					if(file_exists($path) && !$this->isAdmin) {
						return $path;
					}
				}
				return \Pecee\UI\Site::GetInstance()->getJsPath() . DIRECTORY_SEPARATOR . $file;
				break;
			case self::TYPE_CSS:
				if(\PC\Plugin::Instance()->getPlugin()) {
					$path = sprintf('plugin/%s/theme/%s/css/%s',
							\PC\Plugin::Instance()->getPlugin()->name,
							\PC\Plugin::Instance()->getPlugin()->application->Theme,
							$file);
					if(file_exists($path) && !$this->isAdmin) {
						return $path;
					}
				}
				return \Pecee\UI\Site::GetInstance()->getCssPath() . DIRECTORY_SEPARATOR . $file;
				break;
		}
		return '';
	}

	protected function getTempFile() {
		return sprintf('%s.%s', self::TEMP_DIR.DIRECTORY_SEPARATOR.md5(\PC\Router::GetCurrentRoute(FALSE)), $this->type);
	}
}