<?php
namespace PC\Controller\Admin;
class ControllerJs extends \PC\Controller\File\ControllerAbstract {
	public function __construct() {
		$this->isAdmin = TRUE;
		parent::__construct(self::TYPE_JAVASCRIPT);
	}
}