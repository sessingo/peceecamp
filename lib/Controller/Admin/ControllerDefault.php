<?php
namespace PC\Controller\Admin;
class ControllerDefault extends \PC\Controller\Admin\ControllerAbstract {
	public function __construct() {
		parent::__construct(FALSE);
	}
	public function indexView() {
		if(!\Pecee\Model\User\ModelUser::IsLoggedIn()) {
			\Pecee\Auth::GetInstance()->setAuthLevel(NULL);
			echo new \PC\Widget\Admin\Login\LoginHome();
		}else{
			echo new \PC\Widget\Admin\AdminHome();
		}
	}

	/*public function forgotView() {
		if(!\Pecee\Model\User\ModelUser::IsLoggedIn()) {
			echo new \PC\Widget\Admin\Login\LoginForgotUser();
		} else {
			\PC\Router::Redirect('', '');
		}
	}*/

	public function signoutView() {
		if(\Pecee\Model\User\ModelUser::IsLoggedIn()) {
			\Pecee\Model\User\ModelUser::Current()->signOut();
		}
		\PC\Router::Redirect(\PC\Router::GetRoute(\Pecee\Registry::GetInstance()->get('AdminUrl','admin'), ''));
	}

	public function settingsView() {
		echo new \PC\Widget\Admin\Settings\SettingsHome();
	}
}