<?php
namespace PC\Controller\Admin;
class ControllerUser extends \PC\Controller\Admin\ControllerAbstract {
	public function indexView() {
		echo new \PC\Widget\Admin\User\UserHome();
	}
	public function addView() {
		echo new \PC\Widget\Admin\User\UserAdd();
	}
	public function deleteView($userId) {
		echo new \PC\Widget\Admin\User\UserDelete($userId);
	}
	public function editView($userId) {
		echo new \PC\Widget\Admin\User\UserEdit($userId);
	}
}