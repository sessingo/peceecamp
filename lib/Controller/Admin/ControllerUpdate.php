<?php
namespace PC\Controller\Admin;
class ControllerUpdate extends \PC\Controller\Admin\ControllerAbstract {

	protected $updateLog;

	public function __construct() {
		$this->updateLog = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'PeceeCampUpdateLog.txt';
	}

	public function latestJson() {
		$result = array('result' => FALSE);
		$update = new \PC\Update();

		try {
			$updateAvailible = $update->updateAvailable();
			$result['result'] = TRUE;
			$result['version'] = $updateAvailible;
			if($updateAvailible) {
				$result['changelog'] = $update->getChangelog();
			}
		}catch(\Exception $e) {
			$result['msg'] = $e->getMessage();
		}

		$this->asJSON($result);
	}

	public function pingJson() {
		$status = array('status' => NULL);
		if(file_exists($this->updateLog)) {
			$log = file_get_contents($this->updateLog);
			$tmp = explode('|', $log);
			if(count($tmp) > 1) {
				$status['progress'] = $tmp[0];
				$status['status'] = $tmp[1];
			}
		}

		$this->asJSON($status);
	}

	protected function setProgress($status, $percentage) {
		$file = fopen($this->updateLog, 'w+');
		if($file) {
			fwrite($file, sprintf('%s|%s', $percentage, $status));
			fclose($file);
		}
	}

	public function updateJson() {
		if(file_exists($this->updateLog)) {
			unlink($this->updateLog);
		}

		$this->setProgress($this->_('Admin/LiveUpdate/Status/Searching'),1);
		// Ensure that the script doesn't time out
		set_time_limit(9999999);

		$result = array('result' => FALSE);

		try {
			$settings = new \PC\Update();
			if($settings->updateAvailable()) {
				$this->setProgress($this->_('Admin/LiveUpdate/Status/DownloadingFiles'), 10);

				$file = tempnam(sys_get_temp_dir(), '');

				$f = fopen($file, 'w+');
				if($f) {
					fwrite($f, $settings->getUpdate());
					fclose($f);

					if(file_exists($file)) {
						$this->setProgress($this->_('Admin/LiveUpdate/Status/Extracting'), 30);

						$dir = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'PeceeCamp';

						if(is_dir($dir)) {
							// Clean old updates
							\Pecee\IO\File::DeleteTree($dir);
						}

						@mkdir($dir);

						$zip = new \ZipArchive();
						$zip->open($file);
						$zip->extractTo($dir);

						$dir = $dir . DIRECTORY_SEPARATOR . $zip->getNameIndex(0);

						if(is_dir($dir)) {
							$this->setProgress($this->_('Admin/LiveUpdate/Status/CleaningUp'), 60);

							// Cleanup extracted archive
							\Pecee\IO\File::DeleteTree($dir . DIRECTORY_SEPARATOR . 'config');
							\Pecee\IO\File::DeleteTree($dir . DIRECTORY_SEPARATOR . 'cache');
							\Pecee\IO\File::DeleteTree($dir . DIRECTORY_SEPARATOR . 'Themes');

							// Remove stuff from plugin directory
							\Pecee\IO\File::DeleteTree($dir . DIRECTORY_SEPARATOR . 'www'.DIRECTORY_SEPARATOR.'plugin'.DIRECTORY_SEPARATOR.'CMS'.DIRECTORY_SEPARATOR.'theme');

							$pluginNames = array();
							$plugins = \PC\Plugin::Instance()->getPlugins();
							foreach($plugins as $plugin) {
								$pluginNames[] = $plugin->name;
							}

							// Remove plugins which are not in use
							$pluginDir = $dir . DIRECTORY_SEPARATOR . 'Plugins';
							if(($handle = opendir($pluginDir)) !== FALSE) {
								while (false !== ($entry = readdir($handle))) {
									if(!in_array($entry, array('.','..'))) {
										if(!in_array($entry, $pluginNames)) {
											\Pecee\IO\File::DeleteTree($pluginDir . DIRECTORY_SEPARATOR . $entry);
										}
									}
								}
								closedir($handle);
							}

							// Clean plugin from www-folder
							$pluginDir = $dir . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'plugin';
							if(($handle = opendir($pluginDir)) !== FALSE) {
								while (false !== ($entry = readdir($handle))) {
									if(!in_array($entry, array('.','..'))) {
										if(!in_array($entry, $pluginNames)) {
											\Pecee\IO\File::DeleteTree($pluginDir . DIRECTORY_SEPARATOR . $entry);
										}
									}
								}
								closedir($handle);
							}
							$this->setProgress($this->_('Admin/LiveUpdate/Status/ApplyingUpdate'), 85);

							// Apply update
							\Pecee\IO\File::Move($dir, dirname($_SERVER['DOCUMENT_ROOT']));

							// Run update file to execute modifications to the new update, if any.
							$updateFolder = dirname($_SERVER['DOCUMENT_ROOT']) . DIRECTORY_SEPARATOR . 'setup';
							$updateFile = $updateFolder . DIRECTORY_SEPARATOR . 'update.php';
							if(file_exists($updateFile)) {
								try {
									@include($updateFile);
								}catch(\Exception $e) {

								}
							}

							$result['result'] = TRUE;

							$this->setProgress($this->_('Admin/LiveUpdate/Status/Complete'), 100);
						}
					}
				}
			}
		}catch(\Exception $e) {
			$result['msg'] = $e->getMessage();
		}

		$this->asJSON($result);
	}
}