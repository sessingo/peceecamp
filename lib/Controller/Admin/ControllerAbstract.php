<?php
namespace PC\Controller\Admin;
abstract class ControllerAbstract extends \PC\Controller\ControllerAbstract {
	public function __construct($checkAuth=TRUE) {
		if(\Pecee\Model\User\ModelUser::IsLoggedIn()) {
			\Pecee\Model\User\ModelUser::Current()->registerActivity();
		}
		if($checkAuth) {
			\Pecee\Auth::GetInstance()->check();
		}
		$this->checkSecure();
	}
	protected function checkSecure() {
		if(\Pecee\Registry::GetInstance()->get('RequireSSL',FALSE) && !\Pecee\Url::IsSecure(\Pecee\Url::CurrentPageUrl(FALSE))) {
			\PC\Router::Redirect(sprintf('https://%s%s', $_SERVER['HTTP_HOST'], \PC\Router::GetCurrentRoute()));
		}
	}
}