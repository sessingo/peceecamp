<?php
namespace PC\Controller\Admin;
class ControllerApplication extends \PC\Controller\Admin\ControllerAbstract {
	public function indexView() {
		echo new \PC\Widget\Admin\Application\ApplicationHome();
	}
	public function addView() {
		echo new \PC\Widget\Admin\Application\ApplicationAdd();
	}
	public function editView($appId) {
		\PC\Plugin::Instance()->setActiveApp(\PC\Model\ModelApplication::GetByApplicationID($appId));
		echo new \PC\Widget\Admin\Application\ApplicationEdit();
	}
}