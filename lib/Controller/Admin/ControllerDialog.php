<?php
namespace PC\Controller\Admin;
class ControllerDialog extends \PC\Controller\Admin\ControllerAbstract {
	public function __construct() {
		header('Content-Type: text/html; charset=utf-8');
	}
	public function viewView() {
		$args=func_get_args();
		if(count($args) > 0) {
			$args=array_map('ucfirst', $args);
			$widget = sprintf('\\PC\\Widget\\Admin\\Dialog\\%s', join('\\', $args));
			if(@class_exists($widget)) {
				$class=\PC\Router::LoadClass($widget);
				echo $class;
			}
		}
	}
}