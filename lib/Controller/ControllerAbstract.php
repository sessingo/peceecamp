<?php
namespace PC\Controller;
abstract class ControllerAbstract extends \Pecee\Controller\Controller {
	public function _($text,$args=NULL) {
		try {
			$args = array_splice(func_get_args(), 1);
			$out = \PC\Language::GetInstance()->_($text, $args);
		} catch(\PC\Xml\Translate\TranslateException $e) {
			return \Pecee\Language::Instance()->_($text, $args);
		}
		return $out;
	}
}