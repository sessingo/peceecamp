<?php
namespace PC;
class Helper {

	const USER_TYPE_EDITOR=0;
	const USER_TYPE_ADMIN=1;
	const LANG_EN_UK='en-UK';
	const LANG_DA_DK='da-DK';
	const LANG_COOKIE_NAME = 'locale';

	public static $LANGUAGES = array(self::LANG_DA_DK,self::LANG_EN_UK);
	public static $USERTYPES = array(self::USER_TYPE_EDITOR, self::USER_TYPE_ADMIN);

	public static function GenerateKey($key, $args) {
		$out = array();
		$args = func_get_args();
		array_shift($args);
		if(count($args) > 0) {
			foreach($args as $name=>$val) {
				$out[] = $name.':'.$val;
			}
		}
		return $key . join('|', $out);
	}
}