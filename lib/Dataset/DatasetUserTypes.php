<?php
namespace PC\Dataset;
class DatasetUserTypes extends \PC\Dataset\DatasetAbstract {
	public function __construct() {
		$userTypes = array($this->_('Admin/UserType/Administrator') => \PC\Helper::USER_TYPE_ADMIN,
							$this->_('Admin/UserType/Editor') => \PC\Helper::USER_TYPE_EDITOR);

		foreach($userTypes as $name=>$type) {
			$this->createArray($type, $name);
		}
	}
}