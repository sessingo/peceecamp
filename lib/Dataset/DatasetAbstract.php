<?php
namespace PC\Dataset;
abstract class DatasetAbstract extends \Pecee\Dataset {
	public function _($text,$args=NULL) {
		try {
			$args = array_splice(func_get_args(), 1);
			$out = \PC\Language::GetInstance()->_($text, $args);
		} catch(\PC\Xml\Translate\TranslateException $e) {
			return \Pecee\Language::Instance()->_($text, $args);
		}
		return $out;
	}
}