<?php
namespace PC\Dataset;
class DatasetLanguages extends \PC\Dataset\DatasetAbstract {
	public function __construct($choose=NULL) {
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}

		$languages = array($this->_('Admin/Language/English') => \PC\Helper::LANG_EN_UK,
			$this->_('Admin/Language/Dansk') => \PC\Helper::LANG_DA_DK);

		foreach($languages as $lang=>$locale) {
			$this->createArray($locale, ucfirst($lang));
		}
	}
}