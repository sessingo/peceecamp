<?php
namespace PC\Dataset;
class DatasetAvailablePlugins extends \PC\Dataset\DatasetAbstract {
	public function __construct($showChoose=TRUE) {
		if($showChoose) {
			$this->createArray('', $this->_('Admin/Application/SelectPlugin'));
		}
		$plugins=\PC\Plugin::Instance()->getPlugins();
		if($plugins) {
			foreach($plugins as $plugin) {
				$this->createArray($plugin->name, $plugin->name);
			}
		}
	}
}