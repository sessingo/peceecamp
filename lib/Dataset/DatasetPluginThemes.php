<?php
namespace PC\Dataset;
class DatasetPluginThemes extends \PC\Dataset\DatasetAbstract {
	public function __construct($plugin) {
		$plugin=\PC\Plugin::Instance()->getByName($plugin);
		if($plugin) {
			foreach($plugin->themes as $theme) {
				$this->createArray($theme, $theme);
			}
		}
	}
}