<?php
namespace PC;
class Update {
	const USERNAME = 'PeceeCamp';
	const PASSWORD = 'xufRUb5RUP';

	protected $latest;

	protected function download($url) {
		$resp = NULL;
		try {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_USERPWD, self::USERNAME . ":" . self::PASSWORD);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_TIMEOUT, 99999);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			$resp = curl_exec($ch);

			// validate CURL status
			if(curl_errno($ch)) {
				throw new \Exception(curl_error($ch), 500);
			}

			// validate HTTP status code (user/password credential issues)
			$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if ($status_code != 200) {
				throw new \Exception("Response with Status Code [" . $status_code . "].", 500);
			}
		} catch(\Exception $ex) {
			curl_close($ch);
			throw new \Exception('Unable to properly download file from url=[' + $url + ']', 500, $ex);
		}
		curl_close($ch);
		return $resp;
	}

	public function getUpdate() {
		return $this->download('https://bitbucket.org/sessingo/peceecamp/get/master.zip');
	}

	public function getChangelog() {
		$changelog = $this->download('https://bitbucket.org/api/1.0/repositories/sessingo/peceecamp/changesets/master/');
		if($changelog) {
			$changelog = json_decode($changelog);
			return isset($changelog->message) ? nl2br($changelog->message) : '';
		}
		return '';
	}

	public function updateAvailable() {
		if(!$this->latest) {
			$version = $this->download('https://bitbucket.org/sessingo/peceecamp/raw/master/VERSION.txt');
			if($version && $version > self::GetVersion()) {
				$this->latest = $version;
			} else {
				$this->latest = FALSE;
			}
		}
		return $this->latest;
	}

	public static function GetVersion() {
		return file_get_contents('VERSION.txt', FILE_USE_INCLUDE_PATH);
	}
}