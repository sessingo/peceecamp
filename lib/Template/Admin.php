<? /* @var $this \PC\Widget\Admin\AdminAbstract */
echo $this->getSite()->getDocType(); ?>
<html>
	<head>
		<?= $this->printHeader(); ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<div class="col1">
					<a href="<?= $this->getRoute('', ''); ?>"><img src="/gfx/pecee-logo.png" alt="" /></a>
				</div>
				<div class="col2">
					<div class="level">
						<?= $this->renderLevel()?>
					</div>
				</div>
				<div class="col3">
					<?= $this->topMenu;?>
				</div>
			</div>
			<div class="inner">
				<div class="sidebar">
					<?= $this->leftMenu; ?>
				</div>
				<div class="main-ctn">
					<div class="inside">
						<?= $this->getContentHtml(); ?>
					</div>
				</div>
			</div>
			<div class="footer">
				<div class="col1">
					&copy; <?= date('Y', time()); ?> <a href="http://www.pecee.dk" target="_blank">Pecee</a>
				</div>
				<div class="col2">
					Version: <?= \PC\Update::GetVersion();?>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			admin.init({
				defaultRoute: '<?= $this->getRoute();?>',
				adminPath: '<?= \Pecee\Registry::GetInstance()->get('AdminUrl'); ?>',
				imageUploadUrl: '<?= $this->getRoute(NULL, 'file', array('upload'), array('type' => 'image', 'id' => session_id()));?>',
				fileUploadUrl: '<?= $this->getRoute(NULL, 'file', array('upload'), array('id' => session_id()));?>',
				imageGetJsonUrl: '<?= $this->getRoute(NULL, 'file', array('getimages'), array('id' => session_id()));?>'
			});
		</script>
	</body>
</html>