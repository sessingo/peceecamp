<? /* @var $this \PC\Widget\Admin\Application\ApplicationAdd */ ?>
<div class="headline">
	<h3><?= $this->_('Admin/Application/AddApplication'); ?></h3>
	<ul class="hor-nav">
		<li><?= $this->tab->button('app', $this->_('Admin/Application/Application')); ?></li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>

	<?= $this->form()->start('addApplication')->addAttribute('class', 'form'); ?>
		<?= $this->tab->start('app', TRUE);?>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/Application/Name'), 'name'); ?>
			<?= $this->form()->input('name', 'text', NULL, TRUE)->addAttribute('ID', 'name'); ?>
			<?= $this->validationFor('name'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/Application/Plugin'), 'plugin')?>
			<?= $this->form()->selectStart('plugin', new \PC\Dataset\DatasetAvailablePlugins(), $this->getParam('plugin'), TRUE)->addAttribute('ID', 'plugin'); ?>
			<?= $this->validationFor('plugin'); ?>
		</div>
		<? if($this->getParam('plugin',FALSE)): ?>
			<div class="input">
				<?= $this->form()->label($this->_('Admin/Application/Theme'), 'theme')?>
				<?= $this->form()->selectStart('theme', new \PC\Dataset\DatasetPluginThemes($this->getParam('plugin')), NULL, TRUE)
					->addAttribute('ID', 'theme'); ?>
				<?= $this->validationFor('theme'); ?>
			</div>
		<? endif; ?>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/Application/UseFromFrontend'), 'useSlug'); ?>
			<?= $this->form()->bool('useFromFrontend', NULL, TRUE)->addAttribute('ID','useSlug')->addAttribute('onclick','$(\'.js-slug\').toggleClass(\'hidden\')');?>
		</div>
		<div class="input js-slug hidden">
			<?= $this->form()->label($this->_('Admin/Application/Host'), 'host'); ?>
			<?= $this->form()->input('host', 'text', NULL, TRUE)->addAttribute('ID', 'host'); ?>
			<?= $this->validationFor('host'); ?>
		</div>
		<div class="input js-slug hidden">
			<?= $this->form()->label($this->_('Admin/Application/Slug'), 'slug'); ?>
			<?= $this->form()->input('slug', 'text', NULL, TRUE)->addAttribute('ID', 'slug'); ?>
			<?= $this->validationFor('slug'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/Application/Icon'), 'icon'); ?>
			<?= $this->form()->input('icon', 'text', NULL, TRUE)->addAttribute('ID', 'icon'); ?>
			<?= $this->validationFor('icon'); ?>
		</div>
		<?= $this->tab->end(); ?>

		<div class="btn">
			<?= $this->form()->submit('submit', $this->_('Admin/Application/Save')); ?>
		</div>
	<?= $this->form()->end();?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#plugin').ddslick('onselect', function(out) {
				top.location.href='?plugin='+out.selectedData.value;
			});
		});
	</script>
</div>