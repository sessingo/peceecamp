<? /* @var $this \PC\Widget\Admin\Application\ApplicationEdit */ ?>
<div class="headline">
	<ul class="hor-nav">
		<li><?= $this->tab->button('app', $this->_('Admin/Application/Application')); ?></li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>

	<?= $this->form()->start('editApplication')->addAttribute('class', 'form'); ?>
		<?= $this->tab->start('app', TRUE);?>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/Application/Name'), 'name'); ?>
			<?= $this->form()->input('name', 'text', $this->plugin->application->getName(), TRUE)->addAttribute('ID', 'name'); ?>
			<?= $this->validationFor('name'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/Application/Plugin'), 'plugin')?>
			<?= $this->form()->selectStart('plugin', new \PC\Dataset\DatasetAvailablePlugins(FALSE), $this->plugin->application->getPlugin(), TRUE)
				->addAttribute('ID', 'plugin'); ?>
			<?= $this->validationFor('plugin'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/Application/Theme'), 'theme')?>
			<?= $this->form()->selectStart('theme', new \PC\Dataset\DatasetPluginThemes($this->getParam('plugin', $this->plugin->application->getPlugin())),$this->plugin->application->getTheme(), TRUE)
				->addAttribute('ID', 'theme'); ?>
			<?= $this->validationFor('theme'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/Application/UseFromFrontend'), 'useSlug'); ?>
			<?= $this->form()->bool('useFromFrontend', $this->plugin->application->getUseFromFrontend())->addAttribute('ID','useSlug')->addAttribute('onclick','$(\'.js-slug\').toggleClass(\'hidden\')');?>
		</div>
		<div class="input js-slug<?= ($this->plugin->application->getUseFromFrontend()) ? '' : ' hidden'?>">
			<?= $this->form()->label($this->_('Admin/Application/Host'), 'host'); ?>
			<?= $this->form()->input('host', 'text', $this->plugin->application->getHost(), TRUE)->addAttribute('ID', 'host'); ?>
			<?= $this->validationFor('host'); ?>
		</div>
		<div class="input js-slug<?= ($this->plugin->application->getUseFromFrontend()) ? '' : ' hidden'?>">
			<?= $this->form()->label($this->_('Admin/Application/Slug'), 'slug'); ?>
			<?= $this->form()->input('slug', 'text', $this->plugin->application->getSlug(), TRUE)->addAttribute('ID', 'slug'); ?>
			<?= $this->validationFor('slug'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/Application/Icon'), 'icon'); ?>
			<?= $this->form()->input('icon', 'text', $this->plugin->application->getIcon(), TRUE)->addAttribute('ID', 'icon'); ?>
			<?= $this->validationFor('icon'); ?>
		</div>
		<?= $this->tab->end();?>

		<div class="btn">
			<?= $this->form()->submit('submit', $this->_('Admin/Application/Update')); ?>
		</div>
	<?= $this->form()->end();?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#plugin').change(function() {
				location.href='?plugin='+$(this).val();
			});
		});
	</script>
</div>