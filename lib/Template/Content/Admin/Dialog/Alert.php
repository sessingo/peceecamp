<? /* @var $this \PC\Widget\Admin\Dialog\Alert */?>
<div class="head">
	<a href="javascript:;" onclick="confirmAlert(true);"></a>
	<h3 class="js-alert-title"></h3>
</div>
<div class="inner">
	<div style="line-height:22px;" class="js-alert-msg"></div>
	<div class="options">
		<a href="javascript:;" class="btn blue" onclick="confirmAlert(true);"><?= $this->_('Admin/Dialog/OK'); ?></a>
	</div>
</div>
<script type="text/javascript">
	function confirmAlert(status) {
		var fn=admin.dialog.o.alertCallback;
		if(fn != null) {
			admin.dialog.o.alertCallback=null;
			fn(status);
		}
		dialog.closeActive();
		return false;
	}
</script>