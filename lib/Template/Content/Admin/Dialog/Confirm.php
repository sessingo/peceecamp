<? /* @var $this \PC\Widget\Admin\Dialog\Confirm */?>
<div class="head">
	<a href="javascript:;" onclick="confirmDialog(false);"></a>
	<h3 class="js-confirm-title"></h3>
</div>
<div class="inner">
	<div style="padding-top:10px;">
		<div style="float:left;margin-right:10px;">
			<img src="/gfx/dialog/question.png" alt="" style="margin-bottom:10px;" />
		</div>
		<div style="line-height:22px;" class="js-confirm-msg"></div>
		<div class="clear"></div>
	</div>
	<div class="options">
		<a href="javascript:;" class="btn" onclick="confirmDialog(false);"><?= $this->_('Admin/Dialog/Cancel'); ?></a>
		<a href="javascript:;" class="btn blue" style="margin-left:8px;" onclick="confirmDialog(true);"><?= $this->_('Admin/Dialog/OK'); ?></a>
	</div>
</div>
<script type="text/javascript">
	function confirmDialog(status) {
		var fn=admin.dialog.o.confirmCallback;
		if(fn != null) {
			admin.dialog.o.confirmCallback=null;
			fn(status);
		}
		dialog.closeActive();
		return false;
	}
</script>

