<? /* @var $this \PC\Widget\Admin\User\UserEdit */ ?>
<h3><?= $this->_('Admin/User/EditTitle', $this->user->getUsername()); ?></h3>
<?= $this->form()->start('addUser')->addAttribute('class', 'form'); ?>
	<?= $this->showErrors('addUser'); ?>
	<div class="input">
		<?= $this->form()->label($this->_('Admin/User/Username'), 'username'); ?>
		<?= $this->form()->input('username', 'text', $this->user->getUsername(), TRUE)->addAttribute('id', 'username');?>
		<?= $this->validationFor('username'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Admin/User/FirstName'), 'firstname'); ?>
		<?= $this->form()->input('firstname', 'text', $this->user->data->firstname, TRUE)->addAttribute('id', 'firstname');?>
		<?= $this->validationFor('firstname'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Admin/User/LastName'), 'lastname'); ?>
		<?= $this->form()->input('lastname', 'text', $this->user->data->lastname, TRUE)->addAttribute('id', 'lastname');?>
		<?= $this->validationFor('lastname'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Admin/User/ResetPassword'), 'password'); ?>
		<?= $this->form()->input('password', 'password')->addAttribute('id', 'password');?>
		<?= $this->validationFor('password'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Admin/User/Email'), 'email'); ?>
		<?= $this->form()->input('email', 'text', $this->user->getEmail(), TRUE)->addAttribute('id', 'email');?>
		<?= $this->validationFor('email'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Admin/User/Type'), 'type'); ?>
		<?= $this->form()->selectStart('type', new \PC\Dataset\DatasetUserTypes(), $this->user->getAdminLevel())->addAttribute('id', 'type');?>
	</div>
	
	<div class="btn">
		<?= $this->form()->submit('submit', $this->_('Admin/User/Update'));?>
	</div>
<?= $this->form()->end(); ?>