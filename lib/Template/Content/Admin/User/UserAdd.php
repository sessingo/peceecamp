<? /* @var $this \PC\Widget\Admin\User\UserAdd */ ?>
<div class="headline">
	<ul class="hor-nav">
		<li><a href="javascript:;" class="active"><?= $this->_('Admin/User/AddUser')?></a></li>
	</ul>
</div>

<div class="ctn">
	<?= $this->form()->start('addUser')->addAttribute('class', 'form'); ?>
		<?= $this->showErrors('addUser'); ?>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/User/Username'), 'username'); ?>
			<?= $this->form()->input('username', 'text')->addAttribute('id', 'username');?>
			<?= $this->validationFor('username'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/User/FirstName'), 'firstname'); ?>
			<?= $this->form()->input('firstname', 'text')->addAttribute('id', 'firstname');?>
			<?= $this->validationFor('firstname'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/User/LastName'), 'lastname'); ?>
			<?= $this->form()->input('lastname', 'text')->addAttribute('id', 'lastname');?>
			<?= $this->validationFor('lastname'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/User/Password'), 'password'); ?>
			<?= $this->form()->input('password', 'password')->addAttribute('id', 'password');?>
			<?= $this->validationFor('password'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/User/Email'), 'email'); ?>
			<?= $this->form()->input('email', 'text')->addAttribute('id', 'email');?>
			<?= $this->validationFor('email'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/User/Type'), 'type'); ?>
			<?= $this->form()->selectStart('type', new \PC\Dataset\DatasetUserTypes())->addAttribute('id', 'type');?>
		</div>
		
		<div class="btn">
			<?= $this->form()->submit('submit', $this->_('Admin/User/Create'));?>
		</div>
	<?= $this->form()->end(); ?>
</div>