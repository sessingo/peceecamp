<? /* @var $this \PC\Widget\Admin\User\UserHome */ ?>
<div class="headline">	
	<ul class="hor-nav">
		<li><a href="javascript:;" class="active"><?= $this->_('Admin/User/ManageUsers')?></a></li>
	</ul>
	<ul>
		<li>
			<a href="<?= $this->getRoute('user', 'add'); ?>" class="btn-s"><?= $this->_('Admin/User/AddUser')?></a>
		</li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>
	<table class="dataTable">
		<thead>
			<tr>
				<th><?= $this->_('Admin/User/Username'); ?></th>
				<th class="center"><?= $this->_('Admin/User/LastOnline')?></th>
				<th class="center"><?= $this->_('Admin/User/Type'); ?></th>
				<th style="width:100px;" class="center"><?= $this->_('Admin/User/Actions');?></th>
			</tr>
		</thead>
		<tbody>
			<? foreach($this->users->getRows() as $key=>$user) : ?>
			<tr class="<?= ($key%2==0) ? 'odd' : 'even' ?> gradeX">
				<td><?= $user->getUsername(); ?></td>
				<td class="center"><?= $user->getLastActivity(); ?></td>
				<td class="center"><?= $this->getUserType($user->getAdminLevel()); ?></td>
				<td class="center">
					<div class="actions">
						<a href="<?= $this->getRoute(null, 'edit', array($user->getUserID()));?>"><?= $this->_('Admin/User/Edit'); ?></a> 
						<a href="<?= $this->getRoute(null, 'delete', array($user->getUserID()));?>" onclick="return confirm('<?= $this->_('Admin/User/ConfirmDelete');?>');"><?= $this->_('Admin/User/Delete'); ?></a>
					</div>
				</td>
			</tr>
			<? endforeach; ?>
		</tbody>
	</table>
</div>