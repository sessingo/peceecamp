<? /* @var $this \PC\Widget\Admin\Settings\SettingsHome */ ?>

<h3><?= $this->_('Admin/Settings/Settings'); ?></h3>
<ul class="hor-nav">
	<li><?= $this->tab->button('generel', $this->_('Admin/Settings/Generel')); ?></li>
	<li><?= $this->tab->button('changepassword', $this->_('Admin/Settings/ChangePassword'))?></li>
	<? if(\Pecee\Model\User\ModelUser::Current()->getAdminLevel() > \PC\Helper::USER_TYPE_EDITOR) : ?>
	<li><?= $this->tab->button('updates', $this->_('Admin/LiveUpdate/Updates'));?></li>
	<? endif; ?>
</ul>

<div class="ctn">
	<?= $this->showFlash(); ?>

	<?= $this->form()->start('settings')->addAttribute('class','form')?>
		<?= $this->tab->start('generel', TRUE); ?>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/Settings/Language'), 'language'); ?>
			<?= $this->form()->selectStart('locale', new \PC\Dataset\DatasetLanguages(), \Pecee\Locale::Instance()->getLocale())->addAttribute('ID', 'locale'); ?>
			<?= $this->validationFor('locale'); ?>
		</div>

		<div class="btn">
			<?= $this->form()->submit('submit', $this->_('Admin/Settings/UpdateButton')); ?>
		</div>
		<?= $this->tab->end(); ?>

		<?= $this->tab->start('changepassword'); ?>
		<div class="input">
			<?= $this->form()->label($this->_('Admin/Settings/Password'), 'password'); ?>
			<?= $this->form()->input('password', 'password')->addAttribute('id', 'password');?>
			<?= $this->validationFor('password'); ?>
		</div>

		<div class="input">
			<?= $this->form()->label($this->_('Admin/Settings/NewPassword'), 'newPassword'); ?>
			<?= $this->form()->input('newPassword', 'password')->addAttribute('id', 'newPassword');?>
			<?= $this->validationFor('newPassword'); ?>
		</div>

		<div class="btn">
			<?= $this->form()->submit('submit', $this->_('Admin/Settings/UpdateButton')); ?>
		</div>

		<?= $this->tab->end();?>
		<? if(\Pecee\Model\User\ModelUser::Current()->getAdminLevel() > \PC\Helper::USER_TYPE_EDITOR) : ?>
		<?= $this->tab->start('updates'); ?>

		<p>
			<?= $this->_('Admin/LiveUpdate/Description')?>
		</p>
		<div style="margin-top:15px;border-top:1px solid #EEE;padding-top:15px;">
			<div id="loader" style="display:none;">
				<img src="<?= $this->getRessource('/gfx/admin/spinner.gif')?>" alt="" style="vertical-align:middle;"/>
				<?= $this->_('Admin/LiveUpdate/SearchingForUpdates')?>
			</div>
			<div id="no-update" style="display:none;">
				<?= $this->_('Admin/LiveUpdate/NoUpdatesAvailable')?>
				<div style="margin-top:10px;">
					<a href="#" class="btn-s js-check-version"><?= $this->_('Admin/LiveUpdate/SearchForUpdates')?></a>
				</div>
			</div>
			<div id="update-info" style="display:none;">
				<b><?= $this->_('Admin/LiveUpdate/UpdateAvailable')?></b> <span id="update-version"></span>
				<div id="changelog" style="margin-top:10px;margin-bottom:10px;display:none;max-height:150px;overflow:auto;"></div>
				<div style="border-top:1px solid #EEE;padding-top:15px;margin-top:15px;">
					<?= $this->form()->input('update', 'button', 'Update')->addAttribute('ID','update');?>
				</div>
			</div>

			<div class="progressbar" id="update-container">
				<div class="progress" id="update-progress"></div>
				<span class="status" id="update-status"></span>
			</div>
		</div>

		<?= $this->tab->end();?>
		<? endif; ?>

		<script type="text/javascript">
			$(document).ready(function() {

				var update = function() {
					var pingTimer = null;

					$('#update-container').show();

					// Start update
					$.get('<?= $this->getRoute('update', 'update.json');?>');

					var ping = function() {
						$.get('<?= $this->getRoute('update', 'ping.json'); ?>', function(r) {
							if(r != null) {
								if(r.status != null) {
									$('#update-progress').animate({ width: r.progress+'%' },100);
									$('#update-status').text(r.progress + '% ' +  r.status);

									if(parseInt(r.progress) == 100) {
										clearTimeout(pingTimer);
										pingTimer = null;
									} else {
										pingTimer = setTimeout(function() {
											ping();
										}, 50);
									}
								} else {
									clearTimeout(pingTimer);
									pingTimer = null;
								}
							}
						});
					};

					setTimeout(function() {
						ping();
					}, 300);
				};

				$('#update').bind('click', function() {
					$('#update-info').hide();
					update();
				});


				var checkVersion = function() {
					$('#loader').show();
					$('#no-update').hide();
					$.get('<?= $this->getRoute('update', 'latest.json'); ?>', function(r) {
						$('#loader').hide();
						if(r != null) {
							if(!r.result) {
								alert(r.msg);
							} else {
								if(r.version != false) {
									$('#update-info').show();
									$('#update-version').text(r.version);
									$('#changelog').html(r.changelog);
									$('#changelog').show();
								} else {
									$('#update-info').hide();
									$('#no-update').show();
								}
							}
						}
					});
				};

				checkVersion();

				$('a.js-check-version').live('click', function(e) {
					e.preventDefault();
					checkVersion();
				});
			});
		</script>

	<?= $this->form()->end(); ?>
</div>