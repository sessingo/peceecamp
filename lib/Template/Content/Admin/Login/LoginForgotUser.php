<? /* @var $this \PC\Widget\Admin\Login\LoginForgotUser */ ?>

<?= $this->form()->start('forgotUser');?>
	
	<h2>Recover username/password</h2>
	
	<?= $this->showMessages('recover_error'); ?>
	
	<div class="input">
		<?= $this->form()->input('email', 'text')->addAttribute('placeholder', 'name@example.com')?>
		<?= $this->validationFor('email'); ?>
	</div>
	
	<div class="padding-t">
		<?= $this->form()->submit('submit', 'Recover'); ?>
	</div>
	
<?= $this->form()->end();?>