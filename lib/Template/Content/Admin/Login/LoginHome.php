<? /* @var $this \PC\Widget\Admin\Login\LoginHome */ ?>
<?= $this->form()->start('login'); ?>
	<?= $this->showMessages('login_error'); ?>
	<div class="input">
		<?= $this->form()->input('username', 'text', NULL, TRUE)
			->addAttribute('ID', 'username')
			->addAttribute('placeholder', $this->_('Admin/Login/Username')); ?>
		<?= $this->validationFor('username'); ?>
	</div>
	<div class="input">
		<?= $this->form()->input('password', 'password')
			->addAttribute('ID', 'password')
			->addAttribute('placeholder', $this->_('Admin/Login/Password')); ?>
		<?= $this->validationFor('password'); ?>
	</div>

	<div class="input">
		<?= $this->form()->bool('remember')->addAttribute('ID', 'remember');?>
		<?= $this->form()->label($this->_('Admin/Login/Remember'), 'remember');?>
	</div>
	<div class="padding-t">
		<?= $this->form()->submit('submit', $this->_('Admin/Login/Login'));?>
	</div>
	<? /* <div style="padding-top:10px;">
		<a href="<?= \PC\Router::GetRoute(NULL, 'forgot'); ?>"><?= $this->_('Admin/Login/ForgotPassword')?></a>
	</div> */ ?>
<?= $this->form()->end();?>