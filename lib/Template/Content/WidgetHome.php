<? /* @var $this \PC\Widget\WidgetHome */ ?>
<style>
	body
	{
		font-family:Arial;
		font-size:14px;
	}

	ul.error
	{
		margin:0;
		padding:0;
		margin-top:30px;
		text-align:center;
		list-style:none;
		padding:10px;
		font-size:16px;
		font-weight:bold;
	}
</style>
<div style="margin:50px;">

	<div style="text-align:center;padding:10px;margin-20px;">
		<a href="http://www.pecee.dk" title="Pecee - kreativt socialt web bureau"><img src="/gfx/pecee-logo.png" alt="" /></a>
	</div>

	<? if($this->hasErrors()) : ?>
		<?= $this->showErrors(); ?>
	<? endif; ?>
</div>