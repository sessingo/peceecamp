<? /* @var $this \PC\Widget\Admin\AdminAbstract */
echo $this->getSite()->getDocType(); ?>
<html>
	<head>
		<?= $this->printHeader(); ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="inner main-ctn">
				<?= $this->getContentHtml(); ?>
			</div>
		</div>
		<script type="text/javascript">
			admin.init({
				defaultRoute: '<?= $this->getRoute();?>',
				adminPath: '<?= \Pecee\Registry::GetInstance()->get('AdminUrl'); ?>'
			});
		</script>
	</body>
</html>