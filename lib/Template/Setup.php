<? /* @var $this \PC\Widget\Site\SiteAbstract */
echo $this->getSite()->getDocType(); ?>
<html>
	<head>
		<?= $this->printHeader(); ?>
		<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="wrapper">
			<div class="setup">
				<div class="top">
					<img src="/gfx/pecee-logo.png" alt="" />
				</div>
				<?= $this->getContentHtml(); ?>
			</div>
		</div>
	</body>
</html>