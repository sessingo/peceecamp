<? /* @var $this Widget_Ajax_Dialog_Abstract */ ?>
<? if(isset($this->title)) : ?>
<div class="head">
	<? if($this->enableDialogClose) : ?>
	<a href="javascript:void(0);" onclick="dialog.closeActive();"></a>
	<? endif; ?>
	<h3><?= $this->title; ?></h3>
</div>
<? endif; ?>
<div class="inner">
	<?= $this->getContentHtml(); ?>
</div>