<? /* @var $this \PC\Widget\Site\SiteAbstract */
echo $this->getSite()->getDocType(); ?>
<html>
	<head>
		<?= $this->printHeader(); ?>
	</head>
	<body>
		<div class="ctn wrapper">
			<?= $this->getContentHtml(); ?>
		</div>
	</body>
</html>