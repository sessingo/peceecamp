<? /* @var $this \PC\Widget\Admin\Login\LoginAbstract */
echo $this->getSite()->getDocType(); ?>
<html>
	<head>
		<?= $this->printHeader(); ?>
	</head>
	<body>
		<div class="wrapper">
			<div class="logo">
				<a href="<?= \PC\Router::GetRoute(NULL, ''); ?>"><img src="/gfx/pecee-logo.png" alt="" /></a>
			</div>
			<div class="inner">
				<?= $this->getContentHtml(); ?>
			</div>
			<div class="language">
				<?= $this->form()->selectStart('language', new \PC\Dataset\DatasetLanguages(), $this->currentLocale, TRUE);?>
			</div>
		</div>
		<script>
			$(document).ready(function() {
				$('.language select').bind('change', function() {
					var loc = location.href.split('?');
					top.location.href = loc[0] + '?lang=' + $(this).val();
				});
			});
		</script>
	</body>
</html>