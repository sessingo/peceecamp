<?php
namespace PC;
class Router extends \Pecee\Router {
	/**
	 * Calculates template path from given Widget name.
	 * @param string $widgetName
	 * @return string
	 */
	public static function GetTemplatePath($widgetName) {
		$path=explode('\\', $widgetName);
		array_shift($path);
		array_shift($path);
		return 'Template' . DIRECTORY_SEPARATOR . 'Content' . DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, $path) . '.php';
	}
	public function routeRequest() {
		$path = ltrim(substr($_SERVER['REQUEST_URI'],strlen(dirname($_SERVER['SCRIPT_NAME']))),'/');
		\Pecee\Debug::Instance()->add('Router: routing request ' . self::GetCurrentRoute(TRUE,FALSE));
		$path = current(explode('?', $path));
		$this->path = explode('/',trim($path,'/'));
		$adminUrl = strtolower(\Pecee\Registry::GetInstance()->get('AdminUrl', 'admin'));
		$isAdmin=(strtolower($this->path[0])==$adminUrl);
		$app=NULL;
		\PC\Plugin::Instance()->setIsAdmin($isAdmin);
		if(!\PC\Plugin::Instance()->getPlugin()) {
			if(!$isAdmin && isset($this->path[0])) {
				$path=join($this->path,'/');
				$app=\PC\Model\ModelApplication::GetBySlug($path, $_SERVER['HTTP_HOST']);
				if($app->hasRow()) {
					\Pecee\Debug::Instance()->add('Router: found application "' . $app->getName() . '" using plugin "' . $app->getPlugin() . '"');
					$path = trim(substr($path, strlen($app->getSlug())),'/');
					$this->path = ($path) ? explode('/', $path) : array();
				}
			}
			if($isAdmin && is_null($app) && isset($this->path[1])) {
				$app=\PC\Model\ModelApplication::GetByApplicationID($this->path[1]);
			}
			\PC\Plugin::Instance()->setActiveApp($app);
		}
		$this->calculatePath();
		$path = explode('/',trim($path,'/'));
		if($this->hasAlias()) {
			foreach($this->alias as $alias) {
				$path = $alias->getPath(join($this->path, '/'));
			}
			$path = explode('/',trim($path,'/'));
			$this->originalPath = $this->path;
			$this->path = $path;
		}
		$this->params = $path;
		$this->originalController = $this->controller;
		$this->originalMethod = $this->method;
		$this->calculatePath();

		$path=$this->path;
		$p=$path;
		$controller=self::CONTROLLER_DEFAULT;
		$method=self::METHOD_DEFAULT;

		$plugin=\PC\Plugin::Instance()->getPlugin();

		if(count($p) > 0) {
			$index = (!$isAdmin) ? 0 : (($plugin) ? 2 : 1);
			if(!$isAdmin) {
				$index=$index-1;
			}
			if(count($p) > $index) {
				$controller=ucfirst($path[$index]);
				$p=array_slice($p,1);
				if(count($p) > $index) {
					$method=$p[$index];
					$p=array_slice($p,($index+1));
				} else {
					$p=array_slice($p,2);
				}
			}
		}

		$this->params=$p;

		if($isAdmin) {
			\Pecee\Auth::GetInstance()->setAuthLevel(\PC\Helper::USER_TYPE_EDITOR);
			$controller = 'Admin\\Controller' . $controller;
		} else {
			$controller = 'Controller' . $controller;
		}
		$oldController = $controller;
		if($plugin) {
			$controller = sprintf('\\%s\\%s\\Controller\\%s', $plugin->name, ucfirst($plugin->application->Theme), $controller);
			if(class_exists($controller)){
				$class = new $controller();
				$m = $this->ensurePrefix($method);
				if(method_exists($class, $m)) {
					\Pecee\Debug::Instance()->add('Router: loading controller ' . $controller . '->' . $m);
					return call_user_func_array(array($class, $m), $this->params);
				}
			}
		}

		$controller= ($plugin) ? '\\'.$plugin->name . '\\Controller\\'.$oldController : '\\PC\\Controller\\'.$oldController;
		if(class_exists($controller)){
			$class = new $controller();
			$m = $this->ensurePrefix($method);
			if(method_exists($class, $m)) {
				\Pecee\Debug::Instance()->add('Router: loading controller ' . $controller . '->' . $m);
				return call_user_func_array(array($class, $m), $this->params);
			}
		}

		$oldTheme = NULL;
		if(!class_exists($controller)){
			if(!$isAdmin && isset($plugin->application)) {
				$oldTheme = $plugin->application->Theme;
				$c = sprintf('\\%s\\%s\\Controller\\ControllerDefault', $plugin->name, ucfirst($plugin->application->Theme));
				if(class_exists($c)){
					$class = new $c();
				}
			}

			$controller= '\\PC\\Controller\\Site\\'.$oldController;
			$path=array_slice($this->path, ($isAdmin) ? 2 : 1);
			if(count($path) > 0) {
				$method=$path[0];
				$this->params=array_slice($path, 1);
			} else {
				if(count($this->path) > 0) {
					$method = array_pop(array_slice($this->path,1, 1));
				}
			}
		}

		if(!class_exists($controller)) {
			$controller = ($plugin) ? '\\'.$plugin->name . '\\Controller\\Site\\'.$oldController : '\\PC\\Controller\\Controller\\'.$oldController;
			if(!class_exists($controller)){
				if(!$isAdmin) {
					$theme = ($oldTheme) ? $oldTheme : $plugin->application->Theme;
					$c = sprintf('\\%s\\%s\\Controller\\ControllerDefault', $plugin->name, ucfirst($theme));
					if(class_exists($c)){
						$class = new $c();
						$m = $this->ensurePrefix(self::METHOD_DEFAULT);
						if(method_exists($class, $m)) {
							\Pecee\Debug::Instance()->add('Router: loading controller ' . $controller . '->' . $m);
							return call_user_func_array(array($class, $m), $this->params);
						}
					}
				}

				$controller= ($plugin) ? '\\'.$plugin->name . '\\Controller\\Site\\ControllerDefault' : '\\PC\\Controller\ControllerDefault';
				$path=array_slice($this->path, ($isAdmin) ? 2 : 1);
				if(count($path) > 0) {
					$method=$path[0];
					$this->params=array_slice($path, 1);
				} else {
					$method = array_pop(array_slice($this->path,1, 1));
				}
			}

			$method = $this->ensurePrefix($method);
			if(!class_exists($controller) && $plugin) {
				$controller = '\\'.$plugin->name . '\\ControllerDefault';
			}

			if(!class_exists($controller)){
				throw new \Pecee\Router\RouterException('The class ' . $controller . ' couldt not be loaded');
			}
		} else {
			$method = $this->ensurePrefix($method);
		}

		$class = class_exists($controller) ? new $controller() : NULL;
		if(!$class) {
			$peceeController='Pecee\Controller\Controller'.$this->controller;
			if(class_exists($peceeController)){
				$class = new $peceeController();
				if(method_exists($class, $method)) {
					\Pecee\Debug::Instance()->add('Router: loading controller ' . $controller . '->' . $method);
					return call_user_func_array(array($class, $method), $this->params);
				}
			}
		}

		if(!is_callable(array($class, $method))) {
			throw new \Pecee\Router\RouterException('Method ' . $method . ' is yet not implemented in ' . $controller . ' or controller not found');
		}

		\Pecee\Debug::Instance()->add('Router: loading controller ' . $controller . '->' . $method);
		return call_user_func_array(array($class, $method), $this->params);
	}

	protected function ensurePrefix($method) {
		// Ensure that allowedPrefix rules is maintained
		if(strrpos($method, '.') !== FALSE) {
			$p=substr($method,strrpos($method, '.')+1);
			if(in_array(strtolower($p), self::$ALLOWED_PREFIXES)) {
				$p=str_replace('-', '', $p);
				$prefix=ucfirst(self::$ALLOWED_PREFIXES[array_search($p, self::$ALLOWED_PREFIXES)]);
				return substr($method, 0, (strripos($method, $p)-1)) . $prefix;
			}
		}
		return $method.self::CONTROLLER_METHOD_PREFIX;
	}
}