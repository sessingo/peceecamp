<?php
namespace PC\Widget\Admin;
class AdminAbstract extends \PC\Widget\WidgetAbstract {
	protected $plugin;
	protected $topMenu;
	protected $leftMenu;
	protected $applications;
	protected $level;
	protected $menus;
	protected $ignoreTemplate=FALSE;

	protected $tab;

	const MENU_TYPE_POSTS='posts';
	const MENU_TYPE_USER='user';
	const MENU_TYPE_PLUGIN='plugin';
	const MENU_TYPE_CONFIGURE='configure';

	public function __construct($adminLevel=0,$setMenu=TRUE) {
		$this->menus=array();
		$this->level=array();
		$this->addLevel($this->_('Admin/Home/Home'), $this->getRoute('', ''));
		$this->setAuthLevel($adminLevel);
		$this->plugin=\PC\Plugin::Instance()->getPlugin();

		parent::__construct();

		$this->getSite()->addWrappedCss('reset.css');
		$this->getSite()->addWrappedCss('typography.css');
		$this->getSite()->addWrappedCss('admin-global.css');
		$this->getSite()->addWrappedCss('admin.css');
		$this->getSite()->addWrappedCss('jquery.dialog.css');
		$this->getSite()->addWrappedCss('jquery-ui-1.9.2.custom.min.css');
		$this->getSite()->addWrappedCss('tables.css');
		$this->getSite()->addWrappedJs('jquery-1.8.3.min.js');
		$this->getSite()->addWrappedJs('jquery.dialog.js');
		$this->getSite()->addWrappedJs('jquery.uniform.min.js');
		$this->getSite()->addWrappedJs('jquery.dataTables.min.js');
		$this->getSite()->addWrappedJs('jquery.ddslick.min.js');
		$this->getSite()->addWrappedJs('jquery.autosize.js');
		$this->getSite()->addWrappedJs('admin.js');

		$this->getSite()->addJs('/js/jquery-ui-1.9.2.custom.min.js');

		$this->getSite()->setTitle('PeceeCamp');

		$this->tab=new \Pecee\UI\Tablist();

		$this->applications=\PC\Model\ModelApplication::Get(NULL, NULL);
		$this->cssWrapRoute = \PC\Router::GetRoute(\Pecee\Registry::GetInstance()->get('AdminUrl','admin'),'css',array('wrap'));
		$this->jsWrapRoute = \PC\Router::GetRoute(\Pecee\Registry::GetInstance()->get('AdminUrl','admin'),'js',array('wrap'));
		$this->setTemplate('Admin.php');
		$this->setContentTemplate();

		if($setMenu) {
			$this->setTopMenu();
			$this->setLeftMenu();
		}
	}

	protected function prependSiteTitle($title) {
		$this->_site->setTitle(($title . ' - ' .$this->_site->getTitle()));
	}

	protected function setTopMenu() {
		$this->topMenu=new \Pecee\UI\Menu\Menu();
		if($this->isAdmin()) {
			$this->topMenu->addItem($this->_('Admin/Application/AddApplication'), $this->getRoute('application', 'add'))->addAttribute('class', 'apps');
		}
		$this->topMenu->addItem($this->_('Admin/Settings/Settings'), $this->getRoute('default', 'settings'))->addAttribute('class', 'settings');
		$this->topMenu->addItem($this->_('Admin/LogOut'), $this->getRoute('default', 'signout'))->addAttribute('class', 'logout');
	}

	protected function setLeftMenu() {
		$this->leftMenu=new \Pecee\UI\Menu\Menu();
		$this->leftMenu->addAttribute('class', 'nav');

		if($this->applications->hasRows()) {
			$sub=new \Pecee\UI\Menu\Menu();
			/* @var $app \PC\Model\ModelApplication */
			foreach($this->applications->getRows() as $app) {
				$item=$this->leftMenu->addItem($app->getName(), $this->getRoute($app->getApplicationID()));
				$item->addLinkAttribute('style', sprintf('background-image: url(%s)',  $app->getIconUri()));
				if($this->plugin && $this->plugin->application->ApplicationID == $app->getApplicationID()) {
					$item->addClass('active');
					$this->menus[self::MENU_TYPE_PLUGIN]=$item;
				}
			}
		}

		if($this->isAdmin()) {
			$user = $this->leftMenu->addItem($this->_('Admin/User/Users'), $this->getRoute('user'))->addClass('users');
			$sub=new \Pecee\UI\Menu\Menu();
			$sub->addItem($this->_('Admin/User/Home'), $this->getRoute('user'));
			$sub->addItem($this->_('Admin/User/Add'), $this->getRoute('user','add'));
			$user->addMenu($sub);
			$this->menus[self::MENU_TYPE_USER] = $user;
		}

		/* If the plugin is active */
		if($this->plugin) {
			$this->addLevel($this->plugin->application->getName(), $this->getRoute($this->plugin->application->ApplicationID, ''));

			$menu=\PC\UI\Plugin\PluginMenu::Instance();
			if($this->isAdmin()) {
				$this->menus[self::MENU_TYPE_CONFIGURE]=$menu->addItem($this->_('Admin/Application/Configure'), $this->getRoute('application', 'edit', array($this->plugin->application->getApplicationID())));
			}
			$this->menus[self::MENU_TYPE_PLUGIN]->addMenu($menu);
		}
	}

	/**
	 * Get current menu
	 * @param string $type
	 * @throws InvalidArgumentException
	 * @return \Pecee\UI\Menu\Menu|\Pecee\UI\Menu\MenuItems
	 */
	public function getMenu($type=NULL) {
		$type=(is_null($type)) ? self::MENU_TYPE_PLUGIN : $type;
		if(!isset($this->menus[$type])) {
			throw new \InvalidArgumentException('Invalid menutype or yet not implemented');
		}
		$menu=$this->menus[$type];
		if($type==self::MENU_TYPE_PLUGIN) {
			return $menu->getMenu();
		}
		return $menu;
	}

	protected function isAdmin() {
		return (\Pecee\Model\User\ModelUser::Current()->getAdminLevel() == \PC\Helper::USER_TYPE_ADMIN);
	}

	protected function addLevel($level, $url=NULL) {
		$this->level[$level]=$url;
		return $this;
	}

	protected function renderLevel() {
		$o=array();
		foreach($this->level as $level=>$url) {
			if(!is_null($url)) {
				$o[]=sprintf('<a href="%s">%s</a>', $url, $level);
			} else {
				$o[]=$level;
			}
		}
		return join(' <span>&raquo;</span> ', $o);
	}

	public function showFlash($formName=NULL) {
		$o=$this->showMessages('error', $formName);
		$o.=$this->showMessages('warning', $formName);
		$o.=$this->showMessages('info', $formName);
		$o.=$this->showMessages('success', $formName);
		return $o;
	}

	public function showMessages($type, $formName = NULL) {
		if(is_null($formName) || is_null($this->getFormName()) || $formName == $this->getFormName()) {
			if($this->hasMessages($type)) {
				$iconMap=array('info' => '*', 'warning' => '!', 'error' => 'X', 'success' => '=');
				$o = sprintf('<div class="msg %s"><table style="width:100%%;"><td class="ico"><div>%s</div></td><td class="txt">', $type, $iconMap[$type]);
				$msg=array();
				/* @var $error \Pecee\UI\Form\FormMessage */
				foreach($this->getMessages($type) as $error) {
					$msg[] = sprintf('%s', $error->getMessage());
				}

				$o .= join('<br/>', $msg) . '</td></table></div>';
				return $o;
			}
		}
		return '';
	}

	protected function validationFor($name) {
		if($this->form()->validationFor($name)) {
			return '<span class="msg error">'.$this->form()->validationFor($name).'</span>';
		}
		return '';
	}

	protected function getRoute($controller = NULL, $method = NULL, $methodParams = NULL, $getParams = NULL, $includeMethodParams = FALSE, $doRewrite = TRUE) {
		$method=(!$method) ? array() : array($method);
		$methodParams=(!is_array($methodParams)) ? array() : $methodParams;
		return \PC\Router::GetRoute(\Pecee\Registry::GetInstance()->get('AdminUrl','admin'), $controller, array_merge($method, $methodParams), $getParams, $includeMethodParams, $doRewrite);
	}

	protected function setContentTemplate() {
		if($this->plugin && !$this->ignoreTemplate) {
			$filename = explode('\\', get_class($this));
			$filename=array_slice($filename, 2);
			$filename=join(DIRECTORY_SEPARATOR, $filename) . '.php';
			$p=$this->plugin->path.DIRECTORY_SEPARATOR;
			$theme = $p.'Theme'.DIRECTORY_SEPARATOR.$this->plugin->application->getTemplate(). DIRECTORY_SEPARATOR . 'Content' . DIRECTORY_SEPARATOR . $filename;
			$template=(file_exists($theme)) ? $theme : $p .'Template'.DIRECTORY_SEPARATOR.'Content' . DIRECTORY_SEPARATOR . $filename;
			$this->_contentTemplate = $template;
		}
	}

	public function getRessource($url) {
		if($this->plugin) {
			return \PC\Router::GetRoute('plugin', $this->plugin->name) . $url;
		}
		return $url;
	}
}