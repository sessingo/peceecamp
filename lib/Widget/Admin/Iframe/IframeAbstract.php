<?php
namespace PC\Widget\Admin\Iframe;
class IframeAbstract extends \PC\Widget\Admin\AdminAbstract {
	protected $plugin;
	protected $ignoreTemplate=FALSE;

	public function __construct($adminLevel=0) {
		$this->setAuthLevel($adminLevel);
		$this->plugin=\PC\Plugin::Instance()->getPlugin();

		parent::__construct();

		$this->getSite()->addWrappedCss('reset.css');
		$this->getSite()->addWrappedCss('typography.css');
		$this->getSite()->addWrappedCss('admin-global.css');
		$this->getSite()->addWrappedCss('admin.css');
		$this->getSite()->addWrappedCss('admin-iframe.css');
		$this->getSite()->addWrappedCss('tables.css');
		$this->getSite()->addWrappedJs('jquery-1.8.3.min.js');
		$this->getSite()->addWrappedJs('jquery.uniform.min.js');
		$this->getSite()->addWrappedJs('jquery.dataTables.min.js');
		$this->getSite()->addWrappedJs('jquery.ddslick.min.js');
		$this->getSite()->addWrappedJs('jquery.autosize.js');
		$this->getSite()->addWrappedJs('admin.js');

		$this->getSite()->addJs('/js/jquery-ui-1.9.2.custom.min.js');

		$this->getSite()->setTitle('PeceeCamp');

		$this->cssWrapRoute = \PC\Router::GetRoute(\Pecee\Registry::GetInstance()->get('AdminUrl','admin'),'css',array('wrap'));
		$this->jsWrapRoute = \PC\Router::GetRoute(\Pecee\Registry::GetInstance()->get('AdminUrl','admin'),'js',array('wrap'));

		$this->setTemplate('AdminIframe.php');
		$this->setContentTemplate();
	}

	protected function prependSiteTitle($title) {
		$this->_site->setTitle(($title . ' - ' .$this->_site->getTitle()));
	}

	protected function isAdmin() {
		return (\Pecee\Model\User\ModelUser::Current()->getAdminLevel() == \PC\Helper::USER_TYPE_ADMIN);
	}

	public function showFlash($formName=NULL) {
		$o=$this->showMessages('error', $formName);
		$o.=$this->showMessages('warning', $formName);
		$o.=$this->showMessages('info', $formName);
		$o.=$this->showMessages('success', $formName);
		return $o;
	}

	public function showMessages($type, $formName = NULL) {
		if(is_null($formName) || is_null($this->getFormName()) || $formName == $this->getFormName()) {
			if($this->hasMessages($type)) {
				$iconMap=array('info' => '*', 'warning' => '!', 'error' => 'X', 'success' => '=');
				$o = sprintf('<div class="msg %s"><table style="width:100%%;"><td class="ico"><div>%s</div></td><td class="txt">', $type, $iconMap[$type]);
				$msg=array();
				/* @var $error \Pecee\UI\Form\FormMessage */
				foreach($this->getMessages($type) as $error) {
					$msg[] = sprintf('%s', $error->getMessage());
				}

				$o .= join('<br/>', $msg) . '</td></table></div>';
				return $o;
			}
		}
		return '';
	}

	protected function validationFor($name) {
		if($this->form()->validationFor($name)) {
			return '<span class="msg error">'.$this->form()->validationFor($name).'</span>';
		}
		return '';
	}

	protected function getRoute($controller = NULL, $method = NULL, $methodParams = NULL, $getParams = NULL, $includeMethodParams = FALSE, $doRewrite = TRUE) {
		$method=(!$method) ? array() : array($method);
		$methodParams=(!is_array($methodParams)) ? array() : $methodParams;
		return \PC\Router::GetRoute(\Pecee\Registry::GetInstance()->get('AdminUrl','admin'), $controller, array_merge($method, $methodParams), $getParams, $includeMethodParams, $doRewrite);
	}

	protected function setContentTemplate() {
		if($this->plugin && !$this->ignoreTemplate) {
			$filename = explode('\\', get_class($this));
			$filename=array_slice($filename, 2);
			$filename=join(DIRECTORY_SEPARATOR, $filename) . '.php';
			$p=$this->plugin->path.DIRECTORY_SEPARATOR;
			$theme = $p.'Theme'.DIRECTORY_SEPARATOR.$this->plugin->application->getTemplate(). DIRECTORY_SEPARATOR . 'Content' . DIRECTORY_SEPARATOR . $filename;
			$template=(file_exists($theme)) ? $theme : $p .'Template'.DIRECTORY_SEPARATOR.'Content' . DIRECTORY_SEPARATOR . $filename;
			$this->_contentTemplate = $template;
		}
	}
}