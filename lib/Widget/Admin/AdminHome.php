<?php
namespace PC\Widget\Admin;
class AdminHome extends \PC\Widget\Admin\AdminAbstract {
	public function __construct() {
		parent::__construct();
	}

	protected function getTimeOfDay() {
		$h = date('H');
		if($h <= 11) {
			return $this->_('Admin/Home/GoodMorning');
		} elseif($h <= 16) {
			return $this->_('Admin/Home/GoodAfternoon');
		} elseif($h <= 23) {
			return $this->_('Admin/Home/GoodEvening');
		}
		return $this->_('Admin/Home/GoodNight');
	}
}