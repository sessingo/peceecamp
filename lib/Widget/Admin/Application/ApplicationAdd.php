<?php
namespace PC\Widget\Admin\Application;
class ApplicationAdd extends \PC\Widget\Admin\AdminAbstract {
	public function __construct() {
		parent::__construct(\PC\Helper::USER_TYPE_ADMIN);

		$this->prependSiteTitle($this->_('Admin/Application/Add'));

		$this->addLevel($this->_('Admin/Application/Applications'), NULL);
		$this->addLevel($this->_('Admin/Application/Add'), $this->getRoute(NULL, 'add'));

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Admin/Application/Plugin'), 'plugin', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty($this->_('Admin/Application/PluginRequired')));
			$this->addInputValidation($this->_('Admin/Application/Name'), 'name', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			if($this->data->useFromFrontend && \PC\Model\ModelApplication::CheckHost(trim($this->data->slug, '/'), $this->data->host)->hasRow()) {
				$this->setMessage($this->_('Admin/Application/SlugInUse'), 'error', 'slug');
			}

			if(!$this->hasErrors()) {
				$app=new \PC\Model\ModelApplication();
				$app->setUserID(\Pecee\Model\User\ModelUser::Current()->getUserID());
				$app->setPlugin($this->data->plugin);
				$app->setTheme($this->data->theme);
				$app->setName($this->data->name);
				$app->setUseFromFrontend($this->data->useFromFrontend);
				$app->setHost($this->data->host);
				$app->setSlug(trim($this->data->slug, '/'));
				$app->setIcon($this->data->icon);
				$app->save();

				\PC\Router::Redirect($this->getRoute($app->getSlug()));
			}
		}
	}
}