<?php
namespace PC\Widget\Admin\Application;
class ApplicationHome extends \PC\Widget\Admin\AdminAbstract {
	protected $applications;
	public function __construct() {
		parent::__construct(\PC\Helper::USER_TYPE_ADMIN);

		$this->prependSiteTitle($this->_('Admin/Application/Applications'));

		$this->addLevel($this->_('Admin/Application/Applications'), $this->getRoute());
		$this->applications=\PC\Model\ModelApplication::Get(NULL, NULL);
	}
}