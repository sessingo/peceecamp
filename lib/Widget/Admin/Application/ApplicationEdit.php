<?php
namespace PC\Widget\Admin\Application;
class ApplicationEdit extends \PC\Widget\Admin\AdminAbstract {
	public function __construct() {

		$this->ignoreTemplate=TRUE;
		$this->title=$this->_('Admin/Application/ConfigureApplication');
		parent::__construct(\PC\Helper::USER_TYPE_ADMIN);

		$this->prependSiteTitle($this->_('Admin/Application/ConfigureApplication'));

		$this->getMenu(self::MENU_TYPE_CONFIGURE)->addClass('active');

		$this->addLevel($this->_('Admin/Application/Configure'));

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Admin/Application/Plugin'), 'plugin', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty($this->_('Admin/Application/PluginRequired')));
			$this->addInputValidation($this->_('Admin/Application/Name'), 'name', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			if($this->data->useFromFrontend) {
				$app = \PC\Model\ModelApplication::CheckHost(trim($this->data->slug, '/'), $this->data->host);
				if($app->hasRow() && $app->getApplicationID() != $this->plugin->application->getApplicationID()) {
					$this->setMessage($this->_('Admin/Application/SlugInUse'), 'error', 'slug');
				}
			}

			if(!$this->hasErrors()) {
				$this->plugin->application->setUserID(\Pecee\Model\User\ModelUser::Current()->getUserID());
				$this->plugin->application->setPlugin($this->data->plugin);
				$this->plugin->application->setTheme($this->data->theme);
				$this->plugin->application->setName($this->data->name);
				$this->plugin->application->setUseFromFrontend($this->data->useFromFrontend);
				$this->plugin->application->setHost($this->data->host);
				$this->plugin->application->setSlug(trim($this->data->slug, '/'));
				$this->plugin->application->setIcon($this->data->icon);
				$this->plugin->application->update();

				$this->setMessage($this->_('Admin/Application/ApplicationUpdated'), 'success');

				\PC\Router::Refresh();
			}
		}
	}
}