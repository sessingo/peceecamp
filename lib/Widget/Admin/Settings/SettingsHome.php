<?php
namespace PC\Widget\Admin\Settings;
class SettingsHome extends \PC\Widget\Admin\Settings\SettingsAbstract {
	public function __construct() {
		parent::__construct();

		if($this->isPostBack()) {
			$user = \Pecee\Model\User\ModelUser::Current(TRUE);

			// Set locale in user settings.
			$user->data->locale = $this->data->locale;

			if($this->data->password) {
				if(md5($this->data->password) != $user->getPassword()) {
					$this->setError($this->_('Admin/Settings/IncorrectPassword'));
				}

				if(!$this->hasErrors()) {
					$user->setPassword($this->data->newPassword);
				}
			}

			$user->update();

			if(!$this->hasErrors()) {
				$this->setMessage($this->_('Admin/Settings/SettingsSaved'), 'success');
				\PC\Router::Refresh();
			}
		}
	}
}