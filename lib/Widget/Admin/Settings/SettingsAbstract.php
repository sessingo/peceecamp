<?php
namespace PC\Widget\Admin\Settings;
abstract class SettingsAbstract extends \PC\Widget\Admin\AdminAbstract {
	public function __construct() {
		parent::__construct();
		$this->prependSiteTitle($this->_('Admin/Settings/Settings'));
		$this->addLevel($this->_('Admin/Settings/Settings'));
	}
}