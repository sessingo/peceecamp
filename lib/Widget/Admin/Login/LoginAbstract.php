<?php
namespace PC\Widget\Admin\Login;
abstract class LoginAbstract extends \PC\Widget\WidgetAbstract {
	protected $currentLocale;
	public function __construct() {

		if($this->hasParam('lang') && in_array($this->getParam('lang'), \PC\Helper::$LANGUAGES)) {
			\Pecee\Cookie::Create(\PC\Helper::LANG_COOKIE_NAME, $this->getParam('lang'));
			\PC\Router::GoBack();
		}

		parent::__construct(NULL);
		$this->setTemplate('Login.php');
		$this->getSite()->addWrappedCss('reset.css');
		$this->getSite()->addWrappedCss('admin-global.css');
		$this->getSite()->addWrappedCss('admin-login.css');
		$this->getSite()->addWrappedJs('jquery-1.8.3.min.js');
		$this->getSite()->setTitle('PeceeCamp');

		$this->currentLocale = \Pecee\Cookie::Get(\PC\Helper::LANG_COOKIE_NAME);

		if($this->currentLocale && in_array($this->currentLocale, \PC\Helper::$LANGUAGES)) {
			\Pecee\Locale::Instance()->setLocale($this->currentLocale);
		}
	}

	protected function validationFor($name) {
		if($this->form()->validationFor($name)) {
			return '<span class="msg error">'.$this->form()->validationFor($name).'</span>';
		}
		return '';
	}
}