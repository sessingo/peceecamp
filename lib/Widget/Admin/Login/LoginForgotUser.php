<?php
namespace PC\Widget\Admin\Login;
class LoginForgotUser extends \PC\Widget\Admin\Login\LoginAbstract {
	public function __construct() {
		parent::__construct();

		$this->prependSiteTitle($this->_('Admin/ForgotUser/ForgotTitle'));

		if($this->isPostBack()) {

			$this->addInputValidation('E-mail', 'email', new \PC\UI\Form\Validate\ValidateEmail());

			if(!$this->hasErrors()) {
				$user = \Pecee\Model\User\ModelUser::GetByEmail($this->data->email);

				if(!$user->hasRow()) {
					$this->setError($this->_('Incorrect e-mail'));
				} else {
					$reset = new \Pecee\Model\User\ModelUserReset($user->getUserID());
					$key = $reset->getKey();

					\Pecee\Util::SendMail($user->getEmail(), 'Recover lost login information',
					sprintf('Hello %s.

Your '), 'noreply@pecee.dk');

					\PC\Router::Refresh();
				}

			}
		}
	}
}