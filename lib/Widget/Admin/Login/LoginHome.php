<?php
namespace PC\Widget\Admin\Login;
class LoginHome extends \PC\Widget\Admin\Login\LoginAbstract {
	public function __construct() {
		parent::__construct();

		$this->prependSiteTitle($this->_('Admin/Login/LoginTitle'), ' - ');

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Admin/Login/Username'), 'username', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation($this->_('Admin/Login/Password'), 'password', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			if(!$this->hasErrors()) {
				$user=\Pecee\Model\User\ModelUser::Authenticate($this->data->username, $this->data->password, $this->data->remember);
				if(!($user instanceof \Pecee\Model\User\ModelUser)) {
					$this->setMessage($this->_('Admin/Login/InvalidLogin'), 'login_error');
					sleep(3);
				}
				if($this->hasParam('ref')) {
					\PC\Router::Redirect($this->getParam('ref'));
				}
				\PC\Router::Refresh();
			} else {
				// For security purposes
				sleep(3);
			}
		}
	}
}