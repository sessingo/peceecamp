<?php
namespace PC\Widget\Admin\User;
class UserDelete extends \PC\Widget\Admin\AdminAbstract {
	public function __construct($userId) {
		parent::__construct(\PC\Helper::USER_TYPE_ADMIN);

		$user=\Pecee\Model\User\ModelUser::GetByUserID($userId);
		if($user->hasRow()) {
			$user->delete();
		}
		$this->setMessage($this->_('Admin/User/UserDeleted', $user->getUsername()), 'success');
		\PC\Router::Redirect($this->getRoute('user'));
	}
}