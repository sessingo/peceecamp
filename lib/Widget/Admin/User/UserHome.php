<?php
namespace PC\Widget\Admin\User;
class UserHome extends \PC\Widget\Admin\AdminAbstract {
	protected $users;
	public function __construct() {
		parent::__construct(\PC\Helper::USER_TYPE_ADMIN);

		$this->prependSiteTitle($this->_('Admin/User/Users'));

		$menu=$this->getMenu(self::MENU_TYPE_USER)->addClass('active');
		$menu->getMenu()->getItem(0)->addClass('active');
		$this->addLevel($this->_('Admin/User/Users'), $this->getRoute());
		$this->users=\Pecee\Model\User\ModelUser::Get(NULL, NULL, 0, NULL, 10, 0);
	}

	protected function getUserType($adminLevel) {
		switch($adminLevel) {
			case 1:
				return $this->_('Admin/UserType/Administrator');
		}

		return $this->_('Admin/UserType/Editor');
	}
}