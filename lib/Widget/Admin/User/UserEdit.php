<?php
namespace PC\Widget\Admin\User;
class UserEdit extends \PC\Widget\Admin\AdminAbstract {
	protected $user;
	public function __construct($userId) {
		parent::__construct(\PC\Helper::USER_TYPE_ADMIN);

		$this->getMenu(self::MENU_TYPE_USER)->addClass('active');
		$this->addLevel($this->_('Admin/User/Users'), $this->getRoute());
		$this->user=\Pecee\Model\User\ModelUser::GetByUserID($userId);

		$this->prependSiteTitle($this->_('Admin/User/EditTitle', $this->user->getUsername()));

		$this->addLevel($this->user->getUsername(), $this->getRoute());

		$this->addInputValidation($this->_('Admin/User/Username'), 'username', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
		$this->addInputValidation($this->_('Admin/User/Email'), 'email', new \PC\UI\Form\Validate\ValidateEmail());

		if($this->isPostBack()) {
			$this->user->setUsername($this->data->username);
			if($this->data->password) {
				$this->user->setPassword($this->data->password);
			}
			$this->user->data->firstname = $this->data->firstname;
			$this->user->data->lastname = $this->data->lastname;

			$this->user->setAdminLevel($this->data->type);
			$this->user->setEmail($this->data->email);
			$this->user->update();
			\PC\Router::Redirect($this->getRoute('user'));
		}
	}
}