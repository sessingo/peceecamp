<?php
namespace PC\Widget\Admin\User;
class UserAdd extends \PC\Widget\Admin\AdminAbstract {
	public function __construct() {
		parent::__construct(\PC\Helper::USER_TYPE_ADMIN);

		$this->prependSiteTitle($this->_('Admin/User/AddUser'));

		$menu=$this->getMenu(self::MENU_TYPE_USER)->addClass('active');
		$menu->getMenu()->getItem(1)->addClass('active');
		$this->addLevel($this->_('Admin/User/Users'), $this->getRoute('user'));
		$this->addLevel($this->_('Admin/User/Add'), $this->getRoute('user', 'add'));
		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Admin/User/Username'), 'username', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation($this->_('Admin/User/Password'), 'password', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation($this->_('Admin/User/Email'), 'email', new \PC\UI\Form\Validate\ValidateEmail());

			if(!$this->hasErrors()) {
				$user=new \Pecee\Model\User\ModelUser($this->data->username, $this->data->password);
				$user->setEmail($this->data->email);
				$user->setAdminLevel($this->data->type);
				$user->data->firstname = $this->data->firstname;
				$user->data->lastname = $this->data->lastname;
				$user->save();
				$this->setMessage($this->_('Admin/User/UserAdded', $this->data->username), 'success');
				\PC\Router::Redirect($this->getRoute('user'));
			}
		}
	}
}