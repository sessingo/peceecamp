<?php
namespace PC\Widget\Admin\Dialog;
abstract class DialogAbstract extends \PC\Widget\Admin\AdminAbstract {
	protected $title;
	protected $enableDialogClose=TRUE;
	protected $isAjaxRequest;
	public function __construct() {
		// Redirect page if the user is no longer logged in.
		\Pecee\Auth::GetInstance()->check();

		$this->enableDialogClose=true;
		parent::__construct();
		if($this->isAjaxRequest || $this->isAjaxRequest()) {
			$this->setTemplate('Dialog.php');
		}
	}
	public function doUserCallback($javascript='') {
		die('<script type="text/javascript">
				var fn=global.dialog.o.userCallback;
				if(fn != null) {
					global.dialog.o.userCallback=null;
					fn(status);
				}
				dialog.closeActive();
				return false;
			</script>');
	}
	public function close($javascript='') {
		die('<script type="text/javascript">
				$(document).ready(function() {
				dialog.closeActive(); '.$javascript.'});
				</script>');
	}
	public function refresh() {
		die('<script type="text/javascript">
				$(document).ready(function() {
				dialog.closeAll(); location.reload(true); });
				</script>');
	}
	public function redirect($url) {
		die('<script type="text/javascript">
				$(document).ready(function() {
				dialog.closeAll(); top.location.href=\''.addslashes($url).'\'; });
				</script>');
	}

	public function setTitle($title) {
		$this->title=$title;
	}
	public function setEnableDialogClose($bool) {
		$this->enableDialogClose=$bool;
	}
}