<?php
namespace PC\Widget;
class WidgetHome extends \Pecee\Widget\Widget {
	public function __construct() {
		parent::__construct();
		$this->setContentTemplate(\PC\Router::GetTemplatePath(get_class($this)));
	}

	public function setError($message) {
		parent::setError($message);
	}
}