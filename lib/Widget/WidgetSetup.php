<?php
namespace PC\Widget;
class WidgetSetup extends \PC\Widget\WidgetAbstract {
	public function __construct() {
		parent::__construct();
		$this->getSite()->addCss('/css/setup.css');
		$this->getSite()->addJs('/js/jquery-1.8.3.min.js');
		$this->setTemplate('Setup.php');

		$this->getSite()->setTitle('Install PeceeCamp');

		if($this->isPostBack()) {

			$this->addInputValidation('Host', 'dbHost', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation('Username', 'dbUsername', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			//$this->addInputValidation('Password', 'dbPassword', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			$this->addInputValidation('Database', 'dbDatabase', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			$this->addInputValidation('Username', 'username', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation('Password', 'password', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			$this->addInputValidation('Admin url', 'adminUrl', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			if(!$this->hasErrors()) {

				$cachePath = dirname($_SERVER['DOCUMENT_ROOT']) . DIRECTORY_SEPARATOR . 'cache';
				if(!is_dir($cachePath)) {
					if(!@mkdir($cachePath)) {
						$this->setError('Failed to create cache path!');
					}
					@chmod($cachePath, 0777);
				}

				if(!$this->hasErrors()) {
					try {
						$key = \Pecee\Registry::GetInstance();
						$key->set('DBUsername', $this->data->dbUsername);
						$key->set('DBPassword', $this->data->dbPassword);
						$key->set('DBHost', $this->data->dbHost);
						$key->set('DBDatabase', $this->data->dbDatabase);

						$db = \Pecee\DB\DB::GetInstance();
					} catch(\Exception $e) {
						$this->setError($e->getMessage());
					}

					if(!$this->hasErrors()) {
						try {
							$db->runSql(dirname($_SERVER['DOCUMENT_ROOT']) . '/setup/sql/Setup.sql');
						} catch(\Exception $e) {
							$this->setError($e->getMessage());
						}

						if($this->data->testSite) {

							if(!$this->data->fileStorage || !is_dir($this->data->fileStorage)) {
								$this->setError('Invalid file storage path');
							}

							if(!$this->hasErrors()) {
								try {
									$db->runSql(dirname($_SERVER['DOCUMENT_ROOT']) . '/setup/sql/ExampleData.sql');
								} catch(\Exception $e) {
									$this->setError($e->getMessage());
								}

								if(!$this->hasErrors()) {
									// Create default settings
									$plugin = \PC\Plugin::Instance();
									$plugin->setActiveApp(\PC\Model\ModelApplication::GetByApplicationID(1));

									$settings = \CMS\Model\ModelSettings::GetInstance();
									$settings->setCreateThumbs(TRUE);
									$settings->setFileStoragePath($this->data->fileStorage);
									$settings->update();

									@copy(dirname($_SERVER['DOCUMENT_ROOT']) . '/setup/data/28.jpg', $this->data->fileStorage . DIRECTORY_SEPARATOR . '28.jpg');
								}

							}
						}

						if(!$this->hasErrors()) {
							// Save user
							$user = new \Pecee\Model\User\ModelUser();
							$user->setUsername($this->data->username);
							$user->setPassword($this->data->password);
							$user->setAdminLevel(1);
							$user->save();

							// Save config
							$handle = fopen(dirname($_SERVER['DOCUMENT_ROOT']) . '/config/config.php', 'w+');
							fwrite($handle, $this->getConfig());
							fclose($handle);

							\PC\Router::Redirect('/'.trim($this->data->adminUrl,'/'));
						}
					}
				}
			}
		}
	}

	protected function getConfig() {
		return sprintf('<?php
$key = \Pecee\Registry::GetInstance();

/* ---------- Configuration start ---------- */
$key->set(\'RequireSSL\', %s);

/* Admin url */
$key->set(\'AdminUrl\', \'%s\'); // What url the administration will be availible at (admin = /admin/)

/* Database */
$key->set(\Pecee\DB\DB::SETTINGS_USERNAME, \'%s\');
$key->set(\Pecee\DB\DB::SETTINGS_PASSWORD, \'%s\');
$key->set(\Pecee\DB\DB::SETTINGS_HOST, \'%s\');
$key->set(\Pecee\DB\DB::SETTINGS_DATABASE, \'%s\');

/* Cache */
\PC\Cache::GetInstance()->setType(\PC\Cache::TYPE_FILE); // To disable cache, just remove this line

/* Language */
\Pecee\Language::Instance()->setType(\Pecee\Language::TYPE_XML); // USE XML-ENGINE

$key->set(\Pecee\Language::SETTINGS_CACHE_ENABLE, FALSE);
/* ---------- Configuration end ---------- */',
				(($this->data->requireSSL) ? 'TRUE' : 'FALSE'),
				trim($this->data->adminUrl, '/'),
				$this->data->dbUsername,
				$this->data->dbPassword,
				$this->data->dbHost,
				$this->data->dbDatabase);
	}
}