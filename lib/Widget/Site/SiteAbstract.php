<?php
namespace PC\Widget\Site;
abstract class SiteAbstract extends \PC\Widget\WidgetAbstract {
	public function __construct() {
		parent::__construct();

		// Set correct js/css routes
		$slug = '';
		if($this->plugin->application->getSlug()) {
			$slug = '/' . $this->plugin->application->getSlug();
		}
		$this->setCssWrapRoute($slug.'/css/wrap/');
		$this->setJsWrapRoute($slug.'/js/wrap/');

		$this->setContentTemplate();
		$this->setTemplate('Default.php');
	}

	protected function addInputValidation($index, $type) {
		if(in_array(\Pecee\Util::GetTypeOf($type), array('Pecee\UI\Form\Validate\ValidateInput', '\PC\UI\Form\Validate\ValidateInput'))) {
			$this->data->addInputValidation($index, $index, $type);
		} else {
			$this->files->addInputValidation($index, $index, $type);
		}
	}

	public function addJs($path) {
		$this->getSite()->addJs(\PC\Router::GetRoute('plugin', $this->plugin->name, array('theme', $this->plugin->application->getTemplate())) . $path);
	}

	public function addCss($path) {
		$this->getSite()->addCss(\PC\Router::GetRoute('plugin', $this->plugin->name, array('theme', $this->plugin->application->getTemplate())) . $path);
	}

	protected  function getPath() {
		return $this->plugin->themesdir . DIRECTORY_SEPARATOR .$this->plugin->application->getTemplate().DIRECTORY_SEPARATOR;
	}

	protected function setTemplate($file) {
		if(is_null($file)) {
			$this->_template = NULL;
		} else {
			$this->_template = $this->getPath() .'/Template/'.$file;
		}
	}

	protected function setContentTemplate() {
		$path = array();
		$add=FALSE;
		foreach(explode('\\', get_class($this)) as $p) {
			if($add) {
				$path[] = $p;
			}
			if(!$add && strtolower($p) == 'widget') {
				$add=TRUE;
			}

		}

		$this->_contentTemplate = $this->getPath() . 'Template/Content' . DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, $path) . '.php';
	}
}