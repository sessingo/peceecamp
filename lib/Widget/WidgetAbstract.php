<?php
namespace PC\Widget;
abstract class WidgetAbstract extends \Pecee\Widget\Widget {
	protected $plugin;
	public function __construct() {
		$this->plugin=\PC\Plugin::Instance()->getPlugin();
		parent::__construct();
		$this->_contentTemplate = \PC\Router::GetTemplatePath(get_class($this));
	}

	public function _($text,$args=NULL) {
		try {
			$args = array_splice(func_get_args(), 1);
			$out = \PC\Language::GetInstance()->_($text, $args);
		} catch(\PC\Xml\Translate\TranslateException $e) {
			return \Pecee\Language::Instance()->_($text, $args);
		}
		return $out;
	}

	public function getRessource($url) {
		if($this->plugin) {
			return \PC\Router::GetRoute('plugin', $this->plugin->name, array('theme', $this->plugin->application->Theme)) . $url;
		}
		return $url;
	}
}