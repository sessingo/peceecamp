<?php
namespace PC\Model;
class ModelApplication extends \Pecee\Model\Model {
	const TEMPLATE_DEFAULT='default';
	protected $texts;
	public function __construct() {
		parent::__construct('Application', array('ApplicationID' => '', 'UserID' => '', 'Plugin' => '', 'Name' => '',
												'Host' => NULL, 'Slug' => '', 'Theme' => NULL, 'Icon' => NULL, 'UseFromFrontend' => FALSE,
												'PubDate' => \Pecee\Date::ToDateTime(time())));
	}

	public function getStorageUri() {
		return sprintf('/plugin/%s/theme/%s/', $this->Plugin, $this->getTemplate());
	}

	public function getIconUri() {
		return ($this->Icon) ? rtrim($this->getStorageUri(),'/') . $this->Icon : '/gfx/menu/app.png';
	}

	public function getTemplate() {
		return ($this->Theme) ?  $this->Theme : self::TEMPLATE_DEFAULT;
	}

	public function save() {
		parent::save();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	public function update() {
		parent::update();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	public function delete() {
		parent::delete();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	/**
	 * @return \PC\Model\ModelApplication
	 */
	public static function GetByApplicationID($applicationId) {
		$key = \PC\Helper::GenerateKey('ModelApplicationGetByApplicationId', $applicationId);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$val = self::FetchOne('SELECT * FROM `Application` WHERE `ApplicationID` = %s', $applicationId);
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	/**
	 * @return \PC\Model\ModelApplication
	 */
	public static function CheckHost($slug, $domain=NULL) {
		$key = \PC\Helper::GenerateKey('ModelApplicationCheckHost', $slug, $domain);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$where = array('`UseFromFrontend` = 1 && INSTR(%s,`Slug`) > 0');
			if(!is_null($domain)) {
				$where[] = \Pecee\DB\DB::FormatQuery('(`Host` = %s)', array($domain));
			}
			$val = self::FetchOne('SELECT * FROM `Application` WHERE ' . join(' && ', $where) . ' ORDER BY `Slug` DESC', $slug);
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	/**
	 * @return \PC\Model\ModelApplication
	 */
	public static function GetBySlug($slug, $domain=NULL) {
		$key = \PC\Helper::GenerateKey('ModelApplicationGetBySlug', $slug, $domain);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$where = array('`UseFromFrontend` = 1 && INSTR(%s,`Slug`) > 0');
			if(!is_null($domain)) {
				$where[] = \Pecee\DB\DB::FormatQuery('(`Host` IS NULL || `Host` = \'\' || `Host` = %s)', array($domain));
			}
			$val = self::FetchOne('SELECT * FROM `Application` WHERE ' . join(' && ', $where) . ' ORDER BY `Slug` DESC', $slug);
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	public static function Get($rows=NULL, $page=NULL){
		return self::FetchPage('`ApplicationID`', 'SELECT * FROM `Application`', $rows, $page);
	}
 }