<?php
namespace PC;
class Plugin {
	const DIRECTORY='Plugin';
	protected static $instance;
	protected $path;
	protected $plugins;
	protected $plugin;
	protected $isAdmin;
	public static function Instance() {
		if(self::$instance==NULL) {
			self::$instance=new self();
		}
		return self::$instance;
	}
	public function __construct() {
		$this->plugins=array();
		$this->findPlugins();
		set_include_path(get_include_path() . PATH_SEPARATOR . $this->getDir() . DIRECTORY_SEPARATOR);
		set_include_path(get_include_path() . PATH_SEPARATOR . $this->getThemesDir() . DIRECTORY_SEPARATOR);
	}

	public function getByName($name){
		if(count($this->plugins) > 0){
			foreach($this->plugins as $plugin) {
				if(strtolower($plugin->name)==strtolower($name)) {
					return $plugin;
				}
			}
		}
		return NULL;
	}

	public function exists($name) {
		return ($this->getByName($name));
	}

	public function getBase() {
		return dirname(dirname(__FILE__));
	}

	public function getDir() {
		return $this->getBase() . DIRECTORY_SEPARATOR . 'Plugins';
	}

	public function getThemesDir() {
		return $this->getBase() . DIRECTORY_SEPARATOR . 'Themes';
	}

	protected function getVars($config) {
		require($config);
		//return (object)get_defined_vars();
	}

	public function findPlugins(){
		if(is_dir($this->getDir())) {
			$handle=opendir($this->getDir());
			if($handle) {
				$excludeDir=array('.', '..', '.svn', 'Abstract', '.DS_Store');

				while (FALSE !== ($dirs = readdir($handle))) {
					if(!in_array($dirs, $excludeDir)) {
						$pluginName=$dirs;
						$pluginPath=$this->getDir() . DIRECTORY_SEPARATOR.$dirs;
						$themesdir = $this->getThemesDir() . DIRECTORY_SEPARATOR . $dirs;
						$themes=array();
						if(is_dir($themesdir)) {
							$res=opendir($themesdir);
							while(($dir = readdir($res)) != FALSE) {
								if(!in_array($dir, $excludeDir)) {
									$themes[]=$dir;
								}
							}
						}
						$this->plugins[]=(object)array('name'=> $pluginName, 'themes' => $themes, 'path' => $pluginPath, 'themesdir' => $themesdir);
					}
				}
				closedir($handle);
			}
		}
	}

	public function getPlugins() {
		return $this->plugins;
	}
	public function getPlugin() {
		return $this->plugin;
	}
	public function setActiveApp($app) {
		if(!is_null($app) && $app instanceof \PC\Model\ModelApplication && $app->hasRow()) {
			$this->plugin=$this->getByName($app->getPlugin());
			if($this->isAdmin) {
				\PC\Language::GetInstance()->getLanguage()->setXmlDir($this->plugin->path . DIRECTORY_SEPARATOR . 'lang');
			}
			$this->plugin->application=$app;
			$config=$this->plugin->path. DIRECTORY_SEPARATOR. 'config.php';
			$settings=new \stdClass();
			if(file_exists($config)) {
				$this->getVars($config);
			}
			$config=$this->plugin->themesdir. DIRECTORY_SEPARATOR. $this->plugin->application->Theme . DIRECTORY_SEPARATOR . 'config.php';
			if(file_exists($config)) {
				$this->getVars($config);
			}
		}
	}

	protected function getRoute($controller = NULL, $method = NULL, $methodParams = NULL, $getParams = NULL, $includeMethodParams = FALSE, $doRewrite = TRUE) {
		$method=(!$method) ? array() : array($method);
		$methodParams=(!is_array($methodParams)) ? array() : $methodParams;
		return \PC\Router::GetRoute(\Pecee\Registry::GetInstance()->get('AdminUrl','admin'), $controller, array_merge($method, $methodParams), $getParams, $includeMethodParams, $doRewrite);
	}

	public function getIsAdmin() {
		return $this->isAdmin;
	}

	public function setIsAdmin($admin) {
		$this->isAdmin = $admin;
	}

	/**
	 * Translates message
	 * @param string $messageID
	 * @param array $args
	 * @return Language
	 */
	public function _($key, $args = NULL) {
		if (!is_array($args)) {
			$args = func_get_args();
			$args = array_slice($args, 1);
		}
		return vsprintf(\PC\Language::GetInstance()->_($key), $args);
	}
}