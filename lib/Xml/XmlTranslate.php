<?php
namespace PC\Xml;
class XmlTranslate {
	protected $xml;
	protected $dir;

	public function lookup($key) {
		if(!$this->dir) {
			throw new \PC\Xml\Translate\TranslateException('XML language directory must be specified: \Pecee\Language::SETTINGS_XML_DIRECTORY');
		}
		if(!$this->xml) {
			$this->setLanguageXml();
		}
		$xml=new \SimpleXmlElement($this->xml);
		$node=NULL;
		if(strpos($key, '/') > -1) {
			$children=explode('/', $key);
			foreach($children as $i=>$child) {
				if($i==0) {
					$node=(isset($xml->$child) ? $xml->$child : NULL);
				} else {
					$node=(isset($node->$child) ? $node->$child : NULL);
				}
			}
		} else {
			$node=isset($xml->$key) ? $xml->$key : NULL;
		}
		if(!is_null($node)) {
			return $node;
		}
		throw new \PC\Xml\Translate\TranslateException(sprintf('Key "%s" does not exist for locale "%s (path: %s)"', $key, \Pecee\Locale::Instance()->getLocale(), $this->dir));
	}

	protected function setLanguageXml() {
		$locale = \Pecee\Locale::Instance()->getLocale();
		$path = sprintf('%s/%s.xml', $this->dir, $locale);
		if(!file_exists($path)) {
			if($locale != \PC\Helper::LANG_EN_UK) {
				$path = sprintf('%s/%s.xml', $this->dir, \PC\Helper::LANG_EN_UK);
			}
			if(!file_exists($path)) {
				throw new \PC\Xml\Translate\TranslateException(sprintf('Language file %s not found for locale %s', $path, $locale));
			}
		}
		$this->xml = file_get_contents($path);
	}

	public function setXmlDir($dir) {
		$this->dir = $dir;
	}

	public function getXmlDir() {
		return $this->dir;
	}
}