<?php
namespace PC;
class ObjectManager {
	protected static $Instance;
	protected $objects;

	public static function GetInstance() {
		if(is_null(self::$Instance)) {
			self::$Instance = new self();
		}
		return self::$Instance;
	}

	public function __construct() {
		$this->extensions = array();
	}

	public function set($class, $replacementClass) {
		if(!class_exists($class)) {
			throw new \InvalidArgumentException('Class '.$class.' does not exist');
		}

		$this->objects[$class] = $replacementClass;
	}

	/**
	 * Get class
	 * @param string $class
	 * @throws \InvalidArgumentException
	 * @return object
	 */
	public function get($class) {
		$class = (isset($this->objects[$class])) ? $this->objects[$class] : $class;
		if(!class_exists($class)) {
			throw new \InvalidArgumentException('Class "'.$class.'" does not exist');
		}
		return $class;
	}
}