<?php
namespace PC;
use Pecee\Str;

class Language {
	protected static $Instance;

	public static function GetInstance() {
		if(is_null(self::$Instance)) {
			self::$Instance = new self();
		}
		return self::$Instance;
	}

	protected $language;

	public function __construct() {
		$this->language = new \PC\Xml\XmlTranslate();
		$this->language->setXmlDir(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'lang');
		if(\Pecee\Model\User\ModelUser::IsLoggedIn()) {

			$locale = \Pecee\Cookie::Get(\PC\Helper::LANG_COOKIE_NAME);
			if($locale && in_array($locale, \PC\Helper::$LANGUAGES)) {
				\Pecee\Locale::Instance()->setLocale($locale);
			} else {
				$locale = Str::GetFirstOrValue(\Pecee\Model\User\ModelUser::Current(TRUE)->data->locale,FALSE);
				if($locale && in_array($locale,\PC\Helper::$LANGUAGES)) {
					\Pecee\Locale::Instance()->setLocale($locale);
				}
			}
		}
	}

	public function getLanguage() {
		return $this->language;
	}

	/**
	 * Translates message
	 * @param string $messageID
	 * @param array $args
	 * @return Language
	 */
	public function _($key, $args = NULL) {
		if (!is_array($args)) {
			$args = func_get_args();
			$args = array_slice($args, 1);
		}
		return vsprintf($this->language->lookup($key), $args);
	}
}