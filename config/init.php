<?php
/* Defines include paths */
function abspath() {
	return dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR;
}

// Defines include paths
set_include_path(abspath() . PATH_SEPARATOR . abspath() . 'lib' . DIRECTORY_SEPARATOR . PATH_SEPARATOR . abspath() . 'Pecee' . DIRECTORY_SEPARATOR);

// Setup class routing and autoload
function __autoload($class){
	$file = NULL;
	if(strpos($class, 'PC\\') !== FALSE) {
		$file = abspath() . 'lib'. DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, str_replace('PC\\', '', $class).'.php');
	} else {
		if(strpos($class, 'Pecee\\') !== FALSE) {
			$file = abspath() . 'Pecee'. DIRECTORY_SEPARATOR .'lib'. DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, str_replace('Pecee\\', '', $class).'.php');
		} else {
			$file = str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
		}
	}
	if($file) {
		$exists = stream_resolve_include_path($file);
		if($exists !== FALSE) {
			include $file;
		}
	}
}