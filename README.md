# README #

This README is to document the steps necessary to get PeceeCamp up and running on your server.

### Required ###

* PHP 5.2 or later
* PHP Extension: mbstring
* PHP Extension: curl
* Url-rewrite activated on your webserver

### How to get PeceeCamp up and running? ###

* Copy all files from the zip to the location that you want PeceeCamp to be stored (usually /var/www/PeceeCamp).
* Make sure that folder and all it's directories have the necessary read/write access in order for auto-update to work.
* Setup your webserver so that the directory PeceeCamp/www folder is the one that is accessible through the web.
* Open a browser and go to the url of PeceeCamp and follow the instructions on the screen.
