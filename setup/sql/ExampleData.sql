-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Vært: localhost
-- Genereringstid: 04. 01 2015 kl. 21:44:11
-- Serverversion: 5.5.38
-- PHP-version: 5.4.4-14+deb7u14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `PeceeCampTest`
--

--
-- Data dump for tabellen `Application`
--

INSERT INTO `Application` (`ApplicationID`, `UserID`, `Plugin`, `Name`, `Host`, `Slug`, `Theme`, `Icon`, `UseFromFrontend`, `PubDate`) VALUES
(1, 1, 'CMS', 'Example site', NULL, '', 'DefaultTheme', NULL, 1, '2014-12-11 09:14:51');

--
-- Data dump for tabellen `CMS_Language`
--

INSERT INTO `CMS_Language` (`LanguageID`, `ApplicationID`, `Name`, `Alias`) VALUES
(5, 1, 'English', 'en');

--
-- Data dump for tabellen `CMS_NodeHost`
--

INSERT INTO `CMS_NodeHost` (`NodeHostID`, `NodeID`, `LanguageID`, `Hostname`) VALUES
(10, 9, 5, '');

--
-- Data dump for tabellen `CMS_PropertyModule`
--

INSERT INTO `CMS_PropertyModule` (`PropertyModuleID`, `PropertyTabID`, `NodeID`, `Name`, `Description`, `ControlID`, `Required`, `Module`, `Data`, `Order`, `Inheritable`) VALUES
(3, 1, 3, 'Footer text', '', 'footerText', NULL, '\\CMS\\Widget\\Module\\ModuleRichtext', NULL, 0, 1),
(4, 2, 6, 'Content', '', 'content', NULL, '\\CMS\\Widget\\Module\\ModuleRichtext', NULL, 2, 1),
(5, 2, 6, 'Sub header', '', 'subHeader', NULL, '\\CMS\\Widget\\Module\\ModuleTextbox', NULL, 1, 1),
(6, 3, 8, 'Content', '', 'sidebarContent', NULL, '\\CMS\\Widget\\Module\\ModuleRichtext', NULL, 0, 1),
(7, 4, 10, 'E-mail', '', 'email', 1, '\\CMS\\Widget\\Module\\ModuleTextbox', NULL, 0, 1),
(8, 5, 5, 'Image', '', 'slideImage', 1, '\\CMS\\Widget\\Module\\ModuleImage', NULL, 0, 1),
(9, 5, 5, 'Style', '', 'styling', NULL, '\\CMS\\Widget\\Module\\ModuleRadio', 'YToyOntzOjQ6IkxlZnQiO3M6MDoiIjtzOjU6IlJpZ2h0IjtzOjU6InJpZ2h0Ijt9', 0, 1),
(10, 5, 5, 'Header 1', '', 'slideHeader1', NULL, '\\CMS\\Widget\\Module\\ModuleTextbox', NULL, 0, 1),
(11, 5, 5, 'Header 2', '', 'slideHeader2', NULL, '\\CMS\\Widget\\Module\\ModuleTextbox', NULL, 0, 1),
(12, 5, 5, 'Link', '', 'slideLink', NULL, '\\CMS\\Widget\\Module\\ModuleTextbox', NULL, 0, 1),
(13, 6, 14, 'Content', '', 'content', NULL, '\\CMS\\Widget\\Module\\ModuleRichtext', NULL, 0, 1),
(14, NULL, 2, 'Title', '', 'setTitle', 0, '\\CMS\\Widget\\Module\\ModuleTextbox', NULL, 0, 1),
(15, NULL, 2, 'Content', '', 'setContent', 0, '\\CMS\\Widget\\Module\\ModuleRichtext', NULL, 0, 1);

--
-- Data dump for tabellen `CMS_PropertyTab`
--

INSERT INTO `CMS_PropertyTab` (`PropertyTabID`, `NodeID`, `Name`, `Order`, `Inheritable`) VALUES
(1, 3, 'Footer', 0, 1),
(2, 6, 'Content', 0, 1),
(3, 8, 'Sidebar', 0, 1),
(4, 10, 'Contact', 0, 1),
(5, 5, 'Slider', 0, 1),
(6, 14, 'Post', 0, 1);

--
-- Data dump for tabellen `Node`
--

INSERT INTO `Node` (`NodeID`, `ParentNodeID`, `Path`, `Type`, `Title`, `Content`, `PubDate`, `ChangedDate`, `ActiveFrom`, `ActiveTo`, `Active`, `Level`, `Order`) VALUES
(1, 0, '0', 'settings', 'Settings', NULL, '2014-12-11 09:14:51', '2015-01-04 21:38:54', NULL, NULL, 1, 1, 0),
(2, 0, '0', 'macro', 'Sidebar: textbox', NULL, '2014-12-11 09:15:45', '2014-12-26 02:09:25', NULL, NULL, 1, 1, 0),
(3, 0, '0', 'property', 'Site', NULL, '2014-12-11 09:20:27', '2014-12-26 02:05:05', NULL, NULL, 1, 1, 0),
(4, 0, '0', 'property', 'Slider', NULL, '2014-12-11 09:20:32', '2014-12-11 09:34:21', NULL, NULL, 1, 1, 0),
(5, 4, '0>4', 'property', 'Slide', NULL, '2014-12-11 09:20:36', '2014-12-26 02:07:22', NULL, NULL, 1, 2, 0),
(6, 3, '0>3', 'property', 'Textpage', NULL, '2014-12-11 09:20:45', '2014-12-26 02:07:32', NULL, NULL, 1, 2, 0),
(7, 3, '0>3', 'property', 'Frontpage', NULL, '2014-12-11 09:20:52', '2014-12-26 02:07:41', NULL, NULL, 1, 2, 0),
(8, 3, '0>3', 'property', 'Blog', NULL, '2014-12-11 09:20:59', '2014-12-26 02:07:53', NULL, NULL, 1, 2, 0),
(9, 0, '0', 'page', 'Site', NULL, '2014-12-11 09:22:07', '2014-12-26 02:11:27', NULL, NULL, 1, 1, 1),
(10, 3, '0>3', 'property', 'Contact', NULL, '2014-12-11 09:26:54', '2014-12-26 02:08:19', NULL, NULL, 1, 2, 0),
(11, 3, '0>3', 'property', 'Search', NULL, '2014-12-11 09:27:03', '2014-12-26 02:08:26', NULL, NULL, 1, 2, 0),
(12, 3, '0>3', 'property', '404-page', NULL, '2014-12-11 09:27:12', '2014-12-26 02:08:31', NULL, NULL, 1, 2, 0),
(13, 8, '0>8>3', 'property', 'Category', NULL, '2014-12-11 09:29:05', '2014-12-26 02:08:01', NULL, NULL, 1, 3, 0),
(14, 8, '0>8>3', 'property', 'Post', NULL, '2014-12-11 09:29:12', '2014-12-26 02:08:08', NULL, NULL, 1, 3, 0),
(15, 0, '0', 'folder', 'Slider', NULL, '2014-12-11 09:35:04', '2014-12-11 09:48:08', NULL, NULL, 1, 1, 0),
(16, 9, '0>9', 'page', 'Frontpage', NULL, '2014-12-11 09:37:53', '2014-12-30 15:16:53', NULL, NULL, 1, 2, 1),
(17, 9, '0>9', 'page', 'Text page', NULL, '2014-12-11 09:38:51', '2014-12-30 15:16:53', NULL, NULL, 1, 2, 2),
(18, 17, '0>17>9', 'page', 'Sub page', NULL, '2014-12-11 09:39:34', '2014-12-26 02:11:35', NULL, NULL, 1, 3, 4),
(19, 9, '0>9', 'page', 'Blog', NULL, '2014-12-11 09:39:59', '2014-12-30 15:16:54', NULL, NULL, 1, 2, 3),
(20, 19, '0>19>9', 'page', 'Tech', NULL, '2014-12-11 09:40:13', '2014-12-26 02:11:35', NULL, NULL, 1, 3, 6),
(21, 19, '0>19>9', 'page', 'Funny', NULL, '2014-12-11 09:40:23', '2014-12-26 02:11:37', NULL, NULL, 1, 3, 7),
(22, 9, '0>9', 'page', 'Contact', NULL, '2014-12-11 09:40:37', '2014-12-30 15:16:55', NULL, NULL, 1, 2, 5),
(23, 9, '0>9', 'page', '404: Page not found', NULL, '2014-12-11 09:40:58', '2014-12-30 15:16:55', NULL, NULL, 1, 2, 6),
(24, 21, '0>21>19>9', 'page', 'My first blog post!', NULL, '2014-12-11 09:42:26', '2014-12-26 02:11:37', NULL, NULL, 1, 4, 10),
(26, 16, '0>16>9', 'page', 'Slider', NULL, '2014-12-11 09:43:45', '2014-12-26 02:11:34', NULL, NULL, 1, 3, 11),
(27, 26, '0>26>16>9', 'page', 'Example site!', NULL, '2014-12-11 09:43:56', '2014-12-26 02:11:34', NULL, NULL, 1, 4, 12),
(28, 15, '0>15', 'file', '182.jpg', NULL, '2014-12-11 09:44:36', '2014-12-11 09:48:10', NULL, NULL, 1, 2, 0),
(29, 9, '0>9', 'page', 'Search', NULL, '2014-12-11 09:46:09', '2014-12-30 15:16:54', NULL, NULL, 1, 2, 4);

--
-- Data dump for tabellen `NodeData`
--

INSERT INTO `NodeData` (`NodeID`, `Key`, `Value`) VALUES
(4, 'applicationid', '1'),
(4, 'alias', 'frontpageSlider'),
(4, 'icon', 'folder.png'),
(4, 'widgets', 'Tjs='),
(4, 'properties', '5'),
(15, 'applicationid', '1'),
(28, 'applicationid', '1'),
(28, 'filename', '28.jpg'),
(28, 'mime', 'image/jpeg'),
(28, 'bytes', '111278'),
(28, 'userid', '1'),
(28, 'ipaddress', '127.0.0.1'),
(28, 'width', '1920'),
(28, 'height', '500'),
(28, 'slug', '/slider/182.jpg'),
(3, 'applicationid', '1'),
(3, 'alias', 'site'),
(3, 'icon', 'house.png'),
(3, 'widgets', 'Tjs='),
(3, 'properties', '6,7,8,10,11,12'),
(5, 'applicationid', '1'),
(5, 'alias', 'slide'),
(5, 'icon', 'picture_empty.png'),
(5, 'widgets', 'Tjs='),
(6, 'applicationid', '1'),
(6, 'alias', 'page'),
(6, 'icon', 'layout_content.png'),
(6, 'widgets', 'YToxOntpOjA7czozMzoiXENNU1xEZWZhdWx0VGhlbWVcV2lkZ2V0XFRleHRwYWdlIjt9'),
(6, 'properties', '6'),
(7, 'applicationid', '1'),
(7, 'alias', 'frontpage'),
(7, 'icon', 'page_green.png'),
(7, 'widgets', 'YToxOntpOjA7czozNDoiXENNU1xEZWZhdWx0VGhlbWVcV2lkZ2V0XEZyb250cGFnZSI7fQ=='),
(7, 'properties', '4,5'),
(8, 'applicationid', '1'),
(8, 'alias', 'blog'),
(8, 'icon', 'page_white_stack.png'),
(8, 'widgets', 'YToxOntpOjA7czozNDoiXENNU1xEZWZhdWx0VGhlbWVcV2lkZ2V0XEJsb2dcSG9tZSI7fQ=='),
(8, 'properties', '13'),
(13, 'applicationid', '1'),
(13, 'alias', 'blogCategory'),
(13, 'icon', 'tag_blue.png'),
(13, 'widgets', 'YToxOntpOjA7czozNDoiXENNU1xEZWZhdWx0VGhlbWVcV2lkZ2V0XEJsb2dcSG9tZSI7fQ=='),
(13, 'properties', '14'),
(14, 'applicationid', '1'),
(14, 'alias', 'blogPost'),
(14, 'icon', 'page_white_text.png'),
(14, 'widgets', 'YToxOntpOjA7czozNDoiXENNU1xEZWZhdWx0VGhlbWVcV2lkZ2V0XEJsb2dcUG9zdCI7fQ=='),
(10, 'applicationid', '1'),
(10, 'alias', 'contact'),
(10, 'icon', 'application_form.png'),
(10, 'widgets', 'YToxOntpOjA7czozMjoiXENNU1xEZWZhdWx0VGhlbWVcV2lkZ2V0XENvbnRhY3QiO30='),
(11, 'applicationid', '1'),
(11, 'alias', 'searchpage'),
(11, 'icon', 'zoom.png'),
(11, 'widgets', 'YToxOntpOjA7czozMToiXENNU1xEZWZhdWx0VGhlbWVcV2lkZ2V0XFNlYXJjaCI7fQ=='),
(12, 'applicationid', '1'),
(12, 'alias', '404page'),
(12, 'icon', 'page_delete.png'),
(12, 'widgets', 'YToxOntpOjA7czozMzoiXENNU1xEZWZhdWx0VGhlbWVcV2lkZ2V0XEVycm9yNDA0Ijt9'),
(2, 'applicationid', '1'),
(2, 'widget', '\\CMS\\DefaultTheme\\Widget\\Macro\\TextBox'),
(9, 'applicationid', '1'),
(9, 'userid', '1'),
(9, 'metatitle', 'Site'),
(9, 'propertynodeid', '3'),
(9, 'visibleinmenu', '1'),
(9, 'slug', 'site'),
(9, 'widget', ''),
(9, 'metadescription', ''),
(9, 'metakeywords', NULL),
(9, 'footertext', '<p>Pain is just weakness leaving the body!</p>'),
(26, 'applicationid', '1'),
(26, 'userid', '1'),
(26, 'metatitle', 'Slider'),
(26, 'propertynodeid', '4'),
(26, 'visibleinmenu', '1'),
(26, 'slug', 'frontpage/slider'),
(26, 'widget', ''),
(26, 'metadescription', ''),
(26, 'metakeywords', NULL),
(27, 'applicationid', '1'),
(27, 'userid', '1'),
(27, 'metatitle', 'PeceeCamp'),
(27, 'propertynodeid', '5'),
(27, 'visibleinmenu', '1'),
(27, 'slug', 'frontpage/slider/example-site'),
(27, 'slideimage', '28'),
(27, 'styling', ''),
(27, 'slideheader1', 'Example site!'),
(27, 'slideheader2', 'Powered by PeceeCamp'),
(27, 'slidelink', ''),
(27, 'widget', ''),
(27, 'metadescription', ''),
(27, 'metakeywords', NULL),
(18, 'applicationid', '1'),
(18, 'userid', '1'),
(18, 'metatitle', 'Sub page'),
(18, 'propertynodeid', '6'),
(18, 'visibleinmenu', '1'),
(18, 'slug', 'text-page/sub-page'),
(18, 'subheader', ''),
(18, 'content', '<p>This is a subpage to my textpage!</p>'),
(18, 'footertext', ''),
(18, 'widget', '\\CMS\\DefaultTheme\\Widget\\Textpage'),
(18, 'metadescription', ''),
(18, 'metakeywords', NULL),
(20, 'applicationid', '1'),
(20, 'userid', '1'),
(20, 'metatitle', 'Tech'),
(20, 'propertynodeid', '13'),
(20, 'visibleinmenu', '1'),
(20, 'slug', 'blog/tech'),
(20, 'sidebarcontent', ''),
(20, 'footertext', ''),
(20, 'widget', '\\CMS\\DefaultTheme\\Widget\\Blog\\Home'),
(20, 'metadescription', ''),
(20, 'metakeywords', NULL),
(21, 'applicationid', '1'),
(21, 'userid', '1'),
(21, 'metatitle', 'Funny'),
(21, 'propertynodeid', '13'),
(21, 'visibleinmenu', '1'),
(21, 'slug', 'blog/funny'),
(21, 'sidebarcontent', ''),
(21, 'footertext', ''),
(21, 'widget', '\\CMS\\DefaultTheme\\Widget\\Blog\\Home'),
(21, 'metadescription', ''),
(21, 'metakeywords', NULL),
(24, 'applicationid', '1'),
(24, 'userid', '1'),
(24, 'metatitle', 'My first blog post!'),
(24, 'propertynodeid', '14'),
(24, 'visibleinmenu', '1'),
(24, 'slug', 'blog/funny/my-first-blog-post'),
(24, 'widget', '\\CMS\\DefaultTheme\\Widget\\Blog\\Post'),
(24, 'content', '<p><strong>\r\n	Hey, lets go with a joke!</strong></p><blockquote>\r\n	Wife: "I look fat. Can you give me a compliment?" <br>\r\n	Husband: "You have perfect eyesight."\r\n</blockquote>'),
(24, 'sidebarcontent', '<div:macro widget="%5CCMS%5CDefaultTheme%5CWidget%5CMacro%5CTextBox" data-id="2" data-data="%7B%22applicationid%22%3A%221%22%2C%22widget%22%3A%22%5C%5CCMS%5C%5CDefaultTheme%5C%5CWidget%5C%5CMacro%5C%5CTextBox%22%2C%22settitle%22%3A%22Specific+macro+content%22%2C%22setcontent%22%3A%22%3Cp%3EThis+macro+has+content+specific+for+this+post.%3C%5C%2Fp%3E%22%7D" style="display:block;background-color:#EEE;margin:10px;font-size:12px;font-weight:bold;padding:5px;border:3 px solid #CCC;text-align:center;">\r\n		Sidebar: textbox\r\n	</div:macro>'),
(24, 'footertext', ''),
(24, 'metadescription', ''),
(24, 'metakeywords', NULL),
(16, 'applicationid', '1'),
(16, 'userid', '1'),
(16, 'metatitle', 'Frontpage'),
(16, 'propertynodeid', '7'),
(16, 'visibleinmenu', '1'),
(16, 'slug', 'frontpage'),
(16, 'footertext', ''),
(16, 'widget', '\\CMS\\DefaultTheme\\Widget\\Frontpage'),
(16, 'metadescription', ''),
(16, 'metakeywords', ''),
(17, 'applicationid', '1'),
(17, 'userid', '1'),
(17, 'metatitle', 'Text page'),
(17, 'propertynodeid', '6'),
(17, 'visibleinmenu', '1'),
(17, 'slug', 'text-page'),
(17, 'subheader', 'Hello world!'),
(17, 'content', '<p>This is my very first text page - powered by PeceeCamp!</p>'),
(17, 'footertext', ''),
(17, 'widget', '\\CMS\\DefaultTheme\\Widget\\Textpage'),
(17, 'metadescription', ''),
(17, 'metakeywords', ''),
(19, 'applicationid', '1'),
(19, 'userid', '1'),
(19, 'metatitle', 'Blog'),
(19, 'propertynodeid', '8'),
(19, 'visibleinmenu', '1'),
(19, 'slug', 'blog'),
(19, 'sidebarcontent', '<div:macro widget="%5CCMS%5CDefaultTheme%5CWidget%5CMacro%5CTextBox" data-id="2" data-data="%7B%22applicationid%22%3A%221%22%2C%22widget%22%3A%22%5C%5CCMS%5C%5CDefaultTheme%5C%5CWidget%5C%5CMacro%5C%5CTextBox%22%2C%22settitle%22%3A%22Macro+example%22%2C%22setcontent%22%3A%22%3Cp%3EThis+macro-element+has+been+added+from+within+the+richtext+editor.%3C%5C%2Fp%3E%22%7D" style="display:block;background-color:#EEE;margin:10px;font-size:12px;font-weight:bold;padding:5px;border:3 px solid #CCC;text-align:center;">\r\n	Sidebar: textbox\r\n</div:macro>'),
(19, 'widget', '\\CMS\\DefaultTheme\\Widget\\Blog\\Home'),
(19, 'footertext', ''),
(19, 'metadescription', ''),
(19, 'metakeywords', NULL),
(29, 'applicationid', '1'),
(29, 'userid', '1'),
(29, 'metatitle', 'Search'),
(29, 'propertynodeid', '11'),
(29, 'visibleinmenu', '1'),
(29, 'slug', 'search'),
(29, 'footertext', '<p>Search and you shall find...</p>'),
(29, 'widget', '\\CMS\\DefaultTheme\\Widget\\Search'),
(29, 'metadescription', ''),
(29, 'metakeywords', ''),
(22, 'applicationid', '1'),
(22, 'userid', '1'),
(22, 'metatitle', 'Contact'),
(22, 'propertynodeid', '10'),
(22, 'visibleinmenu', '1'),
(22, 'slug', 'contact'),
(22, 'email', 'john.doe@example.com'),
(22, 'footertext', ''),
(22, 'widget', '\\CMS\\DefaultTheme\\Widget\\Contact'),
(22, 'metadescription', ''),
(22, 'metakeywords', ''),
(23, 'applicationid', '1'),
(23, 'userid', '1'),
(23, 'metatitle', '404: Page not found'),
(23, 'propertynodeid', '12'),
(23, 'visibleinmenu', ''),
(23, 'slug', '404-page-not-found'),
(23, 'footertext', '<p>Looks like a ninja stole the page...</p>'),
(23, 'widget', '\\CMS\\DefaultTheme\\Widget\\Error404'),
(23, 'metadescription', ''),
(23, 'metakeywords', NULL),
(1, 'applicationid', '1'),
(1, 'fileurl', '/file'),
(1, 'maxfilesize', '50'),
(1, 'filetypes', 'zip, jpg, jpeg, png, gif, bmp, pdf, doc, docx'),
(1, 'filestoragepath', '/storage/peceecamp'),
(1, 'createthumbs', '1'),
(1, 'thumbnails', 'a:3:{i:0;a:1:{s:6:"slider";a:2:{i:0;s:4:"1920";i:1;s:3:"500";}}i:1;a:1:{s:5:"small";a:2:{i:0;s:3:"300";i:1;s:3:"190";}}i:2;a:1:{s:5:"large";a:2:{i:0;s:3:"940";i:1;s:3:"520";}}}'),
(1, 'rootnodeid', '9'),
(1, '404nodeid', '23'),
(1, 'frontpagenodeid', '16'),
(1, 'disablehistory', '1'),
(1, 'sitedata', 'a:1:{s:13:"youTubeApiKey";s:39:"AIzaSyCo2MI8WQ1wFnHEYtfU8JjgMlWv74PEZ5g";}'),
(1, 'usinghostnames', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
