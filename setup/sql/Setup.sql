-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Vært: localhost
-- Genereringstid: 04. 01 2015 kl. 02:11:30
-- Serverversion: 5.5.38
-- PHP-version: 5.4.4-14+deb7u14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `PeceeCampTest`
--

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Application`
--

CREATE TABLE IF NOT EXISTS `Application` (
  `ApplicationID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` bigint(20) NOT NULL,
  `Plugin` varchar(255) NOT NULL,
  `Name` varchar(300) NOT NULL,
  `Host` varchar(355) DEFAULT NULL,
  `Slug` varchar(355) NOT NULL,
  `Theme` varchar(300) DEFAULT NULL,
  `Icon` varchar(255) DEFAULT NULL,
  `UseFromFrontend` tinyint(1) DEFAULT NULL,
  `PubDate` datetime NOT NULL,
  PRIMARY KEY (`ApplicationID`),
  KEY `UserID` (`UserID`),
  KEY `Plugin` (`Plugin`),
  KEY `Host` (`Host`),
  KEY `Slug` (`Slug`),
  KEY `PubDate` (`PubDate`),
  KEY `UseFromFrontend` (`UseFromFrontend`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `ApplicationText`
--

CREATE TABLE IF NOT EXISTS `ApplicationText` (
  `ApplicationTextID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ApplicationID` int(11) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Text` longtext,
  `Language` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`ApplicationTextID`),
  KEY `ApplicationID` (`ApplicationID`),
  KEY `Key` (`Key`),
  KEY `Language` (`Language`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `CMS_Language`
--

CREATE TABLE IF NOT EXISTS `CMS_Language` (
  `LanguageID` int(11) NOT NULL AUTO_INCREMENT,
  `ApplicationID` int(11) NOT NULL,
  `Name` varchar(300) NOT NULL,
  `Alias` varchar(255) NOT NULL,
  PRIMARY KEY (`LanguageID`),
  KEY `ApplicationID` (`ApplicationID`),
  KEY `Name` (`Name`),
  KEY `Alias` (`Alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `CMS_LanguageKey`
--

CREATE TABLE IF NOT EXISTS `CMS_LanguageKey` (
  `LanguageKeyID` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserID` bigint(20) NOT NULL,
  `ApplicationID` int(11) NOT NULL,
  `ParentLanguageKeyID` bigint(20) DEFAULT NULL,
  `Name` varchar(355) NOT NULL,
  `PubDate` datetime NOT NULL,
  `ChangedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`LanguageKeyID`),
  KEY `UserID` (`UserID`),
  KEY `ApplicationID` (`ApplicationID`),
  KEY `ParentLanguageKeyID` (`ParentLanguageKeyID`),
  KEY `Name` (`Name`),
  KEY `PubDate` (`PubDate`),
  KEY `ChangedDate` (`ChangedDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `CMS_LanguageText`
--

CREATE TABLE IF NOT EXISTS `CMS_LanguageText` (
  `LanguageTextID` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserID` bigint(20) NOT NULL,
  `LanguageID` int(11) NOT NULL,
  `LanguageKeyID` bigint(20) NOT NULL,
  `Text` longtext NOT NULL,
  `ChangedDate` datetime NOT NULL,
  PRIMARY KEY (`LanguageTextID`),
  KEY `UserID` (`UserID`),
  KEY `LanguageID` (`LanguageID`),
  KEY `LanguageKeyID` (`LanguageKeyID`),
  KEY `ChangedDate` (`ChangedDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `CMS_NodeHost`
--

CREATE TABLE IF NOT EXISTS `CMS_NodeHost` (
  `NodeHostID` int(11) NOT NULL AUTO_INCREMENT,
  `NodeID` bigint(20) NOT NULL,
  `LanguageID` int(11) NOT NULL,
  `Hostname` varchar(255) NOT NULL,
  PRIMARY KEY (`NodeHostID`),
  KEY `NodeID` (`NodeID`),
  KEY `LanguageID` (`LanguageID`),
  KEY `Hostname` (`Hostname`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `CMS_PropertyModule`
--

CREATE TABLE IF NOT EXISTS `CMS_PropertyModule` (
  `PropertyModuleID` int(11) NOT NULL AUTO_INCREMENT,
  `PropertyTabID` int(11) DEFAULT NULL,
  `NodeID` bigint(20) NOT NULL,
  `Name` varchar(300) NOT NULL,
  `Description` varchar(300) DEFAULT NULL,
  `ControlID` varchar(300) NOT NULL,
  `Required` tinyint(1) DEFAULT NULL,
  `Module` varchar(300) NOT NULL,
  `Data` longtext,
  `Order` int(11) NOT NULL DEFAULT '0',
  `Inheritable` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`PropertyModuleID`),
  KEY `PropertyTabID` (`PropertyTabID`),
  KEY `NodeID` (`NodeID`),
  KEY `Order` (`Order`),
  KEY `Inheritable` (`Inheritable`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `CMS_PropertyTab`
--

CREATE TABLE IF NOT EXISTS `CMS_PropertyTab` (
  `PropertyTabID` int(11) NOT NULL AUTO_INCREMENT,
  `NodeID` bigint(20) NOT NULL,
  `Name` varchar(300) NOT NULL,
  `Order` int(11) NOT NULL,
  `Inheritable` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`PropertyTabID`),
  KEY `NodeID` (`NodeID`),
  KEY `Order` (`Order`),
  KEY `Inheritable` (`Inheritable`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `CMS_Rewrite`
--

CREATE TABLE IF NOT EXISTS `CMS_Rewrite` (
  `RewriteID` int(11) NOT NULL AUTO_INCREMENT,
  `ApplicationID` int(11) NOT NULL,
  `OriginalUrl` varchar(600) NOT NULL,
  `RewriteUrl` varchar(600) NOT NULL,
  `NodeID` bigint(20) DEFAULT NULL,
  `Host` varchar(300) DEFAULT NULL,
  `Regex` tinyint(4) DEFAULT NULL,
  `Order` int(11) NOT NULL,
  PRIMARY KEY (`RewriteID`),
  KEY `ApplicationID` (`ApplicationID`),
  KEY `OriginalUrl` (`OriginalUrl`),
  KEY `RewriteUrl` (`RewriteUrl`),
  KEY `NodeID` (`NodeID`),
  KEY `Host` (`Host`),
  KEY `Regex` (`Regex`),
  KEY `Order` (`Order`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `Node`
--

CREATE TABLE IF NOT EXISTS `Node` (
  `NodeID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ParentNodeID` bigint(20) NOT NULL DEFAULT '0',
  `Path` varchar(500) DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `Title` varchar(500) DEFAULT NULL,
  `Content` longtext,
  `PubDate` datetime NOT NULL,
  `ChangedDate` datetime DEFAULT NULL,
  `ActiveFrom` datetime DEFAULT NULL,
  `ActiveTo` datetime DEFAULT NULL,
  `Active` tinyint(1) NOT NULL,
  `Level` bigint(20) NOT NULL DEFAULT '0',
  `Order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`NodeID`),
  KEY `ParentNodeID` (`ParentNodeID`),
  KEY `Path` (`Path`),
  KEY `Type` (`Type`),
  KEY `Title` (`Title`),
  KEY `PubDate` (`PubDate`),
  KEY `ChangedDate` (`ChangedDate`),
  KEY `ActiveFrom` (`ActiveFrom`),
  KEY `ActiveTo` (`ActiveTo`),
  KEY `Active` (`Active`),
  KEY `Level` (`Level`),
  KEY `Order` (`Order`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `NodeData`
--

CREATE TABLE IF NOT EXISTS `NodeData` (
  `NodeID` bigint(20) NOT NULL,
  `Key` varchar(300) NOT NULL,
  `Value` longtext,
  KEY `NodeID` (`NodeID`),
  KEY `Key` (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `UserID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Username` varchar(300) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `LastActivity` datetime DEFAULT NULL,
  `AdminLevel` tinyint(1) NOT NULL,
  `Deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`UserID`),
  KEY `Username` (`Username`),
  KEY `Password` (`Password`),
  KEY `AdminLevel` (`AdminLevel`),
  KEY `Deleted` (`Deleted`),
  KEY `LastActivity` (`LastActivity`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `UserData`
--

CREATE TABLE IF NOT EXISTS `UserData` (
  `UserID` bigint(20) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Value` text,
  KEY `UserID` (`UserID`),
  KEY `Key` (`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `UserIncorrectLogin`
--

CREATE TABLE IF NOT EXISTS `UserIncorrectLogin` (
  `Username` varchar(300) NOT NULL,
  `RequestTime` int(11) NOT NULL,
  `IPAddress` varchar(32) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '1',
  KEY `Username` (`Username`),
  KEY `RequestTime` (`RequestTime`),
  KEY `IPAddress` (`IPAddress`),
  KEY `Active` (`Active`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `UserPasswordReset`
--

CREATE TABLE IF NOT EXISTS `UserPasswordReset` (
  `UserID` bigint(20) NOT NULL,
  `Key` varchar(32) NOT NULL,
  `Date` int(11) NOT NULL,
  KEY `UserID` (`UserID`),
  KEY `Key` (`Key`),
  KEY `Date` (`Date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
