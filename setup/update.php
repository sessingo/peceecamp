<?php

/**
 * NOTE:
 * This file is a temporary file, which will execute stuff that
 * are nessesary in order to complete an update progress.
 * The file will be automaticlly deleted upon finishing the
 * update.
 */

$db = \Pecee\DB\DB::GetInstance();

// Remove unused tables if they exist
// $db->nonQuery('DROP TABLE `CMS_Macro`, `CMS_MacroProperty`;');

// Changes to the PropertyModule table
//$db->nonQuery('ALTER TABLE  `CMS_PropertyModule` CHANGE  `PropertyTabID`  `PropertyTabID` INT( 11 ) NULL');

// Perform changes to Applications table
//$db->nonQuery('ALTER TABLE `Application` DROP `FacebookAppID`, DROP `FacebookPageID`, DROP `FacebookSecret`;');
//$db->nonQuery('ALTER TABLE  `Application` ADD  `UseSlug` TINYINT( 1 ) NULL AFTER  `Icon`');
//$db->nonQuery('ALTER TABLE  `Application` ADD INDEX (  `UseSlug` )');

$db->nonQuery('DROP TABLE  `ApplicationText`');
$db->nonQuery('ALTER TABLE `Application` DROP `FacebookAppID`,DROP `FacebookPageID`,DROP `FacebookSecret`,ADD  `UseFromFrontend` TINYINT( 1 ) NULL AFTER  `Icon`;');

// Execute database modifications, if any
$db->runSql(dirname($_SERVER['DOCUMENT_ROOT']) . DIRECTORY_SEPARATOR . 'sql/Setup.sql');