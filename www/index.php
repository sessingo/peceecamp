<?php
use Pecee\Registry;
require_once '../config/init.php';

// Run the setup-wizard if no configuration is created
if(!file_exists('../config/config.php')) {
	echo new \PC\Widget\WidgetSetup();
	die();
} else {
	include 'config/config.php';
}

// Setup default authentication
$auth = \Pecee\Auth::GetInstance();
$auth->setAdminIP('127.0.0.1');

\Pecee\Debug::Instance()->add('Initializing framework');

// Enable gzip
if(!in_array('ob_gzhandler', ob_list_handlers())) {
	ob_start ("ob_gzhandler");
}

/*set_time_limit(400);*/

$router = \PC\Router::GetInstance();
try {
	$router->routeRequest();
}catch(\Pecee\Auth\AuthException $e) {
	\PC\Router::Redirect(\PC\Router::GetRoute(Registry::GetInstance()->get('AdminUrl','admin'), '', NULL, array('ref' => $_SERVER['REQUEST_URI'])));
}catch(\Exception $e) {
	$auth->setAuthLevel(NULL);
	$widget = new \PC\Widget\WidgetHome();
	$widget->setError($e->getMessage());
	echo $widget;
}