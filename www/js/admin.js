$.xhrPool = []; // array of uncompleted requests
$.xhrPool.abortAll = function() { // our abort function
	$(this).each(function(idx, jqXHR) { 
		jqXHR.abort();
	});
	$.xhrPool.length = 0;
};
 
$.ajaxSetup({
	beforeSend: function(jqXHR) { 
		// before jQuery send the request we will push it to our array
		$.xhrPool.push(jqXHR);
	},
	complete: function(jqXHR) { 
		// when some of the requests completed it will splice from the array
		var index = $.xhrPool.indexOf(jqXHR);
		if (index > -1) {
			$.xhrPool.splice(index, 1);
		}
	}
});
var admin={
	options: { defaultRoute: '', adminPath: 'admin' },
	init: function(options) {
		this.options = $.extend(this.options, options);
		$('table.dataTable.dynamic').dataTable( {
	        'bJQueryUI': true,
	        'bLengthChange': false,
	        'bFilter': false,
	        'bInfo': false
	    });
		
		$('.msg').live('click', function(e) {
			e.preventDefault();
			$(this).slideUp(240, function() {
				$(this).remove();
			});
		});
		
		$('a[rel*=new]').live('click', function(e) {
			e.preventDefault();
			window.open($(this).attr('href'));
		});
		
		$('textarea.js-grow').autosize();
		
		$('select').not('.disable-ddslick').ddslick({ imagePosition: 'left', width: 360 });
		
		this.setMenu();
		this.dialog.init();
		this.overlay.init();
	},
	setMenu: function() {
		var nav=$('.sidebar .nav');
		nav.find('li.active').find('ul').show();
		nav.find('ul li.active').parent().show();
		nav.find('> li').each(function() {
			var el=$(this);
			el.find('a:first').bind('click', function(e) {
				nav.find('li.active').removeClass('active');
				$(this).parent().addClass('active');
				if($(this).attr('href') == '#') {
					e.preventDefault();
				}
				var m=el.find('ul:first');
				if(m.length > 0) {
					e.preventDefault();
				}
				if(m.is(':visible')) {
					el.find('ul:first').slideUp(100);
				} else {
					nav.find('li ul').slideUp(100);
					el.find('ul:first').slideDown(100);
				}
			});
		});
	},
	dialog: {
		init: function() {
			/* Bind login buttons */
			$('a.js-dialog').live('click',function(e) {
				e.preventDefault();
				$.xhrPool.abortAll();
				if($(this).data('href')==null) {
					throw "Missing required data-parameter (href).";
				}
				parent.dialog.show({	
					url: $(this).data('href'),
					type: parent.dialog.dialogTypes.ajax,
					onLoad: function() {
						$('select').not('.disable-ddslick').ddslick({ imagePosition: "left" });
					}
				});
			});
			
			this.bindForm();
		},
		o: { 
			userCallback: null,
			alertCallback: null,
			confirmCallback: null 
		},
		alert: function(title, msg, fn) {
			this.o.alertCallback=fn;
			$.xhrPool.abortAll();
			dialog.show({ 	
				url: '/'+ admin.options.adminPath +'/dialog/view/alert',
				type: dialog.dialogTypes.ajax,
				onLoad: function(d) {
					$('.js-alert-title:last').text(title);
					$('.js-alert-msg:last').html(msg);
				}
			});
		},
		confirm: function(title, msg, fn) {
			this.o.confirmCallback=fn;
			$.xhrPool.abortAll();
			dialog.show({ 	
				url: '/'+ admin.options.adminPath +'/dialog/view/confirm/?title='+ encodeURIComponent(title) + '&msg=' + encodeURIComponent(msg),
				type: dialog.dialogTypes.ajax,
				onLoad: function(d) {
					$('.js-confirm-title:last').text(title);
					$('.js-confirm-msg:last').html(msg);
				}
			});
		},
		bindForm: function() {
			$('.screenoverlay:first .dialog:last form:first').live('submit', function(e) {
				e.preventDefault();
				var action=$(this).attr('action');
				var d=$('.screenoverlay:first .dialog:last');
				var c=d.find('.contents > .inside');
				if(action) {
					// Scroll to top
					var top = $(this).parent().offset().top - 100;
					if(top < $(window).scrollTop()) {
						$('html, body').animate({ scrollTop: top }, 150);
					}
					var obj = $(this);
					var settings = {};
					if(window.FormData != null) {
						/* ADD FILES TO PARAM AJAX */
						var formData = new FormData();
						$.each($(obj).find("input[type='file']"), function(i, tag) {
					        $.each($(tag)[0].files, function(i, file) {
					            formData.append(tag.name, file);
					        });
					    });
						
						var params = $(obj).serializeArray();
					    $.each(params, function (i, val) {
					        formData.append(val.name, val.value);
					    });
					    
					    settings = { processData: false, contentType: false };
					} else {
						formData = obj.serializeArray();
					}
					
					settings = $.extend(settings, { 
						url: action, 
						cache: false, 
						type: 'POST', 
						data: formData, 
						success: function(r) {
							d.removeClass('loading');
							c.html(r);
							$('.screenoverlay:first .dialog:last').uniform();
							$('select').not('disable-ddslick').ddslick({ imagePosition: 'left', width: 360 });
							dialog.center();
						} 
					});
					
					c.find('.inner').html('');
					d.addClass('loading');
					
					$.ajax(settings);
				}
			});

		}
	},
	overlay: {
		init: function() {
			$('a.js-overlay').live('mousemove', function(e) {
				var url=$(this).attr('href');
				var o=$('#js-overlay');
				
				if(o.length == 0) {
					o=$('<div id="js-overlay" style="display:none;z-index:999;"><img src="'+url+'" alt="" style="max-width:50%;" /></div>');
					$('body').append(o);	
				} 
				
				var show=function(el) {
					var posY=(e.screenY/$(window).height())*100;
					var posX=(e.pageX / $(window).width())*100;
					var left=(posX > 50) ? (e.pageX - o.width())-20 : e.pageX+20;
					var top=(posY>80) ? e.pageY-o.height() : e.pageY+20;
					el.css({ 'display': 'block','position': 'absolute', 'top': top, 'left': left });
				};
				
				if(!o.is(':visible')) {
					o.find('img').load(function() {
						show(o);
					});
				} else {
					show(o);
				}
			}).live('mouseleave', function() {
				$('div#js-overlay').remove();
			});
		}
	},
	utils: {
		popup: function(url, w, h) {
			var left = ($(window).width()/2)-(w/2);
			var top = ($(window).height()/2)-(h/2);
			return window.open(url, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
		},
		setFrameHeight: function(frame) {
			var f = $('#frame');
			f.css('height', f.contents().find('html').outerHeight());
		}
	}
};