/**
 * Pecee dialog
 * By: Simon Sessingø
 * @version 0.9.1
 * @author Pecee
 * @website http://www.pecee.dk
 */
var dialog = {
	dialogTypes: {
		ajax: 'ajax',
		html: 'html',
		iframe: 'iframe'
	},
	settings: { frameWidth: '100%', frameHeight: '100%' },
	overlay: {
		close: function(o) {
			var s=dialog.settings;
			var onclose=s.onClose;
			if(onclose!=null) {
				onclose(dialog);
			}
			o.fadeOut(s.fadeOutTimeout, function() {
				o.remove();
				var c=s.onClosed;
				if(c!=null) {
					c(dialog);
				}
			});
		},
		show: function(fn,settings) {
			var c=settings.classes;
			var o=$('<div class="'+c.overlay+'"/>');
			o.css('z-index', settings.defaultIndex+$('.'+c.overlay).length);
			$('body').prepend(o);
			o.fadeIn(settings.fadeInTimeout, function() {
				fn();
			});
		},
		center: function(settings) {
			 $('.'+settings.classes.overlay).css({ height: $(document).height(), width: $(document).width() });
		}
	},
	show: function(settings) {
		this.reset();
		this.settings = $.extend(this.settings, settings);
		if(this.settings.onStart != null) {
			this.settings.onStart(this);
		}
		this.start();
	},
	replace: function(settings) {
		var c=this.settings.classes;
		 $('.'+c.overlay).remove();
		 this.show(settings);
	},
	start: function() {
		$(document).live('keydown',function(e) {
			if(self.settings.keyboardEnabled) {
				var key = e.keyCode || e.which;
				if(key != null && key==27) {
					self.closeActive();
				}
			}
		});
		var self=this;
		
		$.when(self.getContent()).then(function(content){
			self.overlay.show(function() {
				var s=self.settings;
				var c=s.classes;
				var container=$('<div class="'+c.container+'"/>');
				var ctn=$('<div class="'+c.contents+'"/>');
				
				var d=$('<div class="'+c.dialog+'"/>');
				/*d.append('</div>');*/
				d.css('z-index', $('.'+c.overlay).length*s.defaultIndex);
				$('.'+c.overlay+':first').prepend(d);
				if(s.title != null) {
					var head=$('<div class="'+c.header+'">').html('<h3>'+s.title+'</h3>');
					if(s.showClose) {
						head.append('<a href="javascript:dialog.closeActive();" class="'+c.close+'">'+s.text.close+'</a>');
					}
					ctn.append(head);
				}
				
				ctn.append('<div class="'+c.inner+'">'+content+'</div>');
				container.append(ctn);
				d.append(container);
				
				$(window).resize(function() {
					self.center();
				});
				if(s.keepCentered) {
					$(window).bind('scroll', function() {
						self.center();
					});
				}
				self.center();
				var load=s.onLoad;
				if(load!=null) {
					load(d, self);
				}
				d.fadeIn(s.fadeInTimeout);
			}, self.settings);
		});
	},
	reset: function() {
		this.settings={
			async:true,
			keyboardEnabled: true,
			showClose: true,
			keepCentered: false,
			fadeInTimeout: 200,
			fadeOutTimeout: 110,
			type: 'ajax',
			html: null,
			url: '',
			title: null,
			text: {
				close: ''
			},
			onStart: null,
			onLoad: null,
			onClose: null,
			onClosed: null,
			onCenter: null,
			defaultIndex: 50,
			classes: {
				overlay: 'screenoverlay',
				dialog: 'dialog',
				container: 'container',
				header: 'head',
				contents: 'contents',
				inner: 'inside',
				close: 'close'
			},
			ajaxSettings: {  }
		};
	},
	closeActive: function() {
		var c=this.settings.classes;
		if(c != null) {
			this.overlay.close($('.'+c.overlay+':first'));
		}
		this.reset();
	},
	closeAll: function() {
		var c=this.settings.classes;
		if(c != null) {
			this.overlay.close($('.'+c.overlay));
			$('.'+c.dialog).remove();
		}
		this.reset();
	},
	center: function() {
		var s=this.settings;
		var f = s.onCenter;
		var doCenter=true;
		var c=s.classes;
		var e = $('.'+c.container);
		if(f != null) {
			if(!f(e, this)) {
				doCenter=false;
			}
		}
		
		if(doCenter) {
			if(s.keepCentered) {
				e.css({ 'position': 'fixed', 'left': ($(window).width()-e.outerWidth())/2+$(window).scrollLeft() });
			} else {
				e.css({ 'position': 'absolute', 'left': ($(window).width()-e.outerWidth())/2+$(window).scrollLeft(), 'top': $(window).scrollTop() });
			} 
			this.overlay.center(s);
		}
	},
	getContent: function() {
		var self=this;
		var s=self.settings;
		var t=self.dialogTypes;
		
		if(s.html != null) {
			return s.html;
		}
		
		switch(s.type) {
			default:
			case t.ajax: {
				var o = '';
				
				var data = $.extend({
					url: s.url, 
					async: s.async,
					success: function(r) {
						o=r;
					}, 
					error: function(r,t,e) {
						o=e;
					}
				},s.ajaxSettings);
				
				return $.Deferred(function(dfd){
					$.ajax(data).done(function() {
						dfd.resolve(o);
					});
				}).promise();
				break;
			}
			case t.iframe: {
				return '<iframe src="'+s.url+'" style="width:'+s.frameWidth+';height:'+s.frameHeight+';" scrolling="no" frameborder="0" allowTransparency="true"></iframe>';
				break;
			}
		}
	}
};