if (typeof RedactorPlugins === 'undefined') var RedactorPlugins = {};

RedactorPlugins.macro = {
	options: { timer: null },
	init: function() {
		this.buttonAdd('macro', 'Macro', this.insertMacro);
		this.buttonAddSeparatorBefore('macro');
		
		//Add icon to button
        $('a.redactor_btn_macro').css({
        	backgroundImage: 'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAADImlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo3REMyRDVGNkZGMjExMUUzQkEwN0MwMUY2ODg0NDFGMCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo3REMyRDVGN0ZGMjExMUUzQkEwN0MwMUY2ODg0NDFGMCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjdEQzJENUY0RkYyMTExRTNCQTA3QzAxRjY4ODQ0MUYwIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjdEQzJENUY1RkYyMTExRTNCQTA3QzAxRjY4ODQ0MUYwIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+8v5jJgAAAi5JREFUOE+Nk0+IEnEUx/c3Oo5sKQ0ibBG7EZHlIbbYDhVlUTcPpuOggWBQEHQo6FYEEnTp2C28BJEd1LJuG8QSEQS1ZGDBEgTNYStWQdBVWUfHPr9ho6sPHt/3vu/P7/3eb0bMTCcilUpFRqPRus/nC47HY7tWq21MVZxMJnd7PJ5VzLdoVAhh6bpuetzotuRyuR2RSOReNBo9Fw6H31mW5RQKBaXVah0mfAldQ3eineFw+Mk92TCMo3ReZKwfiqIsQwnwjOM4x7DrnPQG/DMYDE76/f4l/JeTyeSbSCQSu1RV/UxwHu0RCIBSNkmYxa/T5DL4Gk6OPo/d4LDbCgvoQtQg5BWG6F2K7oMTFFo8oXkbVPE3QId4KBgMNr10vcGIFwjMQF6rVqsvpG2a5ncSH8MZbHcL+3wgELC63a5Os36xWLS9kDfJXSBpyJ1XZKEUmtZpKieJEQtz36flcrmPLyd1hbhyneAKTbiB7yKcu0T4U/Aa5ivwjqZpi/F4XM9kMgez2ewemSNisZiXZ3lE8RWSNsHKdpEhG2I/AB/iN7A/ggfQtWazaciFaQQPyULIdVRuVr7pT7SPvUTsLLqF/wX/K6qGQqHT7ohyDJayn3t2GPeD5MAjcAss55dt2/KJfmOfwP7/zjLxn+TzeX+v1zOlTcKzSqUylmY6nd4HvodrgHvBVQ66NdW3zbPNcdIy+pyJjkO1+bavTlWMCH6OOT6MNpPNsvlRqVTq/AXNI/pSZAakgQAAAABJRU5ErkJggg==)',
        	backgroundPosition: '50% 50%'
        });
        
        var self = this;
        $(document).bind('insertMacro', function (e) {
        	self.selectionRestore();
			self.insertHtmlAdvanced(e.html, false);
			self.sync();
			e.window.close();
        });
        
        this.options.timer = setTimeout($.proxy(this.observeMacros, this), 1000);
        this.$editor.find('div:macro').on('click', $.proxy(this.observer, this));
    },
    insertMacro: function() {
    	this.selectionSave();
    	dialog.show({	
			url: admin.options.defaultRoute + 'dialog/view/macro/editoradd',
			type: dialog.dialogTypes.ajax,
			onLoad: function() {
				$('select').ddslick({ imagePosition: "left" });
			}
		});
    },
    observeMacros: function() {
    	var self = this;
    	var block = this.getBlock();
    	if(block.nodeName == 'DIV:MACRO' && !$('.redactor-link-tooltip').is(':visible')) {
    		var macro = $(block);
    		
			var pos = macro.offset();
			if (this.opts.iframe)
			{
				var posFrame = this.$frame.offset();
				pos.top = posFrame.top + (pos.top - $(this.document).scrollTop());
				pos.left += posFrame.left;
			}

			var tooltip = $('<span class="redactor-link-tooltip" />');

			var remove = $('<a href="javascript:;">Remove</a>').on('click', $.proxy(function(e) {
				macro.remove();
				tooltip.remove();
			}, this));

			tooltip.append('<a href="javascript:;" class="js-macro-edit">Edit</a>');
			tooltip.append(' | ');
			tooltip.append(remove);
			tooltip.css({
				top: (pos.top + 20) + 'px',
				left: pos.left + (macro.outerWidth()/2) + 'px'
			});

			$('.redactor-link-tooltip').remove();
			$('body').append(tooltip);
			
			$('.js-macro-edit').on('click', function(e){
				e.preventDefault();
				clearTimeout(self.options.timer);
				tooltip.remove();
				
				//this.selectionSave();
				
				var data = jQuery.parseJSON(decodeURIComponent(macro.data('data').replace(/\+/g, ' ')));

				var test = $.extend(data, {
					nodeId: macro.data('id'),
					postback: 1,
					update: true
				});
				
				$(document).unbind().bind('editMacro', function(e) {
					macro.remove();
					tooltip.remove();
	    			self.selectionRestore();
	    			self.insertHtmlAdvanced(e.html, false);
	    			self.sync();
		        });
				
				dialog.show({	
					url: admin.options.defaultRoute + 'dialog/view/macro/editoradd',
					type: dialog.dialogTypes.ajax,
					onLoad: function() {
						$('select').ddslick({ imagePosition: "left" });
						self.options.timer = setTimeout($.proxy(self.observeMacros, self),4);
						admin.dialog.bindForm();
						
						$('.js-editor').redactor({
							toolbarFixedBox: false,
							minHeight: 100,
							maxHeight: 300,
							focus: true,
							autoresize: false,
							imageUpload: admin.options.imageUploadUrl,
							fileUpload: admin.options.fileUploadUrl,
							imageGetJson: admin.options.imageGetJsonUrl
						});
						
					},
					ajaxSettings: {
						type: 'post',
						data: test
					}
				});
				
				
			});
			
			
    	} 
    	this.options.timer = setTimeout($.proxy(this.observeMacros, this),4);
	}
};