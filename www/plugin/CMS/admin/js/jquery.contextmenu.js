/**
 * Pecee jQuery context-menu replacement
 * By: Simon Sessingø
 * @version 0.1
 * @author Pecee
 * @website http://www.pecee.dk
 */
jQuery.fn.contextmenu = function (elements) {
	var menu = $('<ul class="js-context-menu context-menu" style="display:none;"/>'), activeElement=null;
	$.each(elements, function(index) {
		var el = $('<li/>'), url = (this.url != null) ? this.url : 'javascript:;';
		
		if(this.className != null) {
			el.addClass(this.className);
		}
		
		el.append('<a href="'+ url +'">'+ ((this.image != null) ? '<img src="'+this.image+'" alt=""/> ' : '') +' '+ this.name +'</a>');
		
		if(this.linkClass != null) {
			el.find('a').addClass(this.linkClass);
		}
		
		var self = this;
		if(this.callback != null) {
			el.bind('click',function(e) {
				e.preventDefault();
				self.callback(activeElement, el, url);
			});
		}
		
		menu.append(el);
		
	});
	
	$(document).bind('click',function(e) {
		menu.hide();
	});
	
	$(this).live('contextmenu', function(e) {
		e.preventDefault();
		activeElement = $(this);
		$('.js-context-menu').hide();
		menu.css({ position: 'fixed', top: (e.pageY-$(document).scrollTop())+'px', left: (e.pageX+5)+'px', 'z-index': '10' }).show();
	});
	
	$(menu).disableSelection();
	$('body').append(menu);
	return menu;
};