var tree = function(el,options){
	return this.init(el,options);
};

tree.prototype = {
	responses: [],
	request: null,
	el: null,
	options: { 
		sourceUrl: null, 
		editUrl: 'javascript:;', 
		callback: null, 
		onSelectPage: null,
		cookieName: 'ActiveNodeIds',
		setCookie: true
	},
	init: function(el, options) {
		this.el = el;
		this.reset();
		this.options = $.extend(this.options,options);
		var self = this;
		$.cookie.raw = true;
		var el = this.el;
		el.find('.hitarea').die('click').live('click', function(e) {
			e.preventDefault();
			if(self.options.sourceUrl == null) {
				self.loadNode($(this).next());
			} else {
				var nodeId = $(this).next().data('id');
				self.loadNode(self.el.find('a[data-id="'+nodeId+'"]'));
			}
		});
		
		el.find('a').die('click').live('click', function(e) {
			var c = self.options.onSelectPage;
			if(c != null) {
				c(e, $(this).data('id'), $(this).text(), $(this), self);
			}
		});
		return this;
	},
	loadNodeId: function(nodeId) {
		this.loadNode(this.el.find('a[data-id="'+nodeId+'"]'));
	},
	loadNode: function(el) {
		var self = this;
		var $this = el.prev();
		var hitarea = $this.parents('li:first').find('.hitarea:first');
		var nodeId = el.data('id');
		
		if(hitarea.hasClass('collapsed')) {
			hitarea.removeClass('collapsed');
			hitarea.addClass('expanded');
			$this.parents('li:first').find('ol:first').show();
			
			if(self.options.setCookie) {
				var cookieVal = $.cookie(self.options.cookieName);
				cookieVal = (cookieVal == null) ? new Array() : cookieVal.split(',');
				var hasItem = false;
				for(var i=0;i<cookieVal.length;i++) {
					if(String(cookieVal[i]) == String(nodeId)) {
						hasItem=true;
					}
				}
				
				if(!hasItem) {
					cookieVal.push(nodeId);
					$.cookie(self.options.cookieName, cookieVal, { expires: 7, path: '/' });
				}
			}
			
			if(self.options.sourceUrl && self.responses[nodeId] == null && $this.parents('li:first').find('ol:first').length == 0) {
				$('#loader').show();
				if(self.request != null) {
					self.request.abort();
				}
				self.request = $.getJSON(self.options.sourceUrl+'?nodeId=' + nodeId, function(r) {
					self.responses[nodeId] = r;
					self.request = null;
					
					if(r.nodes.length > 0) {	
						var p = $('<ol/>');
						for(var i=0;i<r.nodes.length;i++) {
							var page = r.nodes[i];
							var liAttrs = (page.liAttrs != null) ? page.liAttrs : {};
							var linkAttrs = (page.linkAttrs != null) ? page.linkAttrs : {};
							var html = $('<li id="node_'+page.id+'"><div><div class="hitarea collapsed"></div><a href="'+ self.options.editUrl + page.id +'"><img src="'+self.options.imageUrl + page.icon+'" alt="" />'+page.title+'</a></div></li>');
							html.attr(liAttrs);
							html.find('a').attr(linkAttrs);
							p.append(html);
						}
						el.parent().parent().append(p);
					} else {
						el.parents('li:first').find('.hitarea').remove();
					}
					$('#loader').hide();
					var c = self.options.callback;
					if(c != null) {
						c(self,p);
					}
				});
			}
			
		} else {
			hitarea.addClass('collapsed');
			hitarea.removeClass('expanded');
			$this.parents('li:first').find('ol:first').hide();
			
			if(self.options.setCookie) {
				var cookieVal = $.cookie(self.options.cookieName);
				if(cookieVal != null) {
					cookieVal = cookieVal.split(',');
					var newCookie = [];
					for(var i=0;i<cookieVal.length;i++) {
						if(String(cookieVal[i]) != String(nodeId) && cookieVal[i] != '') {
							newCookie.push(cookieVal[i]);
						}
					}
					$.cookie(self.options.cookieName, newCookie, { expires: 7, path: '/' });
				}
			}
		}
	},
	onSelectPage: function(fn) {
		this.options.onSelectPage = fn;
	},
	onCallback: function(fn) {
		this.options.callback = fn;
	},
	refreshNode: function(nodeId) {
		if(this.responses[nodeId] != null) {
			this.responses.splice(nodeId, 1);
		}
		var link = this.el.find('a[data-id="'+nodeId+'"]');
		if(link.length > -1) {
			var li = link.parents('li:first');
			li.find('.hitarea:first').removeClass('expanded').addClass('collapsed');
			li.find('ol').remove();
		}
		this.loadNodeId(nodeId);
	},
	reset: function() {
		this.options = { 
			sourceUrl: null, 
			editUrl: 'javascript:;', 
			callback: null, 
			onSelectPage: null,
			cookieName: 'ActiveNodeIds',
			setCookie: true
		};
		this.responses = [];
		this.request = null;
		if(this.options.setCookie) {
			$.removeCookie(this.options.cookieName);
		}
	}
};

jQuery.fn.tree = function(options) {
	if(this.length > 1) {
		$.each(this, function() {
			return new tree($(this), options);
		});
	} else {
		return new tree($(this),options);
	}
};