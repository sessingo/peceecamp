function fileUpload(o) { 
	return this.construct(o);	
}
fileUpload.prototype={
	settings: { container: null, browseBtn: null, MaxFileSizeMB: 10, UploadUrl: '/upload', filters : null},
	construct: function(settings) {
		$.extend(this.settings, settings);
		var s=this.settings;
		return new plupload.Uploader({
			runtimes : 'flash,html5,html4',
			browse_button : s.browseBtn,
			container: s.container,
			max_file_size : s.MaxFileSizeMB+'mb',
			url : s.UploadUrl,
			flash_swf_url : '/plugin/CMS/admin/data/plupload/js/plupload.flash.swf',
			filters: s.filters
		});
	}
};