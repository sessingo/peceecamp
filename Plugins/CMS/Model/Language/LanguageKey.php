<?php
namespace CMS\Model\Language;
class LanguageKey extends \Pecee\Model\Model {
	protected $texts;
	public function __construct() {
		parent::__construct('CMS_LanguageKey', array('LanguageKeyID' => '', 'UserID' => '', 'ApplicationID' => '', 'ParentLanguageKeyID' => 0, 'Name' => '', 'PubDate' => \Pecee\Date::ToDateTime(), 'ChangedDate' => NULL));
		$this->ApplicationID = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
		$this->texts = array();
	}

	public function delete() {
		/* @var $languagetext \CMS\Model\Language\LanguageText */
		$languagetext = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Language\LanguageText');

		$texts = $languagetext::GetInstance()->get();
		if($texts->hasRows()) {
			foreach($texts->getRows() as $text) {
				if($text->getLanguageKeyID() == $this->LanguageKeyID) {
					$text->delete();
				}
			}
		}
		parent::delete();
	}

	public function getText($lang) {
		$texts = \CMS\Model\Language\LanguageText::GetInstance()->get()->getArray();
		return (isset($texts[$this->getFullKey()][$lang])) ? $texts[$this->getFullKey()][$lang] : NULL;
	}

	public function getFullKey() {
		$out = array($this->getName());
		$el = $this;
		while($el && $el->hasRow() && $el->getParentLanguageKeyID()) {
			$el = self::GetByID($el->getParentLanguageKeyID());
			if($el->hasRow()) {
				$out[] = $el->getName();
			}
		}
		$out = array_reverse($out);
		return join('/',$out);
	}

	protected function findKeys($keys, $languageKeyId = NULL, $index = '') {
		$out = array();
		if($keys->hasRows()) {
			foreach($keys->getRows() as $key) {
				if($key->getParentLanguageKeyID() == $languageKeyId) {
					$out[$index . '/' . $key->getName()] = $key;
					$new = $this->findKeys($keys, $key->getLanguageKeyID(), $index . '/' . $key->getName());
					if(count($new) > 0) {
						foreach($new as $key=>$value) {
							$out[$key] = $value;
						}
					}
				}
			}
		}
		return $out;
	}

	public function getArray() {
		$self = $this;

		$out = array();
		if($self->hasRows()) {
			foreach($self->getRows() as $key) {
				if(!$key->getParentLanguageKeyID()) {
					$out[$key->getName()] = $key;
					$new = $this->findKeys($self,$key->getLanguageKeyID(), $key->getName());
					if(count($new) > 0) {
						foreach($new as $key=>$value) {
							$out[$key] = $value;
						}
					}

				}
			}
		}
		return $out;
	}

	public static function GetByKey($key, $parentLanguageKeyId = NULL) {
		$applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
		$where = array(\Pecee\DB\DB::FormatQuery('`ApplicationID` = %s', array($applicationId)));
		$where[] = \Pecee\DB\DB::FormatQuery('`Name` = %s', array($key));
		if(!is_null($parentLanguageKeyId)) {
			$where[] = \Pecee\DB\DB::FormatQuery('`ParentLanguageKeyID` = %s', array($parentLanguageKeyId));
		}
		return self::FetchOne('SELECT * FROM `CMS_LanguageKey` WHERE ' . join(' && ', $where));
	}

	public static function Get($parentLanguageKeyId = NULL) {
		$applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
		$where = array(\Pecee\DB\DB::FormatQuery('`ApplicationID` = %s', array($applicationId)));
		if(!is_null($parentLanguageKeyId)) {
			$where[] = \Pecee\DB\DB::FormatQuery('`ParentLanguageKeyID` = %s', array($parentLanguageKeyId));
		}
		return self::FetchAll('SELECT * FROM `CMS_LanguageKey` WHERE ' . join(' && ', $where));
	}

	public static function GetByID($languageKeyId) {
		return self::FetchOne('SELECT * FROM `CMS_LanguageKey` WHERE `LanguageKeyId` = %s', $languageKeyId);
	}
}