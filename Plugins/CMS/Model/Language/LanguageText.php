<?php
namespace CMS\Model\Language;
class LanguageText extends \Pecee\Model\Model {

	protected static $instance;

	public static function GetInstance() {
		if(is_null(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	protected $texts;

	public function __construct() {
		parent::__construct('CMS_LanguageText', array('LanguageTextID' => '', 'LanguageID' => '', 'LanguageKeyID' => '', 'UserID' => '', 'Text' => '', 'ChangedDate' => \Pecee\Date::ToDateTime()));
	}

	public function getArray() {
		if(!$this->texts) {
			$this->texts = array();

			/* @var $languagekey \CMS\Model\Language\LanguageKey */
			$languagekey = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Language\LanguageKey');

			$keys = $languagekey::Get()->getArray();
			if(count($keys) > 0) {
				foreach($keys as $index=>$key) {
					$text = array();
					if($this->hasRows()) {
						foreach($this->getRows() as $row) {
							if($row->getLanguageKeyID() == $key->getLanguageKeyID()) {
								$text[$row->getLanguageAlias()] = $row->getText();
							}
						}
					}
					$this->texts[$index] = $text;
				}
			}
		}
		return $this->texts;
	}

	public function getByLanguageId($languageId) {
		return self::FetchAll('SELECT lt.*, lk.`Name` as `Key` FROM `CMS_LanguageText` lt JOIN `CMS_LanguageKey` lk ON(lk.`LanguageKeyID` = lt.`LanguageKeyID`) WHERE lt.`LanguageID` = %s', array($languageId));
	}

	public function getByKey($languageId, $key) {
		return self::FetchOne('SELECT * FROM `CMS_LanguageText` WHERE `LanguageID` = %s && `LanguageKeyID` = %s', array($languageId, $key));
	}

	public function get() {
		$applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
		return self::FetchAll('SELECT t.*, l.`Alias` AS `LanguageAlias` FROM `CMS_LanguageText` t JOIN `CMS_Language` l ON(l.`LanguageID` = t.`LanguageID`) WHERE l.`ApplicationID` = %s', array($applicationId));
	}
}