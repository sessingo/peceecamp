<?php
namespace CMS\Model;
class ModelMacro extends \Pecee\Model\Node\ModelNode {
	const TYPE_MACRO = 'macro';
	protected $modules;
	public function __construct() {
		parent::__construct();
		$this->data->applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
	}

	public function delete() {
		$modules = $this->getModules();
		if($modules->hasRows()) {
			foreach($modules->getRows() as $module) {
				$module->delete();
			}
		}
		parent::delete();
	}

	public function getModules() {
		if(!$this->modules) {
			/* @var $propertymodule \CMS\Model\Property\PropertyModule */
			$propertymodule = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyModule');

			$this->modules = $propertymodule::GetByNodeId($this->NodeID);
		}
		return $this->modules;
	}

	public function setWidget($widget) {
		$this->data->widget = $widget;
	}

	public function getWidget() {
		return $this->data->widget;
	}

	public function save() {
		$this->Type = self::TYPE_MACRO;
		parent::save();
	}

	public static function Get($query=NULL, $active=NULL, $parentNodeId=NULL, $path=NULL, $order=NULL, $rows=NULL, $page=NULL) {
		$order = (is_null($order)) ? self::ORDER_TITLE_ASC : $order;
		return parent::Get(self::TYPE_MACRO, $query, $active, $parentNodeId, $path, $order, $rows, $page);
	}

	public static function GetById($id) {
		return parent::GetByNodeID($id);
	}

}