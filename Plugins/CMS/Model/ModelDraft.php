<?php
namespace CMS\Model;
use Pecee\Boolean;

class ModelDraft extends \Pecee\Model\Node\ModelNode {
	const TYPE_DRAFT = 'draft';
	protected $property;
	protected $parents;
	protected $page;
	public function __construct() {
		parent::__construct();
		$this->parent = array();
		$this->data->applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
	}

	public function getPubDate() {
		return ($this->ChangedDate) ? $this->ChangedDate : $this->PubDate;
	}

	public function setUserID($userId) {
		$this->data->UserID = $userId;
	}

	public function getUserID() {
		return $this->data->UserID;
	}

	public function getNodeID($draftId=FALSE) {
		if($draftId) {
			return $this->NodeID;
		}
		return $this->getPage()->getNodeID();
	}

	public function getParent() {
		return $this->getPage()->getParent();
	}

	public function getProperty() {
		return $this->getPage()->getProperty();
	}

	public function getChilds($active=NULL, $order=NULL, $rows=NULL,$page=NULL) {
		return $this->getPage()->getChilds($active,$order,$rows,$page);
	}

	public function getPage() {
		if(!$this->page) {

			/* @var $modelpage \CMS\Model\ModelPage */
			$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

			$this->page = $modelpage::GetByNodeID($this->getOriginalNodeID());
		}
		return $this->page;
	}

	public function getData($name, $language=NULL) {
		if(!is_null($language)) {
			$name .= ':' . $language;
		}
		return $this->data->$name;
	}

	public function setOriginalNodeId($nodeId) {
		$this->data->originalNodeId = $nodeId;
	}

	public function getOriginalNodeID() {
		return $this->data->originalNodeId;
	}

	public function setWidget($widget) {
		$this->data->widget = $widget;
	}

	public function getWidget() {
		return $this->data->widget;
	}

	public function setPropertyNodeId($propertyNodeId) {
		$this->data->propertyNodeId = $propertyNodeId;
	}

	public function getPropertyNodeId() {
		return $this->data->propertyNodeId;
	}

	public function getVisibleInMenu() {
		return $this->data->visibleInMenu;
	}

	public function setVisibleInMenu($visible) {
		$this->data->visibleInMenu = $visible;
	}

	public function getMetaTitle() {
		return $this->data->metaTitle;
	}

	public function setMetaTitle($title) {
		$this->data->metaTitle = $title;
	}

	public function getMetaDescription() {
		return $this->data->metaDescription;
	}

	public function setMetaDescription($description) {
		$this->data->metaDescription = $description;
	}

	public function getMetaKeywords() {
		return $this->data->metaKeywords;
	}

	public function setMetaKeywords($keywords) {
		$this->data->metaKeywords = $keywords;
	}

	public function setSlug($slug) {
		$this->data->slug = $slug;
	}

	public function getStartpage() {
		return $this->data->startpage;
	}

	public function setStartpage($bool) {
		$this->data->startpage = $bool;
	}

	public function save() {
		$this->Type = self::TYPE_DRAFT;
		parent::save();
	}

	public static function GetByPageID($pageId, $active=NULL, $parentNodeId=NULL, $order=NULL, $rows=NULL, $page=NULL) {
		$where=array('1=1');
		$where[] = sprintf('(n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
					\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());
		$where[] = \Pecee\DB\DB::FormatQuery('(nd.`Key` = \'originalnodeid\' && nd.`Value` = \'%s\')', array($pageId));
		$where[] =  'n.`Type` = \''.self::TYPE_DRAFT.'\'';
		if(!is_null($active)) {
			$where[] = \Pecee\DB\DB::FormatQuery('n.`Active` = %s', array(Boolean::Parse($active)));
			$where[] = \Pecee\DB\DB::FormatQuery('(ISNULL(n.`ActiveFrom`) && ISNULL(n.`ActiveTo`) || n.`ActiveFrom` <= NOW() && (n.`ActiveTo` >= NOW() || ISNULL(n.`ActiveTo`)))');
		}
		if(!is_null($parentNodeId)) {
			$where[] = "n.`ParentNodeID` = '".\Pecee\DB\DB::Escape($parentNodeId)."'";
		}
		$order=(!is_null($order) && in_array($order, self::$orders)) ? $order : self::ORDER_CHANGED_DESC;
		return self::FetchPage('n.NodeID', 'SELECT n.* FROM `Node` n JOIN `NodeData` nd ON(nd.`NodeID` = n.`NodeID`) WHERE ' . join(' && ', $where) . ' ORDER BY ' . $order, $rows, $page);
	}
}