<?php
namespace CMS\Model;
class ModelLanguage extends \Pecee\Model\Model {

	public function __construct() {
		parent::__construct('CMS_Language', array('LanguageID' => '', 'ApplicationID' => '',
													'Name' => '', 'Alias' => ''));
		$this->ApplicationID = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
	}

	public function delete() {
		/* @var $languagetext \CMS\Model\Language\LanguageText */
		$languagetext = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Language\LanguageText');

		$text = $languagetext::GetInstance()->getByLanguageId($this->LanguageID);
		if($text->hasRows()) {
			foreach($text->getRows() as $text) {
				$text->delete();
			}
		}
		parent::delete();
	}

	public static function Get() {
		$applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
		return self::FetchAll('SELECT * FROM `CMS_Language` WHERE `ApplicationID` = %s ORDER BY `Name` ASC', $applicationId);
	}

	public static function GetByAlias($alias) {
		$applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
		return self::FetchOne('SELECT * FROM `CMS_Language` WHERE `ApplicationID` = %s && `Alias` = %s', $applicationId, $alias);
	}

	public static function GetById($id) {
		return self::FetchOne('SELECT * FROM `CMS_Language` WHERE `LanguageID` = %s', $id);
	}

}