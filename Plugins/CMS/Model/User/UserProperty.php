<?php
namespace CMS\Model\User;
use Pecee\Boolean;

class UserProperty extends \Pecee\Model\Node\ModelNode {
	const TYPE_PROPERTY = 'property-user';

	protected $modules;
	protected $tabs;
	public function __construct() {
		parent::__construct();
		$this->modules = array();
		$this->data->applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
	}

	public function getParents() {
		$out = array();
		$node = self::GetByNodeID($this->ParentNodeID);
		while($node && $node->hasRow()) {
			$out[] = $node;
			$node = self::GetByNodeID($node->ParentNodeID);
		}
		return $out;
	}

	public function setProperties($properties) {
		if(is_array($properties)) {
			$this->data->properties = join(',', $properties);
		}
	}

	public function getProperties() {
		if($this->data->properties) {
			return explode(',', $this->data->properties);
		}
		return NULL;
	}

	public function getIcon() {
		return $this->data->icon;
	}

	public function setIcon($icon) {
		$this->data->icon = $icon;
	}

	public function getAlias() {
		return $this->data->alias;
	}

	public function setAlias($alias) {
		$this->data->alias = trim(str_ireplace(array(' '), '', $alias));
	}

	public function save() {
		$this->Type = self::TYPE_PROPERTY;
		parent::save();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');
		$cacheclass::GetInstance()->clear();
	}

	public function update() {
		parent::update();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');
		$cacheclass::GetInstance()->clear();
	}

	public function delete() {
		parent::delete();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	public function getTabs() {
		if(!$this->tabs) {
			if($this->hasRow()) {

				/* @var $propertytab \CMS\Model\Property\PropertyTab */
				$propertytab = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyTab');

				$tabs = $propertytab::GetByNodeId($this->NodeID);
				if($tabs->hasRows()) {
					foreach($tabs->getRows() as $tab) {
						$this->tabs[] = $tab;
					}
				}

				$parent = self::GetByNodeID($this->ParentNodeID);
				while($parent && $parent->hasRow()) {
					$tabs = $propertytab::GetByNodeId($parent->NodeID);
					if($tabs->hasRows()) {
						foreach($tabs->getRows() as $tab) {
							if($tab->getInheritable()) {
								$this->tabs[] = $tab;
							}
						}
					}
					$parent = self::GetByNodeID($parent->ParentNodeID);
				}
			}
		}
		return $this->tabs;
	}

	public function getModules($propertyTabId=NULL) {
		if(!isset($this->modules[$propertyTabId])) {

			/* @var $propertymodule \CMS\Model\Property\PropertyModule */
			$propertymodule = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyModule');

			$parent = self::GetByNodeID($this->ParentNodeID);
			while($parent && $parent->hasRow()) {
				$modules = $propertymodule::GetByNodeId($parent->NodeID,$propertyTabId);
				if($modules->hasRows()) {
					foreach($modules->getRows() as $module) {
						$this->modules[$propertyTabId][] = $module;
					}
				}
				$parent = self::GetByNodeID($parent->ParentNodeID);
			}

			$modules = $propertymodule::GetByNodeId($this->NodeID,$propertyTabId);
			if($modules->hasRows()) {
				foreach($modules->getRows() as $module) {
					$this->modules[$propertyTabId][] = $module;
				}
			}
		}
		return isset($this->modules[$propertyTabId]) ? $this->modules[$propertyTabId] : NULL;
	}

	/**
	 * Get by parent node id.
	 * @param string $EntityID
	 * @return self
	 */
	public static function GetByParentNodeId($nodeId) {
		$key = \PC\Helper::GenerateKey('GetByParentNodeId'.self::TYPE_PROPERTY, $nodeId);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$where='n.`ParentNodeID` = %s';
			$where.=' && n.`Type` = \''.self::TYPE_PROPERTY.'\'';
			$val = self::FetchAll('SELECT n.* FROM `Node` n WHERE ' . $where . ' ORDER BY ' . self::ORDER_ORDER_ASC, array($nodeId, $nodeId));
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	/**
	 * Get entity by entity id.
	 * @param string $EntityID
	 * @return self
	 */
	public static function GetByNodeID($nodeId) {
		$key = \PC\Helper::GenerateKey('GetByNodeId'.self::TYPE_PROPERTY, $nodeId);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$where='n.`NodeID` = %s';
			$where.=' && n.`Type` = \''.self::TYPE_PROPERTY.'\'';
			$val = self::FetchOne('SELECT n.* FROM `Node` n WHERE ' . $where, array($nodeId));
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	public static function GetByAlias($alias) {
		$key = \PC\Helper::GenerateKey('GetByAlias'.self::TYPE_PROPERTY, $alias);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$where = array('n.`Type` = \''.self::TYPE_PROPERTY.'\'');
			$where[] = sprintf('(n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
					\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());
			$where[] = 'n.`NodeID` = (SELECT nd.`NodeID` FROM `NodeData` nd JOIN `Node` nn ON(nn.`NodeID` = nd.`NodeID`)
					WHERE nn.`Type` = \''.self::TYPE_PROPERTY.'\' && nd.`Key` = \'alias\' && nd.`Value` = \''.\Pecee\DB\DB::Escape($alias).'\' LIMIT 1)';
			$val = self::FetchOne('SELECT n.* FROM `Node` n WHERE ' . join(' && ', $where). ' ORDER BY ' . self::ORDER_ORDER_ASC);
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	/**
	 * Get entities.
	 * @param string|null $Query
	 * @param bool|null $Active
	 * @param int|null $Rows
	 * @param int|null $Page
	 * @return self
	 */
	public static function Get($query=NULL, $active=NULL, $parentNodeId=NULL, $order=NULL, $rows=NULL, $page=NULL) {
		$key = \PC\Helper::GenerateKey('Get'.self::TYPE_PROPERTY , $query, $active, $parentNodeId, $order, $rows, $page);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$where=array('n.`Type` = \''.self::TYPE_PROPERTY.'\'');
			$where[] = sprintf('(n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
					\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());

			if(!is_null($active)) {
				$where[] = \Pecee\DB\DB::FormatQuery('n.`Active` = %s', array(Boolean::Parse($active)));
				$where[] = \Pecee\DB\DB::FormatQuery('(ISNULL(n.`ActiveFrom`) && ISNULL(n.`ActiveTo`) || n.`ActiveFrom` <= NOW() && (n.`ActiveTo` >= NOW() || ISNULL(n.`ActiveTo`)))');
			}
			if(!is_null($parentNodeId)) {
				$where[] = "n.`ParentNodeID` = '".\Pecee\DB\DB::Escape($parentNodeId)."'";
			}

			if(!is_null($query)) {
				$where[] = sprintf('(n.`Title` LIKE \'%s\' OR n.`Content` LIKE \'%s\')', '%'.\Pecee\DB\DB::Escape($query).'%', '%'.\Pecee\DB\DB::Escape($query).'%');
			}
			$order=(!is_null($order) && in_array($order, self::$orders)) ? $order : self::ORDER_ORDER_ASC;
			$val = self::FetchPage('n.NodeID', 'SELECT n.* FROM `Node` n WHERE ' . join(' && ', $where) . ' ORDER BY ' . $order, $rows, $page);
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}
}