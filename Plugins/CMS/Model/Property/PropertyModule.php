<?php
namespace CMS\Model\Property;
class PropertyModule extends \Pecee\Model\Model {
	protected $tab;
	public function __construct() {
		parent::__construct('CMS_PropertyModule', array('PropertyModuleID' => '', 'PropertyTabID' => NULL, 'NodeID' => '', 'Name' => '',
														'Description' => '', 'ControlID' => '', 'Required' => FALSE, 'Module' => '',
														'Data' => NULL, 'Order' => 0, 'Inheritable' => TRUE));
	}

	public function getTab() {
		if(!$this->tab) {
			/* @var $propertytab \CMS\Model\Property\PropertyTab */
			$propertytab = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyTab');

			$this->tab = $propertytab::GetByPropertyTabID($this->PropertyTabID);
		}
		return $this->tab;
	}

	public static function GetByPropertyModuleId($propertyModuleId) {
		$where=array(\Pecee\DB\DB::FormatQuery('`PropertyModuleID` = %s',array($propertyModuleId)));
		return self::FetchOne('SELECT * FROM `CMS_PropertyModule` WHERE ' . join(' && ', $where));
	}

	public static function GetByPropertyTabId($propertyTabId) {
		$where=array(\Pecee\DB\DB::FormatQuery('`PropertyTabID` = %s',array($propertyTabId)));
		return self::FetchAll('SELECT * FROM `CMS_PropertyModule` WHERE ' . join(' && ', $where));
	}

	public static function GetByNodeId($nodeId,$propertyTabId=NULL) {
		$where=array('1=1');
		if(!is_null($propertyTabId)) {
			$where[] = '`PropertyTabID` = \''.\Pecee\DB\DB::Escape($propertyTabId).'\'';
		}
		return self::FetchAll('SELECT * FROM `CMS_PropertyModule` WHERE `NodeID` = %s && ' . join(' && ', $where) . ' ORDER BY `Order` ASC, `PropertyModuleID` ASC', array($nodeId));
	}
}