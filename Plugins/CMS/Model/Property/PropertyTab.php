<?php
namespace CMS\Model\Property;
class PropertyTab extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('CMS_PropertyTab', array('PropertyTabID' => '', 'NodeID' => '', 'Name' => '', 'Order' => 0, 'Inheritable' => TRUE));
	}

	public function delete() {
		/* @var $propertymodule \CMS\Model\Property\PropertyModule */
		$propertymodule = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyModule');

		// Delete all modules attached to this tab
		$modules = $propertymodule::GetByPropertyTabId($this->PropertyTabID);
		if($modules->hasRows()) {
			foreach($modules->getRows() as $module) {
				$module->delete();
			}
		}
		parent::delete();
	}

	public static function Get() {
		return self::FetchAll('SELECT * FROM `CMS_PropertyTab` ORDER BY `Order` ASC, `PropertyTabID` DESC');
	}

	public static function GetByPropertyTabID($propertyTabId) {
		$where=array(\Pecee\DB\DB::FormatQuery('`PropertyTabID` = %s',array($propertyTabId)));
		return self::FetchOne('SELECT * FROM `CMS_PropertyTab` WHERE ' . join(' && ', $where));
	}

	public static function GetByNodeIds(array $nodeIds) {
		return self::FetchAll('SELECT * FROM `CMS_PropertyTab` WHERE `NodeID` IN('.\Pecee\DB\DB::JoinArray($nodeIds).') ORDER BY `Order` ASC');
	}

	/**
	 * @return self
	 */
	public static function GetByNodeId($nodeId) {
		return self::FetchAll('SELECT * FROM `CMS_PropertyTab` WHERE `NodeID` = %s ORDER BY `Order` ASC', array($nodeId));
	}
}