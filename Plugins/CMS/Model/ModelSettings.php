<?php
namespace CMS\Model;
use Pecee\Str;

class ModelSettings extends \Pecee\Model\Node\ModelNode {
	protected static $instance;
	const TYPE_SETTINGS = 'settings';
	protected $language;
	protected $rootSlug;
	protected $rootNodeId;
	public function __construct() {
		parent::__construct();
		$this->data->applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
	}

	public function getLanguage() {
		return $this->language;
	}

	public function setLanguage($alias) {
		$this->language = $alias;
	}

	public function getRootSlug() {
		return $this->rootSlug;
	}

	public function setRootSlug($slug) {
		$this->rootSlug = $slug;
	}

	public function getRootNodeId() {
		return $this->rootNodeId;
	}

	public function setRootNodeId($nodeId) {
		$this->rootNodeId = $nodeId;
	}

	public function getHasRewrites() {
		return $this->data->hasRewrites;
	}

	public function setHasRewrites($bool) {
		$this->data->hasRewrites = $bool;
	}

	public function getUsingHostnames() {
		return $this->data->usingHostnames;
	}

	public function setUsingHostnames($bool) {
		$this->data->usingHostnames = $bool;
	}

	public function getMaxFilesize() {
		if($this->data->maxFileSize) {
			return $this->data->maxFileSize;
		}
		return 50;
	}

	public function setMaxFileSize($size) {
		$this->data->maxFileSize = $size;
	}

	public function getFileTypes($asArray=TRUE) {
		if(!$this->data->fileTypes) {
			$this->data->fileTypes = 'zip, jpg, jpeg, png, gif, bmp, pdf, doc, docx';
		}
		if($this->data->fileTypes && $asArray) {
			$fileTypes = explode(',', $this->data->fileTypes);
			$out = array();
			foreach($fileTypes as $f) {
				$out[] = trim(str_replace('\'', '', $f));
			}
			return $out;
		}
		return $this->data->fileTypes;
	}

	public function setThemes(array $themes) {
		return $this->data->themes = serialize($themes);
	}

	public function getThemes() {
		if($this->data->themes) {
			return unserialize($this->data->themes);
		}
		return array();
	}

	public function setSiteData(array $value) {
		return $this->data->siteData = serialize($value);
	}

	public function setTheme($theme) {
		$themes = $this->getThemes();
		$this->setThemes(array_merge($themes, $theme));
	}

	public function getSiteData($name=NULL) {
		$tmp = unserialize($this->data->siteData);
		$tmp = (is_array($tmp)) ? $tmp : array();
		if(is_null($name)) {
			return $tmp;
		}
		return (isset($tmp[$name]) ? $tmp[$name] : NULL);
	}

	public function getThumbnails() {
		$tmp = unserialize($this->data->thumbnails);
		return (is_array($tmp)) ? $tmp : array();
	}

	public function getThumbnailType($type) {
		foreach($this->getThumbnails() as $thumbnail) {
			$key = array_keys($thumbnail);
			if(strtolower($key[0]) == strtolower($type)) {
				return $thumbnail[$key[0]];
			}
		}
		return NULL;
	}

	public function setThumbnails(array $thumbnails) {
		$this->data->thumbnails = serialize($thumbnails);
	}

	public function setDisableHistory($bool) {
		$this->data->disableHistory = $bool;
	}

	public function getDisableHistory() {
		return $this->data->disableHistory;
	}

	public function setFileTypes($types) {
		$this->data->fileTypes = $types;
	}

	public function setFileStoragePath($path) {
		$this->data->fileStoragePath = $path;
	}

	public function getFileStoragePath() {
		return $this->data->fileStoragePath;
	}

	public function setFileUrl($url) {
		$this->data->fileUrl = $url;
	}

	public function getFileUrl() {
		return Str::GetFirstOrValue($this->data->fileUrl, '/file');
	}

	public function setCreateThumbs($bool) {
		$this->data->createThumbs = $bool;
	}

	public function getCreateThumbs() {
		return $this->data->createThumbs;
	}

	public function getMetaTitle() {
		return $this->data->metaTitle;
	}

	public function setMetaTitle($title) {
		$this->data->metaTitle = $title;
	}

	public function getMetaDescription() {
		return $this->data->metaDescription;
	}

	public function setMetaDescription($description) {
		$this->data->metaDescription = $description;
	}

	public function getMetaKeywords() {
		return $this->data->metaKeywords;
	}

	public function setMetaKeywords($keywords) {
		$this->data->metaKeywords = $keywords;
	}

	public function getFrontpageNodeId() {
		return $this->data->frontpageNodeId;
	}

	public function setFrontpageNodeId($nodeId) {
		$this->data->frontpageNodeId = $nodeId;
	}

	public function get404NodeId() {
		$tmp = '404nodeId';
		return $this->data->$tmp;
	}

	public function set404NodeId($nodeId) {
		$tmp = '404nodeId';
		$this->data->$tmp = $nodeId;
	}

	public function setDisableCache($bool) {
		$this->data->disableCache = $bool;
	}

	public function getDisableCache() {
		return $this->data->disableCache;
	}

	public function save() {
		$this->Type = self::TYPE_SETTINGS;
		parent::save();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	public function update() {
		parent::update();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	/**
	 * @return self
	 */
	public static function GetSettings() {
		$key = 'Settings';

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$settings = $cacheclass::GetInstance()->get($key);
		if(is_null($settings)) {
			$where=array('1=1');
			$where[] = sprintf('(n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
					\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());

			$settings = self::FetchOne('SELECT n.* FROM `Node` n LEFT OUTER JOIN `NodeData` nd ON(nd.`NodeID` = n.`NodeID`) WHERE n.`Type` = \''.self::TYPE_SETTINGS.'\'
				&& ' . join(' && ', $where));

			// Ensure that the page allways exists
			if(!$settings->hasRow()) {
				$settings = new self();
				$settings->setTitle('Settings');
				$settings->save();
			}

			$cacheclass::GetInstance()->set($key, $settings, 9999);
		}
		return $settings;
	}

	/**
	 * Get instance
	 * @return self
	 */
	public static function GetInstance() {
		if(is_null(self::$instance)) {
			self::$instance = self::GetSettings();
		}
		return self::$instance;
	}
}