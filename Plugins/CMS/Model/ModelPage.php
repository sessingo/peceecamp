<?php
namespace CMS\Model;
use Pecee\Boolean;

class ModelPage extends \Pecee\Model\Node\ModelNode {
	const TYPE_PAGE = 'page';
	protected $property;
	protected $parents;

	public function delete() {
		// Delete drafts

		/* @var $modeldraft \CMS\Model\ModelDraft */
		$modeldraft = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelDraft');

		$drafts = $modeldraft::GetByPageID($this->getNodeID());
		if($drafts->hasRows()) {
			foreach($drafts->getRows() as $draft) {
				$draft->delete();
			}
		}

		/* @var $nodehost \CMS\Model\Node\NodeHost */
		$nodehost = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Node\NodeHost');

		$hosts = $nodehost::GetByNodeID($this->getNodeID());
		if($hosts->hasRows()) {
			foreach($hosts->getRows() as $host) {
				$host->delete();
			}
		}

		parent::delete();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	public function __construct() {
		parent::__construct();
		$this->data->applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
		$this->parent = array();
	}

	public function setUserID($userId) {
		$this->data->UserID = $userId;
	}

	public function getUserID() {
		return $this->data->UserID;
	}

	public function getData($name) {
		return $this->data->$name;
	}

	public function getParent() {
		if($this->ParentNodeID) {
			if(!$this->parents[$this->ParentNodeID]) {
				$this->parents[$this->ParentNodeID] = self::GetByNodeID($this->ParentNodeID);
			}
			return $this->parents[$this->ParentNodeID];
		}
		return NULL;
	}

	public function setWidget($widget) {
		$this->data->widget = $widget;
	}

	public function getWidget() {
		return $this->data->widget;
	}

	public function setPropertyNodeId($propertyNodeId) {
		$this->data->propertyNodeId = $propertyNodeId;
	}

	public function getPropertyNodeId() {
		return $this->data->propertyNodeId;
	}

	public function setProperty($property) {
		$this->property = $property;
	}

	public function getProperty() {
		if(!$this->property) {

			/* @var $propertymodel \CMS\Model\ModelProperty */
			$propertymodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');

			$this->property = $propertymodel::GetByNodeID($this->getPropertyNodeId());
		}
		return $this->property;
	}

	public function getVisibleInMenu() {
		return $this->data->visibleInMenu;
	}

	public function setVisibleInMenu($visible) {
		$this->data->visibleInMenu = $visible;
	}

	public function getMetaTitle() {
		return $this->data->metaTitle;
	}

	public function setMetaTitle($title) {
		$this->data->metaTitle = htmlspecialchars($title);
	}

	public function getMetaDescription() {
		return $this->data->metaDescription;
	}

	public function setMetaDescription($description) {
		$this->data->metaDescription = $description;
	}

	public function getMetaKeywords() {
		return $this->data->metaKeywords;
	}

	public function setMetaKeywords($keywords) {
		$this->data->metaKeywords = $keywords;
	}

	public function getSlug($original=FALSE) {
		if($original) {
			return $this->data->slug;
		}
		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$settings = $modelsettings::GetInstance();
		if($settings->getRootSlug()) {
			return '/'.substr($this->data->slug, strlen($settings->getRootSlug())+1);
		}
		return '/'.$this->data->slug;
	}

	public function setSlug($slug) {
		$this->data->slug = trim($slug,'/');
	}

	public function getStartpage() {
		return $this->data->startpage;
	}

	public function setStartpage($bool) {
		$this->data->startpage = $bool;
	}

	public function generateSlug() {
		$slug = \Pecee\Url::UrlEncodeString($this->Title);
		$out = '';

		// Create slug based on path
		if($this->ParentNodeID) {
			$s = array();
			$parent = self::GetByNodeID($this->ParentNodeID);
			/* @var $modelsettings \CMS\Model\ModelSettings */
			$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

			$s[] = trim($parent->getSlug(),'/');

			if(count($s) > 0) {
				$s = array_reverse($s);
				$out .= join('/', $s) . '/';
			}
		}

		$slug = '/'.$out . $slug;

		if($slug != '/'.$this->data->slug) {
			// Create unique slug, if the pretty one is taken
			$s = self::GetBySlug(trim($slug,'/'));
			if($s->hasRow() && $s->getNodeID() != $this->getNodeID()) {
				$slug = $slug . '-'.$this->getNodeID();
			}
			$this->setSlug($slug);
		}
	}

	public function updateWithSlug() {
		$this->generateSlug();
		$this->update();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	public function save() {
		$this->Type = self::TYPE_PAGE;
		if($this->property && $this->property->hasRow()) {
			$this->setPropertyNodeId($this->property->getNodeID());
		}

		parent::save();
		$this->generateSlug();
		$this->update();
	}

	public function update() {
		parent::update();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	public function getChilds($active=NULL) {
		if(!$this->childs) {
			$this->childs = self::Get(NULL, NULL, $active, $this->NodeID);
		}
		return $this->childs;
	}

	public static function GetByHostname($hostname,$active=NULL) {
		$key = \PC\Helper::GenerateKey('GetByHostname'.self::TYPE_PAGE, $hostname, $active);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$where=array(\Pecee\DB\DB::FormatQuery('(nh.`Hostname` = %s || nh.`Hostname` = \'\')', array($hostname)));
			$where[] = sprintf('(n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
					\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());
			if(!is_null($active)) {
				$where[] ='n.`Active` = ' . Boolean::Parse($active,0);
			}
			$where[] = 'n.`Type` = \''.\Pecee\DB\DB::Escape(self::TYPE_PAGE).'\'';
			$val = self::FetchOne('SELECT n.*, l.`Alias` AS `LanguageAlias` FROM `Node` n JOIN `CMS_NodeHost` nh ON(nh.`NodeID` = n.`NodeID`) JOIN `CMS_Language` l ON(l.`LanguageID` = nh.`LanguageID`) WHERE ' . join(' && ', $where));
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	public static function GetBySlug($slug,$active=NULL) {
		$key = \PC\Helper::GenerateKey('GetBySlug'.self::TYPE_PAGE, $slug, $active);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$where=array();
			$where[] = sprintf('(n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
					\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());
			if(!is_null($active)) {
				$where[] ='n.`Active` = ' . Boolean::Parse($active,0);
			}
			$where[] = 'n.`Type` = \''.\Pecee\DB\DB::Escape(self::TYPE_PAGE).'\'';
			$where[] = '(SELECT `NodeID` FROM `NodeData` WHERE `NodeID` = n.`NodeID` && `Key` = \'slug\' && `Value` = \''. \Pecee\DB\DB::Escape($slug) .'\')';
			$val = self::FetchOne('SELECT n.* FROM `Node` n WHERE ' . join(' && ', $where));
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	public static function GetByPropertyNodeId($nodeId) {
		$key = \PC\Helper::GenerateKey('GetByPropertyNodeId'.self::TYPE_PAGE, $nodeId);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$val = self::FetchAll('SELECT n.* FROM `Node` n JOIN `NodeData` nd ON(nd.`NodeID` = n.`NodeID`) WHERE n.`Type` = %s && nd.`Key` = \'propertynodeid\' && nd.`Value` = %s ORDER BY ' . self::ORDER_ORDER_ASC, array(self::TYPE_PAGE, $nodeId));
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	/**
	 * @return self
	 */
	public static function GetByPropertyAlias($alias, $active = NULL) {
		$key = \PC\Helper::GenerateKey('PropertyAlias'.self::TYPE_PAGE, $alias, $active);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			/* @var $propertymodel \CMS\Model\ModelProperty */
			$propertymodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');

			$property = $propertymodel::GetByAlias($alias);
			if($property->hasRow()) {
				$where = array('n.`NodeID` IN (SELECT `NodeID` FROM `NodeData` WHERE `Key` = \'propertynodeid\' && `Value` = '. \Pecee\DB\DB::Escape($property->getNodeID()) .')');
				$where[] = sprintf('(n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
							\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());
				if(!is_null($active)) {
					$where[] = \Pecee\DB\DB::FormatQuery('n.`Active` = %s', array(Boolean::Parse($active)));
					$where[] = \Pecee\DB\DB::FormatQuery('(ISNULL(n.`ActiveFrom`) && ISNULL(n.`ActiveTo`) || n.`ActiveFrom` <= NOW() && (n.`ActiveTo` >= NOW() || ISNULL(n.`ActiveTo`)))');
				}

				$where[] =  'n.`Type` = \''.self::TYPE_PAGE.'\'';
				$val = self::FetchPage('n.`NodeID`','SELECT n.* FROM `Node` n WHERE ' . join(' && ', $where));
				$cacheclass::GetInstance()->set($key, $val, 60*60*24);
			}
		}
		return $val;
	}

	/**
	 * Get childs of type
	 * @return self
	 */
	public function getChildsOfType($alias, $recursive=TRUE, $active=TRUE) {
		if($recursive) {
			$result = self::Get($alias, NULL, $active, NULL, $this->getNodeID());
		} else {
			$result = self::Get($alias, NULL, $active, $this->getNodeID());
		}
		return $result;
	}

	/**
	 * Get entities.
	 * @return self
	 */
	public static function Get($alias=NULL, $query=NULL, $active=NULL, $parentNodeId=NULL, $path = NULL, $order=NULL, $rows=NULL, $page=NULL) {
		$key = \PC\Helper::GenerateKey('Get'.self::TYPE_PAGE, $alias, $query, $active, $parentNodeId, $path, $order, $rows, $page);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$order = (is_null($order)) ? self::ORDER_ORDER_ASC : $order;

			$where=array('n.`Type` = \''.self::TYPE_PAGE.'\'');

			$where[] = sprintf('(n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
						\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());

			if(!is_null($active)) {
				$where[] = \Pecee\DB\DB::FormatQuery('n.`Active` = %s', array(Boolean::Parse($active)));
				$where[] = \Pecee\DB\DB::FormatQuery('(ISNULL(n.`ActiveFrom`) && ISNULL(n.`ActiveTo`) || n.`ActiveFrom` <= NOW() && (n.`ActiveTo` >= NOW() || ISNULL(n.`ActiveTo`)))');
			}
			if(!is_null($parentNodeId)) {
				$where[] = "n.`ParentNodeID` = '".\Pecee\DB\DB::Escape($parentNodeId)."'";
			}

			if(!is_null($path)) {
				$where[] = "n.`Path` LIKE '%>".\Pecee\DB\DB::Escape($path)."%'";
			}

			if(!is_null($query)) {
				$where[] = sprintf('(n.`NodeID` IN(SELECT `NodeID` FROM `NodeData` nd WHERE nd.`Value` LIKE \'%s\') OR
						n.`Title` LIKE \'%s\' OR n.`Content` LIKE \'%s\')',
						'%'.\Pecee\DB\DB::Escape($query).'%',
						'%'.\Pecee\DB\DB::Escape($query).'%',
						'%'.\Pecee\DB\DB::Escape($query).'%');
			}
			$order=(!is_null($order) && in_array($order, self::$orders)) ? $order : self::ORDER_ORDER_ASC;

			if(!is_null($alias)) {
				$where[] = 'n.`NodeID` IN (SELECT `NodeID` FROM `NodeData` WHERE `Key` = \'propertynodeid\' && `Value` = (SELECT `NodeID` FROM `NodeData` WHERE `Key` = \'alias\' && `Value` = \''.$alias.'\'))';
			}

			$val = self::FetchPage('n.NodeID', 'SELECT n.* FROM `Node` n WHERE ' . join(' && ', $where) . ' ORDER BY ' . $order, $rows, $page);
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	/**
	 * Get by node id
	 * @param int $nodeId
	 * @param bool|NULL $active
	 * @return self
	 */
	public static function GetByNodeID($nodeId, $active=NULL) {
		$key = \PC\Helper::GenerateKey('GetByNodeId'.self::TYPE_PAGE, $nodeId, $active);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$val = parent::GetByNodeID($nodeId, $active);
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}
}