<?php
namespace CMS\Model\Node;
class NodeHost extends \Pecee\Model\Model {
	protected $language;
	public function __construct() {
		parent::__construct('CMS_NodeHost', array('NodeHostID' => '', 'NodeID' => '', 'LanguageID' => '', 'Hostname' => ''));
	}

	public function getLanguage() {
		if(!$this->language) {
			/* @var $languagemodel \CMS\Model\ModelLanguage */
			$languagemodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelLanguage');
			$this->language = $languagemodel::GetById($this->LanguageID);
		}
		return $this->language;
	}

	/**
	 * Get by node id
	 * @param int $nodeId
	 * @return self
	 */
	public static function GetByNodeID($nodeId) {
		return self::FetchAll('SELECT * FROM `CMS_NodeHost` WHERE `NodeID` = %s ORDER BY `Hostname` ASC', $nodeId);
	}

	/**
	 * Get by node host id
	 * @param int $nodeHostId
	 * @return self
	 */
	public static function GetByID($nodeHostId) {
		return self::FetchOne('SELECT * FROM `CMS_NodeHost` WHERE `NodeHostID` = %s', $nodeHostId);
	}

	public static function GetByHost($host) {
		return self::FetchOne('SELECT * FROM `CMS_NodeHost` WHERE `Hostname` = %s', $host);
	}

	/**
	 * Get
	 * @return self
	 */
	public static function Get() {
		return self::FetchAll('SELECT * FROM `CMS_NodeHost` ORDER BY `Hostname` ASC');
	}
}