<?php
namespace CMS\Model\Macro;
use Pecee\Str;

class MacroProperty extends \Pecee\Model\Model {

	public function __construct() {
		parent::__construct('CMS_MacroProperty', array('MacroPropertyID' => '', 'NodeID' => '', 'Method' => '', 'Name' => NULL));
	}

	public function getName($strict=FALSE) {
		if($strict) {
			return $this->Name;
		}

		return Str::GetFirstOrValue($this->Name, $this->Method);
	}

	public static function Clear($nodeID) {
		self::NonQuery('DELETE FROM `CMS_MacroProperty` WHERE `NodeID` = %s', $nodeID);
	}

	public static function Get($nodeID) {
		return self::FetchAll('SELECT * FROM `CMS_MacroProperty` WHERE `NodeID` = %s ORDER BY `Name` ASC', $nodeID);
	}

	public static function GetById($macroPropertyId) {
		return self::FetchOne('SELECT * FROM `CMS_MacroProperty` WHERE `MacroPropertyID` = %s', $macroPropertyId);
	}

}