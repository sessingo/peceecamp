<?php
namespace CMS\Model;
class ModelRewrite extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('CMS_Rewrite', array('RewriteID' => '', 'ApplicationID' => '', 'OriginalUrl' => '',
				'RewriteUrl' => '', 'NodeID' => NULL, 'Host' => NULL, 'Regex' => NULL, 'Order' => 0));
		$this->ApplicationID = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
	}

	public function getNode() {
		/* @var $modelpage \CMS\Model\ModelPage */
		$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

		return $modelpage::GetByNodeID($this->NodeID);
	}

	public function exists() {
		$applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
		return (self::Scalar('SELECT `OriginalPath` FROM `CMS_Rewrite` WHERE `ApplicationID` = %s && OriginalPath` = %s',
				$applicationId, $this->OriginalPath));
	}

	/**
	 * Get rewrite by originalpath
	 * @param string $originalPath
	 * @return \Pecee\Model\ModelRouterRewrite
	 */
	public static function GetByOriginalUrl($originalUrl, $host = NULL) {
		$applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
		$where = array(\Pecee\DB\DB::FormatQuery('`ApplicationID` = %s', array($applicationId)));
		if(!is_null($host)) {
			$where[] = \Pecee\DB\DB::FormatQuery('`Host` = %s', array($host));
		}
		return self::FetchOne('SELECT * FROM `CMS_Rewrite` WHERE `OriginalUrl` = %s && ' . join(' && ', $where), $originalUrl);
	}

	public static function HasRewrites() {
		$applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
		return self::Scalar('SELECT `ApplicationID` FROM `CMS_Rewrite` WHERE `ApplicationID` = %s', $applicationId);
	}

	public static function GetByRewritePath($rewriteUrl) {
		$applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
		return self::FetchOne('SELECT * FROM `CMS_Rewrite` WHERE `ApplicationID` = %s && `RewriteUrl` = %s', $applicationId, $rewriteUrl);
	}

	public static function Get($rows = NULL, $page = NULL) {
		$applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
		return self::FetchPage('OriginalUrl', 'SELECT * FROM `CMS_Rewrite` WHERE `ApplicationID` = %s ORDER BY `Order` ASC, `RewriteID` DESC',
				$rows, $page, $applicationId);
	}

	public static function GetByRewriteID($rewriteId) {
		return self::FetchOne('SELECT * FROM `CMS_Rewrite` WHERE `RewriteID` = %s', $rewriteId);
	}
}