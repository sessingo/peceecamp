<?php
namespace CMS\Model;
use Pecee\Boolean;

class ModelUser extends \Pecee\Model\Node\ModelNode {
	const TYPE_USER = 'user';
	const TICKET_AUTH_KEY = 'TicketUserLoginKey';

	protected $property;

	protected static $instance;

	public function __construct() {
		parent::__construct();
		$this->data->applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
		$this->parent = array();
	}

	public function getUserID() {
		return $this->NodeID;
	}

	public function setUsername($username) {
		$this->Title = $username;
	}

	public function getUsername() {
		return $this->Title;
	}

	public function getPassword() {
		return $this->data->password;
	}

	public function setPassword($password) {
		$this->data->password = $password;
	}

	public function setPropertyNodeId($propertyNodeId) {
		$this->data->propertyNodeId = $propertyNodeId;
	}

	public function getPropertyNodeId() {
		return $this->data->propertyNodeId;
	}

	public function setProperty($property) {
		$this->property = $property;
	}

	public function getProperty() {
		if(!$this->property) {
			/* @var $userproperty \CMS\Model\User\UserProperty */
			$userproperty = \PC\ObjectManager::GetInstance()->get('\CMS\Model\User\UserProperty');

			$this->property = $userproperty::GetByNodeID($this->getPropertyNodeId());
		}
		return $this->property;
	}

	public function getParent() {
		if($this->ParentNodeID) {
			if(!$this->parents[$this->ParentNodeID]) {
				$this->parents[$this->ParentNodeID] = self::GetByNodeID($this->ParentNodeID);
			}
			return $this->parents[$this->ParentNodeID];
		}
		return NULL;
	}

	public function getChilds($active=NULL) {
		if(!$this->childs) {
			$this->childs = self::Get(NULL, NULL, $active, $this->NodeID);
		}
		return $this->childs;
	}

	public function save() {
		$this->Type = self::TYPE_USER;
		if($this->property && $this->property->hasRow()) {
			$this->setPropertyNodeId($this->property->getNodeID());
		}
		parent::save();
		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	public function update() {
		parent::update();
		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');
		$cacheclass::GetInstance()->clear();
	}

	public function delete() {
		parent::delete();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	protected static function GenerateLoginKey() {
		return substr(md5(self::TICKET_AUTH_KEY), 0, 15);
	}

	public function signIn($cookieExp){
		$user = array($this->getUserID(), $this->getPassword(), md5(microtime()), $this->getUsername());
		$ticket = \Pecee\Mcrypt::Encrypt(join('|',$user), self::GenerateLoginKey() );
		\Pecee\Cookie::Create('UserTicket', $ticket, $cookieExp);
	}

	public function signOut() {
		if(\Pecee\Cookie::Exists('UserTicket')) {
			\Pecee\Cookie::Delete('UserTicket');
		}
	}

	public static function GetByPropertyNodeId($nodeId) {
		$key = \PC\Helper::GenerateKey('GetByPropertyNodeId'.self::TYPE_USER, $nodeId);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$val = self::FetchAll('SELECT n.* FROM `Node` n JOIN `NodeData` nd ON(nd.`NodeID` = n.`NodeID`) WHERE n.`Type` = %s && nd.`Key` = \'propertynodeid\' && nd.`Value` = %s ORDER BY ' . self::ORDER_ORDER_ASC, array(self::TYPE_USER, $nodeId));
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	public static function GetByKey($key, $value) {
		$k = 'GetByKey'.self::TYPE_USER . $key . $value;

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($k);
		if(is_null($val)) {
			$where = array('1=1');
			$where[] = sprintf('(n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
					\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());

			$val = self::FetchOne('SELECT n.* FROM `Node` n WHERE n.`Type` = %s && n.`NodeID` = (SELECT `NodeID` FROM `NodeData` WHERE `Key` = %s && `Value` = %s LIMIT 1) ' . join(' && ', $where), array(self::TYPE_USER, $key, $value));
			$cacheclass::GetInstance()->set($k, $val, 60*60*24);
		}
		return $val;
	}

	/**
	 * Get by email
	 * @param string $email
	 * @return self
	 */
	public static function GetByUsername($username) {
		$key = \PC\Helper::GenerateKey('GetByUsername'.self::TYPE_USER, $username);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
			$where='n.`Title` = %s && n.`Type` = \''.self::TYPE_USER.'\'';
			$where.= sprintf('&& (n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',$applicationId);
			$val = self::FetchOne('SELECT n.* FROM `Node` n WHERE ' . $where, array($username));
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	/**
	 * Get
	 * @param string|NULL $alias
	 * @param string|NULL $query
	 * @param string|NULL $active
	 * @param string|NULL $parentNodeId
	 * @param string|NULL $path
	 * @param string|NULL $order
	 * @param string|NULL $rows
	 * @param string|NULL $page
	 * @return self
	 */
	public static function Get($alias=NULL, $query=NULL, $active=NULL, $parentNodeId=NULL, $path = NULL, $order=NULL, $rows=NULL, $page=NULL) {
		$key = \PC\Helper::GenerateKey('Get'.self::TYPE_USER, $alias, $query,$active,$parentNodeId,$path, $order,$rows,$page);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$order = (is_null($order)) ? self::ORDER_ORDER_ASC : $order;

			$where=array('n.`Type` = \''.self::TYPE_USER.'\'');
			$where[] = sprintf('(n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
								\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());
			if(!is_null($active)) {
				$where[] = \Pecee\DB\DB::FormatQuery('n.`Active` = %s', array(Boolean::Parse($active)));
				$where[] = \Pecee\DB\DB::FormatQuery('(ISNULL(n.`ActiveFrom`) && ISNULL(n.`ActiveTo`) || n.`ActiveFrom` <= NOW() && (n.`ActiveTo` >= NOW() || ISNULL(n.`ActiveTo`)))');
			}
			if(!is_null($parentNodeId)) {
				$where[] = "n.`ParentNodeID` = '".\Pecee\DB\DB::Escape($parentNodeId)."'";
			}

			if(!is_null($path)) {
				$where[] = "n.`Path` LIKE '%>".\Pecee\DB\DB::Escape($path)."%'";
			}
			if(!is_null($query)) {
				$where[] = sprintf('(n.`NodeID` IN(SELECT `NodeID` FROM `NodeData` nd WHERE nd.`Value` LIKE \'%s\') OR
						n.`Title` LIKE \'%s\' OR n.`Content` LIKE \'%s\')',
						'%'.\Pecee\DB\DB::Escape($query).'%',
						'%'.\Pecee\DB\DB::Escape($query).'%',
						'%'.\Pecee\DB\DB::Escape($query).'%');
			}

			$order=(!is_null($order) && in_array($order, self::$orders)) ? $order : self::ORDER_ORDER_ASC;

			if(!is_null($alias)) {
				$where[] = 'n.`NodeID` IN (SELECT `NodeID` FROM `NodeData` WHERE `Key` = \'propertynodeid\' && `Value` = (SELECT `NodeID` FROM `NodeData` WHERE `Key` = \'alias\' && `Value` = \''.$alias.'\'))';
			}

			$val = self::FetchPage('n.NodeID', 'SELECT n.* FROM `Node` n WHERE ' . join(' && ', $where) . ' ORDER BY ' . $order, $rows, $page);
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	/**
	 * Get by user id
	 * @param int $userId
	 * @return self
	 */
	public static function GetByUserID($userId) {
		return self::GetByNodeID($userId,TRUE);
	}

	/**
	 * Get by data
	 * @param string $key
	 * @param string $value
	 * @return self
	 */
	public static function GetByData($key,$value) {
		$k = 'GetByData'.self::TYPE_USER . $key . $value;

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($k);
		if(is_null($val)) {
			$where='n.`Active` = 1 && n.`Type` = \''.self::TYPE_USER.'\' && nd.`Key` = %s && nd.`Value` = %s';
			$where.= sprintf(' && (n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
					\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());
			$val = self::FetchOne('SELECT n.* FROM `Node` n JOIN `NodeData` nd ON(nd.`NodeID` = n.`NodeID`) WHERE ' . $where, array($key,$value));
			$cacheclass::GetInstance()->set($k, $val, 60*60*24);
		}
		return $val;
	}

	/**
	 * @return self
	 */
	public static function GetByPropertyAlias($alias, $active = NULL) {
		$key = \PC\Helper::GenerateKey('GetByPropertyAlias'.self::TYPE_USER, $alias, $active);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			/* @var $userproperty \CMS\Model\User\UserProperty */
			$userproperty = \PC\ObjectManager::GetInstance()->get('\CMS\Model\User\UserProperty');

			$property = $userproperty::GetByAlias($alias);
			if($property->hasRow()) {

				$where = array('n.`NodeID` IN (SELECT `NodeID` FROM `NodeData` WHERE `Key` = \'propertynodeid\' && `Value` = \''. \Pecee\DB\DB::Escape($property->getNodeID()) .'\')');
				$where[] = sprintf('(n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
									\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());

				if(!is_null($active)) {
					$where[] = \Pecee\DB\DB::FormatQuery('n.`Active` = %s', array(Boolean::Parse($active)));
					$where[] = \Pecee\DB\DB::FormatQuery('(ISNULL(n.`ActiveFrom`) && ISNULL(n.`ActiveTo`) || n.`ActiveFrom` <= NOW() && (n.`ActiveTo` >= NOW() || ISNULL(n.`ActiveTo`)))');
				}

				$where[] =  'n.`Type` = \''.self::TYPE_USER.'\'';
				$val = self::FetchPage('n.`NodeID`','SELECT n.* FROM `Node` n WHERE ' . join(' && ', $where));
				$cacheclass::GetInstance()->set($key, $val, 60*60*24);
			}
		}
		return $val;
	}

	public function getChildsOfType($alias, $recursive=TRUE, $active=TRUE) {
		if($recursive) {
			return self::Get($alias, NULL, $active, NULL, $this->getNodeID());
		}
		return self::Get($alias, NULL, $active, $this->getNodeID());
	}

	/**
	 * Get current user
	 * @return self
	 */
	public static function Current($setData=FALSE) {
		if(!is_null(self::$instance)) {
			return self::$instance;
		}
		if(self::IsLoggedIn()){
			$ticket = \Pecee\Cookie::Get('UserTicket');
			if(trim($ticket) != ''){
				$ticket = \Pecee\Mcrypt::Decrypt($ticket, self::GenerateLoginKey() );
				$user = explode('|', $ticket);
				if(is_array($user)) {
					if($setData) {
						self::$instance = self::GetByUserID($user[0]);
					} else {
						$caller=get_called_class();
						$obj=new $caller();
						$obj->setNodeID($user[0]);
						$obj->setPassword($user[1]);
						$obj->setUsername($user[3]);
						return $obj;
					}
				}
			}
		}
		return self::$instance;
	}

	public static function IsLoggedIn() {
		return \Pecee\Cookie::Exists('UserTicket');
	}

	public static function GetByNodeID($nodeId, $active=NULL) {
		$key = \PC\Helper::GenerateKey('GetByNodeId'.self::TYPE_USER, $nodeId);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$val = parent::GetByNodeID($nodeId, $active);
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}
}