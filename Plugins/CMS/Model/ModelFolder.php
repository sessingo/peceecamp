<?php
namespace CMS\Model;
use Pecee\Boolean;

class ModelFolder extends \Pecee\Model\Node\ModelNode {
	const TYPE_FOLDER = 'folder';

	public function __construct() {
		parent::__construct();
		$this->parent = array();
		$this->data->applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
	}

	public function save() {
		$this->Type = self::TYPE_FOLDER;
		parent::save();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	public function update() {
		parent::update();

		// Update child files (especially slugs)
		/* @var $modelfile \CMS\Model\ModelFile */
		$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

		$files = $modelfile::Get(NULL, NULL, NULL, NULL, $this->NodeID);
		if($files->hasRows()) {
			foreach($files->getRows() as $file) {
				$file->update();
			}
		}

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	public function delete() {

		/* @var $modelfile \CMS\Model\ModelFile */
		$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

		$files = $modelfile::Get(NULL, NULL, NULL, $this->getNodeID());

		// Delete files
		if($files->hasRows()) {
			foreach($files->getRows() as $file) {
				$file->delete();
			}
		}

		// Delete child folders
		$childs = $this->getChilds();
		if($childs->hasRows()) {
			foreach($childs->getRows() as $child) {
				$child->delete();
			}
		}

		// Delete this folder
		parent::delete();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	public function getChilds($active=NULL) {
		if(!$this->childs) {
			$this->childs = self::Get(NULL, $active, NULL, $this->NodeID);
		}
		return $this->childs;
	}

	public function getParent() {
		if($this->ParentNodeID) {
			if(!$this->parents[$this->ParentNodeID]) {
				$this->parents[$this->ParentNodeID] = self::GetByNodeID($this->ParentNodeID);
			}
			return $this->parents[$this->ParentNodeID];
		}
		return NULL;
	}

	public function getFiles($active=NULL) {
		/* @var $modelfile \CMS\Model\ModelFile */
		$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');
		return $modelfile::Get(NULL, NULL, $active, $this->NodeID);
	}

	public static function GetByTitle($title,$active=NULL) {
		$key = \PC\Helper::GenerateKey('GetByTitle'.self::TYPE_FOLDER, $title, $active);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$where=array();
			$where[] = sprintf('(n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
					\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());
			if(!is_null($active)) {
				$where[] ='n.`Active` = ' . Boolean::Parse($active,0);
			}
			$where[] = 'n.`Type` = \''.\Pecee\DB\DB::Escape(self::TYPE_FOLDER).'\'';
			$where[] = \Pecee\DB\DB::FormatQuery('n.`Title` = %s', array($title));
			$val = self::FetchOne('SELECT n.* FROM `Node` n WHERE ' . join(' && ', $where));
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	public static function Get($query=NULL, $active=NULL, $parentNodeId=NULL, $order=NULL, $rows=NULL, $page=NULL) {
		$key = \PC\Helper::GenerateKey('Get'.self::TYPE_FOLDER, $query,$active,$parentNodeId,$order,$rows,$page);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$order = (is_null($order)) ? self::ORDER_ORDER_ASC : $order;

			$where=array('n.`Type` = \''.self::TYPE_FOLDER.'\'');
			$where[] = sprintf('(n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
					\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());

			if(!is_null($active)) {
				$where[] = \Pecee\DB\DB::FormatQuery('n.`Active` = %s', array(Boolean::Parse($active)));
				$where[] = \Pecee\DB\DB::FormatQuery('(ISNULL(n.`ActiveFrom`) && ISNULL(n.`ActiveTo`) || n.`ActiveFrom` <= NOW() && (n.`ActiveTo` >= NOW() || ISNULL(n.`ActiveTo`)))');
			}
			if(!is_null($parentNodeId)) {
				$where[] = "n.`ParentNodeID` = '".\Pecee\DB\DB::Escape($parentNodeId)."'";
			}

			if(!is_null($query)) {
				$where[] = sprintf('(n.`NodeID` IN(SELECT `NodeID` FROM `NodeData` nd WHERE nd.`Value` LIKE \'%s\') OR
						n.`Title` LIKE \'%s\' OR n.`Content` LIKE \'%s\')',
						'%'.\Pecee\DB\DB::Escape($query).'%',
						'%'.\Pecee\DB\DB::Escape($query).'%',
						'%'.\Pecee\DB\DB::Escape($query).'%');
			}

			$order=(!is_null($order) && in_array($order, self::$orders)) ? $order : self::ORDER_ORDER_ASC;
			$val = self::FetchPage('n.NodeID', 'SELECT n.* FROM `Node` n WHERE ' . join(' && ', $where) . ' ORDER BY ' . $order, $rows, $page);
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	/**
	 *
	 * @param unknown $nodeId
	 * @param string $active
	 * @return self
	 */
	public static function GetByNodeID($nodeId, $active=NULL) {
		$key = \PC\Helper::GenerateKey('GetByNodeId'.self::TYPE_FOLDER, $nodeId, $active);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$val = parent::GetByNodeID($nodeId, $active);
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}
}