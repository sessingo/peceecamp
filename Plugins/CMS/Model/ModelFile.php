<?php
namespace CMS\Model;
use Pecee\Boolean;

class ModelFile extends \Pecee\Model\Node\ModelNode {
	const TYPE_FILE = 'file';

	public function __construct() {
		parent::__construct();
		$this->parent = array();
		$this->data->applicationId = \PC\Plugin::Instance()->getPlugin()->application->getApplicationID();
	}

	public function encodeString($str) {
		$str = strtolower($str);
		$str = str_replace('æ', 'a', $str);
		$str = str_replace('ø', 'o', $str);
		$str = str_replace('å', 'a', $str);
		$str = str_replace(' ', '_', $str);
		return $str;
	}

	protected function generateSlug() {
		if($this->Title) {
			$slug = array($this->encodeString($this->Title));

			// Create slug based on path
			if($this->ParentNodeID && $this->ParentNodeID > 0) {
				$parent = self::GetByNodeID($this->ParentNodeID);
				/* @var $modelsettings \CMS\Model\ModelSettings */
				$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

				$settings = $modelsettings::GetInstance();
				while($parent && $parent->hasRow()) {
					$slug[] = $this->encodeString($parent->getTitle());
					if($parent->getParentNodeID() > 0) {
						$parent = self::GetByNodeID($parent->getParentNodeID());
					} else {
						$parent = NULL;
					}
				}
			}

			// Create unique slug, if the pretty one is taken
			$s = self::GetBySlug('/'.join('/', array_reverse($slug)));
			if($s->hasRow() && $s->getNodeID() != $this->getNodeID()) {
				$title = $this->encodeString($this->Title);
				$pos = strrpos($title, '.');

				$slug[0] = substr($title, 0, $pos) . '_' . $this->getNodeID() . substr($title, $pos);
			}

			$slug = array_reverse($slug);
			$this->setSlug('/'. join('/', $slug));
		}
	}

	public function getParent() {
		if($this->ParentNodeID) {
			if(!$this->parents[$this->ParentNodeID]) {
				$this->parents[$this->ParentNodeID] = self::GetByNodeID($this->ParentNodeID);
			}
			return $this->parents[$this->ParentNodeID];
		}
		return NULL;
	}

	public function save() {
		$this->Type = self::TYPE_FILE;
		parent::save();
		$this->update();
	}

	public function update() {
		$this->generateSlug();
		parent::update();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	public function delete() {
		unlink($this->getFullPath());
		parent::delete();

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$cacheclass::GetInstance()->clear();
	}

	public function getSlug() {
		return $this->data->slug;
	}

	public function setSlug($slug) {
		$this->data->slug = $slug;
	}

	public function getFilename() {
		return $this->data->filename;
	}

	public function setFilename($filename) {
		$this->data->filename = $filename;
	}

	public function getOriginalFilename() {
		return $this->getTitle();
	}

	public function setOriginalFilename($filename) {
		$this->setTitle($filename);
	}

	public function getMime() {
		return $this->data->mime;
	}

	public function setMime($mime) {
		$this->data->mime = $mime;
	}

	public function getIpaddress() {
		return $this->data->ipaddress;
	}

	public function setIpaddress($ip) {
		$this->data->ipaddress = $ip;
	}

	public function getUserID() {
		return $this->data->userId;
	}

	public function setUserID($userId) {
		$this->data->userId = $userId;
	}

	public function getWidth() {
		return $this->data->width;
	}

	public function setWidth($width) {
		$this->data->width = $width;
	}

	public function getHeight() {
		return $this->data->height;
	}

	public function setHeight($height) {
		$this->data->height = $height;
	}

	public function getBytes() {
		return $this->data->bytes;
	}

	public function setBytes($bytes) {
		$this->data->bytes = $bytes;
	}

	public function getThumbnail($name, $exact=TRUE) {
		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$out = $modelsettings::GetInstance()->getFileUrl() . $this->getSlug();
		$type = (!$exact) ? '&resize=maxsize' : '';
		$out .= '?type=' . $name . $type;
		return $out;
	}

	public function getUrl($width=NULL,$height=NULL, $exact=TRUE) {
		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$out = $modelsettings::GetInstance()->getFileUrl() . $this->getSlug();
		if(!is_null($width) && !is_null($height)) {
			$type = (!$exact) ? '&resize=maxsize' : '';
			$out .= '?width=' . $width . '&height=' . $height . $type;
		}
		return $out;
	}

	public function getPath() {
		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		return $modelsettings::GetInstance()->getFileStoragePath();
	}

	public function getThumbPath() {
		return $this->getPath() . DIRECTORY_SEPARATOR . 'thumbs';
	}

	public function getFullPath($width=NULL, $height=NULL) {
		if(!is_null($width) && !is_null($height)) {
			$ext = \Pecee\IO\File::GetExtension($this->getFilename());
			return $this->getThumbPath() . DIRECTORY_SEPARATOR . $this->NodeID . sprintf('_%sx%s.%s', $width, $height, $ext);
		}
		return $this->getPath() . DIRECTORY_SEPARATOR . $this->getFilename();
	}

	public static function GetBySlug($slug,$active=NULL) {
		$key = \PC\Helper::GenerateKey('GetBySlug'.self::TYPE_FILE, $slug, $active);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$where=array();
			$where[] = sprintf('(n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
					\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());
			if(!is_null($active)) {
				$where[] ='n.`Active` = ' . Boolean::Parse($active,0);
			}
			$where[] = 'n.`Type` = \''.\Pecee\DB\DB::Escape(self::TYPE_FILE).'\'';
			$where[] = '(SELECT `NodeID` FROM `NodeData` WHERE `NodeID` = n.`NodeID` && `Key` = \'slug\' && `Value` = \''. \Pecee\DB\DB::Escape($slug) .'\')';
			$val = self::FetchOne('SELECT n.* FROM `Node` n WHERE ' . join(' && ', $where));
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	public static function Get($alias=NULL, $query=NULL, $active=NULL, $parentNodeId=NULL, $path = NULL, $order=NULL, $rows=NULL, $page=NULL) {
		$key = \PC\Helper::GenerateKey('Get'.self::TYPE_FILE, $alias,$query,$active,$parentNodeId,$path,$order,$rows,$page);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$order = (is_null($order)) ? self::ORDER_ORDER_ASC : $order;

			$where=array('n.`Type` = \''.self::TYPE_FILE.'\'');

			$where[] = sprintf('(n.`NodeID` = (SELECT `NodeID` FROM `NodeData` nd WHERE nd.`NodeID` = n.`NodeID` && nd.`Key` = \'applicationid\' && nd.`Value` = \'%s\'))',
						\PC\Plugin::Instance()->getPlugin()->application->getApplicationID());

			if(!is_null($active)) {
				$where[] = \Pecee\DB\DB::FormatQuery('n.`Active` = %s', array(Boolean::Parse($active)));
				$where[] = \Pecee\DB\DB::FormatQuery('(ISNULL(n.`ActiveFrom`) && ISNULL(n.`ActiveTo`) || n.`ActiveFrom` <= NOW() && (n.`ActiveTo` >= NOW() || ISNULL(n.`ActiveTo`)))');
			}
			if(!is_null($parentNodeId)) {
				$where[] = "n.`ParentNodeID` = '".\Pecee\DB\DB::Escape($parentNodeId)."'";
			}

			if(!is_null($path)) {
				$where[] = "n.`Path` LIKE '%>".\Pecee\DB\DB::Escape($path)."%'";
			}

			if(!is_null($query)) {
				$where[] = sprintf('(n.`NodeID` IN(SELECT `NodeID` FROM `NodeData` nd WHERE nd.`Value` LIKE \'%s\') OR
						n.`Title` LIKE \'%s\' OR n.`Content` LIKE \'%s\')',
						'%'.\Pecee\DB\DB::Escape($query).'%',
						'%'.\Pecee\DB\DB::Escape($query).'%',
						'%'.\Pecee\DB\DB::Escape($query).'%');
			}

			$order=(!is_null($order) && in_array($order, self::$orders)) ? $order : self::ORDER_ORDER_ASC;

			if(!is_null($alias)) {
				$where[] = 'n.`NodeID` IN (SELECT `NodeID` FROM `NodeData` WHERE `Key` = \'propertynodeid\' && `Value` = (SELECT `NodeID` FROM `NodeData` WHERE `Key` = \'alias\' && `Value` = \''.$alias.'\'))';
			}

			$val = self::FetchPage('n.NodeID', 'SELECT n.* FROM `Node` n WHERE ' . join(' && ', $where) . ' ORDER BY ' . $order, $rows, $page);
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}

	public function getChilds($active=NULL) {
		if(!$this->childs) {
			$this->childs = self::Get(NULL, NULL, $active, $this->NodeID);
		}
		return $this->childs;
	}

	public static function GetByNodeID($nodeId, $active=NULL) {
		$key = \PC\Helper::GenerateKey('GetByNodeId'.self::TYPE_FILE, $nodeId);

		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');

		$val = $cacheclass::GetInstance()->get($key);
		if(is_null($val)) {
			$val = parent::GetByNodeID($nodeId, $active);
			$cacheclass::GetInstance()->set($key, $val, 60*60*24);
		}
		return $val;
	}
}