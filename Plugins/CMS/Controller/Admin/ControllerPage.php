<?php
namespace CMS\Controller\Admin;
use Pecee\Boolean;

class ControllerPage extends \PC\Controller\Admin\ControllerAbstract {

	public function clearcacheJson() {
		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');
		$cacheclass::GetInstance()->clear();
		$this->asJSON(array('result' => TRUE));
	}

	public function generateslugJson() {
		$result = array('result' => FALSE);
		if($this->getPost('nodeId') && $this->getPost('title')) {

			/* @var $modelpage \CMS\Model\ModelPage */
			$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

			$page = $modelpage::GetByNodeID($this->getPost('nodeId'));
			if($page->hasRow()) {
				$page->setTitle($this->getPost('title'));
				$page->generateSlug();
				$result['result'] = TRUE;
				$result['slug'] = trim($page->getSlug(),'/');
			}
		}
		$this->asJSON($result);
	}

	public function cleardraftsView() {
		$result = array('result' => TRUE, 'count' => 0);

		/* @var $modeldraft \CMS\Model\ModelDraft */
		$modeldraft = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelDraft');

		$drafts = $modeldraft::Get($modeldraft::TYPE_DRAFT);
		if($drafts->hasRows()) {
			$i=0;
			foreach($drafts->getRows() as $draft) {
				$i++;
				$draft->delete();
			}
			$result['count'] = $i;
		}
		$this->asJSON($result);
	}

	public function viewdraftView($draftId) {
		if($this->hasParam('mobile')) {
			header('User-agent: Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30');
		}

		/* @var $modeldraft \CMS\Model\ModelDraft */
		$modeldraft = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelDraft');

		$draft = $modeldraft::GetByNodeID($draftId);

		if($draft->hasRow()) {
			\PC\Plugin::Instance()->setIsAdmin(FALSE);
			//$theme = \PC\Plugin::Instance()->getPlugin()->application->getTheme();
			$widget = $draft->getWidget();

			if(class_exists($widget)) {
				$widget = new $widget();
				$widget->setDraft($draft);
				$widget->onSiteRender();
				$widget->renderPage();
				echo $widget;
				die();
			}
		} else {
			die('Not found');
		}
	}

	public function savedraftView($pageId) {
		$result = array('result' => true, 'draftId' => '');

		/* @var $modelpage \CMS\Model\ModelPage */
		$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

		$page = $modelpage::GetByNodeID($pageId);
		if($page->hasRow()) {

			/* @var $modeldraft \CMS\Model\ModelDraft */
			$modeldraft = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelDraft');

			$draft = ($this->getParam('draftId')) ? $modeldraft::GetByNodeID($this->getParam('draftId')) : new $modeldraft();
			$draft->setUserID(\Pecee\Model\User\ModelUser::Current()->getUserID());
			$draft->setSlug($this->getPost('slug'));
			$draft->setOriginalNodeId($pageId);
			$draft->setTitle($this->getPost('title'));
			$draft->setWidget($this->getPost('widget'));
			$draft->setActive(Boolean::Parse($this->getPost('active')));

			if($this->getPost('activeFrom')) {
				$draft->setActiveFrom(\Pecee\Date::ToDateTime(strtotime($this->getPost('activeFrom'))));
			}

			if($this->getPost('activeTo')) {
				$draft->setActiveTo(\Pecee\Date::ToDateTime(strtotime($this->getPost('activeTo'))));
			}

			$draft->setVisibleInMenu(Boolean::Parse($this->getPost('visibleInMenu')));
			$draft->setMetaTitle($this->getPost('metaTitle'));
			$draft->setMetaDescription($this->getPost('metaDescription'));
			$draft->setMetaKeywords($this->getPost('metaKeywords'));

			$modules = $page->getProperty()->getModules();
			if(count($modules) > 0) {
				/* @var $module \CMS\Model\Property\PropertyModule */
				foreach($modules as $module) {
					$controlId = $module->ControlID;
					try {
						$m = $module->getModule();
						$m = new $m();
						$m->renderAdmin($module, $draft);
						$m->onAdminRender();
						$m->__toString();
					}catch(\Exception $e) { }
				}
			}
		}

		if($this->getParam('draftId', FALSE)) {
			$draft->update();
		} else {
			$draft->save();
		}

		$result['draftId'] = $draft->getNodeID(TRUE);

		$this->asJSON($result);
	}

	public function editView($pageId) {
		echo new \CMS\Widget\Admin\Page\PageEdit($pageId);
	}

	public function searchView() {
		$result=array('result' => FALSE, 'nodes' => array());
		/* @var $modelpage \CMS\Model\ModelPage */
		$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

		$pages = $modelpage::Get(NULL, $this->getParam('query'), NULL, NULL, NULL, NULL, 10, 0);
		if($pages->hasRows()) {
			foreach($pages->getRows() as $page) {
				$result['nodes'][] = array('id' => $page->getNodeID(), 'title' => $page->getTitle(), 'slug' => $page->getSlug(), 'icon' => $page->getProperty()->getIcon());
			}
		}
		$this->asJSON($result);
	}

	public function treeView() {
		$parentNodeId = $this->getParam('nodeId');
		$result=array('result' => FALSE, 'nodes' => array());
		/* @var $modelpage \CMS\Model\ModelPage */
		$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');
		$pages = $modelpage::Get(NULL, NULL, NULL, $parentNodeId, NULL, NULL, NULL);
		if($pages->hasRows()) {
			foreach($pages->getRows() as $page) {
				$properties = (is_array($page->getProperty()->getProperties())) ? join(',', $page->getProperty()->getProperties()) : '';
				$result['nodes'][] = array('id' => $page->getNodeID(), 'title' => $page->getTitle(), 'liAttrs' => array('data-accepts' => $properties, 'data-propertyId' => $page->getProperty()->getNodeID()), 'linkAttrs' => array('data-id' => $page->getNodeID(), 'data-slug' => $page->getSlug(), 'class' => 'js-page'), 'icon' => $page->getProperty()->getIcon());
				//$result['nodes'][] = array('id' => $page->getNodeID(), 'title' => $page->getTitle(), 'propertyId' => $page->getProperty()->getNodeID(), 'properties' => $properties, 'slug' => $page->getSlug(), 'icon' => $page->getProperty()->getIcon(), 'classname' => 'js-page');
			}
		}
		$this->asJSON($result);
	}

	public function setparentView() {
		$result=array('result' => FALSE);
		if($this->getPost('node')) {

			$i=0;
			foreach($this->getPost('node') as $nodeId=>$parent) {
				$parent = (strtolower(trim($parent, ':')) == 'null') ? 0 : $parent;
				if($this->getParam('parentNodeId') == $parent) {
					/* @var $modelpage \CMS\Model\ModelPage */
					$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

					$node = $modelpage::GetByNodeID($nodeId);
					if($node->hasRow()) {
						$node->setParentNodeID($parent);
						$node->setOrder($i);
						$node->updateWithSlug();
					}
				}
				$i++;
			}
			$result['result'] = TRUE;
		}
		$this->asJSON($result);
	}

	public function deletepageView() {
		$result=array('result' => FALSE);
		/* @var $modelpage \CMS\Model\ModelPage */
		$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');
		$node = $modelpage::GetByNodeID($this->getPost('nodeId'));
		if($node->hasRow()) {
			$node->delete();
			$result['result']=true;
		}
		$this->asJSON($result);
	}
}