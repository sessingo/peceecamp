<?php
namespace CMS\Controller\Admin;
class ControllerUser extends \PC\Controller\Admin\ControllerAbstract {

	public function clearcacheJson() {
		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');
		$cacheclass::GetInstance()->clear();
		$this->asJSON(array('result' => TRUE));
	}

	public function indexView() {
		echo new \CMS\Widget\Admin\User\UserHome();
	}

	public function editView($userId) {
		echo new \CMS\Widget\Admin\User\UserEdit($userId);
	}

	public function deletepageView() {
		$result=array('result' => FALSE);

		/* @var $modeluser \CMS\Model\ModelUser */
		$modeluser = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelUser');

		$node = $modeluser::GetByNodeID($this->getPost('nodeId'));
		if($node->hasRow()) {
			$node->delete();
			$result['result']=true;
		}
		$this->asJSON($result);
	}

	public function searchView() {
		$result=array('result' => FALSE, 'nodes' => array());
		/* @var $modeluser \CMS\Model\ModelUser */
		$modeluser = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelUser');

		$users = $modeluser::Get(NULL, $this->getParam('query'), NULL, NULL, NULL, NULL, 10, 0);
		if($users->hasRows()) {
			foreach($users->getRows() as $user) {
				$result['nodes'][] = array('id' => $user->getNodeID(), 'title' => $user->getUsername(), 'icon' => $user->getProperty()->getIcon());
			}
		}
		$this->asJSON($result);
	}

	public function treeView() {
		$parentNodeId = $this->getParam('nodeId');
		$result=array('result' => FALSE, 'nodes' => array());
		/* @var $modeluser \CMS\Model\ModelUser */
		$modeluser = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelUser');

		$users = $modeluser::Get(NULL, NULL, NULL, $parentNodeId, NULL, NULL, NULL);
		if($users->hasRows()) {
			foreach($users->getRows() as $user) {
				$properties = (is_array($user->getProperty()->getProperties())) ? join(',', $user->getProperty()->getProperties()) : '';
				$result['nodes'][] = array('id' => $user->getNodeID(), 'title' => $user->getUsername(), 'liAttrs' => array('data-accepts' => $properties, 'data-propertyId' => $user->getProperty()->getNodeID()), 'linkAttrs' => array('data-id' => $user->getNodeID(), 'data-slug' => $user->getSlug(), 'class' => 'js-user'), 'icon' => $user->getProperty()->getIcon());
				//$result['nodes'][] = array('id' => $user->getNodeID(), 'title' => $user->getUsername(), 'propertyId' => $user->getProperty()->getNodeID(), 'properties' => $properties, 'icon' => $user->getProperty()->getIcon(), 'classname' => 'js-user');
			}
		}
		$this->asJSON($result);
	}

	public function setparentView() {
		$result=array('result' => FALSE);
		if(($this->getPost('node') || $this->getPost('user'))) {

			if(count($this->getPost('node')) > 0) {
				$i = 0;

				/* @var $userproperty \CMS\Model\User\UserProperty */
				$userproperty = \PC\ObjectManager::GetInstance()->get('\CMS\Model\User\UserProperty');

				foreach($this->getPost('node') as $nodeId=>$parent) {
					$parent = (strtolower(trim($parent, ':')) == 'null') ? 0 : $parent;
					if($this->getParam('parentNodeId') == $parent) {
						$node = $userproperty::GetByNodeID($nodeId);
						if($node->hasRow()) {
							$node->setParentNodeID($parent);
							$node->setOrder($i);
							$node->update();
						}
					}
					$i++;
				}
			}

			if(count($this->getPost('user')) > 0) {
				foreach($this->getPost('user') as $nodeId=>$parent) {
					$parent = (strtolower(trim($parent, ':')) == 'null') ? 0 : $parent;
					/* @var $modeluser \CMS\Model\ModelUser */
					$modeluser = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelUser');

					$node = $modeluser::GetByNodeID($nodeId);
					if($node->hasRow()) {
						$node->setParentNodeID($parent);
						$node->update();
					}
				}
			}
			$result['result'] = TRUE;
		}
		$this->asJSON($result);
	}
}