<?php
namespace CMS\Controller\Admin;
class ControllerUrl extends \PC\Controller\Admin\ControllerAbstract {
	public function deleteView($rewriteId) {
		echo new \CMS\Widget\Admin\Url\UrlDelete($rewriteId);
	}
}