<?php
namespace CMS\Controller\Admin;
class ControllerPageproperty extends \PC\Controller\Admin\ControllerAbstract {
	public function indexView() {
		echo new \CMS\Widget\Admin\Page\Property\PropertyHome();
	}
	public function editView($propertyId) {
		echo new \CMS\Widget\Admin\Page\Property\PropertyEdit($propertyId);
	}

	public function deletemoduleView($propertyModuleId){
		echo new \CMS\Widget\Admin\Page\Property\Module\ModuleDelete($propertyModuleId);
	}

	public function deletetabView($propertyTabId){
		echo new \CMS\Widget\Admin\Page\Property\Tab\TabDelete($propertyTabId);
	}

	public function setparentView() {
		$result=array('result' => false);
		if($this->getPost('node')) {
			$i=0;
			foreach($this->getPost('node') as $nodeId=>$parent) {
				$parent = (strtolower(trim($parent, ':')) == 'null') ? 0 : $parent;
				if($this->getParam('parentNodeId') == $parent) {

					/* @var $propertymodel \CMS\Model\ModelProperty */
					$propertymodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');

					$node = $propertymodel::GetByNodeID($nodeId);
					if($node->hasRow()) {
						$node->setParentNodeID($parent);
						$node->setOrder($i);
						$node->update();
					}
				}
				$i++;
			}
			$result['result'] = TRUE;
		}
		$this->asJSON($result);
	}

	public function deleteView() {
		$result=array('result' => false);

		/* @var $propertymodel \CMS\Model\ModelProperty */
		$propertymodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');

		$node = $propertymodel::GetByNodeID($this->getPost('nodeId'));
		if($node->hasRow()) {
			/* @var $modelpage \CMS\Model\ModelPage */
			$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');
			$pages = $modelpage::GetByPropertyNodeId($node->getNodeID());
			if($pages->hasRow()) {
				foreach($pages->getRows() as $page) {
					$page->delete();
				}
			}
			$node->delete();
			$result['result'] = TRUE;
		}
		$this->asJSON($result);
	}

	public function ordertabsView() {
		$result=array('result' => false);
		if($this->getParam('nodeId') && $this->getPost('ids')) {

			/* @var $propertytab \CMS\Model\Property\PropertyTab */
			$propertytab = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyTab');

			foreach($this->getPost('ids') as $i=>$propertyTabId) {
				$tab = $propertytab::GetByPropertyTabID($propertyTabId);
				if($tab->hasRow()) {
					if($tab->getNodeID() == $this->getParam('nodeId')) {
						$tab->setOrder($i);
						$tab->update();
					}
				}
			}
			$result['result'] = TRUE;
		}
		$this->asJSON($result);
	}

	public function ordermodulesView() {
		$result=array('result' => false);
		if($this->getParam('nodeId') && $this->getPost('ids')) {

			/* @var $propertymodule \CMS\Model\Property\PropertyModule */
			$propertymodule = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyModule');

			foreach($this->getPost('ids') as $i=>$propertyModuleId) {
				$module = $propertymodule::GetByPropertyModuleId($propertyModuleId);
				if($module->hasRow()) {
					if($module->getNodeID() == $this->getParam('nodeId')) {
						$module->setOrder($i);
						$module->update();
					}
				}
			}
			$result['result']=TRUE;
		}
		$this->asJSON($result);
	}

	public function treeView() {
		$parentNodeId = $this->getParam('nodeId');
		$result=array('result' => FALSE, 'nodes' => array());
		/* @var $modelproperty \CMS\Model\ModelProperty */
		$modelproperty = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');

		$properties = $modelproperty::Get(NULL, NULL, $parentNodeId, NULL, NULL, NULL);
		if($properties->hasRows()) {
			foreach($properties->getRows() as $property) {
				$result['nodes'][] = array('id' => $property->getNodeID(), 'title' => $property->getTitle(), 'liAttrs' => NULL, 'linkAttrs' => array('data-id' => $property->getNodeID(), 'class' => 'js-page'), 'icon' => $property->getIcon());
			}
		}
		$this->asJSON($result);
	}
}