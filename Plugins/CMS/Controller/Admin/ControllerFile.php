<?php
namespace CMS\Controller\Admin;
class ControllerFile extends \PC\Controller\Admin\ControllerAbstract {

	public function clearcacheJson() {
		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');
		$cacheclass::GetInstance()->clear();
		$this->asJSON(array('result' => TRUE));
	}

	public function indexView() {
		echo new \CMS\Widget\Admin\File\FileHome();
	}

	public function searchView() {
		$result=array('result' => false, 'files' => array());
		/* @var $modelfile \CMS\Model\ModelFile */
		$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

		$files = $modelfile::Get(NULL,$this->getParam('query'), NULL, NULL, NULL, NULL, NULL, 10, 0);
		if($files->hasRows()) {
			foreach($files->getRows() as $file) {
				$result['files'][] = array('id' => $file->getNodeID(), 'title' => $file->getTitle(), 'icon' => 'application.png', 'url' => $file->getSlug());
			}
		}
		$this->asJSON($result);
	}

	public function deletefileView() {
		$result=array('result' => false);

		/* @var $modelfile \CMS\Model\ModelFile */
		$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

		$node = $modelfile::GetByNodeID($this->getPost('nodeId'));
		if($node->hasRow()) {
			$node->delete();
			$result['result'] = TRUE;
		}
		$this->asJSON($result);
	}

	public function deletefolderView() {
		$result=array('result' => false);

		/* @var $modelfolder \CMS\Model\ModelFolder */
		$modelfolder = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFolder');

		$node = $modelfolder::GetByNodeID($this->getPost('nodeId'));
		if($node->hasRow()) {
			$node->delete();
			$result['result'] = TRUE;
		}
		$this->asJSON($result);
	}

	public function treeView() {
		$parentNodeId = $this->getParam('nodeId');
		$result=array('result' => false, 'nodes' => array());

		/* @var $modelfolder \CMS\Model\ModelFolder */
		$modelfolder = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFolder');

		$folders = $modelfolder::Get(NULL, NULL, $parentNodeId, NULL, NULL, NULL);
		if($folders->hasRows()) {
			foreach($folders->getRows() as $folder) {
				$result['nodes'][] = array('id' => $folder->getNodeID(), 'title' => $folder->getTitle(), 'linkAttrs' => array('href' => 'javascript:;', 'data-id' => $folder->getNodeID(), 'class' => 'js-folder'), 'icon' => 'folder.png');
				//$result['nodes'][] = array('id' => $folder->getNodeID(), 'title' => $folder->getTitle(), 'icon' => 'folder.png', 'class' => 'js-folder');
			}
		}

		/* @var $modelfile \CMS\Model\ModelFile */
		$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

		$files = $modelfile::Get(NULL, NULL, NULL, $parentNodeId);
		if($files->hasRows()) {
			foreach($files->getRows() as $file) {
				$result['nodes'][] = array('id' => $file->getNodeID(), 'title' => $file->getTitle(), 'linkAttrs' => array('href' => $file->getUrl(), 'data-id' => $file->getNodeID(), 'class' => 'js-file js-overlay'), 'icon' => 'application.png');
				//$result['nodes'][] = array('id' => $file->getNodeID(), 'title' => $file->getTitle(), 'icon' => 'file.png', 'classname' => 'js-file', 'attrs' => 'data-id="'.$file->getNodeID().'" data-href="'.$file->getUrl().'"');
			}
		}

		$this->asJSON($result);
	}

	public function setparentView() {
		$result=array('result' => false);
		if(($this->getPost('node') || $this->getPost('file'))) {

			if(count($this->getPost('node')) > 0) {
				$i = 0;
				foreach($this->getPost('node') as $nodeId=>$parent) {
					$parent = (strtolower(trim($parent, ':')) == 'null') ? 0 : $parent;
					if($this->getParam('parentNodeId') == $parent) {

						/* @var $modelfolder \CMS\Model\ModelFolder */
						$modelfolder = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFolder');

						$node = $modelfolder::GetByNodeID($nodeId);
						if($node->hasRow()) {
							$node->setParentNodeID($parent);
							$node->setOrder($i);
							$node->update();
						}
					}
					$i++;
				}
			}

			if(count($this->getPost('file')) > 0) {
				foreach($this->getPost('file') as $nodeId=>$parent) {
					$parent = (strtolower(trim($parent, ':')) == 'null') ? 0 : $parent;

					/* @var $modelfile \CMS\Model\ModelFile */
					$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

					$node = $modelfile::GetByNodeID($nodeId);
					if($node->hasRow()) {
						$node->setParentNodeID($parent);
						$node->update();
					}
				}
			}
			$result['result']=true;
		}
		$this->asJSON($result);
	}

	public function deleteView($fileId) {
		$file = \Pecee\Model\File\ModelFile::GetById($fileId);
		if($file->hasRow()) {

			/* @var $modelsettings \CMS\Model\ModelSettings */
			$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

			$file->setPath($modelsettings::GetInstance()->getFileStoragePath());
			$file->delete();
		}
		\PC\Router::GoBack();
	}

	public function douploadView() {
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$result=array('result' => 'false', 'msg' => '', 'code' => 0);

		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$settings = $modelsettings::GetInstance();

		ini_set('upload_max_size', $settings->getMaxFilesize());
		ini_set('post_max_size', $settings->getMaxFilesize());

		// 10 minutes execution time
		@set_time_limit(60 * 60 * 2);

		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$filename = isset($_REQUEST["name"]) ? $_REQUEST["name"] : NULL;

		session_id($this->getParam('id'));

		if(count($_FILES) > 0 && $filename && isset($_FILES['file']['tmp_name'])
		&& is_uploaded_file($_FILES['file']['tmp_name']) &&
		file_exists($_FILES['file']['tmp_name'])) {

			$filename=$_FILES['file']['name'];
			$tmp=$_FILES['file']['tmp_name'];
			$filesize=filesize($tmp);
			$result['filename']=$filename; // Add filename to JSON array

			if(($filesize/1024/1024) > $settings->getMaxFilesize()) {
				$result['msg']=$this->_('File/LargerThanAllowed');
			} else {
				$mime = \Pecee\IO\File::GetMimeType($tmp);
				$fileExtension = \Pecee\IO\File::GetExtension($filename);

				if(in_array(strtolower($fileExtension), $settings->getFileTypes())) {
					$userId=\Pecee\Session::Instance()->get('UserID');

					try {

						/* @var $modelfile \CMS\Model\ModelFile */
						$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

						$file = new $modelfile();
						$file->save(); // get node id

						$file->setFilename($file->getNodeID() . '.'. $fileExtension);
						$file->setTitle($filename);
						$file->setMime(\Pecee\IO\File::GetMimeType($tmp));
						$file->setBytes(filesize($tmp));
						$file->setUserID($userId);
						$file->setParentNodeID($this->getParam('parentNodeId'));

						$size = getimagesize($tmp);
						if(isset($size[0]) && isset($size[1])) {
							$file->setWidth($size[0]);
							$file->setHeight($size[1]);
						}

						$file->setIpaddress(\Pecee\Server::GetRemoteAddr());

						if(move_uploaded_file($tmp, $file->getFullPath())) {
							$file->update();
							$result['result']='true';
						}else{
							$result['msg']=$this->_('File/FailedUpload');
						}
					}catch (\Exception $e) {
						$result['result']=false;
						$result['msg']=$e->getMessage();
						$result['code']=$e->getCode();
					}
				} else {
					$result['msg']=$this->_('File/InvalidFileType', $filename, join(', ', $settings->getFileTypes()));
				}
			}
		}
		// Return JSON-RPC response
		return $this->asJSON($result);
	}

	public function getimagesView() {
		session_id($this->getParam('id'));
		$results=array();

		/* @var $modelfile \CMS\Model\ModelFile */
		$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

		$files = $modelfile::Get(NULL, NULL, NULL, NULL, NULL, $modelfile::ORDER_DATE_DESC);
		if($files->hasRows()) {
			/* @var $image \CMS\Model\ModelFile */
			$limit = 200;
			foreach($files->getRows() as $i=>$file) {
				$ext = \Pecee\IO\File::GetExtension($file->getFullPath());
				if(in_array($ext, array('jpg','png','jpeg','bmp','gif'))) {
					$results[] = array('thumb' => $file->getUrl(100,100), 'image' => $file->getUrl(), 'folder' => 'Images');
				}
				if($i > $limit) {
					break;
				}
			}
		}
		$this->asJSON($results);
	}

	public function uploadView() {
		session_id($this->getParam('id'));
		if(count($_FILES) > 0) {
			$att=(isset($_FILES['file'])) ? $_FILES['file'] : NULL;
			if(!is_null($att) && is_file($att['tmp_name'])) {

				/* @var $modelfile \CMS\Model\ModelFile */
				$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

				$file = new $modelfile();

				if($this->getParam('type') == 'image') {
					$size = getimagesize($att['tmp_name']);
					$file->setWidth($size[0]);
					$file->setHeight($size[1]);
				}

				$fileExtension = \Pecee\IO\File::GetExtension($att['name']);

				$file->setMime(\Pecee\IO\File::GetMimeType($att['tmp_name']));
				$file->setBytes(filesize($att['tmp_name']));
				$file->setIPAddress(\Pecee\Server::GetRemoteAddr());
				$file->setUserId(\Pecee\Model\User\ModelUser::Current()->getUserID());
				$file->setFilename($file->getFileID() . '.'. $fileExtension);
				$file->setTitle($att['name']);

				\Pecee\IO\File::CreatePath($file->getThumbPath());

				if(move_uploaded_file($att['tmp_name'], $file->getFullPath())) {
					$file->save();
					$this->asJSON(array('filelink' => $file->getUrl()));
				}
			}
		}
		return '';
	}
}