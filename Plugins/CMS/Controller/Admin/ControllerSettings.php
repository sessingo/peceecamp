<?php
namespace CMS\Controller\Admin;
class ControllerSettings extends \PC\Controller\Admin\ControllerAbstract {
	public function indexView() {
		echo new \CMS\Widget\Admin\Settings\SettingsHome();
	}

	public function cleanupCacheJson() {
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');
		$cache = $cacheclass::GetInstance()->getClass();

		if($cache instanceof \PC\Cache\CacheFile) {
			$cache->cleanup();
		}

		$this->asJSON(array('result' => TRUE));
	}
}