<?php
namespace CMS\Controller\Admin;
class ControllerLanguage extends \PC\Controller\Admin\ControllerAbstract {
	public function indexView($languageId=NULL) {
		echo new \CMS\Widget\Admin\Language\LanguageHome($languageId);
	}

	public function editView($keyId) {
		echo new \CMS\Widget\Admin\Language\KeyEdit($keyId);
	}

	public function deleteKeyJson() {
		$result=array('result' => FALSE);
		/* @var $languagekey \CMS\Model\Language\LanguageKey */
		$languagekey = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Language\LanguageKey');
		$key = $languagekey::GetByID($this->getPost('languageKeyId'));
		if($key->hasRow()) {
			$key->delete();
			$result['result'] = TRUE;
		}
		$this->asJSON($result);
	}

	public function deleteLangJson() {
		$result=array('result' => FALSE);

		/* @var $languagemodel \CMS\Model\ModelLanguage */
		$languagemodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelLanguage');

		$lang = $languagemodel::GetById($this->getPost('languageId'));
		if($lang->hasRow()) {
			$lang->delete();
			$result['result'] = TRUE;
		}
		$this->asJSON($result);
	}

	public function setParentJson() {
		$result=array('result' => FALSE);

		/* @var $languagekey \CMS\Model\Language\LanguageKey */
		$languagekey = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Language\LanguageKey');

		$originalKey = $languagekey::GetByID($this->getPost('languageKeyId'));
		if($originalKey->hasRow()) {
			$nameExist = FALSE;
			$keys = $languagekey::Get($this->getPost('parentLanguageKeyId'));
			if($keys->hasRows()) {
				foreach($keys->getRows() as $key) {
					if($key->getName() == $originalKey->getName() && $key->getLanguageKeyID() != $originalKey->getLanguageKeyID()) {
						$nameExist = TRUE;
						break;
					}
				}
			}

			if(!$nameExist) {
				$originalKey->setParentLanguageKeyID($this->getPost('parentLanguageKeyId'));
				$originalKey->update();
				$result['result'] = TRUE;
			}
		}
		$this->asJSON($result);
	}

	public function searchView() {
		$result=array('result' => FALSE, 'keys' => array());
		/* @var $languagekey \CMS\Model\Language\LanguageKey */
		$languagekey = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Language\LanguageKey');

		$keys = $languagekey::Get()->getArray();
		if(count($keys) > 0) {
			$out = array();
			foreach($keys as $index=>$key) {
				if(stripos($index,$this->getParam('query')) === 0) {
					$out[] = $key;
				}
			}
			foreach($out as $key) {
				$result['keys'][] = array('id' => $key->getLanguageKeyID(), 'name' => $key->getFullKey());
			}
		}
		$this->asJSON($result);
	}
}