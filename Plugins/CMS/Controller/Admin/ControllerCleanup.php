<?php
namespace CMS\Controller\Admin;
class ControllerCleanup extends \PC\Controller\Admin\ControllerAbstract {
	public function cleanupJson() {
		$result = array('result' => TRUE);
		$nodes = \Pecee\Model\Node\ModelNode::Get();
		$deletedNodes = array();
		if($nodes->hasRows()) {
			foreach($nodes->getRows() as $node) {
				// Find nodes with deleted property node
				$property = $node->data->propertynodeid;
				if($property) {
					$property = \Pecee\Model\Node\ModelNode::GetByNodeID($property);
					if(!$property->hasRow()) {
						if(!in_array($node->getNodeID(),$deletedNodes)) {
							$deletedNodes[] = $node->getNodeID();
							$node->delete();
						}
					}
				}

				// Find nodes with non existing parent
				$path = explode('>', $node->getPath());
				if(count($path) > 0) {
					foreach($path as $p) {
						if($p > 0) {
							$property = \Pecee\Model\Node\ModelNode::GetByNodeID($p);
							if(!$property->hasRow()) {
								if(!in_array($node->getNodeID(),$deletedNodes)) {
									$deletedNodes[] = $node->getNodeID();
									$node->delete();
								}
							}
						}
					}
				}
			}
		}

		/* @var $modeldraft \CMS\Model\ModelDraft */
		$modeldraft = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelDraft');

		$nodeData = \Pecee\DB\DB::GetInstance()->listTableArray('SELECT n.* FROM `Node` n WHERE n.`Type` = %s &&
				IFNULL((SELECT `NodeID` FROM `NodeData` WHERE `NodeID` = n.`NodeID` && `Key` = \'originalnodeid\' && `Value`
				NOT IN(SELECT `NodeID` FROM `Node`)), FALSE)', NULL, NULL, $modeldraft::TYPE_DRAFT);

		if(count($nodeData) > 0) {
			$nodeIds = array();
			foreach($nodeData as $node) {
				$nodeIds[] = $node->NodeID;
			}

			$drafts = $modeldraft::GetByNodeIDs($nodeIds);
			if($drafts->hasRows()) {
				foreach($drafts->getRows() as $draft) {
					$draft->delete();
				}
			}
		}

		// Find nodedata with no connection to node
		$nodeData = \Pecee\DB\DB::GetInstance()->listTableArray('SELECT * FROM `NodeData` WHERE `NodeID` NOT IN(SELECT `NodeID` FROM `Node`)');
		if(count($nodeData) > 0) {
			\Pecee\DB\DB::GetInstance()->nonQuery('DELETE FROM `NodeData` WHERE `NodeID` NOT IN(SELECT `NodeID` FROM `Node`)');
		}

		$this->asJSON($result);
	}

	public function analyseJson() {
		$result = array('deletedProperty' => array(), 'deletedParent' => array(), 'deletedDraft' => 0, 'lostNodeData' => 0);
		$deletedNodes = array();
		$total = 0;
		$nodes = \Pecee\Model\Node\ModelNode::Get();
		if($nodes->hasRows()) {
			foreach($nodes->getRows() as $node) {
				// Find nodes with deleted property node
				$property = $node->data->propertynodeid;
				if($property) {
					$property = \Pecee\Model\Node\ModelNode::GetByNodeID($property);
					if(!$property->hasRow()) {
						if(!in_array($node->getNodeID(), $deletedNodes)) {
							$deletedNodes[] = $node->getNodeID();
							$result['deletedProperty'][] = $node->getNodeID();
							$total++;
						}
					}
				}

				// Find nodes with non existing parent
				$path = explode('>', $node->getPath());
				if(count($path) > 0) {
					foreach($path as $p) {
						if($p > 0) {
							$property = \Pecee\Model\Node\ModelNode::GetByNodeID($p);
							if(!$property->hasRow()) {
								if(!in_array($node->getNodeID(), $deletedNodes)) {
									$deletedNodes[] = $node->getNodeID();
									$result['deletedParent'][] = $node->getNodeID();
									$total++;
								}
							}
						}
					}
				}
			}
		}

		/* @var $modeldraft \CMS\Model\ModelDraft */
		$modeldraft = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelDraft');

		$nodeData = \Pecee\DB\DB::GetInstance()->listTableArray('SELECT n.* FROM `Node` n WHERE n.`Type` = %s &&
				IFNULL((SELECT `NodeID` FROM `NodeData` WHERE `NodeID` = n.`NodeID` && `Key` = \'originalnodeid\' && `Value`
				NOT IN(SELECT `NodeID` FROM `Node`)), FALSE)', NULL, NULL, $modeldraft::TYPE_DRAFT);

		if(count($nodeData) > 0) {
			$total += count($nodeData);
			$result['deletedDraft'] = count($nodeData);
		}

		// Find nodedata with no connection to node
		$nodeData = \Pecee\DB\DB::GetInstance()->listTableArray('SELECT * FROM `NodeData` WHERE `NodeID` NOT IN(SELECT `NodeID` FROM `Node`)');
		if(count($nodeData) > 0) {
			$total += count($nodeData);
			$result['lostNodeData'] = count($nodeData);
		}


		$result['total'] = $total;

		$this->asJSON($result);
	}
}