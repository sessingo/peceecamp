<?php
namespace CMS\Controller\Admin;
class ControllerMacro extends \PC\Controller\Admin\ControllerAbstract {
	public function deleteJson() {
		$result=array('result' => FALSE);

		/* @var $macromodel \CMS\Model\ModelMacro */
		$macromodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelMacro');

		$macro = $macromodel::GetById($this->getPost('nodeId'));
		if($macro->hasRow()) {
			$macro->delete();
			$result['result'] = TRUE;
		}
		$this->asJSON($result);
	}
}