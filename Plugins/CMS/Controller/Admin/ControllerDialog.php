<?php
namespace CMS\Controller\Admin;
class ControllerDialog extends \PC\Controller\Admin\ControllerAbstract {
	public function viewView() {
		$args=func_get_args();
		if(count($args) > 0) {
			$args=array_map('ucfirst', $args);
			$widget = sprintf('\\CMS\\Widget\\Dialog\\%s', join('\\', $args));
			if(class_exists($widget)) {
				$class=\PC\Router::LoadClass($widget);
				echo $class;
				return;
			}
			throw new \ErrorException('Dialog '.$widget.' not yet implented');
		}
	}
}