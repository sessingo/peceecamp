<?php
namespace CMS\Controller\Admin;
class ControllerAjax extends \PC\Controller\Admin\ControllerAbstract {
	public function deletefileView() {
		$result=array('result' => false);

		/* @var $modelfile \CMS\Model\ModelFile */
		$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

		$file = $modelfile::GetByNodeID($this->getPost('nodeId'));
		if($file->hasRow()) {
			$file->delete();
			$result['result'] = true;
		}
		$this->asJSON($result);
	}
}