<?php
namespace CMS\Controller\Site;
class ControllerDefault extends \PC\Controller\ControllerAbstract {
	public static $allowedFileTypes=array('jpg','jpeg','png','gif','bmp');
	protected function get404Page() {
		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$settings = $modelsettings::GetInstance();
		if($settings->get404NodeId()) {
			/* @var $modelpage \CMS\Model\ModelPage */
			$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');
			$page = $modelpage::GetByNodeID($settings->get404NodeId());
			if($page->hasRow()) {
				$widget = $this->renderPage($page);
				if($widget) {
					echo $widget;
					die();
				}
			}
		}

		echo new \CMS\Widget\Page\PageError('Page not found', 'We couldn\'t find the page you were looking for.<br/>This is the default 404-template which is visible when no other default 404-page has been configured. You can choose you\'re own by editing the settings for your site.');
	}

	protected function renderPage(\CMS\Model\ModelPage $page) {
		if($page->getActive()) {
			$widget = $page->getWidget();

			\Pecee\Debug::Instance()->add('CMS Plugin: checking custom themes by url');

			// Initialize themes
			/* @var $modelsettings \CMS\Model\ModelSettings */
			$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

			$themes = $modelsettings::GetInstance()->getThemes();
			if(isset($themes[$_SERVER['HTTP_HOST']])) {
				$theme = $themes[$_SERVER['HTTP_HOST']];
				$widget = str_ireplace('\\'.\PC\Plugin::Instance()->getPlugin()->application->getTheme().'\\', '\\'.$theme.'\\', $widget);
				\PC\Plugin::Instance()->getPlugin()->application->setTheme($theme);
			}
			if(class_exists($widget)) {
				$widget = new $widget();
				$widget->setPage($page);
				$widget->onSiteRender();
				$widget->renderPage();
				echo $widget;
				die();
			} else {
				echo new \CMS\Widget\Page\PageError('This page currently has no template configured', 'We\'re sorry - but this page currently doesn\'t have any template configured.<br/><br/>To add a template, simply edit the page and choose a widget to render. If no widgets are visible, remember to add visible widgets on the page-property page.');
				die();
			}
		}
		return NULL;
	}

	public function __call($name, $args){
		$this->indexView();
	}

	protected function showFile($slug) {
		$width = $this->getParam('width');
		$height = $this->getParam('height');

		/* @var $modelfile \CMS\Model\ModelFile */
		$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$file = $modelfile::GetBySlug($slug, TRUE);
		if($file->hasRow()) {
			if($this->hasParam('download')) {
				header('Content-Disposition: attachment; filename="'.$file->getOriginalFilename().'"');
			}
			if(in_array(\Pecee\IO\File::GetExtension($file->getOriginalFilename()), self::$allowedFileTypes)) {
				header(sprintf('Content-type: %s', $file->getMime()));

				$saveThumb = $modelsettings::GetInstance()->getCreateThumbs();

				if($this->getParam('type')) {
					$thumbsize = $modelsettings::GetInstance()->getThumbnailType($this->getParam('type'));
					if(is_array($thumbsize)) {
						$width = $thumbsize[0];
						$height = $thumbsize[1];
						$saveThumb = TRUE;
					}
				}

				if(!is_null($width) && !is_null($height) && ($width < $file->getWidth() || $height < $file->getHeight())) {
					if(!file_exists($file->getFullPath($width, $height))) {
						$image = new \Pecee\Image(file_get_contents($file->getFullPath()), $file->getMime());
						if(strtolower($this->getParam('resize')) == 'maxsize') {
							$thumb = $image->getResizedMaxSize($width, $height);
						} else {
							$thumb = $image->getResizedExact($width, $height);
						}

						if($saveThumb) {
							\Pecee\IO\File::CreatePath($file->getThumbPath());
							$handle = fopen($file->getFullPath($width,$height), 'w+');
							fwrite($handle, $thumb);
							fclose($handle);
						}

						echo $thumb;
						die();
					} else {
						echo readfile($file->getFullPath($width,$height));
						die();
					}
				}
			}

			header('Content-type: ' . $file->getMime());
			$file->setPath($modelsettings::GetInstance()->getFileStoragePath());
			die(readfile($file->getFullPath()));
		}
	}

	public function indexView() {
		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$settings = $modelsettings::GetInstance();

		/* @var $modelpage \CMS\Model\ModelPage */
		$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

		if($settings->getHasRewrites()) {
			\Pecee\Debug::Instance()->add('CMS Plugin: checking custom rewrite rules');
			// Check for url-rewrite rule

			/* @var $rewritemodel \CMS\Model\ModelRewrite */
			$rewritemodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelRewrite');

			$url = $rewritemodel::GetByOriginalUrl(rtrim(\PC\Router::GetCurrentRoute(TRUE,FALSE), '/'), $_SERVER['HTTP_HOST']);
			if($url->hasRow()) {
				if($url->getNodeID()) {
					$this->renderPage($url->getNode());
				} else {
					\PC\Router::Redirect($url->getRewriteUrl());
				}
			}
		}

		\Pecee\Debug::Instance()->add('CMS Plugin: checking url is media-url');
		if(strpos(\PC\Router::GetCurrentRoute(), $settings->getFileUrl()) === 0) {
			$slug = substr(\PC\Router::GetCurrentRoute(TRUE,FALSE), strlen($settings->getFileUrl()));
			$this->showFile($slug);
		}

		$slug = explode('?', \PC\Router::GetCurrentRoute());
		$slug = trim($slug[0],'/');

		if($slug) {
			/* @var $modelpage \CMS\Model\ModelPage */
			$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

			if($settings->getUsingHostnames() && $settings->getRootSlug()) {
				\Pecee\Debug::Instance()->add('CMS Plugin: found root node with slug "'.$settings->getRootSlug().'"');

				if(stripos($slug, $settings->getRootSlug()) !== 0 || substr($slug, 0, strlen($settings->getRootSlug())) != $settings->getRootSlug()) {
				    $slug = $settings->getRootSlug() .'/' . $slug;
				}
			}
			$page = $modelpage::GetBySlug($slug);
			if($page->hasRow()) {
				\Pecee\Debug::Instance()->add('CMS Plugin: found page with slug "'.$slug.'"');
				$this->renderPage($page);
			}

			// SHOW 404
			echo $this->get404Page();
			die();
		}

		// FIND FRONTPAGE FROM SETTINGS
		/* @var $modelpage \CMS\Model\ModelPage */
		$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

		$page = $modelpage::GetByNodeID($settings->getFrontpageNodeId());
		if($page->hasRow()) {
			\Pecee\Debug::Instance()->add('CMS Plugin: using page defined af frontpage');
			$this->renderPage($page);
		}

		// SHOW 404
		echo $this->get404Page();
		die();
	}
}