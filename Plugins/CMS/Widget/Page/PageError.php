<?php
namespace CMS\Widget\Page;
class PageError extends \PC\Widget\Site\SiteAbstract {
	protected $title;
	protected $description;
	public function __construct($title, $description=NULL) {
		parent::__construct();

		$this->title = $title;
		$this->description = $description;

		$this->_template = $this->plugin->path.DIRECTORY_SEPARATOR.'Template' . DIRECTORY_SEPARATOR . 'ErrorPage.php';
		$this->_contentTemplate = NULL;
		$this->getSite()->addCss('/plugin/'.$this->plugin->name.'/admin/css/error-page.css');
	}
}