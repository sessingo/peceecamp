<?php
namespace CMS\Widget\Dialog\Language;
class Edit extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $language;
	public function __construct() {
		parent::__construct();
		$this->title = $this->_('Language/EditLanguage');

		/* @var $languagemodel \CMS\Model\ModelLanguage */
		$languagemodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelLanguage');

		$this->language = $languagemodel::GetById($this->getParam('languageId'));

		if(!$this->language->hasRow()) {
			$this->close();
		}

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Language/Name'), 'name', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation($this->_('Language/Alias'), 'alias', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			$lang = $languagemodel::GetByAlias($this->data->alias);
			if($lang->hasRow() && $lang->getLanguageID() != $this->language->getLanguageID()) {
				$this->setError('Language alias already exists!');
			}

			if(!$this->hasErrors()) {

				$this->language->setName(ucfirst($this->data->name));
				$this->language->setAlias($this->data->alias);
				$this->language->update();

				$this->setMessage($this->_('Language/LanguageUpdated'), 'success');

				$this->refresh();
			}
		}

	}
}