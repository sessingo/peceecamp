<?php
namespace CMS\Widget\Dialog\Language;
class Create extends \PC\Widget\Admin\Dialog\DialogAbstract {
	public function __construct() {
		parent::__construct();
		$this->title = $this->_('Language/NewLanguage');

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Language/Name'), 'name', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation($this->_('Language/Alias'), 'alias', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			/* @var $languagemodel \CMS\Model\ModelLanguage */
			$languagemodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelLanguage');

			$lang = $languagemodel::GetByAlias($this->data->alias);
			if($lang->hasRow()) {
				$this->setError('Language alias already exists!');
			}

			if(!$this->hasErrors()) {
				$language = new $languagemodel();
				$language->setName(ucfirst($this->data->name));
				$language->setAlias($this->data->alias);
				$language->save();

				$this->setMessage($this->_('Language/LanguageAdded', $language->getName()), 'success');

				$this->refresh();
			}
		}

	}
}