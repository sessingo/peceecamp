<?php
namespace CMS\Widget\Dialog\Language\Key;
class Create extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $parentLanguageKeyId;
	public function __construct() {
		parent::__construct();
		$this->title = $this->_('Language/Key/NewKey');

		/* @var $languagekey \CMS\Model\Language\LanguageKey */
		$languagekey = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Language\LanguageKey');

		$this->parentLanguageKeyId = $this->getParam('parentLanguageKeyId');

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Language/Key/Name'), 'name', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			$key = $languagekey::GetByKey($this->data->name, $this->parentLanguageKeyId);
			if($key->hasRow()) {
				$this->setError($this->_('Language/Key/KeyAlreadyExist'));
			}

			if(!$this->hasErrors()) {
				$key = new $languagekey();
				$key->setUserID(\Pecee\Model\User\ModelUser::Current()->getUserID());
				$key->setName($this->data->name);
				$key->setParentLanguageKeyID($this->parentLanguageKeyId);
				$key->save();

				$this->setMessage($this->_('Language/Key/KeyAdded'), 'success');

				$this->refresh();
			}
		}

	}
}