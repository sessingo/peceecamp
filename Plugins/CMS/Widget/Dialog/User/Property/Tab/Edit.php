<?php
namespace CMS\Widget\Dialog\User\Property\Tab;
class Edit extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $tab;
	public function __construct() {
		parent::__construct();

		$this->title = $this->_('Property/Tab/EditTab');

		/* @var $propertytab \CMS\Model\Property\PropertyTab */
		$propertytab = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyTab');

		$this->tab = $propertytab::GetByPropertyTabID($this->getParam('propertyTabId'));

		if(!$this->tab->hasRow()) {
			$this->close();
		}

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Property/Tab/Name'), 'name', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			if(!$this->hasErrors()) {
				$this->tab->setName($this->data->name);
				$this->tab->setOrder($this->data->order);
				$this->tab->setInheritable($this->data->inheritable);
				$this->tab->update();

				$this->refresh();
			}
		}
	}
}