<?php
namespace CMS\Widget\Dialog\User\Property;
class Create extends \PC\Widget\Admin\Dialog\DialogAbstract {
	public function __construct() {
		parent::__construct();

		$this->title = $this->_('Property/NewProperty');

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Property/Title'), 'title', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			if(!$this->hasErrors()) {
				/* @var $userproperty \CMS\Model\User\UserProperty */
				$userproperty = \PC\ObjectManager::GetInstance()->get('\CMS\Model\User\UserProperty');

				$node = new $userproperty();
				$node->setParentNodeID($this->getParam('nodeId',0));
				$node->setTitle($this->data->title);
				$node->setAlias(\CMS\Helper::GenerateAlias($this->data->title));
				$node->setActive(TRUE);
				$node->setIcon('folder_user.png');
				$node->save();

				$this->refresh();
			}
		}
	}
}