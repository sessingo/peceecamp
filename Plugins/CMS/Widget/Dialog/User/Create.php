<?php
namespace CMS\Widget\Dialog\User;
use Pecee\UI\Html\HtmlSelectOption;
class Create extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $parentPage;
	protected $excludeTemplates=NULL;
	protected $properties;
	public function __construct() {
		parent::__construct();
		$this->title = $this->_('User/New');
		$this->properties = array();
		$properties = NULL;

		/* @var $modeluser \CMS\Model\ModelUser */
		$modeluser = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelUser');

		/* @var $userproperty \CMS\Model\User\UserProperty */
		$userproperty = \PC\ObjectManager::GetInstance()->get('\CMS\Model\User\UserProperty');

		if($this->getParam('parentNodeId')) {
			$this->parentPage = $modeluser::GetByNodeID($this->getParam('parentNodeId'));
			if($this->parentPage->hasRow()) {
				$p = $this->parentPage->getProperty()->getProperties();
				if(count($p) > 0) {
					$properties = $userproperty::GetByNodeIDs($p);
				}
			}
		} else {
			if(is_null($properties)) {
				$properties = $userproperty::Get(NULL, NULL, NULL, NULL, NULL, NULL);
			}
		}

		if($properties && $properties->hasRows()) {
			$this->properties = $properties;
		}

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('User/Username'), 'username', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation($this->_('User/Type'), 'propertyNodeId', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			if(!$this->hasErrors()) {
				$page = new $modeluser();
				$page->setUsername($this->data->username);
				$page->setPropertyNodeId($this->data->propertyNodeId);
				$page->setParentNodeID($this->getParam('parentNodeId',0));
				$page->setActive(FALSE);

				$page->save();
				$this->setMessage($this->_('User/UserAdded'), 'success');

				$this->refresh();
			}
		}
	}

	protected function renderProperties() {
		$select = $this->form()->selectStart('propertyNodeId', NULL, NULL, TRUE);
		$path = '/plugin/'.$this->plugin->name.'/admin/gfx/ico/';

		if($this->properties && $this->properties->hasRows()) {
			/* @var $property \CMS\Model\ModelProperty*/
			foreach($this->properties->getRows() as $property) {
				$option = new HtmlSelectOption($property->getTitle(), $property->getNodeID());
				$option->addAttribute('data-imagesrc', $path.$property->getIcon());
				if($this->isPostBack() && $option->getValue() == $this->data->icon) {
					$option->addAttribute('selected','selected');
				}

				$select->addOption($option);
			}
		}
		return $select;
	}
}