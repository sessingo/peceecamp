<?php
namespace CMS\Widget\Dialog\Page;
class Hosts extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $page;
	protected $hosts;
	protected $host;
	public function __construct() {
		parent::__construct();

		$this->title = $this->_('Page/ManageHostname/ManageHostname');

		/* @var $modelpage \CMS\Model\ModelPage */
		$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

		$this->page = $modelpage::GetByNodeID($this->getParam('nodeId'));
		if(!$this->page->hasRow()) {
			$this->close();
		}

		/* @var $nodehost \CMS\Model\Node\NodeHost */
		$nodehost = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Node\NodeHost');

		$this->hosts = $nodehost::GetByNodeID($this->page->getNodeID());

		$hosts = $nodehost::Get();

		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$settings = $modelsettings::GetInstance();
		$settings->setUsingHostnames($hosts->hasRows());
		$settings->update();

		if($this->isPostBack()) {
			if($this->data->nodeHostId) {
				$this->host = $nodehost::GetByID($this->data->nodeHostId);
			}

			if($this->data->delete && $this->host && $this->host->hasRow()) {
				$this->host->delete();
				$this->setMessage($this->_('Page/ManageHostname/DomainDeleted'), 'success', 'managehosts');
				\PC\Router::Refresh();
			}

			if(!$this->data->postback) {

				if(trim($this->data->domain) && !\Pecee\Url::IsValidHostname(trim($this->data->domain))) {
					$this->setError($this->_('Page/ManageHostname/ErrorInvalidDomain'));
				}

				$this->addInputValidation($this->_('Page/ManageHostname/Language'), 'languageId', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

				if(!$this->hasErrors()) {
					$host = $nodehost::GetByHost(trim($this->data->domain));
					if($host->hasRow()) {
						if($this->host && $this->host->hasRow()) {
							if($host->getHostname() == trim($this->data->domain) && $this->host->getNodeHostID() != $host->getNodeHostID()) {
								$this->setError($this->_('Page/ManageHostname/ErrorDomainExist'));
							}
						} else {
							if($host->getHostname() == trim($this->data->domain)) {
								$this->setError($this->_('Page/ManageHostname/ErrorDomainExist'));
							}
						}
					}

					if(!$this->hasErrors()) {
						if($this->host && $this->host->hasRow()) {
							$this->host->setHostname(trim($this->data->domain));
							$this->host->setLanguageID($this->data->languageId);
							$this->host->update();

							$this->setMessage($this->_('Page/ManageHostname/DomainUpdated'), 'success', 'managehosts');
						} else {
							$host = new $nodehost();
							$host->setHostname(trim($this->data->domain));
							$host->setLanguageID($this->data->languageId);
							$host->setNodeID($this->page->getNodeID());
							$host->save();

							$this->setMessage($this->_('Page/ManageHostname/DomainCreated'), 'success', 'managehosts');
						}

						\PC\Router::Refresh();
					}
				}
			}
		}
	}

	protected function getLanguages() {
		/* @var $modellanguage \CMS\Model\ModelLanguage */
		$modellanguage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelLanguage');
		$languages = $modellanguage::Get();

		$out = array();
		if($languages->hasRows()) {
			foreach($languages->getRows() as $language) {
				$out[$language->getLanguageID()] = $language->getName();
			}
		}
		return $out;
	}
}