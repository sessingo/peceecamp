<?php
namespace CMS\Widget\Dialog\Page;
class Publish extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $page;
	protected $createSlug;
	public function __construct() {
		parent::__construct();

		/* @var $modelpage \CMS\Model\ModelPage */
		$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

		$this->page = $modelpage::GetByNodeID($this->getParam('nodeId'));

		if(!$this->page->hasRow()) {
			$this->close();
		}

		$this->title = $this->_('Page/PublishPage', $this->page->getTitle());

		if($this->isPostBack()) {

			$this->createSlug = $this->data->createSlug;

			$this->publishPage($this->page);
			if($this->data->publishChilds) {
				$this->publishChilds($this->page);
			}

			$this->setMessage($this->_('Page/PublishFinished'), 'success');
			$this->refresh();
		}

	}

	protected function publishPage(\CMS\Model\ModelPage $page) {
		$page->setActive(TRUE);

		// If theres only one widget, then we set that as default
		$property = $page->getProperty();
		if($property->hasRow()) {
			$widgets = $property->getWidgets();
			if(count($widgets) == 1) {
				$page->setWidget($widgets[0]);
			}
		}

		if($this->createSlug) {
			$page->updateWithSlug();
		} else {
			$page->update();
		}

		$this->publishChilds($page);
	}

	protected function publishChilds(\CMS\Model\ModelPage $page) {
		$childs = $page->getChilds();
		if($childs->hasRows()) {
			foreach($childs->getRows() as $page) {
				$this->publishPage($page);
			}
		}
	}
}