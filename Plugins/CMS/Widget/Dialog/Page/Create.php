<?php
namespace CMS\Widget\Dialog\Page;
use Pecee\UI\Html\HtmlSelectOption;
class Create extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $parentPage;
	protected $excludeTemplates=NULL;
	protected $properties;
	public function __construct() {
		parent::__construct();
		$this->title = $this->_('Page/NewPage');
		$this->properties = array();
		$properties = NULL;

		if($this->getParam('parentNodeId')) {

			/* @var $modelpage \CMS\Model\ModelPage */
			$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

			$this->parentPage = $modelpage::GetByNodeID($this->getParam('parentNodeId'));
			if($this->parentPage->hasRow()) {
				$p = $this->parentPage->getProperty()->getProperties();
				if(count($p) > 0) {
					/* @var $propertymodel \CMS\Model\ModelProperty */
					$propertymodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');

					$properties = $propertymodel::GetByNodeIDs($p);
				}
			}
		} else {
			if(is_null($properties)) {
				/* @var $propertymodel \CMS\Model\ModelProperty */
				$propertymodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');

				$properties = $propertymodel::Get(NULL, NULL, NULL, NULL, NULL, NULL);
			}
		}

		if($properties && $properties->hasRows()) {
			$this->properties = $properties;
		}

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Page/Title'), 'title', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation($this->_('Page/Type'), 'propertyNodeId', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			if(!$this->hasErrors()) {

				/* @var $modelpage \CMS\Model\ModelPage */
				$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

				$page = new $modelpage();
				$page->setUserID(\Pecee\Model\User\ModelUser::Current()->getUserID());
				$page->setTitle($this->data->title);
				$page->setMetaTitle($this->data->title);
				$page->setParentNodeID($this->getParam('parentNodeId',0));
				$page->setPropertyNodeId($this->data->propertyNodeId);
				$page->setVisibleInMenu(TRUE);
				$page->setActive(FALSE);

				$pages = $modelpage::Get(NULL, NULL, NULL, $this->data->parentNodeID);
				$order = 0;
				if($pages->hasRows()) {
					foreach($pages->getRows() as $p) {
						$order++;
					}
				}

				$page->setOrder($order+1);
				$page->save();

				$this->redirect($this->getRoute(NULL, NULL, array('page','edit',$page->getNodeID())));
			}
		}
	}

	protected function renderProperties() {
		$select = $this->form()->selectStart('propertyNodeId', NULL, NULL, TRUE);
		$path = '/plugin/'.$this->plugin->name.'/admin/gfx/ico/';

		if($this->properties && $this->properties->hasRows()) {
			/* @var $property \CMS\Model\ModelProperty*/
			foreach($this->properties->getRows() as $property) {
				$option = new HtmlSelectOption($property->getTitle(), $property->getNodeID());
				$option->addAttribute('data-imagesrc', $path.$property->getIcon());
				if($this->isPostBack() && $option->getValue() == $this->data->icon) {
					$option->addAttribute('selected','selected');
				}

				$select->addOption($option);
			}
		}
		return $select;
	}
}