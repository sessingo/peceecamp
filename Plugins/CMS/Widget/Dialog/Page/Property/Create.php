<?php
namespace CMS\Widget\Dialog\Page\Property;
class Create extends \PC\Widget\Admin\Dialog\DialogAbstract {
	public function __construct() {
		parent::__construct();

		$this->title = $this->_('Property/NewProperty');

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Property/Title'), 'title', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			if(!$this->hasErrors()) {

				/* @var $propertymodel \CMS\Model\ModelProperty */
				$propertymodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');

				$node = new $propertymodel();
				$node->setParentNodeID($this->getParam('nodeId',0));
				$node->setTitle($this->data->title);
				$node->setAlias(\CMS\Helper::GenerateAlias($this->data->title));
				$node->setActive(TRUE);
				$node->setIcon('page.png');

				$node->save();

				$this->refresh();
			}
		}
	}
}