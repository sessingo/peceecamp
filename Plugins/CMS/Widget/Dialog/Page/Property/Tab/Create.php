<?php
namespace CMS\Widget\Dialog\Page\Property\Tab;
class Create extends \PC\Widget\Admin\Dialog\DialogAbstract {
	public function __construct() {
		parent::__construct();

		$this->title = $this->_('Property/Tab/NewTab');

		if(!$this->getParam('nodeId')){
			$this->close();
		}

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Property/Tab/Name'), 'name', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			if(!$this->hasErrors()) {

				/* @var $propertytab \CMS\Model\Property\PropertyTab */
				$propertytab = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyTab');

				$tab = new $propertytab();
				$tab->setName($this->data->name);
				$tab->setNodeID($this->getParam('nodeId'));
				$tab->setInheritable($this->data->inheritable);
				$tab->save();

				$this->refresh();
			}
		}
	}
}