<?php
namespace CMS\Widget\Dialog\Page\Property\Module;
class Create extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $moduleContent;
	public function __construct() {
		parent::__construct();

		$this->title = $this->_('Property/Module/NewModule');

		if(!$this->getParam('nodeId')){
			$this->close();
		}

		if($this->isPostBack()) {

			/* @var $propertymodule \CMS\Model\Property\PropertyModule */
			$propertymodule = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyModule');

			$module = new $propertymodule();

			if($this->data->module) {
				$module->setModule($this->data->module);

				$widget = $module->getModule();
				$widget = new $widget();
				$widget->addModule($module);
				$widget->onWidgetAdded();

				$this->moduleContent = $widget->__toString();
			}

			if(!$this->data->postback) {
				$this->addInputValidation($this->_('Property/Module/Name'), 'name', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
				$this->addInputValidation($this->_('Property/Module/ID'), 'controlId', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
				$this->addInputValidation($this->_('Property/Module/Tab'), 'propertyTabId', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
				$this->addInputValidation($this->_('Property/Module/Module'), 'module', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());


				if(!$this->hasErrors() && $this->data->module) {
					$module->setName($this->data->name);
					$module->setDescription($this->data->description);
					$module->setControlID($this->data->controlId);
					$module->setPropertyTabID($this->data->propertyTabId);
					$module->setNodeID($this->getParam('nodeId'));
					$module->setRequired($this->data->required);
					$module->setOrder($this->data->order);
					$module->setInheritable($this->data->inheritable);

					$module->save();
					$this->refresh();
				}
			}
		}
	}
}