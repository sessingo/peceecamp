<?php
namespace CMS\Widget\Dialog\Page\Property\Module;
class Edit extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $moduleContent;
	protected $module;
	public function __construct() {
		parent::__construct();

		$this->title = $this->_('Property/Module/EditModule');

		/* @var $propertymodule \CMS\Model\Property\PropertyModule */
		$propertymodule = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyModule');

		$this->module = $propertymodule::GetByPropertyModuleId($this->getParam('propertyModuleId'));

		if(!$this->module->hasRow()) {
			$this->close();
		}

		if($this->data->module) {
			$this->module->setModule($this->data->module);
		}

		$widget = $this->module->getModule();
		if(class_exists($widget)) {
			$widget = new $widget();
			$widget->addModule($this->module);
			$widget->onWidgetAdded();

			$this->moduleContent = $widget->__toString();
		}

		if($this->isPostBack() && !$this->data->postback) {
			$this->addInputValidation($this->_('Property/Module/Name'), 'name', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation($this->_('Property/Module/ID'), 'controlId', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation($this->_('Property/Module/Tab'), 'propertyTabId', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation($this->_('Property/Module/Module'), 'module', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			$this->module->setName($this->data->name);
			$this->module->setDescription($this->data->description);
			$this->module->setControlID($this->data->controlId);
			$this->module->setPropertyTabID($this->data->propertyTabId);
			$this->module->setRequired($this->data->required);
			$this->module->setOrder($this->data->order);
			$this->module->setInheritable($this->data->inheritable);

			if($this->data->module) {
				if(!$this->hasErrors()) {
					$this->module->update();
					$this->refresh();
				}
			}
		}
	}
}