<?php
namespace CMS\Widget\Dialog\Macro;
class Editoradd extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $macro;
	protected $macros;
	protected $widgets;
	public function __construct() {
		parent::__construct();

		$this->form()->setPrefixElements(FALSE);
		$this->title = $this->_('Settings/Macro/AddMacro');

		$this->widgets = array('' => $this->_('Settings/Macro/ChooseMacro'));

		/* @var $macromodel \CMS\Model\ModelMacro */
		$macromodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelMacro');

		$this->macros = $macromodel::Get();

		$this->jsEvent = ($this->data->update) ? 'editMacro' : 'insertMacro';


		if($this->macros->hasRows()) {
			foreach($this->macros->getRows() as $macro) {
				$this->widgets[$macro->getNodeID()] = $macro->getTitle();
				if($this->isPostBack() && $macro->getNodeID() == $this->data->nodeId) {
					$this->macro = $macro;
				}
			}

			if($this->data->update) {
				$data = new \Pecee\Util\Map();
				$arr = array_merge($this->macro->data->getData(), $this->data->getArray());
				$data->setData($arr);
				$this->macro->data = $data;
			}
		}
	}

	protected function getObject() {
		$data = (array)$this->macro->data->getData();
		return urlencode(json_encode($data));
	}

	protected function renderModule(&$module) {
		$widget = $module->getModule();
		$widget = new $widget();
		if($this->getParam('edit')) {
			$widget->setAutoPostback(TRUE);
		}
		$widget->renderAdmin($module,$this->macro);
		$widget->onAdminRender();
		return $widget;
	}
}