<?php
namespace CMS\Widget\Dialog\Macro;
class Properties extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $macro;
	protected $properties;
	public function __construct() {
		parent::__construct();
		$this->title = $this->_('Settings/Macro/SaveProperties');

		/* @var $macromodel \CMS\Model\ModelMacro */
		$macromodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelMacro');

		$this->macro = $macromodel::GetById($this->getParam('nodeId'));

		if(!$this->macro->hasRow()) {
			$this->close();
		}

		$macros = new \CMS\Macro();
		$properties = $macros->getMacros();
		foreach($properties as $macro) {
			if($macro->widget == $this->macro->getWidget()) {
				$this->properties = $macro->methods;
				break;
			}
		}

		if($this->isPostBack()) {
			/* @var $propertymodule \CMS\Model\Property\PropertyModule */
			$propertymodule = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyModule');

			$modules = $propertymodule::GetByNodeId($this->macro->getNodeID());

			$controlIds = array();

			if($modules->hasRows()) {
				foreach($modules->getRows() as $module) {
					if(!in_array($module->getControlID(), $this->data->methods)) {
						$module->delete();
					} else {
						$controlIds[] = $module->getControlID();
					}
				}
			}

			foreach($this->data->methods as $method) {
				if(!in_array($method, $controlIds)) {
					$p = new $propertymodule();
					$p->setNodeID($this->macro->getNodeID());
					$p->setControlID($method);
					$p->save();
				}
			}

			$this->close('$(\'#frame\').attr(\'src\', $(\'#frame\').attr(\'src\'));');
		}
	}

	protected function hasProperty($name) {
		if($this->macro->hasRows() && $this->macro->getModules()->hasRows()) {
			foreach($this->macro->getModules()->getRows() as $module) {
				if($module->getControlID() == $name) {
					return TRUE;
				}
			}
		}
		return FALSE;
	}
}