<?php
namespace CMS\Widget\Dialog\Macro;
class Create extends \PC\Widget\Admin\Dialog\DialogAbstract {
	public function __construct() {
		parent::__construct();
		$this->title = $this->_('Settings/Macro/NewMacro');

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Settings/Macro/Title'), 'title', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			if(!$this->hasErrors()) {

				/* @var $macromodel \CMS\Model\ModelMacro */
				$macromodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelMacro');

				$macro = new $macromodel();
				$macro->setTitle($this->data->title);
				$macro->save();

				$this->setMessage($this->_('Settings/Macro/MacroCreated'), 'success');

				$this->refresh();
			}
		}

	}
}