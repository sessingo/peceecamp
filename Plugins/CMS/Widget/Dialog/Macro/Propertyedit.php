<?php
namespace CMS\Widget\Dialog\Macro;
class Propertyedit extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $macro;
	protected $module;
	protected $moduleContent;
	public function __construct() {
		parent::__construct();
		$this->title = $this->_('Settings/Macro/EditProperty');

		/* @var $macromodel \CMS\Model\ModelMacro */
		$macromodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelMacro');

		$this->macro = $macromodel::GetById($this->getParam('nodeId'));

		if(!$this->macro->hasRow()) {
			$this->close();
		}

		/* @var $propertymodule \CMS\Model\Property\PropertyModule */
		$propertymodule = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyModule');

		$this->module = $propertymodule::GetByPropertyModuleId($this->getParam('moduleId'));

		if($this->data->module) {
			$this->module->setModule($this->data->module);
		}

		if($this->module->getModule()) {
			$widget = $this->module->getModule();
			$widget = new $widget();
			$widget->addModule($this->module);
			$widget->onWidgetAdded();

			$this->moduleContent = $widget->__toString();
		}
		if($this->isPostBack() && !$this->data->postback) {
			$this->addInputValidation($this->_('Settings/Macro/PropertyName'), 'name', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation($this->_('Settings/Macro/Widget'), 'module', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			$this->module->setName($this->data->name);

			if($this->data->module) {
				if(!$this->hasErrors()) {
					$this->module->update();
					$this->close('$(\'#frame\').attr(\'src\', $(\'#frame\').attr(\'src\'));');
				}
			}
		}
	}
}