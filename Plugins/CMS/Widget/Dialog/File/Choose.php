<?php
namespace CMS\Widget\Dialog\File;
class Choose extends \PC\Widget\Admin\Dialog\DialogAbstract {
	public function __construct() {
		parent::__construct();
		$this->title = $this->_('File/ChooseFile');
	}
}