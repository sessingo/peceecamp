<?php
namespace CMS\Widget\Dialog\File;
class Edit extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $file;
	public function __construct() {
		parent::__construct();

		/* @var $modelfile \CMS\Model\ModelFile */
		$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

		$this->file = $modelfile::GetByNodeID($this->getParam('nodeId'));

		if(!$this->file->hasRow()) {
			$this->close();
		}

		$this->title = $this->_('File/EditFile', $this->file->getTitle());

		if($this->isPostBack()) {
			/* @var $modelsettings \CMS\Model\ModelSettings */
			$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

			$this->addInputValidation($this->_('File/File'), 'file', new \PC\UI\Form\Validate\File\FileNotNullOrEmpty($this->files->file));
			$this->addInputValidation($this->_('File/File'), 'file', new \PC\UI\Form\Validate\File\FileAllowedExtension($this->files->file, $modelsettings::GetInstance()->getFileTypes()));

			if(!$this->hasErrors() && $this->files->file) {
				$fileExtension = \Pecee\IO\File::GetExtension($this->files->file->name);

				$this->file->setOriginalFilename($this->files->file->name);
				$this->file->setMime(\Pecee\IO\File::GetMimeType($this->files->file->tmpName));
				$this->file->setBytes(filesize($this->files->file->tmpName));

				if(move_uploaded_file($this->files->file->tmpName, $this->file->getFullPath())) {
					$this->file->update();
					$this->setMessage($this->_('File/FileSaved'), 'success');
				}else{
					$this->setError('Kunne ikke uploade filen');
				}

				$this->refresh();
			}
		}
	}
}