<?php
namespace CMS\Widget\Dialog\File;
class Upload extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $maxFiles = 100;
	public function __construct() {
		parent::__construct();

		\Pecee\Session::Instance()->set('UserID', \Pecee\Model\User\ModelUser::Current()->getUserID());

		$this->title = $this->_('File/UploadFiles');
	}

	protected function getMaxFilesize() {
		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		return $modelsettings::GetInstance()->getMaxFilesize();
	}

	protected function getExtensions() {
		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$extensions = $modelsettings::GetInstance()->getFileTypes();
		$out = array();
		if(count($extensions) > 0) {
			foreach($extensions as $ext) {
				$out[] = $ext;
			}
		}

		return join(',', $out);
	}
}