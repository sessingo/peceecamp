<?php
namespace CMS\Widget\Dialog\File\Folder;
class Edit extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $folder;
	protected $parentPage;
	protected $excludeTemplates=NULL;
	protected $properties;
	public function __construct() {
		parent::__construct();
		$this->title = $this->_('File/EditFolder');
		$this->properties = array();
		$properties = NULL;

		/* @var $modelfolder \CMS\Model\ModelFolder */
		$modelfolder = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFolder');

		$this->folder = $modelfolder::GetByNodeID($this->getParam('nodeId'));

		if($this->getParam('parentNodeId')) {
			$this->parentPage = $modelfolder::GetByNodeID($this->getParam('parentNodeId'));
			if($this->parentPage->hasRow()) {
				$p = $this->parentPage->getProperties();
				if(count($p) > 0) {
					$properties = $modelfolder::GetByNodeIDs($p);
				}
			}
		} else {
			if(is_null($properties)) {
				$properties = $modelfolder::Get(NULL, NULL, 0, NULL, NULL, NULL);
			}
		}

		if($properties && $properties->hasRows()) {
			foreach($properties->getRows() as $property) {
				$this->properties[$property->getNodeID()] = $property->getTitle();
			}
		}

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('File/Name'), 'name', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			// Ensure that no duplicate folders are created
			$exist = $modelfolder::GetByTitle($this->data->name);
			if($exist->hasRow() && $exist->getNodeID() != $this->folder->getNodeID()) {
				$this->setError($this->_('File/FolderExists'));
			}

			if(!$this->hasErrors()) {
				$this->folder->setTitle($this->data->name);
				$this->folder->setParentNodeID($this->getParam('parentNodeId', $this->folder->getParentNodeID()));
				$this->folder->update();

				$this->setMessage($this->_('File/FolderAdded'), 'success');

				$this->refresh();
			}
		}
	}
}