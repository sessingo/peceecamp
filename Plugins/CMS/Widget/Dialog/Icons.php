<?php
namespace CMS\Widget\Dialog;
class Icons extends \PC\Widget\Admin\Dialog\DialogAbstract {
	protected $icons;
	public function __construct() {
		parent::__construct();
		$this->title = $this->_('Icons/ChooseIcon');
		$this->setIcons();
	}

	protected function setIcons() {
		$this->icons = array();
		$icoDir = $_SERVER['DOCUMENT_ROOT'] .'/plugin/'.$this->plugin->name.'/admin/gfx/ico';
		if(($handle = opendir($icoDir)) !== FALSE) {
			while (false !== ($entry = readdir($handle))) {
				if(!in_array($entry, array('.','..'))) {
					$this->icons[] = $entry;
				}
			}
			closedir($handle);
		}
		rsort($this->icons);
		$this->icons = array_values($this->icons);
	}
}