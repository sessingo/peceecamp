<?php
namespace CMS\Widget\Macro;
abstract class MacroAbstract extends \PC\Widget\Site\SiteAbstract {
	protected $page;
	public function __construct() {
		parent::__construct();
		$this->setTemplate(NULL);

	}
	public function setPage($page) {
		$this->page = $page;
	}
	public function onSiteRender() { }
}