<?php
namespace CMS\Widget;
abstract class WidgetAbstract extends \PC\Widget\Site\SiteAbstract {
	protected $_nodes;
	protected $_language;
	protected $_languageText;

	/**
	 * @var \CMS\Model\ModelPage
	 */
	protected $page;

	public function __construct() {
		parent::__construct();
		$this->_nodes = array();

		// Set default language if any
		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$this->setLanguage($modelsettings::GetInstance()->getLanguage());
	}

	abstract public function onSiteRender();
	abstract public function renderPage();

	public function setDraft(\CMS\Model\ModelDraft $draft) {
		$this->page = $draft;
	}

	public function setPage(\CMS\Model\ModelPage $page) {
		$this->page = $page;
	}

	public function widget(\Pecee\Widget\Widget $widget) {
		if($widget->getTemplate() == 'Template\Default.php') {
			$widget->setTemplate(NULL);
		}
		$widget->setPage($this->page);
		$widget->onSiteRender();
		$widget->renderPage();
		echo $widget;
	}

	protected function insertMacros($content) {
		$matches = array();
		preg_match_all('%(<div:macro\s(.*?)\sstyle="(.*?)</div:macro>?)%is', $content, $matches);
		if(count($matches) > 0) {
			foreach($matches[2] as $key=>$match) {
				$matches2 = array();
				preg_match_all('/(.*?)="(.*?)"/is', $match, $matches2);

				$widget = '';
				$out = NULL;

				foreach($matches2[1] as $key2=>$match2) {
					if(strtolower($match2) == 'widget') {
						$widget = trim(urldecode($matches2[2][$key2]));
					} else {
						$out = json_decode(urldecode($matches2[2][$key2]));
					}
				}

				/* @var $widget \CMS\Widget\Macro\MacroAbstract */
				$widget = new $widget();
				$widget->setPage($this->page);

				foreach($out as $key3=>$value) {
					if(stripos($key3, 'set') > -1) {
						$widget->$key3($value);
					}
				}
				$widget->onSiteRender();
				$content = str_replace($matches[1][$key], $widget->render(), $content);
			}
		}

		return $content;
	}

	public function getData($name, $fetchParent=TRUE) {
		if(!$this->page->getData($name) && $fetchParent) {
			$parent = $this->page->getParent();
			while($parent && $parent->hasRow()) {
				if($parent->getData($name)) {
					return $this->insertMacros($parent->getData($name));
				}
				$parent = $parent->getParent();
			}
		}
		return $this->insertMacros($this->page->getData($name));
	}

	public function getFile($nodeId) {
		if(!is_null($nodeId) && !empty($nodeId)) {
			if(!isset($this->_nodes[$nodeId])) {
				/* @var $modelfile \CMS\Model\ModelFile */
				$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

				$this->_nodes[$nodeId] = $modelfile::GetByNodeID($nodeId);
			}
			if($this->_nodes[$nodeId]->hasRow()) {
				return $this->_nodes[$nodeId];
			}
		}
		return NULL;
	}

	public function getNodeUrl($nodeId) {
		if(!isset($this->_nodes[$nodeId])) {
			/* @var $modelpage \CMS\Model\ModelPage */
			$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

			$this->_nodes[$nodeId] = $modelpage::GetByNodeID($nodeId);
		}
		if($this->_nodes[$nodeId]->hasRow()) {
			return $this->_nodes[$nodeId]->getSlug();
		}
		return NULL;
	}

	public function setLanguage($languageAlias) {
		$this->_language = $languageAlias;
	}

	public function getLanguage() {
		return $this->_language;
	}

	/**
	 * Translates message
	 * @param string $messageID
	 * @param array $args
	 * @return string
	 */
	public function _($key, $args = NULL) {
		if(!$this->_languageText) {
			/* @var $languagetext \CMS\Model\Language\LanguageText */
			$languagetext = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Language\LanguageText');

			$this->_languageText = $languagetext::GetInstance()->get();
		}
		if (!is_array($args)) {
			$args = func_get_args();
			$args = array_slice($args, 1);
		}
		$arr = $this->_languageText->getArray();
		if(isset($arr[$key][$this->_language])) {
			return vsprintf($arr[$key][$this->_language], $args);
		}
		return $key;
	}
}