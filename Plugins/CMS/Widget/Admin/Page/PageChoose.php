<?php
namespace CMS\Widget\Admin\Page;
class PageChoose extends \PC\Widget\Admin\AdminAbstract {
	protected $name;
	protected $value;
	protected $page;
	protected $defaultNode;
	protected $properties;
	protected $allowedProperties;
	public function __construct($name, $value=NULL, $defaultNode = NULL, $properties = NULL) {
		parent::__construct(0, FALSE);
		$this->name = $name;
		$this->value = ($this->isPostBack()) ? $this->data->$name : $value;
		$this->properties = $properties;

		$this->defaultNode = (is_null($defaultNode) ? $this->getParam('nodeId') : $defaultNode);

		if(!is_null($this->value)) {
			/* @var $modelpage \CMS\Model\ModelPage */
			$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

			$this->page = $modelpage::GetByNodeID($this->value);
		}

		$this->setTemplate(NULL);
		$this->_contentHtml = NULL;
	}
}