<?php
namespace CMS\Widget\Admin\Page;
abstract class PageAbstract extends \CMS\Widget\Admin\AdminAbstract {
	public function __construct($adminLevel=0) {
		parent::__construct($adminLevel);
		$this->addLevel($this->_('Page/Pages'), $this->getRoute(NULL, ''));
		$this->page=$this->getParam('page',0);
		$this->getMenu()->getItem(0)->addClass('active');
	}
}