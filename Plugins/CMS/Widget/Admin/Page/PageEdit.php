<?php
namespace CMS\Widget\Admin\Page;
use Pecee\Boolean;

class PageEdit extends \CMS\Widget\Admin\Page\PageAbstract {
	protected $page;
	protected $history;
	protected $widgets;
	protected $modules;
	protected $languages;
	public function __construct($nodeId) {
		parent::__construct();

		/* @var $modeldraft \CMS\Model\ModelDraft */
		$modeldraft = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelDraft');

		/* @var $modelpage \CMS\Model\ModelPage */
		$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

		if($this->hasParam('draftId')) {
			$this->page = $modeldraft::GetByNodeID($this->getParam('draftId'));
		} else {
			$this->page = $modelpage::GetByNodeID($nodeId);
		}

		$this->history = $modeldraft::GetByPageID($this->page->getNodeID(), NULL, NULL, NULL, 40);

		$this->modules = array();

		/* @var $languagemodel \CMS\Model\ModelLanguage */
		$languagemodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelLanguage');

		$this->languages = $languagemodel::Get();
		$this->renderModules();

		if(!$this->page->hasRow()) {
			\PC\Router::Redirect($this->getRoute(NULL, NULL));
		}

		$this->widgets = $this->page->getProperty()->getWidgets();
		$hasWidgets = TRUE;
		if(!$this->widgets) {
			$hasWidgets = FALSE;
			$this->widgets = \CMS\Module::GetWidgets();
		}

		if(count($this->widgets) > 0) {
			$tmp = array();

			if(!$hasWidgets) {
				$tmp[''] = $this->_('Page/ChooseWidget');;
			}

			foreach($this->widgets as $widget) {
				$tmp[$widget] = $widget;
			}
			$this->widgets = $tmp;
		}

		$this->addLevel($this->_('Page/EditPage'));
		$this->prependSiteTitle($this->_('Page/EditPage'));

		$this->getSite()->addCss('/plugin/CMS/admin/data/redactor/redactor/redactor.css');
		$this->getSite()->addJs('/plugin/CMS/admin/data/redactor/redactor/redactor.js');
		$this->getSite()->addJs('/plugin/CMS/admin/js/redactor-plugins/macro.js');
		$this->getSite()->addJs('/plugin/CMS/admin/js/redactor-plugins/fullscreen.js');
	}

	protected function renderModules() {
		$tabs = $this->page->getProperty()->getTabs();
		if(count($tabs) > 0) {
			foreach($tabs as $tab) {
				$modules = $this->page->getProperty()->getModules($tab->getPropertyTabID());
				if(count($modules) > 0) {
					foreach($modules as $module) {
						$widget = $module->getModule();
						if(class_exists($widget)) {
							$widget = new $widget();
							$widget->renderAdmin($module,$this->page);
							$widget->onAdminRender();
							$this->modules[$tab->getName()][] = $widget;
						}
					}
				}
			}
		}
	}

	public function getSlugs() {
		$out = array();

		/* @var $nodehost \CMS\Model\Node\NodeHost */
		$nodehost = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Node\NodeHost');

		$hosts = $nodehost::Get();
		if($hosts->hasRows()) {
			$rootNodeId = NULL;
			foreach($hosts->getRows() as $host) {
				if(strstr($this->page->getPath(), $host->getNodeID()) !== FALSE) {
					$rootNodeId = $host->getNodeID();
					break;
				}
			}

			if(!is_null($rootNodeId)) {

				/* @var $modelpage \CMS\Model\ModelPage */
				$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

				$rootPage = $modelpage::GetByNodeID($rootNodeId);

				$hosts = $nodehost::GetByNodeID($rootNodeId);
				if($hosts->hasRow()) {
					foreach($hosts->getRows() as $host) {
						$slug = substr($this->page->getSlug(), strlen($rootPage->getSlug()));
						$host = ($host->getHostname()=='') ? $_SERVER['HTTP_HOST'] : $host->getHostname();
						$out[] = $host . $slug;
					}
				}
			}
		}

		return $out;
	}

	public function render() {

		$this->addInputValidation($this->_('Page/Title'), 'title', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

		if($this->isPostBack() && !$this->hasErrors()) {

			/* @var $modelpage \CMS\Model\ModelPage */
			$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

			$slug = $modelpage::GetBySlug(trim($this->data->slug,'/'));
			if($slug->hasRow() && $slug->getNodeID() != $this->page->getNodeID()) {
				$this->setMessage($this->_('Page/SlugInUseError'),'error');
			}

			if(!$this->hasErrors()) {
				$this->page->setUserID(\Pecee\Model\User\ModelUser::Current()->getUserID());
				$this->page->setTitle($this->data->title);
				$this->page->setWidget($this->data->widget);
				$this->page->setSlug($this->data->slug);
				$this->page->setActive(Boolean::Parse($this->data->active));

				if($this->data->activeFrom) {
					$this->page->setActiveFrom(\Pecee\Date::ToDateTime(strtotime($this->data->activeFrom)));
				}

				if($this->data->activeTo) {
					$this->page->setActiveTo(\Pecee\Date::ToDateTime(strtotime($this->data->activeTo)));
				}

				$this->page->setVisibleInMenu(Boolean::Parse($this->data->visibleInMenu));
				$this->page->setMetaTitle($this->data->metaTitle);
				$this->page->setMetaDescription($this->data->metaDescription);
				$this->page->setMetaKeywords($this->data->metaKeywords);

				$this->setMessage($this->_('Page/PageSaved'), 'success');

				$this->page->update();

				\PC\Router::Redirect($this->getRoute(NULL, NULL));
			}
		}

		return parent::render();
	}

	protected function getDisableHistory() {
		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		return $modelsettings::GetInstance()->getDisableHistory();
	}
}