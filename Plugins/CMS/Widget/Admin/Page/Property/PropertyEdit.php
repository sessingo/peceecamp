<?php
namespace CMS\Widget\Admin\Page\Property;
class PropertyEdit extends \CMS\Widget\Admin\Page\Property\PropertyAbstract {
	protected $property;

	public function __construct($nodeId) {
		parent::__construct();

		$this->addLevel($this->_('Property/Edit'));

		$this->prependSiteTitle($this->_('Property/Edit'));

		$this->getSite()->addJs('/plugin/CMS/admin/js/jquery.ui.nestedSortable.js');

		/* @var $propertymodel \CMS\Model\ModelProperty */
		$propertymodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');

		$this->property = $propertymodel::GetByNodeID($nodeId);
		if(!$this->property->hasRow()) {
			\PC\Router::Redirect($this->getRoute(NULL, 'pageproperty'));
		}

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Property/Title'), 'title', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation($this->_('Property/Alias'), 'alias', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation($this->_('Property/Icon'), 'icon', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			$alias = $propertymodel::GetByAlias($this->data->alias);
			if($alias->hasRow() && $alias->getNodeID() != $this->property->getNodeID()) {
				$this->setError($this->_('Property/AliasExists'));
			}

			if(!$this->hasErrors()) {
				$this->property->setTitle($this->data->title);
				$this->property->setIcon($this->data->icon);
				$this->property->setAlias($this->data->alias);
				$this->property->setWidgets($this->data->widget);
				$this->property->setProperties($this->data->properties);
				$this->property->update();

				$this->setMessage($this->_('Property/PropertySaved'), 'success');

				\PC\Router::Redirect($this->getRoute(NULL, 'pageproperty'));
			}
		}
	}

	protected function loopProperties($parentNodeId = 0) {
		/* @var $propertymodel \CMS\Model\ModelProperty */
		$propertymodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');

		$pages = $propertymodel::Get(NULL, NULL, $parentNodeId, NULL, NULL, NULL);
		if($pages->hasRows()) {
			$css = ($parentNodeId > 0) ? ' style="display:none;"' : '';
			$out = '<ol class="js-page"'.$css.'>';
			foreach($pages->getRows() as $p) {
				$active = (is_array($this->property->getProperties()) && in_array($p->getNodeID(), $this->property->getProperties()));
				$out .= '<li id="node_'.$p->getNodeID().'"><div><div class="hitarea collapsed"></div><a href="javascript:;" data-type="property" data-id="'.$p->getNodeID().'"'.(($active) ? ' class="active"' : '').'><img src="/plugin/'. $this->plugin->name .'/admin/gfx/ico/'. $p->getIcon().'" alt="" />'.$p->getTitle().'</a></div>';
				$out .= '<input type="checkbox" name="properties[]" value="'.$p->getNodeID().'" '.(($active) ? 'checked="checked"' : '').'style="display:none;" />';
				$out .= $this->loopProperties($p->getNodeID());
				$out .= '</li>';
			}
			$out .= '</ol>';
			return $out;
		}
		return '';
	}

	protected function loopWidgets($path = '') {
		$p = $this->plugin->themesdir . DIRECTORY_SEPARATOR . $this->plugin->application->Theme .'/Widget/' . $path;

		$files = array();
		$folders = array();

		if(($handle = opendir($p)) !== FALSE) {
			while (false !== ($entry = readdir($handle))) {
				if(strpos($entry, '.php') > -1) {
					$name = explode('.',$entry);
					$widget = ucfirst($name[0]);
					$widget = str_replace(DIRECTORY_SEPARATOR, '\\', $path) . $widget;
					$widget = sprintf('\\CMS\\%s\\Widget\\%s', $this->plugin->application->Theme, $widget);
					try {
						if(class_exists($widget)) {
							$class = new \ReflectionClass($widget);
							if(!$class->isAbstract()) {
								$files[$widget] = ucfirst($name[0]);
							}
						}
					} catch(\Exception $e) {
						// TODO: Handle class errors
					}
				}

				if(!in_array($entry, array('.', '..', '.svn','Macro','Module')) && is_dir($p . DIRECTORY_SEPARATOR .$entry)) {
					$folders[] = $entry;
				}
			}
			closedir($handle);
		}


		$out = '';
		if(count($files) > 0 || count($folders) > 0) {
			$attr = ($path == '') ? ' class="treeview" id="widgets"' : ' style="display:none;"';
			$out = '<ol'.$attr.'>';
			if(count($folders) > 0) {
				foreach($folders as $folder) {
					$out .= '<li><div><div class="hitarea collapsed"></div><a href="javascript:;" data-id="'.md5($path . $folder).'"><img src="/plugin/'.$this->plugin->name.'/admin/gfx/ico/folder.png" alt="" />'.$folder.'</a></div>';
					$out .= $this->loopWidgets($path . $folder . DIRECTORY_SEPARATOR);
					$out .= '</li>';
				}
			}
			if(count($files) > 0) {
				foreach($files as $widget=>$file) {
					$active = (is_array($this->property->getWidgets()) && in_array($widget, $this->property->getWidgets()));
					$out .= '<li><div><a href="javascript:;" data-type="file" data-id="'.md5($path . $file).'"'.(($active) ? ' class="active"' : '').'><img src="/plugin/'.$this->plugin->name.'/admin/gfx/ico/application.png" alt="" />'.$file.'</a></div>';
					$out .= '<input type="checkbox" name="widget[]" value="'.$widget.'" '.(($active) ? 'checked="checked"' : '').'style="display:none;" />';
					$out .= '</li>';
				}
			}
			$out .= '</ol>';
		}
		return $out;
	}
}