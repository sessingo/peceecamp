<?php
namespace CMS\Widget\Admin\Page\Property;
class PropertyChoose extends \PC\Widget\Admin\AdminAbstract {
	protected $name;
	protected $value;
	protected $page;
	protected $defaultNode;
	protected $selectMultiple;
	public function __construct($name, $value=NULL, $defaultNode = NULL, $selectMultiple = FALSE) {
		parent::__construct(0, FALSE);
		$this->name = $name;
		$this->value = ($this->isPostBack()) ? $this->data->$name : $value;

		$this->defaultNode = (is_null($defaultNode) ? $this->getParam('nodeId') : $defaultNode);
		$this->selectMultiple = $selectMultiple;

		if(!is_null($this->value)) {
			/* @var $modelproperty \CMS\Model\ModelProperty */
			$modelproperty = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');
			$this->page = $modelproperty::GetByNodeID($this->value);
		}

		$this->setTemplate(NULL);
		$this->_contentHtml = NULL;
	}

	public function getName() {
		if($this->selectMultiple) {
			/* @var $modelproperty \CMS\Model\ModelProperty */
			$modelproperty = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');

			$properties = $modelproperty::GetByNodeIDs(explode(',',$this->value));
			if($properties->hasRows()) {
				$out = array();
				foreach($properties->getRows() as $property) {
					$out[] = $property->getTitle();
				}
				return join(', ', $out);
			}
		} else {
			return $this->page->getTitle();
		}
		return '';
	}
}