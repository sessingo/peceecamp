<?php
namespace CMS\Widget\Admin\Page\Property;
abstract class PropertyAbstract extends \CMS\Widget\Admin\Page\PageAbstract {
	public function __construct() {
		parent::__construct(1);
		$this->addLevel($this->_('Property/Structure'), $this->getRoute(NULL, 'pageproperty'));
	}
}