<?php
namespace CMS\Widget\Admin\Page\Property;
class PropertyHome extends \CMS\Widget\Admin\Page\Property\PropertyAbstract {
	protected $pages;
	protected $activeNodes;
	public function __construct() {
		parent::__construct();

		$this->prependSiteTitle($this->_('Property/Structure'));

		$this->getSite()->addJs('/plugin/CMS/admin/js/jquery.ui.nestedSortable.js');
		$this->getSite()->addJs('/plugin/CMS/admin/js/jquery.contextmenu.js');

		/* @var $propertymodel \CMS\Model\ModelProperty */
		$propertymodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');

		$this->activeNodes = array();
		if(\Pecee\Cookie::Exists('PagePropertyNodes')) {
			$this->activeNodes = explode(',',\Pecee\Cookie::Get('PagePropertyNodes', ''));
		}

		$this->pages = $propertymodel::Get(NULL, NULL, 0, NULL, NULL, NULL);
	}

	protected function loopPages(\CMS\Model\ModelProperty $property) {
		if($property->hasRow()) {
			$propertymodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');

			$pages = $propertymodel::Get(NULL, NULL, $property->getNodeID(), NULL, NULL, NULL);
			if($pages->hasRows()) {
				$style = (in_array($property->getNodeID(), $this->activeNodes)) ? '' : ' style="display:none;"';
				$out = '<ol class="js-page"'.$style.'>';
				foreach($pages->getRows() as $p) {
					$class = (in_array($p->getNodeID(), $this->activeNodes) ? 'expanded' : 'collapsed');
					$out .= '<li id="node_'.$p->getNodeID().'"><div><div class="hitarea '.$class.'"></div><a href="'.$this->getRoute(NULL, 'pageproperty', array('edit', $p->getNodeID())).'" data-id="'.$p->getNodeID().'"><img src="/plugin/'. $this->plugin->name .'/admin/gfx/ico/'. $p->getIcon().'" alt="" />'.$p->getTitle().'</a></div>';
					$out .= $this->loopPages($p);
					$out .= '</li>';
				}
				$out .= '</ol>';
				return $out;
			}
		}
		return '';
	}
}