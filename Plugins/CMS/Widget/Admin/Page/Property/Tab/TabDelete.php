<?php
namespace CMS\Widget\Admin\Page\Property\Tab;
class TabDelete extends \CMS\Widget\Admin\Page\Property\PropertyAbstract {
	public function __construct($propertyTabId) {
		parent::__construct();

		/* @var $propertytab \CMS\Model\Property\PropertyTab */
		$propertytab = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyTab');

		$tab = $propertytab::GetByPropertyTabID($propertyTabId);
		if($tab->hasRow()) {
			$tab->delete();
		}

		\PC\Router::GoBack();
	}
}