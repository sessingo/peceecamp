<?php
namespace CMS\Widget\Admin\Page\Property\Module;
class ModuleDelete extends \CMS\Widget\Admin\Page\Property\PropertyAbstract {
	public function __construct($propertyModuleId) {
		parent::__construct();

		/* @var $propertymodule \CMS\Model\Property\PropertyModule */
		$propertymodule = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyModule');

		$module = $propertymodule::GetByPropertyModuleId($propertyModuleId);
		if($module->hasRow()) {
			$module->delete();
		}

		\PC\Router::GoBack();
	}
}