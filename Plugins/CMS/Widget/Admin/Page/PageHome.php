<?php
namespace CMS\Widget\Admin\Page;
class PageHome extends \CMS\Widget\Admin\Page\PageAbstract {
	protected $pages;
	protected $activeNodes;
	public function __construct() {
		parent::__construct();

		$this->prependSiteTitle($this->_('Page/Pages'));

		$this->getSite()->addJs('/plugin/CMS/admin/js/jquery.ui.nestedSortable.js');
		$this->getSite()->addJs('/plugin/CMS/admin/js/jquery.contextmenu.js');

		$this->activeNodes = array();
		if(\Pecee\Cookie::Exists('PageActiveNodes')) {
			$this->activeNodes = explode(',',\Pecee\Cookie::Get('PageActiveNodes', ''));
		}

		if(count($this->activeNodes) > 0) {
			/* @var $modelpage \CMS\Model\ModelPage */
			$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');
			$this->pages = $modelpage::Get(NULL, NULL, NULL, 0);
		}
	}

	protected function loopPages(\CMS\Model\ModelPage $page) {
		if($page->hasRow()) {
			if(in_array($page->getNodeID(), $this->activeNodes)) {
				$out = '<ol>';

				/* @var $modelpage \CMS\Model\ModelPage */
				$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

				$pages = $modelpage::Get(NULL, NULL, NULL, $page->getNodeID());
				if($pages->hasRows()) {
					foreach($pages->getRows() as $p) {
						$properties = (is_array($p->getProperty()->getProperties())) ? join(',', $p->getProperty()->getProperties()) : '';
						$class = (in_array($p->getNodeID(), $this->activeNodes) ? 'expanded' : 'collapsed');
						$out .= '<li id="node_'.$p->getNodeID().'" data-accepts="'.$properties.'" data-propertyId="'.$p->getProperty()->getNodeID().'"><div><div class="hitarea '.$class.'"></div><a href="'.$this->getRoute(NULL, 'page', array('edit', $p->getNodeID())).'" data-id="'.$p->getNodeID().'" class="js-page" data-slug="'.$p->getSlug().'" data-level="'.$p->getLevel().'"><img src="/plugin/'.$this->plugin->name.'/admin/gfx/ico/'.$p->getProperty()->getIcon().'" alt="" />'.$p->getTitle().'</a></div>';
						if(in_array($p->getNodeID(), $this->activeNodes)) {
							$out .= $this->loopPages($p);
						}
						$out .= '</li>';
					}
				}
				$out .= '</ol>';
				return $out;
			}
		}
	}
}