<?php
namespace CMS\Widget\Admin\Iframe;
abstract class IframeAbstract extends \PC\Widget\Admin\Iframe\IframeAbstract {
	public function __construct($adminLevel=0) {
		parent::__construct($adminLevel);

		$this->getSite()->addCss($this->getRessource('admin/css/iframe.css'));

		$this->getSite()->addJs($this->getRessource('admin/js/jquery.cookie.js'));
		$this->getSite()->addJs($this->getRessource('admin/js/tree.js'));
	}
}