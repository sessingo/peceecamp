<?php
namespace CMS\Widget\Admin\Iframe\Settings;
class Thumbnails extends \CMS\Widget\Admin\Iframe\IframeAbstract {
	protected $settings;
	public function __construct() {
		parent::__construct(1);

		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$this->settings = $modelsettings::GetInstance();

		if($this->isPostBack()) {

			if($this->settings->hasRow() && !$this->hasErrors()) {

				$thumbnails = array();
				foreach($this->data->name as $key=>$name) {
					if(!empty($name)) {
						$thumbnails[] = array($name => array($this->data->width[$key], $this->data->height[$key]));
					}
				}

				$this->settings->setThumbnails($thumbnails);
				$this->settings->update();

				$this->setMessage($this->_('Settings/SettingsSaved'), 'success');
				\PC\Router::Refresh();
			}
		}
	}

	protected function getValue($width,$index=NULL) {
		$key = array_keys($width);
		$val = $key[0];
		if(!is_null($index)) {
			$val = $width[$val][$index];
		}
		return $val;
	}
}