<?php
namespace CMS\Widget\Admin\Iframe\Settings;
class Variables extends \CMS\Widget\Admin\Iframe\IframeAbstract {
	protected $settings;
	public function __construct() {
		parent::__construct(1);

		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$this->settings = $modelsettings::GetInstance();

		if($this->isPostBack()) {
			if($this->settings->hasRow() && !$this->hasErrors()) {
				$out = array();
				foreach($this->data->variableName as $i=>$name) {
					if(strlen($name) > 0) {
						$out[$name] = $this->data->variableData[$i];
					}
				}

				$this->settings->setSiteData($out);
				$this->settings->update();

				$this->setMessage($this->_('Settings/SettingsSaved'), 'success');
				\PC\Router::Refresh();
			}
		}
	}
}