<?php
namespace CMS\Widget\Admin\Iframe\Settings;
class Site extends \CMS\Widget\Admin\Iframe\IframeAbstract {
	protected $settings;
	public function __construct() {
		parent::__construct(1);

		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$this->settings = $modelsettings::GetInstance();

		if($this->isPostBack()) {

			if($this->settings->hasRow() && !$this->hasErrors()) {
				$tmp = '404nodeId';
				$this->settings->set404NodeId($this->data->$tmp);
				$this->settings->setFrontpageNodeId($this->data->frontpageNodeId);

				$this->settings->update();

				$this->setMessage($this->_('Settings/SettingsSaved'), 'success');
				\PC\Router::Refresh();
			}
		}
	}
}