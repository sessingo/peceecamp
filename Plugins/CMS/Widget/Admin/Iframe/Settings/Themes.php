<?php
namespace CMS\Widget\Admin\Iframe\Settings;
class Themes extends \CMS\Widget\Admin\Iframe\IframeAbstract {
	protected $settings;
	protected $themes;
	public function __construct() {
		parent::__construct(1);

		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$this->settings = $modelsettings::GetInstance();

		$this->themes = array();

		foreach($this->plugin->themes as $theme) {
			$this->themes[$theme] = $theme;
		}

		if($this->isPostBack()) {

			if($this->settings->hasRow() && !$this->hasErrors()) {

				$themes = array();
				if($this->data->themeHost) {
					foreach($this->data->themeHost as $i=>$host) {
						if($host) {
							$themes[$host] = $this->data->themeTheme[$i];
						}
					}
				}

				$this->settings->setThemes($themes);
				$this->settings->update();

				$this->setMessage($this->_('Settings/SettingsSaved'), 'success');
				\PC\Router::Refresh();
			}
		}
	}
}