<?php
namespace CMS\Widget\Admin\Iframe\Settings;
class Files extends \CMS\Widget\Admin\Iframe\IframeAbstract {
	protected $settings;
	public function __construct() {
		parent::__construct(1);

		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$this->settings = $modelsettings::GetInstance();

		if($this->isPostBack()) {

			$this->addInputValidation($this->_('Settings/MaxFilesize'), 'maxFilesize', new \PC\UI\Form\Validate\ValidateInteger());
			$this->addInputValidation($this->_('Settings/FileTypes'), 'filetypes', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			if($this->settings->hasRow() && !$this->hasErrors()) {
				$this->settings->setFileUrl($this->data->fileUrl);
				$this->settings->setMaxFileSize($this->data->maxFilesize);
				$this->settings->setFileTypes($this->data->filetypes);
				$this->settings->setFileStoragePath($this->data->fileStoragePath);
				$this->settings->setCreateThumbs($this->data->createThumbs);

				$this->settings->update();

				$this->setMessage($this->_('Settings/SettingsSaved'), 'success');
				\PC\Router::Refresh();
			}
		}
	}
}