<?php
namespace CMS\Widget\Admin\Iframe\Settings;
class Macro extends \CMS\Widget\Admin\Iframe\IframeAbstract {
	protected $macro;
	protected $macros;
	protected $moduleContent;
	public function __construct() {
		parent::__construct(1);

		/* @var $macromodel \CMS\Model\ModelMacro */
		$macromodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelMacro');

		$this->macro = $macromodel::GetById($this->getParam('nodeId'));

		$this->macros = new \CMS\Dataset\DatasetMacros();
		$this->macros = $this->macros->getArray();

		if($this->macro && $this->macro->hasRow()) {

			$modules = $this->macro->getModules();

			if($modules->hasRows()) {
				foreach($modules->getRows() as $key=>$module) {
					if($this->data->macroPropertyDeleted[$module->getPropertyModuleID()] == '1') {
						$module->delete();
					}
				}
			}


			if($this->isPostBack() && !$this->data->postback) {
				$this->addInputValidation($this->_('Settings/Macro/Title'), 'title', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
				$this->addInputValidation($this->_('Settings/Macro/Widget'), 'widget', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

				if(!$this->hasErrors()) {

					// Update macro
					$this->macro->setTitle($this->data->title);
					$this->macro->setWidget($this->data->widget);
					$this->macro->update();

					$this->setMessage($this->_('Settings/Macro/MacroUpdated'), 'success');

					\PC\Router::Refresh();

				}
			}
		}
	}
}