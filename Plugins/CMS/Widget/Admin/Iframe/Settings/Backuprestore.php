<?php
namespace CMS\Widget\Admin\Iframe\Settings;
class Backuprestore extends \CMS\Widget\Admin\Iframe\IframeAbstract {
	protected $nodesRestored;
	public function __construct() {
		parent::__construct(1);

		if($this->isPostBack()) {

			$file = $this->files->file;
			$this->addInputValidation($this->_('Settings/BackupRestore/BackupFile'), 'file', new \PC\UI\Form\Validate\File\FileNotNullOrEmpty($file));

			if(!$this->hasErrors()) {

				$xml = file_get_contents($file->tmpName);

				if(!\Pecee\Util::is_valid_xml($xml)) {
					$this->setError($this->_('Settings/BackupRestore/InvalidBackupFile'));
				}

				if(!$this->hasErrors()) {

					$doc = new \SimpleXMLElement($xml);
					$nodesRestored = $this->saveNodes($doc->Nodes);

					$this->setMessage($this->_('Settings/BackupRestore/PagesRestored', $nodesRestored), 'success');

					\PC\Router::Refresh();
				}
			}
		}
	}

	protected function saveNodes(\SimpleXMLElement $nodes) {
		$output = 0;
		/* @var $node SimpleXMLElement */
		foreach($nodes->Node as $node) {
			$n = $this->getClass((string)$node['Type']);
			if($n instanceof \Pecee\Model\Node\ModelNode) {
				$n->NodeID = (string)$node['NodeID'];
				$n->ParentNodeID = (string)$node['ParentNodeID'];
				$n->Path = (string)$node['Path'];
				$n->Type = (string)$node['Type'];
				$n->Title = (string)$node['Title'];
				$n->Content = (string)$node->Content;
				$n->PubDate = (string)$node['PubDate'];
				$n->ChangedDate = (string)$node['ChangedDate'];

				if((string)$node['ActiveFrom']) {
					$n->ActiveFrom = (string)$node['ActiveFrom'];
				}


				if((string)$node['ActiveTo']) {
					$n->ActiveTo = (string)$node['ActiveTo'];
				}

				$n->Active = (string)$node['Active'];
				$n->Level = (string)$node['Level'];
				$n->Order = (string)$node['Order'];

				/* @var $data SimpleXMLElement */
				foreach($node->Data->children() as $data) {
					$name = (string)$data[@name];
					$n->data->set($name, (string)$data);
				}

				$propertyNode = $nodes->xpath('/Site/Properties/Property[@NodeID=\''. $n->getPropertyNodeId() .'\']');
				if($propertyNode) {
					$propertyNode = $propertyNode[0];

					/* @var $modeluser \CMS\Model\ModelUser */
					$modeluser = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelUser');

					if($n->getType() == $modeluser::TYPE_USER) {
						/* @var $userproperty \CMS\Model\User\UserProperty */
						$userproperty = \PC\ObjectManager::GetInstance()->get('\CMS\Model\User\UserProperty');

						$p = new $userproperty();
					} else {
						/* @var $propertymodel \CMS\Model\ModelProperty */
						$propertymodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');

						$p = new $propertymodel();
					}

					if($propertyNode) {
						$p->NodeID = $n->getPropertyNodeId();
						$p->ParentNodeID = (string)$propertyNode['ParentNodeID'];
						$p->Path = (string)$propertyNode['Path'];
						$p->Type = (string)$propertyNode['Type'];
						$p->Title = (string)$propertyNode['Title'];
						$p->Content = (string)$propertyNode->Content;
						$p->PubDate = (string)$propertyNode['PubDate'];
						$p->ChangedDate = (string)$propertyNode['ChangedDate'];
						$p->Active = (string)$propertyNode['Active'];
						$p->Level = (string)$propertyNode['Level'];
						$p->Order = (string)$propertyNode['Order'];

						/* @var $data SimpleXMLElement */
						foreach($propertyNode->Data->children() as $data) {
							$name = (string)$data[@name];
							$p->data->set($name, (string)$data);
						}
					}
					try {
						$p->save();
					} catch(\Exception $e) {

					}
					$n->setProperty($p);
				}

				$nodesSaved = $this->saveNodes($node) + 1;

				try {
					$n->save();
					$output = $output + $nodesSaved;
				} catch(\Exception $e) {

				}
			}

		}
		return $output;
	}

	public function getClass($type) {

		/* @var $modelfile \CMS\Model\ModelFile */
		$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

		/* @var $modelpage \CMS\Model\ModelPage */
		$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

		/* @var $modelfolder \CMS\Model\ModelFolder */
		$modelfolder = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFolder');

		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		/* @var $modeluser \CMS\Model\ModelUser */
		$modeluser = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelUser');

		switch($type) {
			case $modelsettings::TYPE_SETTINGS:
				return new $modelsettings();
			case $modelpage::TYPE_PAGE:
				return new $modelpage();
			case $modeluser::TYPE_USER:
				return new $modeluser();
			case $modelfolder::TYPE_FOLDER:
				return new $modelfolder();
			case $modelfile::TYPE_FILE:
				return new $modelfile();
		}
	}
}