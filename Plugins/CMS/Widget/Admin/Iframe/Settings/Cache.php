<?php
namespace CMS\Widget\Admin\Iframe\Settings;
class Cache extends \CMS\Widget\Admin\Iframe\IframeAbstract {
	protected $settings;
	public function __construct() {
		parent::__construct(1);

		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$this->settings = $modelsettings::GetInstance();

		if($this->isPostBack()) {
			if($this->settings->hasRow() && !$this->hasErrors()) {
				$this->settings->setDisableCache($this->data->disableCache);

				$this->settings->update();

				$this->setMessage($this->_('Settings/SettingsSaved'), 'success');
				\PC\Cache::GetInstance()->clear();
				\PC\Router::Refresh();
			}
		}
	}

	protected function getCacheType() {
		/* @var $cacheclass \PC\Cache */
		$cacheclass = \PC\ObjectManager::GetInstance()->get('\PC\Cache');
		return $cacheclass::GetInstance()->getType();
	}
}