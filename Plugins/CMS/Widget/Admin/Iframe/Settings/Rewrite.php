<?php
namespace CMS\Widget\Admin\Iframe\Settings;
class Rewrite extends \CMS\Widget\Admin\Iframe\IframeAbstract {
	protected $page;
	protected $rewrites;
	protected $rewrite;
	public function __construct() {
		parent::__construct(1);

		$this->prependSiteTitle($this->_('Settings/UrlRewrite/UrlRewrite'));

		$this->page = $this->getParam('page',0);

		/* @var $rewritemodel \CMS\Model\ModelRewrite */
		$rewritemodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelRewrite');

		$this->rewrites = $rewritemodel::Get(20, $this->page);

		if(!is_null($this->getParam('rewriteId'))) {
			$this->rewrite = $rewritemodel::GetByRewriteID($this->getParam('rewriteId'));
		}

		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		$settings = $modelsettings::GetInstance();
		$settings->setHasRewrites($rewritemodel::HasRewrites());
		$settings->update();

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Settings/UrlRewrite/OriginalUrl'), 'originalUrl', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			if(!$this->data->urlRewrite && !$this->data->nodeId) {
				$this->setError($this->_('Settings/UrlRewrite/EmptyRewriteUrlOrPage'));
			}


			if(!$this->hasErrors()) {

				if($this->rewrite && $this->rewrite->hasRow()) {
					$this->rewrite->setOriginalUrl($this->data->originalUrl);
					$this->rewrite->setRewriteUrl($this->data->UrlRewrite);
					$this->rewrite->setHost($this->data->host);
					$this->rewrite->setRegex($this->data->regex);
					$this->rewrite->setNodeID($this->data->nodeId);
					$this->rewrite->update();
					$this->setMessage($this->_('Settings/UrlRewrite/RuleUpdated'), 'success');

					\PC\Router::Refresh();
				}

				$rule = $rewritemodel::GetByOriginalUrl($this->data->originalUrl);
				if($rule->hasRow()) {
					$this->setError($this->_('Settings/UrlRewrite/RuleAlreadyExists'));
				}

				if(!$this->hasErrors()) {
					$rewrite = new $rewritemodel();
					$rewrite->setOriginalUrl($this->data->originalUrl);
					$rewrite->setRewriteUrl($this->data->UrlRewrite);
					$rewrite->setNodeID($this->data->nodeId);
					$rewrite->setHost($this->data->host);
					$rewrite->setRegex($this->data->regex);
					$rewrite->save();

					$this->setMessage($this->_('Settings/UrlRewrite/RuleCreated'), 'success');

					\PC\Router::Refresh();
				}
			}
		}
	}
}