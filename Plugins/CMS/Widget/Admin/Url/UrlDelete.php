<?php
namespace CMS\Widget\Admin\Url;
class UrlDelete extends \CMS\Widget\Admin\AdminAbstract {
	public function __construct($rewriteId) {
		parent::__construct();

		/* @var $rewritemodel \CMS\Model\ModelRewrite */
		$rewritemodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelRewrite');

		$rewrite = $rewritemodel::GetByRewriteID($rewriteId);
		if($rewrite->hasRow()) {
			$rewrite->delete();
			$this->setMessage($this->_('Settings/UrlRewrite/RuleDeleted'), 'success');
		}

		\PC\Router::GoBack();
	}
}