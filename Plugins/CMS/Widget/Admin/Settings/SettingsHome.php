<?php
namespace CMS\Widget\Admin\Settings;
class SettingsHome extends \CMS\Widget\Admin\AdminAbstract {
	protected $macros;
	public function __construct() {
		parent::__construct(1);
		$this->getMenu()->getItem(4)->addClass('active');
		$this->addLevel($this->_('Settings/Settings'), $this->getRoute(NULL, 'settings'));
		$this->getSite()->addJs('/plugin/CMS/admin/js/jquery.contextmenu.js');


		$this->prependSiteTitle($this->_('Settings/Settings'));

		/* @var $macromodel \CMS\Model\ModelMacro */
		$macromodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelMacro');

		$this->macros = $macromodel::Get();
	}
}