<?php
namespace CMS\Widget\Admin;
abstract class AdminAbstract extends \PC\Widget\Admin\AdminAbstract {
	public function __construct($adminLevel=0) {
		parent::__construct($adminLevel);

		$this->getSite()->addCss('/plugin/CMS/admin/css/tree.css');
		$this->getSite()->addCss('/plugin/CMS/admin/css/cms.css');
		$this->getSite()->addJs('/plugin/CMS/admin/js/jquery.cookie.js');
		$this->getSite()->addJs('/plugin/CMS/admin/js/tree.js');
	}
}