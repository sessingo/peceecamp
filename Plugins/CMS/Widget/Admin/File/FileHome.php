<?php
namespace CMS\Widget\Admin\File;
class FileHome extends \CMS\Widget\Admin\File\FileAbstract {
	protected $folders;
	protected $files;
	protected $activeNodes;
	public function __construct() {
		parent::__construct();

		$this->prependSiteTitle($this->_('File/Files'));

		$this->getSite()->addJs('/plugin/CMS/admin/js/jquery.ui.nestedSortable.js');
		$this->getSite()->addJs('/plugin/CMS/admin/js/jquery.contextmenu.js');

		$this->activeNodes = array();
		if(\Pecee\Cookie::Exists('FileActiveNodes')) {
			$this->activeNodes = explode(',',\Pecee\Cookie::Get('FileActiveNodes', ''));
		}

		if(count($this->activeNodes) > 0) {
			/* @var $modelfolder \CMS\Model\ModelFolder */
			$modelfolder = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFolder');

			$this->folders = $modelfolder::Get(NULL, NULL, 0, NULL, NULL, NULL);

			/* @var $modelfile \CMS\Model\ModelFile */
			$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');
			$this->files = $modelfile::Get(NULL, NULL, NULL, 0);
		}
	}

	protected function loopFolders(\CMS\Model\ModelFolder $folder) {
		if($folder->hasRow()) {
			if(in_array($folder->getNodeID(), $this->activeNodes)) {
				$out = '<ol>';

				/* @var $modelfolder \CMS\Model\ModelFolder */
				$modelfolder = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFolder');

				$folders = $modelfolder::Get(NULL, NULL, $folder->getNodeID(), NULL, NULL, NULL);
				if($folders->hasRows()) {
					foreach($folders->getRows() as $f) {
						$class = (in_array($f->getNodeID(), $this->activeNodes) ? 'expanded' : 'collapsed');
						$out .= '<li id="node_'.$f->getNodeID().'"><div><div class="hitarea '.$class.'"></div><a href="javascript:;" class="js-folder" data-id="'.$f->getNodeID().'"><img src="/plugin/'.$this->plugin->name.'/admin/gfx/ico/folder.png" alt="" />'.$f->getTitle().'</a></div>';
						if(in_array($f->getNodeID(), $this->activeNodes)) {
							$out .= $this->loopFolders($f);
						}
						$out .= '</li>';
					}
				}

				/* @var $modelfile \CMS\Model\ModelFile */
				$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

				$files = $modelfile::Get(NULL, NULL, NULL, $folder->getNodeID());
				if($files->hasRows()) {
					foreach($files->getRows() as $f) {
						$out .= '<li id="file_'.$f->getNodeID().'" class="no-nesting"><div><a href="'.$f->getUrl().'" class="js-file js-overlay" data-id="'.$f->getNodeID().'"><img src="/plugin/'.$this->plugin->name.'/admin/gfx/ico/application.png" alt="" />'.$f->getOriginalFilename().'</a></div></li>';
					}
				}


				$out .= '</ol>';
				return $out;
			}
		}
	}
}