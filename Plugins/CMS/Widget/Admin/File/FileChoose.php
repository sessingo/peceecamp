<?php
namespace CMS\Widget\Admin\File;
class FileChoose extends \PC\Widget\Admin\AdminAbstract {
	protected $name;
	protected $value;
	protected $page;
	protected $defaultNode;
	public function __construct($name, $value=NULL, $defaultNode = NULL) {
		parent::__construct(0, FALSE);
		$this->name = $name;
		$this->value = ($this->isPostBack()) ? $this->data->$name : $value;

		$this->defaultNode = (is_null($defaultNode) ? $this->getParam('nodeId') : $defaultNode);

		if(!is_null($this->value)) {

			/* @var $modelfolder \CMS\Model\ModelFolder */
			$modelfolder = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFolder');

			$this->page = $modelfolder::GetByNodeID($this->value);
		}

		$this->setTemplate(NULL);
		$this->_contentHtml = NULL;
	}
}