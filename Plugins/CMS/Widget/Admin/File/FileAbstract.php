<?php
namespace CMS\Widget\Admin\File;
abstract class FileAbstract extends \CMS\Widget\Admin\AdminAbstract {
	public function __construct() {
		parent::__construct();
		$this->addLevel($this->_('File/Files'), $this->getRoute(NULL, 'file'));
		$this->getMenu()->getItem(1)->addClass('active');
	}
}