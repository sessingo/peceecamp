<?php
namespace CMS\Widget\Admin\User\Property;
abstract class PropertyAbstract extends \CMS\Widget\Admin\User\UserAbstract {
	public function __construct() {
		parent::__construct(1);
		$this->addLevel($this->_('Property/Structure'), $this->getRoute(NULL, 'userproperty'));
	}
}