<?php
namespace CMS\Widget\Admin\User\Property;
class PropertyEdit extends \CMS\Widget\Admin\User\Property\PropertyAbstract {
	protected $property;
	public function __construct($nodeId) {
		parent::__construct();

		$this->addLevel($this->_('Property/Edit'));

		$this->prependSiteTitle($this->_('Property/Edit'));

		$this->getSite()->addJs('/plugin/CMS/admin/js/jquery.ui.nestedSortable.js');

		/* @var $userproperty \CMS\Model\User\UserProperty */
		$userproperty = \PC\ObjectManager::GetInstance()->get('\CMS\Model\User\UserProperty');

		$this->property = $userproperty::GetByNodeID($nodeId);
		if(!$this->property->hasRow()) {
			\PC\Router::Redirect($this->getRoute(NULL, 'property'));
		}

		if($this->isPostBack()) {
			$this->addInputValidation($this->_('Property/Title'), 'title', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation($this->_('Property/Alias'), 'alias', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			$this->addInputValidation($this->_('Property/Icon'), 'icon', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			/* @var $userproperty \CMS\Model\User\UserProperty */
			$userproperty = \PC\ObjectManager::GetInstance()->get('\CMS\Model\User\UserProperty');

			$alias = $userproperty::GetByAlias($this->data->alias);
			if($alias->hasRow() && $alias->getNodeID() != $this->property->getNodeID()) {
				$this->setError($this->_('Property/AliasExists'));
			}

			if(!$this->hasErrors()) {
				$this->property->setTitle($this->data->title);
				$this->property->setIcon($this->data->icon);
				$this->property->setAlias($this->data->alias);
				$this->property->setProperties($this->data->properties);
				$this->property->update();

				$this->setMessage($this->_('Property/PropertySaved'), 'success');
				\PC\Router::Redirect($this->getRoute(NULL, 'userproperty'));
			}
		}
	}

	protected function loopProperties($parentNodeId = 0) {
		/* @var $userproperty \CMS\Model\User\UserProperty */
		$userproperty = \PC\ObjectManager::GetInstance()->get('\CMS\Model\User\UserProperty');

		$pages = $userproperty::Get(NULL, NULL, $parentNodeId, NULL, NULL, NULL);
		if($pages->hasRows()) {
			$css = ($parentNodeId > 0) ? ' style="display:none;"' : '';
			$out = '<ol class="js-page"'.$css.'>';
			foreach($pages->getRows() as $p) {
				$active = (is_array($this->property->getProperties()) && in_array($p->getNodeID(), $this->property->getProperties()));
				$out .= '<li id="node_'.$p->getNodeID().'"><div><div class="hitarea collapsed"></div><a href="javascript:;" data-type="property" data-id="'.$p->getNodeID().'"'.(($active) ? ' class="active"' : '').'><img src="/plugin/'. $this->plugin->name .'/admin/gfx/ico/'. $p->getIcon().'" alt="" />'.$p->getTitle().'</a></div>';
				$out .= '<input type="checkbox" name="properties[]" value="'.$p->getNodeID().'" '.(($active) ? 'checked="checked"' : '').'style="display:none;" />';
				$out .= $this->loopProperties($p->getNodeID());
				$out .= '</li>';
			}
			$out .= '</ol>';
			return $out;
		}
		return '';
	}
}