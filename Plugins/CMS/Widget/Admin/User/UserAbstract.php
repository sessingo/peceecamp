<?php
namespace CMS\Widget\Admin\User;
abstract class UserAbstract extends \CMS\Widget\Admin\AdminAbstract {
	public function __construct($adminLevel=0) {
		parent::__construct($adminLevel);
		$this->addLevel($this->_('User/User'), $this->getRoute(NULL, 'user'));
		$this->getMenu()->getItem(2)->addClass('active');
	}
}