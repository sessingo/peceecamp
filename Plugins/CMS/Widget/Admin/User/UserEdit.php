<?php
namespace CMS\Widget\Admin\User;
class UserEdit extends \CMS\Widget\Admin\User\UserAbstract {
	protected $page;
	protected $history;
	protected $modules;
	public function __construct($nodeId) {
		parent::__construct();

		/* @var $modeldraft \CMS\Model\ModelDraft */
		$modeldraft = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelDraft');

		/* @var $modeluser \CMS\Model\ModelUser */
		$modeluser = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelUser');

		if($this->hasParam('draftId')) {
			$this->page = $modeldraft::GetByNodeID($this->getParam('draftId'));
		} else {
			$this->page = $modeluser::GetByNodeID($nodeId);
		}

		$this->history = $modeldraft::GetByPageID($this->page->getNodeID(), NULL, NULL, NULL, 40);

		$this->modules = array();
		$this->renderModules();

		if(!$this->page->hasRow()) {
			\PC\Router::Redirect($this->getRoute(NULL, 'user'));
		}

		$this->addLevel($this->_('User/Edit'));

		$this->prependSiteTitle($this->_('User/Edit'));

		$this->getSite()->addCss('/plugin/CMS/admin/data/redactor/redactor/redactor.css');
		$this->getSite()->addJs('/plugin/CMS/admin/data/redactor/redactor/redactor.min.js');
	}

	protected function renderModules() {
		$tabs = $this->page->getProperty()->getTabs();

		if(count($tabs) > 0) {
			foreach($tabs as $tab) {
				$modules = $this->page->getProperty()->getModules($tab->getPropertyTabID());
				if(count($modules) > 0) {
					foreach($modules as $module) {
						$widget = $module->getModule();
						if(class_exists($widget)) {
							$widget = new $widget();
							$widget->renderAdmin($module,$this->page);
							$widget->onAdminRender();
							$this->modules[$tab->getName()][] = $widget;
						}
					}
				}
			}
		}
	}

	public function render() {

		$this->addInputValidation($this->_('User/Name'), 'title', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

		if($this->isPostBack() && !$this->hasErrors()) {
			if(!$this->hasErrors()) {
				$this->page->setTitle($this->data->title);

				if($this->data->password) {
					$this->page->setPassword(md5($this->data->password));
				}

				$this->page->update();

				$this->setMessage($this->_('User/UserSaved'), 'success');

				\PC\Router::Refresh();
			}
		}

		return parent::render();
	}

	protected function getDisableHistory() {
		/* @var $modelsettings \CMS\Model\ModelSettings */
		$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

		return $modelsettings::GetInstance()->getDisableHistory();
	}
}