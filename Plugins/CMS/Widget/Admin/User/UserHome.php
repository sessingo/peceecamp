<?php
namespace CMS\Widget\Admin\User;
class UserHome extends \CMS\Widget\Admin\User\UserAbstract {
	protected $users;
	protected $activeNodes;
	public function __construct() {
		parent::__construct();

		$this->prependSiteTitle($this->_('User/User'));

		$this->getSite()->addJs('/plugin/CMS/admin/js/jquery.ui.nestedSortable.js');
		$this->getSite()->addJs('/plugin/CMS/admin/js/jquery.contextmenu.js');

		$this->activeNodes = array();
		if(\Pecee\Cookie::Exists('UserActiveNodes')) {
			$this->activeNodes = explode(',',\Pecee\Cookie::Get('UserActiveNodes', ''));
		}

		if(count($this->activeNodes) > 0) {
			/* @var $modeluser \CMS\Model\ModelUser */
			$modeluser = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelUser');

			$this->users = $modeluser::Get(NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL);
		}
	}

	protected function loopPages(\CMS\Model\ModelUser $page) {
		if($page->hasRow()) {
			if(in_array($page->getNodeID(), $this->activeNodes)) {
				$out = '<ol>';

				/* @var $modeluser \CMS\Model\ModelUser */
				$modeluser = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelUser');

				$users = $modeluser::Get(NULL, NULL, NULL, $page->getNodeID(), NULL, NULL, NULL, NULL);
				if($users->hasRows()) {
					foreach($users->getRows() as $u) {
						$class = (in_array($u->getNodeID(), $this->activeNodes) ? 'expanded' : 'collapsed');
						$properties = (is_array($u->getProperty()->getProperties())) ? join(',', $u->getProperty()->getProperties()) : '';
						$out .= '<li id="node_'.$u->getNodeID().'" data-accepts="'.$properties.'" data-propertyId="'.$u->getProperty()->getNodeID().'"><div><div class="hitarea '.$class.'"></div><a href="'.$this->getRoute(NULL, 'user', array('edit', $u->getNodeID())).'" class="js-user" data-id="'.$u->getNodeID().'"><img src="/plugin/'.$this->plugin->name.'/admin/gfx/ico/'.$u->getProperty()->getIcon().'" alt="" />'.$u->getUsername().'</a></div></li>';
					}
				}

				$out .= '</ol>';
				return $out;
			}
		}
	}
}