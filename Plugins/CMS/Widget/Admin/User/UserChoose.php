<?php
namespace CMS\Widget\Admin\User;
class UserChoose extends \PC\Widget\Admin\AdminAbstract {
	protected $name;
	protected $value;
	protected $page;
	protected $defaultNode;
	protected $properties;
	public function __construct($name, $value=NULL, $defaultNode = NULL, $properties = NULL) {
		parent::__construct(0, FALSE);
		$this->name = $name;
		$this->value = ($this->isPostBack()) ? $this->data->$name : $value;
		$this->properties = $properties;

		$this->defaultNode = (is_null($defaultNode) ? $this->getParam('nodeId') : $defaultNode);

		if(!is_null($this->value)) {
			/* @var $modeluser \CMS\Model\ModelUser */
			$modeluser = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelUser');

			$this->page = $modeluser::GetByNodeID($this->value);
		}

		$this->setTemplate(NULL);
		$this->_contentHtml = NULL;
	}
}