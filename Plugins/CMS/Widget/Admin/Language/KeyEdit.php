<?php
namespace CMS\Widget\Admin\Language;
class KeyEdit extends \CMS\Widget\Admin\Language\LanguageAbstract {
	protected $languages;
	protected $key;
	public function __construct($keyId) {
		parent::__construct();

		/* @var $languagekey \CMS\Model\Language\LanguageKey */
		$languagekey = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Language\LanguageKey');

		/* @var $languagetext \CMS\Model\Language\LanguageText */
		$languagetext = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Language\LanguageText');

		/* @var $languagemodel \CMS\Model\ModelLanguage */
		$languagemodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelLanguage');

		$this->languages = \CMS\Model\ModelLanguage::Get();
		$this->key = $languagekey::GetByID($keyId);

		$this->addLevel($this->_('Language/Key/Edit'));
		$this->prependSiteTitle($this->_('Language/Key/Edit'));

		if($this->isPostBack()) {

			$this->addInputValidation($this->_('Language/Key/Name'), 'name', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());

			$key = $languagekey::GetByKey($this->data->name, $this->key->getParentLanguageKeyID());
			if($key->hasRow() && $key->getLanguageKeyID() != $this->key->getLanguageKeyID()) {
				$this->setError($this->_('Language/Key/KeyAlreadyExist'));
			}

			if(!$this->hasErrors()) {
				$texts = $this->key->getTexts();

				foreach($this->data->text as $lang=>$newText) {
					$text = NULL;
					if(count($texts) > 0) {
						foreach($texts as $t) {
							if($t->getLanguageAlias() == $lang) {
								$text = $t;
								break;
							}
						}
					}

					if($text) {
						$text->setText($newText);
						$text->update();
					} else {
						$language = $languagemodel::GetByAlias($lang);
						if($language->hasRow()) {
							$text = new $languagetext();
							$text->setUserID(\Pecee\Model\User\ModelUser::Current()->getUserID());
							$text->setLanguageID($language->getLanguageID());
							$text->setLanguageKeyID($this->key->getLanguageKeyID());
							$text->setText($newText);
							$text->save();
						}
					}
				}

				$this->key->setName($this->data->name);
				$this->key->setChangedDate(\Pecee\Date::ToDateTime());
				$this->key->update();

				$this->setMessage($this->_('Language/Key/KeySaved'), 'success');

				\PC\Router::Redirect($this->getRoute(NULL, 'language'));
			}
		}
	}
}