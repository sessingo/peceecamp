<?php
namespace CMS\Widget\Admin\Language;
class LanguageHome extends \CMS\Widget\Admin\Language\LanguageAbstract {
	protected $languages;
	protected $keys;
	protected $activeKeys;
	protected $activeLanguage;
	public function __construct() {
		parent::__construct();

		$this->prependSiteTitle($this->_('Property/Structure'));

		$this->getSite()->addJs('/plugin/CMS/admin/js/jquery.ui.nestedSortable.js');
		$this->getSite()->addJs('/plugin/CMS/admin/js/jquery.contextmenu.js');

		/* @var $propertymodel \CMS\Model\ModelProperty */
		$propertymodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelProperty');

		$this->activeKeys = array();
		if(\Pecee\Cookie::Exists('LanguageKeysActive')) {
			$this->activeKeys = explode(',',\Pecee\Cookie::Get('LanguageKeysActive', ''));
		}

		$this->activeLanguage = array();
		if(\Pecee\Cookie::Exists('LanguageActive')) {
			$this->activeLanguage = explode(',',\Pecee\Cookie::Get('LanguageActive', ''));
		}

		/* @var $languagekey \CMS\Model\Language\LanguageKey */
		$languagekey = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Language\LanguageKey');

		/* @var $languagemodel \CMS\Model\ModelLanguage */
		$languagemodel = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelLanguage');

		$this->languages = $languagemodel::Get();
		$this->keys = $languagekey::Get();
	}

	protected function loopKeys($parentLanguageKeyId=0) {

		/* @var $languagekey \CMS\Model\Language\LanguageKey */
		$languagekey = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Language\LanguageKey');

		$keys = $languagekey::Get($parentLanguageKeyId);
		if($keys->hasRows()) {
			$style = (in_array((string)$parentLanguageKeyId, $this->activeKeys)) ? '' : ' style="display:none;"';
			$out = '<ol class="js-page"'.$style.'>';
			foreach($keys->getRows() as $key) {
				$class = (in_array($key->getLanguageKeyID(), $this->activeKeys) ? 'expanded' : 'collapsed');
				$out .= '<li id="node_'.$key->getLanguageKeyID().'"><div><div class="hitarea '.$class.'"></div><a href="'.$this->getRoute(NULL, 'language', array('edit', $key->getLanguageKeyID())).'" data-id="'.$key->getLanguageKeyID().'"><img src="/plugin/'. $this->plugin->name .'/admin/gfx/ico/note.png" alt="" />'.$key->getName().'</a></div>';
				$out .= $this->loopKeys($key->getLanguageKeyID());
				$out .= '</li>';
			}
			$out .= '</ol>';
			return $out;
		}
		return '';
	}
}