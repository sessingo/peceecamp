<?php
namespace CMS\Widget\Admin\Language;
abstract class LanguageAbstract extends \CMS\Widget\Admin\AdminAbstract {
	public function __construct() {
		parent::__construct(1);

		$this->addLevel($this->_('Language/Language'), $this->getRoute(NULL, 'language'));
		$this->getMenu()->getItem(3)->addClass('active');
	}
}