<?php
namespace CMS\Widget\Module;
class ModuleImage extends \CMS\Widget\Module\ModuleAbstract {
	protected $image;
	protected $required;
	public function __construct($required=TRUE) {
		parent::__construct();
		$this->required = $required;
	}

	public function getName() {
		return $this->_('ModuleName');
	}

	public function onWidgetAdded() {
		if($this->isPostBack()) {
			$name = $this->getInputName();
			$this->module->setData($this->data->$name);
		}
	}

	public function onAdminRender() {
		/* @var $modelfile \CMS\Model\ModelFile */
		$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

		$this->image = $modelfile::GetByNodeID($this->page->data->get($this->getInputName()));

		if($this->isPostBack()) {

			if($this->module->getRequired() && !$this->image->hasRow()) {
				if(empty($_FILES[$this->getInputName()]['tmp_name'])) {
					$this->setError($this->_('CannotBeEmpty', $this->module->getName()));
				}
			}

			if(!$this->hasErrors()) {
				$input = $this->getInputName();
				if(!empty($_FILES[$input]['tmp_name'])) {
					$fileExtension = \Pecee\IO\File::GetExtension($_FILES[$input]['name']);

					if(!$this->image->hasRow()) {
						$this->image = new $modelfile();
						$this->image->save();
						$this->image->setFilename($this->image->getNodeID() . '.'. $fileExtension);
					}

					if($this->module->getData($this->getInputName())) {
						$this->image->setParentNodeID($this->module->getData($this->getInputName()));
					}

					$this->image->setOriginalFilename($_FILES[$input]['name']);
					$this->image->setMime(\Pecee\IO\File::GetMimeType($_FILES[$input]['tmp_name']));
					$this->image->setBytes(filesize($_FILES[$input]['tmp_name']));
					$this->image->setUserId(\Pecee\Model\User\ModelUser::Current()->getUserID());
					$this->image->setIPAddress(\Pecee\Server::GetRemoteAddr());
					$size = @getimagesize($_FILES[$input]['tmp_name']);

					$this->image->setWidth($size[0]);
					$this->image->setHeight($size[1]);

					\Pecee\IO\File::CreatePath($this->image->getThumbPath()); // Ensure that path exists.

					if(move_uploaded_file($_FILES[$input]['tmp_name'], $this->image->getFullPath())) {
						$this->image->update();
						$this->page->setData($this->getInputName(), $this->image->getNodeID());
					}else{
						$this->setError('FailedToUpload');
					}
				} else {
					if(!$this->image->hasRow()) {
						$this->page->removeData($this->getInputName());
					}
				}
			}
		}
	}
}