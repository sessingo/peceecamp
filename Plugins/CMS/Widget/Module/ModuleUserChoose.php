<?php
namespace CMS\Widget\Module;
class ModuleUserChoose extends \CMS\Widget\Module\ModuleAbstract {
	public function __construct() {
		parent::__construct();
	}

	public function getName() {
		return $this->_('ModuleName');
	}

	public function getData($name) {
		$data = unserialize($this->module->getData());
		return (isset($data[$name])) ? $data[$name] : NULL;
	}

	public function onWidgetAdded() {
		if($this->isPostBack()) {
			$name = $this->getInputName();
			$out = array('nodeId' => $this->data->__get($name.'_nodeId'), 'types' => $this->data->__get($name.'_types'));
			$this->module->setData(serialize($out));
		}
	}

	public function onAdminRender() {
		if($this->isPostBack()) {

			if($this->module->getRequired()) {
				$this->addInputValidation($this->module->getName(), $this->getInputName().'_nodeId', new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			}

			if(!$this->hasErrors()) {
				$this->page->setData($this->getInputName().'_nodeId', $this->getPostback());
			}
		}
	}
}