<?php
namespace CMS\Widget\Module;
class ModuleTrueFalse extends \CMS\Widget\Module\ModuleAbstract {
	public function __construct() {
		parent::__construct();
	}

	public function getName() {
		return $this->_('ModuleName');
	}

	public function onWidgetAdded() {

	}

	public function onAdminRender() {
		if($this->isPostBack()) {
			if($this->module->getRequired()) {
				$this->addInputValidation($this->module->getName(), $this->getInputName(), new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			}
			if(!$this->hasErrors()) {
				$this->page->setData($this->getInputName(), $this->data->__get($this->getInputName()));
			}
		}
	}
}