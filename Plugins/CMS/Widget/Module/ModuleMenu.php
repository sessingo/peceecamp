<?php
namespace CMS\Widget\Module;
class ModuleMenu extends \CMS\Widget\Module\ModuleAbstract {
	protected $menu;
	public function __construct() {
		parent::__construct();
	}

	public function getName() {
		return $this->_('ModuleName');
	}

	public function onWidgetAdded() {

	}

	protected function renderMenu($parent = -1) {
		$output = '';
		if(count($this->menu) > 0) {
			$hasItems = FALSE;
			foreach($this->menu as $i=>$menu) {
				if($menu['ParentKey'] == $parent) {
					$hasItems = TRUE;
					break;
				}
			}
			if($parent > -1 && $hasItems) {
				$output .= '<ol>';
			}
			foreach($this->menu as $i=>$menu) {
				if($menu['ParentKey'] == $parent) {
					if(isset($menu['NodeID'])) {

						/* @var $modelpage \CMS\Model\ModelPage */
						$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

						$node = $modelpage::GetByNodeID($menu['NodeID']);
						if($node->hasRow()) {
							$output .= sprintf('<li id="node_'.$i.'">
											<div>
												<a href="javascript:;">%s</a>
												<a href="#" class="js-remove" style="display:none;">(Remove)</a>
											</div>
											<input type="hidden" name="'.$this->getInputName().'_NodeID['.$i.']" value="%s" />
										</li>', $node->getTitle(), $node->getNodeID());
						}
					} else {
						$output .= sprintf('<li id="node_'.$i.'">
										<div>
											<a href="%s" class="%s" target="%s">%s</a>
											<a href="#" class="js-remove" style="display:none;">(Remove)</a>
										</div>
										<input type="hidden" name="'.$this->getInputName().'_label['.$i.']" value="%s" />
										<input type="hidden" name="'.$this->getInputName().'_url['.$i.']" value="%s" />
										<input type="hidden" name="'.$this->getInputName().'_target['.$i.']" value="%s" />
										<input type="hidden" name="'.$this->getInputName().'_className['.$i.']" value="%s" />
									</li>',
								$menu['Url'], $menu['Class'], $menu['Target'], $menu['Label'],
								$menu['Label'], $menu['Url'], $menu['Target'], $menu['Class']);
					}
					$output .= $this->renderMenu($i);
					$output .= '</li>';
				}
			}
			if($parent > -1 && $hasItems) {
				$output .= '</ol>';
			}
		}

		return $output;
	}

	public function onAdminRender() {
		$this->getSite()->addJs('/plugin/CMS/admin/js/jquery.ui.nestedSortable.js');

		$this->menu = unserialize($this->page->getData($this->getInputName()));
		if(!is_array($this->menu)) {
			$this->menu = array();
		}

		if($this->isPostBack()) {
			if($this->module->getRequired()) {
				$this->addInputValidation($this->module->getName(), $this->getInputName(), new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			}
			if(!$this->hasErrors()) {

				if($this->data->__get($this->getInputName())) {
					$output = array();
					$data = \Pecee\Url::QueryStringToArray($this->data->__get($this->getInputName()));
					foreach($data as $el=>$parent) {
						preg_match('/node\\[([0-9]+)\\]/is', $el, $matches);
						$i = $matches[1];
						$parent = (strtolower($parent) == 'null') ? -1 : $parent;

						$name = $this->getInputName() .'_NodeID';
						$name = $this->data->$name;
						if(isset($name[$i])) {
							$output[$i] = array('NodeID' => $name[$i], 'ParentKey' => $parent);
						} else {
							$url = $this->getInputName().'_url';
							$url = $this->data->$url;
							$label = $this->getInputName().'_label';
							$label = $this->data->$label;
							$class = $this->getInputName().'_className';
							$class = $this->data->$class;
							$target = $this->getInputName().'_target';
							$target = $this->data->$target;
							$output[$i] = array('Url' => $url[$i],
									'Label' => $label[$i],
									'Class' => $class[$i],
									'Target' => $target[$i],
									'ParentKey' => $parent);
						}
					}

					$output = serialize($output);

					$this->page->setData($this->getInputName(), $output);
					//$this->page->update();
				}
			}
		}
	}
}