<?php
namespace CMS\Widget\Module;
class ModuleDropdown extends \CMS\Widget\Module\ModuleAbstract {
	protected $dropdown;
	public function __construct() {
		parent::__construct();
		$this->dropdown = array();
	}

	public function getName() {
		return $this->_('ModuleName');
	}

	public function onWidgetAdded() {
		if($this->isPostBack() && isset($this->data->dropdown_name[$this->module->getPropertyModuleID()])) {
			foreach($this->data->dropdown_name[$this->module->getPropertyModuleID()] as $key=>$value) {
				$this->dropdown[$this->data->dropdown_value[$this->module->getPropertyModuleID()][$key]] = $value;
			}
			$this->module->setData(\Pecee\String\Encoding::Base64Encode($this->dropdown));
		} else {
			$dropdown = $this->module->getData();

			if($dropdown) {
				$dropdown = \Pecee\String\Encoding::Base64Decode($dropdown);
				if(is_array($dropdown)) {
					$this->dropdown = $dropdown;
				}
			}
		}
	}

	public function onAdminRender() {

		$dropdown = $this->module->getData();

		if($dropdown) {
			$dropdown = \Pecee\String\Encoding::Base64Decode($dropdown);
			if(is_array($dropdown)) {
				$this->dropdown = $dropdown;
			}
		}

		if($this->isPostBack()) {
			if($this->module->getRequired()) {
				$this->addInputValidation($this->module->getName(), $this->getInputName(), new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			}
			if(!$this->hasErrors()) {
				$this->page->setData($this->getInputName(), $this->data->__get($this->getInputName()));
				//$this->page->update();
			}
		}
	}
}