<?php
namespace CMS\Widget\Module;
class ModuleDateTime extends \CMS\Widget\Module\ModuleAbstract {
	public function __construct() {
		parent::__construct();
		$this->getSite()->addCss('/plugin/CMS/admin/css/modules/jquery-ui-timepicker-addon.css');
		$this->getSite()->addJs('/plugin/CMS/admin/js/modules/jquery-ui-timepicker-addon.js');
	}

	public function getName() {
		return $this->_('ModuleName');
	}

	public function onWidgetAdded() {

	}

	public function onAdminRender() {
		if($this->isPostBack()) {
			if($this->module->getRequired()) {
				$this->addInputValidation($this->module->getName(), $this->getInputName(), new \PC\UI\Form\Validate\ValidateDate());
			}
			if(!$this->hasErrors()) {
				$this->page->setData($this->getInputName(), $this->data->__get($this->getInputName()));
			}
		}
	}
}