<?php
namespace CMS\Widget\Module;
class ModuleFileChoose extends \CMS\Widget\Module\ModuleAbstract {
	public function __construct() {
		parent::__construct();
	}

	public function getName() {
		return $this->_('ModuleName');
	}

	public function onWidgetAdded() {
		if($this->isPostBack()) {
			$name = $this->getInputName();
			$this->module->setData($this->data->$name);
		}
	}

	public function onAdminRender() {
		if($this->isPostBack()) {

			if($this->module->getRequired()) {
				$this->addInputValidation($this->module->getName(), $this->getInputName(), new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			}

			if(!$this->hasErrors()) {
				$input = $this->getInputName('pageId');

				$this->page->setData($this->getInputName(), $this->getPostback());
			}
		}
	}
}