<?php
namespace CMS\Widget\Module;
class ModuleRadio extends \CMS\Widget\Module\ModuleAbstract {
	protected $radio;
	public function __construct() {
		parent::__construct();
		$this->radio = array();
	}

	public function getName() {
		return $this->_('ModuleName');
	}

	public function onWidgetAdded() {

		if($this->isPostBack() && isset($this->data->radio_name[$this->module->getPropertyModuleID()])) {
			foreach($this->data->radio_name[$this->module->getPropertyModuleID()] as $key=>$value) {
				$this->radio[$value] = $this->data->radio_value[$this->module->getPropertyModuleID()][$key];
			}
		} else {
			$radio = $this->module->getData();

			if($radio) {
				$radio = \Pecee\String\Encoding::Base64Decode($radio);
				if(is_array($radio)) {
					$this->radio = $radio;
				}
			}
		}

		if($this->isPostBack()) {
			if(isset($this->data->radio_name[$this->module->getPropertyModuleID()])) {
				$output = array();
				foreach($this->data->radio_name[$this->module->getPropertyModuleID()] as $key=>$radio) {
					$output[$this->data->radio_value[$this->module->getPropertyModuleID()][$key]] = $radio;
				}
				$this->module->setData(\Pecee\String\Encoding::Base64Encode($output));
			}
		}
	}

	public function onAdminRender() {

		$radio = $this->module->getData();

		if($radio) {
			$radio = \Pecee\String\Encoding::Base64Decode($radio);
			if(is_array($radio)) {
				$this->radio = $radio;
			}
		}

		if($this->isPostBack()) {
			if($this->module->getRequired()) {
				$this->addInputValidation($this->module->getName(), $this->getInputName(), new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			}
			if(!$this->hasErrors()) {
				$this->page->setData($this->getInputName(), $this->data->__get($this->getInputName()));
				//$this->page->update();
			}
		}
	}
}