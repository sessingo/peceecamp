<?php
namespace CMS\Widget\Module;
class ModuleFile extends \CMS\Widget\Module\ModuleAbstract {
	protected $file;
	protected $required;
	public function __construct($required=TRUE) {
		parent::__construct();
		$this->required = $required;
	}

	public function getName() {
		return $this->_('ModuleName');
	}

	public function onWidgetAdded() {
		if($this->isPostBack()) {
			$name = $this->getInputName();
			$this->module->setData($this->data->$name);
		}
	}

	public function onAdminRender() {
		/* @var $modelfile \CMS\Model\ModelFile */
		$modelfile = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelFile');

		$this->file = $modelfile::GetByNodeID($this->page->data->get($this->getInputName()));

		if($this->isPostBack()) {

			if($this->module->getRequired() && !$this->file->hasRow()) {
				if(empty($_FILES[$this->getInputName()]['tmp_name'])) {
					$this->setError($this->_('CannotBeEmpty', $this->module->getName()));
				}
			}

			if(!$this->hasErrors()) {
				$input = $this->getInputName();
				if(!empty($_FILES[$input]['tmp_name'])) {
					$fileExtension = \Pecee\IO\File::GetExtension($_FILES[$input]['name']);

					if(!$this->file->hasRow()) {
						$this->file = new $modelfile();
						$this->file->save();
						$this->file->setFilename($this->file->getNodeID() . '.'. $fileExtension);
					}

					if($this->module->getData($this->getInputName())) {
						$this->file->setParentNodeID($this->module->getData($this->getInputName()));
					}

					$this->file->setOriginalFilename($_FILES[$input]['name']);
					$this->file->setMime(\Pecee\IO\File::GetMimeType($_FILES[$input]['tmp_name']));
					$this->file->setBytes(filesize($_FILES[$input]['tmp_name']));
					$this->file->setUserId(\Pecee\Model\User\ModelUser::Current()->getUserID());
					$this->file->setIPAddress(\Pecee\Server::GetRemoteAddr());

					if(move_uploaded_file($_FILES[$input]['tmp_name'], $this->file->getFullPath())) {
						$this->file->update();
						$this->page->setData($this->getInputName(), $this->file->getNodeID());
					}else{
						$this->setError('FailedToUpload');
					}
				} else {
					if(!$this->file->hasRow()) {
						$this->page->removeData($this->getInputName());
					}
				}
			}
		}
	}
}