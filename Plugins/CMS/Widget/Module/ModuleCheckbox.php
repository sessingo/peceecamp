<?php
namespace CMS\Widget\Module;
class ModuleCheckbox extends \CMS\Widget\Module\ModuleAbstract {
	protected $checkbox;
	public function __construct() {
		parent::__construct();
		$this->checkbox = array();
	}

	public function getName() {
		return $this->_('ModuleName');
	}

	public function onWidgetAdded() {

		if($this->isPostBack() && isset($this->data->checkbox_name[$this->module->getPropertyModuleID()])) {
			foreach($this->data->checkbox_name[$this->module->getPropertyModuleID()] as $key=>$value) {
				$this->checkbox[$value] = $this->data->checkbox_value[$this->module->getPropertyModuleID()][$key];
			}
		} else {
			$checkbox = $this->module->getData();

			if($checkbox) {
				$checkbox = \Pecee\String\Encoding::Base64Decode($checkbox);
				if(is_array($checkbox)) {
					$this->checkbox = $checkbox;
				}
			}
		}

		if($this->isPostBack()) {
			if(isset($this->data->checkbox_name[$this->module->getPropertyModuleID()])) {
				$output = array();
				foreach($this->data->checkbox_name[$this->module->getPropertyModuleID()] as $key=>$checkbox) {
					$output[$this->data->checkbox_value[$this->module->getPropertyModuleID()][$key]] = $checkbox;
				}
				$this->module->setData(\Pecee\String\Encoding::Base64Encode($output));
			}
		}
	}

	protected function getChecked($value) {
		$data = \Pecee\String\Encoding::Base64Decode($this->page->getData($this->getInputName()));
		$data = (is_array($data)) ? $data : array();
		foreach($data as $d) {
			if($d == $value) {

				return TRUE;
			}
		}
		return FALSE;
	}

	public function onAdminRender() {
		$data = \Pecee\String\Encoding::Base64Decode($this->page->getData($this->getInputName()));
		$checkbox = $this->module->getData();
		if($checkbox) {
			$checkbox = \Pecee\String\Encoding::Base64Decode($checkbox);
			if(is_array($checkbox)) {
				$this->checkbox = $checkbox;
			}
		}

		if($this->isPostBack()) {
			if($this->module->getRequired()) {
				$this->addInputValidation($this->module->getName(), $this->getInputName(), new \PC\UI\Form\Validate\ValidateNotNullOrEmpty());
			}
			if(!$this->hasErrors()) {
				$this->page->setData($this->getInputName(), \Pecee\String\Encoding::Base64Encode($this->data->__get($this->getInputName())));
				//$this->page->update();
			}
		}
	}
}