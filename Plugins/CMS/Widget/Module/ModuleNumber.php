<?php
namespace CMS\Widget\Module;
class ModuleNumber extends \CMS\Widget\Module\ModuleAbstract {
	public function __construct() {
		parent::__construct();
	}

	public function getName() {
		return $this->_('ModuleName');
	}

	public function onWidgetAdded() {

	}

	public function onAdminRender() {
		if($this->isPostBack()) {
			if($this->module->getRequired()) {
				if(!is_numeric($this->data->__get($this->getInputName()))) {
					$this->setError('InvalidNumber', $this->getInputName());
				}
			}
			if(!$this->hasErrors()) {
				$this->page->setData($this->getInputName(), $this->data->__get($this->getInputName()));
			}
		}
	}
}