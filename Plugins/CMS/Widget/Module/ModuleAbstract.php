<?php
namespace CMS\Widget\Module;
abstract class ModuleAbstract extends \PC\Widget\WidgetAbstract {
	protected $page;
	protected $module;
	protected $autoPostback;
	protected $_language;
	public function __construct() {
		parent::__construct();
		$this->_language = new \PC\Language();
		$reflectionClass = new \ReflectionClass($this);
		$this->_language->getLanguage()->setXmlDir(dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'Module' . DIRECTORY_SEPARATOR . $reflectionClass->getShortName());
	}

	public abstract function getName();
	public abstract function onWidgetAdded();
	public abstract function onAdminRender();

	public function addModule(\CMS\Model\Property\PropertyModule &$module) {
		$this->module = $module;
		$this->_contentTemplate = NULL;
		$this->setTemplate(NULL);

		$classname = explode('\\', get_class($this));
		if(isset($classname[1]) && $classname[1] == $this->plugin->application->getTheme()) {
			$module = array_slice(explode('\\', get_class($this)), 3);
			$filename = $this->plugin->themesdir . DIRECTORY_SEPARATOR . $this->plugin->application->getTheme() . DIRECTORY_SEPARATOR . 'Template' . DIRECTORY_SEPARATOR . 'Content' . DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, $module) . DIRECTORY_SEPARATOR . array_pop($module) . '.php';
			if(file_exists($filename)) {
				$this->_contentTemplate = $filename;
			}
		} else {
			$filename = explode('\\Widget\\', get_class($this));
			$filename = str_replace('\\', DIRECTORY_SEPARATOR, $filename[0]) . DIRECTORY_SEPARATOR . 'Template'.DIRECTORY_SEPARATOR.
						'Content' . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $filename[1]).
						DIRECTORY_SEPARATOR . 'Render.php';

			$filename = dirname($_SERVER['DOCUMENT_ROOT']) . '/Plugins'. DIRECTORY_SEPARATOR .$filename;

			if(file_exists($filename)) {
				$this->_contentTemplate = $filename;
			}
		}
	}

	public function renderAdmin(\CMS\Model\Property\PropertyModule &$module, \Pecee\Model\Node\ModelNode &$page) {
		$this->module = $module;
		$this->page = $page;
		$this->setTemplate(NULL);

		$classname = explode('\\', get_class($this));
		if(isset($classname[1]) && $classname[1] == $this->plugin->application->getTheme()) {
			$module = array_slice(explode('\\', get_class($this)), 3);
			$this->_contentTemplate = $this->plugin->themesdir . DIRECTORY_SEPARATOR . $this->plugin->application->getTheme() . DIRECTORY_SEPARATOR . 'Template' . DIRECTORY_SEPARATOR . 'Content' . DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, $module) . DIRECTORY_SEPARATOR . array_pop($module) . '.php';
		} else {
			$module = array_pop(explode('\\', get_class($this)));
			$filename = explode('\\Widget\\', get_class($this));
			$filename = str_replace('\\', DIRECTORY_SEPARATOR, $filename[0]) . DIRECTORY_SEPARATOR . 'Template'.DIRECTORY_SEPARATOR.
						'Content' . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $filename[1]).
						DIRECTORY_SEPARATOR . $module . '.php';

			$this->_contentTemplate = 'Plugins'. DIRECTORY_SEPARATOR .$filename;
		}
	}

	public function getInputName() {
		return $this->module->getControlID();
	}

	public function getPostback() {
		$index = $this->getInputName();
		return $this->data->$index;
	}

	/**
	 * Adds input validation
	 * @param string $name
	 * @param string $index
	 * @param \Pecee\UI\Form\Validate\ValidateInput|array $type
	 */
	protected function addInputValidation($name, $index, $type) {
		$index = $this->getInputName($index);
		return parent::addInputValidation($name, $index, $type);
	}

	protected function getRoute($controller = NULL, $method = NULL, $methodParams = NULL, $getParams = NULL, $includeMethodParams = FALSE, $doRewrite = TRUE) {
		$method=(!$method) ? array() : array($method);
		$methodParams=(!is_array($methodParams)) ? array() : $methodParams;
		return \PC\Router::GetRoute(\Pecee\Registry::GetInstance()->get('AdminUrl','admin'), $controller, array_merge($method, $methodParams), $getParams, $includeMethodParams, $doRewrite);
	}

	public function _($text,$args=NULL) {
		$out = '';
		try {
			$args = array_splice(func_get_args(), 1);
			$out = $this->_language->_($text, $args);
		} catch(\PC\Xml\Translate\TranslateException $e) {
			$this->setError($e->getMessage());
		}
		return $out;
	}

	public function hasErrors() {
		if($this->autoPostback) {
			return TRUE;
		}

		return parent::hasErrors();
	}

	public function setAutoPostback($bool) {
		$this->autoPostback = $bool;
	}
}