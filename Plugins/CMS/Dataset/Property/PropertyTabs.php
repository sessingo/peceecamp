<?php
namespace CMS\Dataset\Property;
class PropertyTabs extends \PC\Dataset\DatasetAbstract {
	public function __construct($nodeId, $choose) {

		$this->createArray('', $choose);

		/* @var $modelpage \CMS\Model\ModelPage */
		$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

		$page = $modelpage::GetByNodeID($nodeId);
		$parent = $page->getParent();

		$nodeIds = array();

		while($parent && $parent->hasRow()) {
			$nodeIds[] = $parent->getNodeID();
			$parent = $parent->getParent();
		}

		ksort($nodeIds);
		$nodeIds[] = $page->getNodeID();

		/* @var $propertytab \CMS\Model\Property\PropertyTab */
		$propertytab = \PC\ObjectManager::GetInstance()->get('\CMS\Model\Property\PropertyTab');

		$tabs = $propertytab::GetByNodeIds($nodeIds);

		if($tabs->hasRows()) {
			foreach($tabs->getRows() as $tab) {
				$this->createArray($tab->getPropertyTabID(), $tab->getName());
			}
		}

	}
}