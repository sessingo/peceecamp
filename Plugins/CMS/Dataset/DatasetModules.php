<?php
namespace CMS\Dataset;
class DatasetModules extends \PC\Dataset\DatasetAbstract {
	public function __construct($choose) {

		$this->createArray('', $choose);

		$modules = \CMS\Module::GetModules();
		foreach($modules as $module) {
			$this->createArray($module->widget, $module->name);
		}
	}
}