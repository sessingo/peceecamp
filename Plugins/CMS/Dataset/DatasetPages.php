<?php
namespace CMS\Dataset;
class DatasetPages extends \PC\Dataset\DatasetAbstract {
	public function __construct($choose=NULL) {
		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}

		/* @var $modelpage \CMS\Model\ModelPage */
		$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

		$pages = $modelpage::Get(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
		if($pages->hasRows()) {
			if($pages->hasRow()) {
				foreach($pages->getRows() as $page) {
					$this->createArray($page->getNodeID(), $page->getTitle());
				}
			}
		}
	}

}