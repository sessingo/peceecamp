<?php
namespace CMS\Dataset;
class DatasetMacros extends \PC\Dataset\DatasetAbstract {
	public function __construct($choose=NULL) {

		if(!is_null($choose)) {
			$this->createArray('', $choose);
		}

		$macro = new \CMS\Macro();
		foreach($macro->getMacros() as $macro) {
			$this->createArray($macro->widget, $macro->path);
		}
	}
}