<?php
namespace CMS\Dataset\Module;
class ModuleTemplate extends \PC\Dataset\DatasetAbstract {
	public function __construct($excludeTemplates=NULL) {
		$templates = \CMS\Module::GetWidgets();
		foreach($templates as $key=>$name) {
			if(is_null($excludeTemplates) || in_array($name, $excludeTemplates)) {
				$this->createArray($key, $name);
			}
		}
	}
}