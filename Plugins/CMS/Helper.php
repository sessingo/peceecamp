<?php
namespace CMS;
class Helper {
	public static function GenerateAlias($title) {
		$words = explode(' ', strtolower($title));
		$output = '';

		for($i=0;$i<count($words);$i++) {
			$word = $words[$i];
			$output .= ($i == 0) ? $word : ucfirst($word);
		}
		return $output;
	}
}