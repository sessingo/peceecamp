<?php
namespace CMS;
class Macro {
	protected $macros;
	public function __construct() {
		$this->macros = array_reverse($this->loopFolder('Macro' . DIRECTORY_SEPARATOR));
	}

	protected function loopFolder($path) {
		$templates = array();
		$plugin = \PC\Plugin::Instance()->getByName('CMS');
		$p = $plugin->themesdir . DIRECTORY_SEPARATOR . $plugin->application->Theme .'/Widget/' . $path;
		if(is_dir($p) && ($handle = opendir($p)) !== FALSE) {
			while (false !== ($entry = readdir($handle))) {
				if(strpos(strtolower($entry), '.php') > -1 && strpos(strtolower($entry), '.svn') === FALSE) {
					$name = explode('.',$entry);
					$widget = ucfirst($name[0]);
					$widget = str_replace(DIRECTORY_SEPARATOR, '\\', $path) . $widget;
					$widget = sprintf('\\CMS\\%s\\Widget\\%s', $plugin->application->Theme, $widget);

					if(class_exists($widget)) {
						$class = new \ReflectionClass($widget);
						if(!$class->isAbstract()) {
							$params = $class->getMethods (\ReflectionMethod::IS_PUBLIC);
							$methods = array();
							foreach($params as $param) {
								if(stripos($param->getName(), 'set') > -1) {
									//$r = new ReflectionMethod($widget, $param->getName());
									//$methods[]= (object)array('method' => $param->getName(), 'params' => $r->getParameters());
									if(!in_array($param->getName(), array('setPage'))) {
										$methods[]= (object)array('name' => $param->getName());
									}
								}
							}

							$templates[] = (object)array('widget' => $widget, 'path' => $path . $entry, 'methods' => $methods);
						}
					}
				}

				if(!in_array($entry, array('.', '..')) && is_dir($p . DIRECTORY_SEPARATOR .$entry)) {
					$templates = array_merge($this->loopFolder($path . $entry . DIRECTORY_SEPARATOR), $templates);
				}
			}
			closedir($handle);
		}
		return $templates;
	}

	public function getMacros() {
		return $this->macros;
	}

}