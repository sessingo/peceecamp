<? /* @var $this \CMS\Widget\Page\PageError */ ?>
<?= $this->getSite()->getDocType(); ?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?= $this->printHeader(); ?>
		<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="error">
			<div class="inner">
				<img src="/gfx/pecee-logo.png" alt="" />
				<h1><?= $this->title?></h1>
				<p>
					<?= $this->description;?>
				</p>
			</div>
		</div>
	</body>
</html>
