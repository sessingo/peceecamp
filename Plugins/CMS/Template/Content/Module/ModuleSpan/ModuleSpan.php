<? /* @var $this \CMS\Widget\Module\ModuleSpan */ ?>

<div class="input">
	<div class="label">
		<?= $this->module->getName();?>
	</div>
	<span style="margin-top: 8px;display: block;"><?= $this->page->data->get($this->getInputName()); ?></span>
	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>