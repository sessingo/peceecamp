<? /* @var $this \CMS\Widget\Module\ModuleNumber */ ?>

<div class="input">
	<div class="label">
		<?= $this->module->getName();?>
	</div>
	<?= $this->form()->input($this->getInputName(), 'text', $this->page->data->get($this->getInputName()), TRUE);?>
	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>