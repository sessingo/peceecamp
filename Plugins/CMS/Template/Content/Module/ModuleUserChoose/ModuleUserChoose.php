<? /* @var $this \CMS\Widget\Module\ModuleUserChoose */ ?>

<div class="input">
	<div class="label">
		<?= $this->module->getName();?>
	</div>
	<?= $this->widget(new \CMS\Widget\Admin\User\UserChoose($this->getInputName(), $this->page->getData($this->getInputName()), $this->getData('nodeId'), $this->getData('types'))); ?>
	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>