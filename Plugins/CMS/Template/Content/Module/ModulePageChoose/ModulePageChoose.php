<? /* @var $this \CMS\Widget\Module\ModulePageChoose */ ?>

<div class="input">
	<div class="label">
		<?= $this->module->getName();?>
	</div>
	<?= $this->widget(new \CMS\Widget\Admin\Page\PageChoose($this->getInputName(), $this->page->getData($this->getInputName()), $this->getData('nodeId'), $this->getData('types'))); ?>
	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>