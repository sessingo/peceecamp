<? /* @var $this \CMS\Widget\Module\ModulePageChoose */ ?>
<div class="input">
	<div class="label">
		<?= $this->_('StartNode')?>
	</div>
	<?= $this->widget(new \CMS\Widget\Admin\Page\PageChoose($this->getInputName().'_nodeId', $this->getData('nodeId'), 0)); ?>
</div>

<div class="input">
	<div class="label">
		<?= $this->_('Types');?>
	</div>
	<?= $this->widget(new \CMS\Widget\Admin\Page\Property\PropertyChoose($this->getInputName().'_types', $this->getData('types'), 0, TRUE)); ?>
</div>