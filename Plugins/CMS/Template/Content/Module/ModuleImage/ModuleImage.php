<? /* @var $this \CMS\Widget\Module\ModuleImage */ ?>

<div class="input">
	<div class="label">
		<?= $this->module->getName();?>
	</div>
	<?= $this->form()->input($this->getInputName(), 'file'); ?>
	<?= $this->form()->input($this->getInputName().'_remove', 'hidden'); ?>

	<? if($this->image->hasRow()) : ?>
		<a href="javascript:;" class="js-delete" data-id="<?= $this->image->getFileID(); ?>"><?= $this->_('Delete'); ?></a>
		<div style="float:right;">
			<a href="<?= $this->image->getUrl(); ?>" rel="new" class="js-overlay"><img src="<?= $this->image->getUrl(40,40); ?>" alt="" /></a>
		</div>
		<script type="text/javascript">
			$(document).ready(function() {
				$('.js-delete').bind('click', function() {
					$.post('<?= \PC\Router::GetRoute(NULL, NULL, array('ajax', 'deletefile'))?>', 'fileId='+ $(this).data('id'), function(r) {
						if(r == null || r != null && !r.result) {
							alert('<?= $this->_('ErrorDeleting');?>');
						} else {
							$(this).parent().find('img').hide(150);
						}
					});
				});
			});
		</script>
	<? endif; ?>
	<div class="clear"></div>
	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>

