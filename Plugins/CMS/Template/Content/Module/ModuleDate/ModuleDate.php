<? /* @var $this \CMS\Widget\Module\ModuleDate */ ?>

<div class="input">
	<div class="label">
		<?= $this->module->getName();?>
	</div>
	<?= $this->form()->input($this->getInputName(), 'text', $this->page->data->get($this->getInputName()), TRUE)->addAttribute('ID','date_'.$this->getInputName());?>
	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#date_<?= $this->getInputName(); ?>').datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-mm-yy'
		});
	});
</script>