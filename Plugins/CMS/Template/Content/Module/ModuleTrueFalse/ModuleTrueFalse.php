<? /* @var $this \CMS\Widget\Module\ModuleTrueFalse */ ?>

<div class="input">
	<div class="label">
		<?= $this->module->getName();?>
	</div>
	<? if($this->page->getData($this->getInputName())) : ?>
		<?= $this->form()->input($this->getInputName(), 'checkbox', '1', TRUE)->addAttribute('checked','checked');?>
	<? else: ?>
		<?= $this->form()->input($this->getInputName(), 'checkbox', '1', TRUE);?>
	<? endif;?>
	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>