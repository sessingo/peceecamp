<? /* @var $this \CMS\Widget\Module\ModuleFile */ ?>
<div class="input">
	<div class="label">
		<?= $this->_('SaveToFolder')?>
	</div>
	<?= $this->widget(new \CMS\Widget\Admin\File\FileChoose($this->getInputName(), $this->module->getData($this->getInputName()), 0)); ?>
</div>