<? /* @var $this \CMS\Widget\Module\ModuleFile */ ?>

<div class="input">
	<div class="label">
		<?= $this->module->getName();?>
	</div>
	<?= $this->form()->input($this->getInputName(), 'file'); ?>
	<?= $this->form()->input($this->getInputName().'_remove', 'hidden'); ?>

	<? if($this->file->hasRow()) : ?>
		<a href="javascript:;" class="js-delete" data-id="<?= $this->file->getNodeID(); ?>"><?= $this->_('Delete'); ?></a>
		<div id="file" style="float:right;">
			<?= $this->file->getOriginalFilename(); ?>
		</div>
		<script type="text/javascript">
			$(document).ready(function() {
				$('.js-delete').bind('click', function() {
					$.post('<?= \PC\Router::GetRoute(NULL, NULL, array('ajax', 'deletefile'))?>', 'nodeId='+ $(this).data('id'), function(r) {
						if(r == null || r != null && !r.result) {
							alert('<?= $this->_('ErrorDeleting');?>');
						} else {
							$(this).parent().find('.file').hide(150);
						}
					});
				});
			});
		</script>
	<? endif; ?>
	<div class="clear"></div>
	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>

