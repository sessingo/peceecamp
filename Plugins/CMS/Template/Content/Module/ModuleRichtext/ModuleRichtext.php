<? /* @var $this \CMS\Widget\Module\ModuleRichtext */ ?>

<div style="margin-bottom:10px;">
	<h3><?= $this->module->getName();?></h3>
	<?= $this->form()->textarea($this->getInputName(), 20, 10, $this->page->getData($this->getInputName()), TRUE)->addClass('js-editor');?>
	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>