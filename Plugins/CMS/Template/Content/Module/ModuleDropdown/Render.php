<? /* @var $this \CMS\Widget\Module\ModuleDropdown */ ?>

<div style="margin-top:10px;">
	<table id="dropdown-ctn-<?= md5($this->module->getPropertyModuleID());?>" style="width:100%;margin-bottom:15px;">
		<? foreach($this->dropdown as $value=>$name): ?>
		<tr>
			<td>
				<?= $name; ?>
			</td>
			<td>
				<?= $value; ?>
			</td>
			<td>
				<a href="#" class="js-dropdown-remove-<?= md5($this->module->getPropertyModuleID());?>"><?= $this->_('Remove'); ?></a>
				<input type="hidden" name="dropdown_name[<?= $this->module->getPropertyModuleID(); ?>][]" value="<?= $name;?>" />
				<input type="hidden" name="dropdown_value[<?= $this->module->getPropertyModuleID(); ?>][]" value="<?= $value; ?>" />
			</td>
		</tr>
		<? endforeach;?>
	</table>
	<div style="padding-bottom:10px;">
		<input type="text" name="d_name_<?= md5($this->module->getPropertyModuleID());?>" placeholder="<?= $this->_('Name');?>" id="dropdown-name-<?= md5($this->module->getPropertyModuleID());?>" />
		<input type="text" name="d_value_<?= md5($this->module->getPropertyModuleID());?>" placeholder="<?= $this->_('Value');?>" id="dropdown-value-<?= md5($this->module->getPropertyModuleID());?>" />
		<a href="javascript:;" onclick="dropdownAddValue<?= md5($this->module->getPropertyModuleID());?>();" class="btn-s"><?= $this->_('Add')?></a>
	</div>
</div>

<script type="text/javascript">
		function checkTable<?= md5($this->module->getPropertyModuleID());?>() {
			var ctn = $('#dropdown-ctn-<?= md5($this->module->getPropertyModuleID());?>');
			if(ctn.find('tr').length > 0) {
				ctn.show();
			} else {
				ctn.hide();
			}
		};

		function dropdownAddValue<?= md5($this->module->getPropertyModuleID());?>() {
			var ctn = $('#dropdown-ctn-<?= md5($this->module->getPropertyModuleID());?>');
			var name = $('#dropdown-name-<?= md5($this->module->getPropertyModuleID());?>');
			var value = $('#dropdown-value-<?= md5($this->module->getPropertyModuleID());?>');
			ctn.append('<tr><td style="width:50%;">'+name.val()+'</td><td style="width:50%;">'+value.val()+
			'<td align="right"><a href="#" class="js-dropdown-remove-<?= md5($this->module->getPropertyModuleID());?>"><?= $this->_('Remove');?></a>'+
			'<input type="hidden" name="dropdown_name[<?= $this->module->getPropertyModuleID(); ?>][]" value="'+ name.val() +'" />' +
			'<input type="hidden" name="dropdown_value[<?= $this->module->getPropertyModuleID(); ?>][]" value="'+ value.val() +'" />' +
			'</td></tr>');

			name.val('');
			value.val('');

			checkTable<?= md5($this->module->getPropertyModuleID());?>();
		};

		checkTable<?= md5($this->module->getPropertyModuleID());?>();

		$('a.js-dropdown-remove-<?= md5($this->module->getPropertyModuleID());?>').live('click', function(e) {
			e.preventDefault();
			$(this).parents('tr:first').remove();
			checkTable<?= md5($this->module->getPropertyModuleID());?>();
		});
</script>