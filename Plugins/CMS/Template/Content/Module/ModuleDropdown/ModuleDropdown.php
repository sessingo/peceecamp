<? /* @var $this \CMS\Widget\Module\ModuleDropdown */ ?>

<div class="input">
	<div class="label">
		<?= $this->module->getName();?>
	</div>
	<?= $this->form()->selectStart($this->getInputName(), $this->dropdown, $this->page->getData($this->getInputName()), TRUE);?>
	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>