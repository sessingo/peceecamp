<? /* @var $this \CMS\Widget\Module\ModulePassword */ ?>

<div class="input">
	<div class="label">
		<?= $this->module->getName();?>
	</div>
	<?= $this->form()->input($this->getInputName(), 'password', $this->page->data->get($this->getInputName()), TRUE);?>
	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>