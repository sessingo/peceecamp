<? /* @var $this \CMS\Widget\Module\ModuleDateTime */ ?>

<div class="input">
	<div class="label">
		<?= $this->module->getName();?>
	</div>
	<?= $this->form()->input($this->getInputName(), 'text', $this->page->data->get($this->getInputName()), TRUE)->addAttribute('ID','datetime_'.$this->getInputName());?>
	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datetime_<?= $this->getInputName(); ?>').datetimepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-mm-yy'
		});
	});
</script>