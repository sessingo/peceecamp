<? /* @var $this \CMS\Widget\Module\ModuleFileChoose */ ?>

<div class="input">
	<div class="label">
		<?= $this->module->getName();?>
	</div>
	<?= $this->widget(new \CMS\Widget\Admin\File\FileChoose($this->getInputName(), $this->page->getData($this->getInputName()), $this->module->getData())); ?>
	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>