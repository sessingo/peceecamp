<? /* @var $this \CMS\Widget\Module\ModuleFileChoose */ ?>
<div class="input">
	<div class="label">
		<?= $this->_('StartNode')?>
	</div>
	<?= $this->widget(new \CMS\Widget\Admin\File\FileChoose($this->getInputName(), $this->module->getData($this->getInputName()), 0)); ?>
</div>