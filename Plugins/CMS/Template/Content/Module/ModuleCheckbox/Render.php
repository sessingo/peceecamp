<? /* @var $this \CMS\Widget\Module\ModuleCheckbox */ ?>

<table id="checkbox-ctn-<?= md5($this->module->getPropertyModuleID());?>" style="width:100%;margin-bottom:15px;">
	<? foreach($this->checkbox as $value=>$name): ?>
	<tr>
		<td>
			<?= $name; ?>
		</td>
		<td>
			<?= $value; ?>
		</td>
		<td>
			<a href="#" class="js-checkbox-remove-<?= md5($this->module->getPropertyModuleID());?>"><?= $this->_('Remove');?></a>
			<input type="hidden" name="checkbox_name[<?= $this->module->getPropertyModuleID(); ?>][]" value="<?= $name;?>" />
			<input type="hidden" name="checkbox_value[<?= $this->module->getPropertyModuleID(); ?>][]" value="<?= $value; ?>" />
		</td>
	</tr>
	<? endforeach;?>
</table>
<div style="padding-bottom:10px;">
	<input type="text" name="d_name_[<?= $this->module->getPropertyModuleID(); ?>]" placeholder="<?= $this->_('Name');?>" id="checkbox-name-<?= md5($this->module->getPropertyModuleID());?>" />
	<input type="text" name="d_value_[<?= $this->module->getPropertyModuleID(); ?>]" placeholder="<?= $this->_('Value');?>" id="checkbox-value-<?= md5($this->module->getPropertyModuleID());?>" />
	<a href="javascript:;" onclick="checkboxAddValue<?= md5($this->module->getPropertyModuleID());?>();" class="btn-s"><?= $this->_('Add'); ?></a>
</div>

<script type="text/javascript">
		function checkTable<?= md5($this->module->getPropertyModuleID());?>() {
			var ctn = $('#checkbox-ctn-<?= md5($this->module->getPropertyModuleID());?>');
			if(ctn.find('tr').length > 0) {
				ctn.show();
			} else {
				ctn.hide();
			}
		};

		function checkboxAddValue<?= md5($this->module->getPropertyModuleID());?>() {
			var ctn = $('#checkbox-ctn-<?= md5($this->module->getPropertyModuleID());?>');
			var name = $('#checkbox-name-<?= md5($this->module->getPropertyModuleID());?>');
			var value = $('#checkbox-value-<?= md5($this->module->getPropertyModuleID());?>');
			ctn.append('<tr><td style="width:50%;">'+name.val()+'</td><td style="width:50%;">'+value.val()+
			'<td align="right"><a href="#" class="js-checkbox-remove-<?= md5($this->module->getPropertyModuleID());?>"><?= $this->_('Remove');?></a>'+
			'<input type="hidden" name="checkbox_name[<?= $this->module->getPropertyModuleID(); ?>][]" value="'+ name.val() +'" />' +
			'<input type="hidden" name="checkbox_value[<?= $this->module->getPropertyModuleID(); ?>][]" value="'+ value.val() +'" />' +
			'</td></tr>');

			name.val('');
			value.val('');

			checkTable<?= md5($this->module->getPropertyModuleID());?>();
		};

		checkTable<?= md5($this->module->getPropertyModuleID());?>();

		$('a.js-checkbox-remove-<?= md5($this->module->getPropertyModuleID());?>').live('click', function(e) {
			e.preventDefault();
			$(this).parents('tr:first').remove();
			checkTable<?= md5($this->module->getPropertyModuleID());?>();
		});
</script>