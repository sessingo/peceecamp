<? /* @var $this \CMS\Widget\Module\ModuleCheckbox */ ?>

<div class="input">
	<div class="label">
		<?= $this->module->getName();?>
	</div>
	<? if(count($this->checkbox) > 0) : ?>
		<ul style="margin:0;padding:0;list-style:none;float:left;margin-top: 6px;">
		<? $i=0;
		foreach($this->checkbox as $value=>$name) : ?>
			<li style="display:inline-block;margin-right:10px;">
				<input type="checkbox" name="<?= $this->getInputName();?>[]" value="<?= $value?>" id="<?= $this->getInputName() .'_' . $i;?>"<?= ($this->getChecked($value)) ? ' checked="checked"' : '' ?> style="margin:0;"/> 
				<label for="<?= $this->getInputName() .'_' . $i;?>" style="vertical-align:middle;margin-left:3px;"><?= $name; ?></label>
			</li>
		<?  $i++;
		endforeach; ?>
		</ul>
	<? endif; ?>
	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>