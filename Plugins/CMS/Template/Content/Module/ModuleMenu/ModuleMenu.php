<? /* @var $this \CMS\Widget\Module\ModuleMenu */ ?>

<div class="input">
	<div class="label">
		<?= $this->module->getName();?>
	</div>

	<div id="topmenu_<?= $this->getInputName(); ?>" style="float:left;">
		<table style="width:100%;">
			<tr>
				<td style="vertical-align:top;padding-right:10px;min-width:300px;">
					<ol class="treeview" style="margin-left:0;">
						<?= $this->renderMenu(); ?>
					</ol>
				</td>
				<td style="width:230px;vertical-align:top;">
					<div class="tabs">
						<ul>
							<li class="active"><a href="#"><?= $this->_('ChoosePage')?></a></li>
							<li><a href="#"><?= $this->_('EnterUrl')?></a></li>
						</ul>
						<div>
							<a href="javascript:;" id="choosepage_<?= $this->getInputName(); ?>"><?= $this->_('ChoosePage'); ?></a>

						</div>
						<div style="display:none;">
							<input type="text" name="add_label" placeholder="<?= $this->_('Label');?>" style="width:200px;margin:0;float:none;display:block;margin-bottom:10px;"/>
							<input type="text" name="add_url" placeholder="<?= $this->_('Url');?>" style="width:200px;margin:0;float:none;display:block;margin-bottom:10px;"/>
							<input type="text" name="add_target" placeholder="<?= $this->_('Target');?>" style="width:200px;margin:0;float:none;display:block;margin-bottom:10px;"/>
							<input type="text" name="add_class" placeholder="<?= $this->_('Class');?>" style="width:200px;margin:0;float:none;display:block;margin-bottom:10px;"/>
							<a href="#" class="btn-s js-add-url"><?= $this->_('Add');?></a>
						</div>
					</div>
				</td>
			</tr>
		</table>
		<div class="clear"></div>
		<input type="hidden" name="<?= $this->getInputName(); ?>" value="" />
	</div>

	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {

		$('#topmenu_<?= $this->getInputName(); ?> ol.treeview').nestedSortable({
            handle: 'div',
            items: 'li',
            toleranceElement: '> div',
            helper: 'clone',
            tolerance: 'pointer',
            tabSize : 20,
            expandOnHover: 700,
            revert: 100,
            stop: function(e, ui) {
            	var list = $('#topmenu_<?= $this->getInputName(); ?> ol.treeview').nestedSortable('serialize');
            	$('input[name="<?= $this->getInputName(); ?>"]').val(list);
            }
        });

		$('#choosepage_<?= $this->getInputName(); ?>').bind('click',function() {
			dialog.show({
				url: '<?= \PC\Router::GetRoute(NULL, NULL, array('dialog','view','page','choose'));?>',
				type: dialog.dialogTypes.ajax,
				onLoad: function() {
					$('#browser').disableSelection();
					var t = $('#pagechoose').tree({
						sourceUrl: '<?= \PC\Router::GetRoute(NULL, NULL, array('page','tree')); ?>',
						imageUrl: '/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/'
					});
					t.onSelectPage(function(e, nodeId, name) {
						if(nodeId > 0) {
							var index = $('#topmenu_<?= $this->getInputName(); ?> ol.treeview li').size();
							$('#topmenu_<?= $this->getInputName(); ?> ol.treeview').append('<li id="node_'+ index +'">'+
									'<div><a href="javascript:;">'+name+'</a> '+
									'<a href="#" class="js-remove" style="display:none;">(Remove)</a>'+
									'</div>'+
									'<input type="hidden" name="<?= $this->getInputName(); ?>_NodeID['+index+']" value="'+nodeId+'" />'+
									'</li>');

							var list = $('#topmenu_<?= $this->getInputName(); ?> ol.treeview').nestedSortable('serialize');
				        	$('input[name="<?= $this->getInputName(); ?>"]').val(list);

							dialog.closeActive();
						}
					});
					t.refreshNode(0);
				}
			});
		});

		$('#topmenu_<?= $this->getInputName(); ?> ol > li div').live('mouseenter', function() {
			$(this).find('a.js-remove').show();
		}).live('mouseleave', function() {
			$(this).find('a.js-remove').hide();
		});

		$('#topmenu_<?= $this->getInputName(); ?> .js-add-url').bind('click', function(e) {
			e.preventDefault();
			var p = $(this).parent();
			var url = p.find('input[name="add_url"]');
			var label = p.find('input[name="add_label"]');
			var target = p.find('input[name="add_target"]');
			var className = p.find('input[name="add_class"]');

			var errors = [];

			if(url.val() == '') {
				errors.push('  - <?= $this->_('ErrorNoUrl'); ?>');
			}

			if(label.val() == '') {
				errors.push('  - <?= $this->_('ErrorNoLabel'); ?>');
			}

			if(errors.length > 0) {
				alert('Error:\n'+ errors.join('\n'));
				return false;
			}

			var index = $('#topmenu_<?= $this->getInputName(); ?> ol.treeview li').size();
			$('#topmenu_<?= $this->getInputName(); ?> ol.treeview').append('<li id="node_'+ index +'">'+
				'<div><a href="'+ url.val() +'" class="'+className.val()+'" target="'+target.val()+'">'+label.val()+'</a> '+
				'<a href="#" class="js-remove" style="display:none;">(Remove)</a>'+
				'</div>'+
				'<input type="hidden" name="<?= $this->getInputName(); ?>_label['+index+']" value="'+label.val()+'" />'+
				'<input type="hidden" name="<?= $this->getInputName(); ?>_url['+index+']" value="'+url.val()+'" />'+
				'<input type="hidden" name="<?= $this->getInputName(); ?>_target['+index+']" value="'+target.val()+'" />'+
				'<input type="hidden" name="<?= $this->getInputName(); ?>_className['+index+']" value="'+className.val()+'" />'+
				'</li>');

			url.val('');
			label.val('');
			target.val('');
			className.val('');

			var list = $('#topmenu_<?= $this->getInputName(); ?> ol.treeview').nestedSortable('serialize');
        	$('input[name="<?= $this->getInputName(); ?>"]').val(list);
		});

		$('#topmenu_<?= $this->getInputName(); ?> ol.treeview a.js-remove').live('click', function(e) {
			e.preventDefault();
			$(this).parents('li:first').hide(100, function() {
				$(this).remove();
				var list = $('#topmenu_<?= $this->getInputName(); ?> ol.treeview').nestedSortable('serialize');
            	$('input[name="<?= $this->getInputName(); ?>"]').val(list);
			});
		});

		$('#topmenu_<?= $this->getInputName(); ?> .tabs ul > li a').live('click', function(e) {
			e.preventDefault();
			$(this).parents('.tabs:first').find('li.active').removeClass('active');
			$(this).parent().addClass('active');
			$(this).parents('.tabs:first').find('div').hide();
			$(this).parents('.tabs:first').find('div:eq('+ $(this).parent().index() +')').show();
		});
	});
</script>