<? /* @var $this \CMS\Widget\Module\ModuleRadio */ ?>

<table id="radio-ctn-<?= md5($this->module->getPropertyModuleID());?>" style="width:100%;margin-bottom:15px;">
	<? foreach($this->radio as $name=>$value): ?>
	<tr>
		<td>
			<?= $name; ?>
		</td>
		<td>
			<?= $value; ?>
		</td>
		<td>
			<a href="#" class="js-radio-remove-<?= md5($this->module->getPropertyModuleID());?>"><?= $this->_('Remove')?></a>
			<input type="hidden" name="radio_name[<?= $this->module->getPropertyModuleID(); ?>][]" value="<?= $name;?>" />
			<input type="hidden" name="radio_value[<?= $this->module->getPropertyModuleID(); ?>][]" value="<?= $value; ?>" />
		</td>
	</tr>
	<? endforeach;?>
</table>
<div style="padding-bottom:10px;">
	<input type="text" name="d_name_[<?= $this->module->getPropertyModuleID(); ?>]" placeholder="<?= $this->_('Name');?>" id="radio-name-<?= md5($this->module->getPropertyModuleID());?>" />
	<input type="text" name="d_value_[<?= $this->module->getPropertyModuleID(); ?>]" placeholder="<?= $this->_('Value');?>" id="radio-value-<?= md5($this->module->getPropertyModuleID());?>" />
	<a href="javascript:;" onclick="radioAddValue<?= md5($this->module->getPropertyModuleID());?>();" class="btn-s"><?= $this->_('Add');?></a>
</div>

<script type="text/javascript">
		function checkTable<?= md5($this->module->getPropertyModuleID());?>() {
			var ctn = $('#radio-ctn-<?= md5($this->module->getPropertyModuleID());?>');
			if(ctn.find('tr').length > 0) {
				ctn.show();
			} else {
				ctn.hide();
			}
		};

		function radioAddValue<?= md5($this->module->getPropertyModuleID());?>() {
			var ctn = $('#radio-ctn-<?= md5($this->module->getPropertyModuleID());?>');
			var name = $('#radio-name-<?= md5($this->module->getPropertyModuleID());?>');
			var value = $('#radio-value-<?= md5($this->module->getPropertyModuleID());?>');
			ctn.append('<tr><td style="width:50%;">'+name.val()+'</td><td style="width:50%;">'+value.val()+
			'<td align="right"><a href="#" class="js-radio-remove-<?= md5($this->module->getPropertyModuleID());?>"><?= $this->_('Remove'); ?></a>'+
			'<input type="hidden" name="radio_name[<?= $this->module->getPropertyModuleID(); ?>][]" value="'+ name.val() +'" />' +
			'<input type="hidden" name="radio_value[<?= $this->module->getPropertyModuleID(); ?>][]" value="'+ value.val() +'" />' +
			'</td></tr>');

			name.val('');
			value.val('');

			checkTable<?= md5($this->module->getPropertyModuleID());?>();
		};

		checkTable<?= md5($this->module->getPropertyModuleID());?>();

		$('a.js-radio-remove-<?= md5($this->module->getPropertyModuleID());?>').live('click', function(e) {
			e.preventDefault();
			$(this).parents('tr:first').remove();
			checkTable<?= md5($this->module->getPropertyModuleID());?>();
		});
</script>