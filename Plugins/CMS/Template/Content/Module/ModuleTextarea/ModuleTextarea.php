<? /* @var $this \CMS\Widget\Module\ModuleTextarea */ ?>

<div class="input">
	<div class="label">
		<?= $this->module->getName();?>
	</div>
	<?= $this->form()->textarea($this->getInputName(), 10, 10, $this->page->getData($this->getInputName()), TRUE)->addClass('js-grow')->addAttribute('style','width:560px;height:100px;max-height:350px;');?>
	<div class="description">
		<?= $this->module->getDescription();?>
	</div>
</div>