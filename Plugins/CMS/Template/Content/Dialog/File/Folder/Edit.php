<? /* @var $this \CMS\Widget\Dialog\File\Folder\Edit */ ?>

<?= $this->form()->start('editFolder')->addAttribute('class', 'form'); ?>
	<?= $this->showErrors('Create.php'); ?>
	<div class="input">
		<?= $this->form()->label($this->_('File/Name'), 'name'); ?>
		<?= $this->form()->input('name', 'text', $this->folder->getTitle(), TRUE)->addAttribute('id', 'name');?>
	</div>

	<div class="btn">
		<?= $this->form()->submit('submit', $this->_('File/Save'));?>
	</div>
<?= $this->form()->end();?>