<? /* @var $this \CMS\Widget\Dialog\File\Folder\Create */ ?>

<?= $this->form()->start('createFolder')->addAttribute('class', 'form'); ?>
	<?= $this->showErrors('createFolder'); ?>
	<div class="input">
		<?= $this->form()->label($this->_('File/Name'), 'name'); ?>
		<?= $this->form()->input('name', 'text')->addAttribute('id', 'name');?>
	</div>

	<div class="btn">
		<?= $this->form()->submit('submit', $this->_('File/Create'));?>
	</div>
<?= $this->form()->end();?>