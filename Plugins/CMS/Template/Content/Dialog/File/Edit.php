<? /* @var $this \CMS\Widget\Dialog\File\Edit */ ?>

<?= $this->form()->start('editFile', 'post', NULL, \Pecee\UI\Form\Form::FORM_ENCTYPE_FORM_DATA); ?>
	<?= $this->showFlash(); ?>
	<?= $this->form()->input('file', 'file'); ?>

	<div class="options">
		<?= $this->form()->submit('save', $this->_('File/Save')); ?>
	</div>

<?= $this->form()->end();?>