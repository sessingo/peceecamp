<? /* @var $this \CMS\Widget\Dialog\File\Upload */ ?>

<div id="form">
	<div id="container" style="text-align:center;padding:40px 0 30px 0;">
		<a href="javascript:;" class="btn grey submit-content" id="pickfiles"><?= $this->_('File/ChooseFiles')?></a>
	</div>
</div>
<div id="progress" style="padding:30px 0;display:none;">
	<p class="large" style="text-align:center;margin-bottom:15px;">
		<?= $this->_('File/Uploading')?>
	</p>
	<div id="progress" class="progressbar" style="display:block;"><div></div></div>
</div>

<script type="text/javascript">
	$(document).ready(function() {

		$.getScript('/plugin/CMS/admin/js/plupload.full.js', function() {
			$.getScript('/plugin/CMS/admin/js/fileupload.js', function() {
				var uploader=new fileUpload({
					container: 'container',
					browseBtn: 'pickfiles',
					urlstream_upload: true,
					MaxFileSizeMB: <?= $this->getMaxFilesize(); ?>,
					UploadUrl: '<?= $this->getRoute(NULL, 'file', array('doUpload'), array('id' => session_id(), 'parentNodeId' => $this->getParam('parentNodeId')));?>',
					filters: [{title: '<?= $this->_('File/AllFiles'); ?>', extensions : '<?= $this->getExtensions(); ?>'}]
				});

				var totalSize=0;
				var progress=0;
				var errorNumbers= [];
				var errors= [];
				var filesCount=0;
				var errorsCount=0;

				var reset=function() {
					totalSize=0;
					progress=0;
					errorNumbers=[];
					errors=[];
					filesCount=0;
					errorsCount=0;
					$('#form').css({ height: 'auto', 'opacity': 1, overflow: 'visible' });
					$('#progress .progressbar div').css({width: '0px'});
					$('#progress').css({ opacity: '0' });
				};

				uploader.bind('QueueChanged', function(up, files) {
					$('#form').animate({height: 0, overflow: 'none', 'opacity': 0}, 250, function() {
						uploader.start();
						$('#progress').fadeIn(150);
					});
				});

				uploader.bind('FilesAdded', function(up, files) {
					filesCount=files.length;
					<? if(!\Pecee\Auth::GetInstance()->hasAdminIP()) : ?>
					if(filesCount > <?= $this->maxFiles; ?>) {
						alert('<?= $this->_('File/UploadError'); ?>', '<?= $this->_('File/MaxFilesReached', $this->maxFiles);?>');
						return false;
					}
					<? endif; ?>
					for(var i in files) {
						totalSize+=files[i].size;
					}
				});

				uploader.bind('ChunkUploaded', function(up, file, r) {
					r=$.parseJSON(r.response);
					if(r!=null && r.result != 'true') {
						errorsCount++;
						if($.inArray(r.code, errorNumbers) == -1) {
							errors.push(' - '+r.msg);
							errorNumbers.push(r.code);
						}
					}
					var loaded=file.size-(file.size-file.loaded);
					progress+=loaded;
					var percent=(progress/totalSize)*100;
					$('#progress .progressbar div').animate({width: percent+'%'}, 150);
				});

				uploader.bind('UploadComplete', function(up, files) {
					reset();
					if(errors.length > 0) {
						alert('File/UploadError');
						if(filesCount > 0) {
							top.location.href='<?= $this->getRoute(NULL, 'file');?>';
						}
					} else {
						top.location.href='<?= $this->getRoute(NULL, 'file');?>';
					}
				});

				uploader.init();
			});
		});

	});
</script>