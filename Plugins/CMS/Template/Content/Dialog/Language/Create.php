<? /* @var $this \CMS\Widget\Dialog\Language\Create */ ?>

<?= $this->form()->start('addLanguage')->addAttribute('class', 'form'); ?>
	<?= $this->showErrors('addLanguage'); ?>
	<div class="input">
		<?= $this->form()->label($this->_('Language/Name'), 'name'); ?>
		<?= $this->form()->input('name', 'text')->addAttribute('id', 'name')->addAttribute('onkeyup', 'checkValue(this);');?>
	</div>

	<div class="input">
		<?= $this->form()->label($this->_('Language/Alias'), 'alias'); ?>
		<?= $this->form()->input('alias', 'text')->addAttribute('id', 'alias');?>
	</div>

	<div class="btn">
		<?= $this->form()->submit('submit', $this->_('Language/Save'));?>
	</div>

	<script type="text/javascript">
		var checkValue = function(el) {
			var val = $(el).val().match(/[A-Za-z0-9]+/);
			if(val != null) {
				val = val[0];
				val = (val.length > 2) ? val.substr(0,2) : val;
				$('#alias').val(val.toLowerCase());
			}
		};
	</script>
<?= $this->form()->end();?>