<? /* @var $this \CMS\Widget\Dialog\Language\Key\Create */ ?>

<?= $this->form()->start('addLanguageKey')->addAttribute('class', 'form'); ?>
	<?= $this->showErrors('addLanguageKey'); ?>
	<div class="input">
		<?= $this->form()->label($this->_('Language/Key/Name'), 'name'); ?>
		<?= $this->form()->input('name', 'text')->addAttribute('id', 'name');?>
	</div>

	<div class="btn">
		<?= $this->form()->submit('submit', $this->_('Language/Key/Save'));?>
	</div>
<?= $this->form()->end();?>