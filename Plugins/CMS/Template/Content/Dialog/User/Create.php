<? /* @var $this \CMS\Widget\Dialog\User\Create */ ?>

<?= $this->form()->start('createUser')->addAttribute('class', 'form'); ?>
	<?= $this->showErrors('createUser'); ?>
	<div class="input">
		<?= $this->form()->label($this->_('User/Name'), 'username'); ?>
		<?= $this->form()->input('username', 'text')->addAttribute('id', 'username');?>
	</div>

	<div class="input">
		<?= $this->form()->label($this->_('User/UserType'), 'propertyNodeId'); ?>
		<?= $this->renderProperties(); ?>
	</div>

	<div class="btn">
		<?= $this->form()->submit('submit', $this->_('User/Create'));?>
	</div>
<?= $this->form()->end();?>