<? /* @var $this \CMS\Widget\Dialog\User\Property\Tab\Edit */ ?>

<?= $this->showFlash(); ?>

<?= $this->form()->start('edit-tab', 'post')->addClass('form'); ?>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Tab/Name'), 'name'); ?>
		<?= $this->form()->input('name', 'text', $this->tab->getName(), TRUE)->addAttribute('ID', 'name'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Tab/Inheritable'), 'inheritable'); ?>
		<?= $this->form()->bool('inheritable', $this->tab->getInheritable(), TRUE, TRUE)->addAttribute('ID', 'inheritable'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Tab/Order'), 'order'); ?>
		<?= $this->form()->input('order', 'text', $this->tab->getOrder(), TRUE)->addAttribute('ID', 'order'); ?>
	</div>
	<div class="btn">
		<?= $this->form()->submit('submit', $this->_('Property/Tab/Save'));?>
	</div>
<?= $this->form()->end(); ?>