<? /* @var $this \CMS\Widget\Dialog\User\Property\Create */ ?>

<?= $this->showFlash(); ?>

<?= $this->form()->start('create-property', 'post')->addClass('form'); ?>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Title'), 'title'); ?>
		<?= $this->form()->input('title', 'text')->addAttribute('ID', 'title'); ?>
	</div>
	<div class="btn">
		<?= $this->form()->submit('submit', $this->_('Property/Save'));?>
	</div>
<?= $this->form()->end(); ?>