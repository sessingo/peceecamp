<? /* @var $this \CMS\Widget\Dialog\Page\Property\Tab\Create */ ?>

<?= $this->showFlash(); ?>

<?= $this->form()->start('createtab', 'post')->addClass('form'); ?>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Tab/Name'), 'name'); ?>
		<?= $this->form()->input('name', 'text')->addAttribute('ID', 'name'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Tab/Inheritable'), 'inheritable'); ?>
		<?= $this->form()->bool('inheritable', '1', TRUE, TRUE)->addAttribute('ID', 'inheritable'); ?>
	</div>
	<div class="btn">
		<?= $this->form()->submit('submit', $this->_('Property/Tab/Save'));?>
	</div>
<?= $this->form()->end(); ?>