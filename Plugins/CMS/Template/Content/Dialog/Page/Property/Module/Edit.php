<? /* @var $this \CMS\Widget\Dialog\Page\Property\Module\Edit */ ?>

<?= $this->showFlash(); ?>

<?= $this->form()->start('edit-module', 'post')->addClass('form'); ?>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/Name'), 'name'); ?>
		<?= $this->form()->input('name', 'text', $this->module->getName(), TRUE)->addAttribute('ID', 'name'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/Description'), 'description'); ?>
		<?= $this->form()->input('description', 'text', $this->module->getDescription(), TRUE)
			->addAttribute('ID', 'description')
			->addAttribute('maxlength', 255); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/ID'), 'controlId'); ?>
		<?= $this->form()->input('controlId', 'text', $this->module->getControlID(), TRUE)->addAttribute('ID', 'controlId'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/Module'), 'module'); ?>
		<?= $this->form()->selectStart('module', new \CMS\Dataset\DatasetModules($this->_('Property/Module/Choose')), $this->module->getModule(), TRUE)->addAttribute('ID', 'module'); ?>
	</div>
	<? if($this->moduleContent) : ?>
		<div style="margin: 10px 0;">
			<?= $this->moduleContent; ?>
		</div>
	<? endif; ?>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/Tab'), 'propertyTabId'); ?>
		<?= $this->form()->selectStart('propertyTabId', new \CMS\Dataset\Property\PropertyTabs($this->module->getNodeID(), $this->_('Property/Module/Choose')), $this->module->getPropertyTabID(), TRUE)->addAttribute('ID', 'propertyTabId'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/Required'), 'required'); ?>
		<?= $this->form()->bool('required', TRUE, TRUE)->setChecked($this->module->getRequired())->addAttribute('ID', 'required'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/Inheritable'), 'inheritable'); ?>
		<?= $this->form()->bool('inheritable', $this->module->getInheritable(), TRUE, TRUE)->addAttribute('ID', 'inheritable'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/Order'), 'order'); ?>
		<?= $this->form()->input('order', 'text', $this->module->getOrder(), TRUE)->addAttribute('ID', 'order'); ?>
	</div>
	<div class="btn">
		<?= $this->form()->submit('submit', $this->_('Property/Module/Save'))->addAttribute('id','submit');?>
	</div>
	<?= $this->form()->input('postback', 'hidden', '')->addAttribute('ID', 'postback'); ?>
<?= $this->form()->end(); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$('#module').live('change', function() {
			$('#postback').val('1');
			$('#submit').click();
		});
	});
</script>