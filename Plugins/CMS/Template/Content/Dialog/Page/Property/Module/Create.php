<? /* @var $this \CMS\Widget\Dialog\Page\Property\Module\Create */ ?>

<?= $this->showFlash(); ?>

<?= $this->form()->start('create-property', 'post')->addClass('form'); ?>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/Name'), 'name'); ?>
		<?= $this->form()->input('name', 'text', NULL, TRUE)->addAttribute('ID', 'name'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/Description'), 'description'); ?>
		<?= $this->form()->input('description', 'text', NULL, TRUE)->addAttribute('ID', 'description')->addAttribute('maxlength', 255); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/ID'), 'controlId'); ?>
		<?= $this->form()->input('controlId', 'text', NULL, TRUE)->addAttribute('ID', 'controlId'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/Module'), 'module'); ?>
		<?= $this->form()->selectStart('module', new \CMS\Dataset\DatasetModules($this->_('Property/Module/Choose')), NULL, TRUE)->addAttribute('ID', 'module'); ?>
	</div>
	<? if($this->moduleContent) : ?>
		<div style="margin: 10px 0;">
			<?= $this->moduleContent; ?>
		</div>
	<? endif; ?>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/Tab'), 'propertyTabId'); ?>
		<?= $this->form()->selectStart('propertyTabId', new \CMS\Dataset\Property\PropertyTabs($this->getParam('nodeId'), $this->_('Property/Module/Choose')), NULL, TRUE)->addAttribute('ID', 'propertyTabId'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/Required'), 'required'); ?>
		<?= $this->form()->bool('required', TRUE, TRUE)->addAttribute('ID', 'required'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/Inheritable'), 'inheritable'); ?>
		<?= $this->form()->bool('inheritable','1', TRUE, TRUE)->addAttribute('ID', 'inheritable'); ?>
	</div>
	<div class="input">
		<?= $this->form()->label($this->_('Property/Module/Order'), 'order'); ?>
		<?= $this->form()->input('order', 'text', NULL, TRUE)->addAttribute('ID', 'order'); ?>
	</div>
	<div class="btn">
		<?= $this->form()->submit('submit', $this->_('Property/Module/Save'))->addAttribute('id','submit');?>
	</div>
	<?= $this->form()->input('postback', 'hidden', '')->addAttribute('ID', 'postback'); ?>
<?= $this->form()->end(); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$('#module').live('change', function() {
			$('#postback').val('1');
			$('#submit').click();
		});
	});
</script>