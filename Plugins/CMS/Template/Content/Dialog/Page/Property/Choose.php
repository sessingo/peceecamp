<? /* @var $this \CMS\Widget\Dialog\Page\Property\Choose */ ?>

<div id="loader"></div>
<ul id="propertychoose" class="treeview">
	<li>
		<div class="hitarea collapsed"></div>
		<a href="javascript:;" class="js-root" data-id="<?= $this->getParam('nodeId', '0')?>" style="font-weight:bold;"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/folder.png" alt="" /> <?= $this->plugin->application->Name; ?></a>
	</li>
</ul>
<div class="clear"></div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#propertychoose').disableSelection();
	});
</script>