<? /* @var $this \CMS\Widget\Dialog\Page\Create */ ?>

<?= $this->form()->start('createPage')->addAttribute('class', 'form'); ?>
	<?= $this->showErrors('createPage'); ?>
	<div class="input">
		<?= $this->form()->label($this->_('Page/Title'), 'title'); ?>
		<?= $this->form()->input('title', 'text')->addAttribute('id', 'title');?>
	</div>

	<div class="input">
		<?= $this->form()->label($this->_('Page/Type'), 'propertyNodeId'); ?>
		<?= $this->renderProperties(); ?>
	</div>

	<div class="btn">
		<?= $this->form()->submit('submit', $this->_('Page/Create'));?>
	</div>
<?= $this->form()->end();?>