<? /* @var $this \CMS\Widget\Dialog\Page\Publish */ ?>

<?= $this->form()->start('pagePublish')->addAttribute('class', 'form'); ?>
	<?= $this->showErrors('pagePublish'); ?>

	<div class="input">
		<?= $this->form()->label($this->_('Page/RecreateUrl'), 'createSlug'); ?>
		<?= $this->form()->input('createSlug', 'checkbox', 1, TRUE)->addAttribute('ID', 'createSlug'); ?>
	</div>

	<div class="input">
		<?= $this->form()->label($this->_('Page/PublishChilds'), 'publishChilds'); ?>
		<?= $this->form()->input('publishChilds', 'checkbox', 1, TRUE)->addAttribute('ID', 'publishChilds'); ?>
	</div>

	<div class="btn">
		<?= $this->form()->submit('submit', $this->_('Page/Save'));?>
	</div>
<?= $this->form()->end();?>