<? /* @var $this \CMS\Widget\Dialog\Page\Hosts */ ?>

<?= $this->form()->start('managehosts')->addAttribute('class', 'form'); ?>
	<?= $this->form()->input('nodeHostId', 'hidden', NULL, TRUE)->addAttribute('ID', 'nodeHostId'); ?>
	<?= $this->form()->input('delete', 'hidden', NULL, TRUE)->addAttribute('ID', 'delete'); ?>
	<?= $this->form()->input('postback', 'hidden', '0')->addAttribute('ID', 'postback'); ?>
	<? if($this->isPostBack() && $this->host && $this->host->hasRow()) : ?>
		<h3><?= $this->_('Page/ManageHostname/EditDomain', $this->host->getHostname())?></h3>
		<?= $this->showFlash('managehosts'); ?>

		<div class="input noline">
			<?= $this->form()->label($this->_('Page/ManageHostname/Domain'), 'domain'); ?>
			<?= $this->form()->input('domain', 'text', $this->host->getHostname())->addAttribute('ID','domain'); ?>
		</div>

		<div class="input">
			<?= $this->form()->label($this->_('Page/ManageHostname/Language'), 'languageId'); ?>
			<?= $this->form()->selectStart('languageId', $this->getLanguages(), $this->host->getLanguageID(), TRUE)->addAttribute('ID','languageId'); ?>
		</div>

		<div class="btn" style="margin-bottom:15px;">
			<?= $this->form()->submit('editDomain', $this->_('Page/ManageHostname/Edit'))->addAttribute('ID','submit')->addClass('btn-s');?>
		</div>
	<? else: ?>
		<h3><?= $this->_('Page/ManageHostname/AddDomain'); ?></h3>
		<?= $this->showFlash('managehosts'); ?>

		<div class="input noline">
			<?= $this->form()->label($this->_('Page/ManageHostname/Domain'), 'domain'); ?>
			<?= $this->form()->input('domain', 'text', NULL, TRUE)->addAttribute('ID','domain'); ?>
		</div>

		<div class="input">
			<?= $this->form()->label($this->_('Page/ManageHostname/Language'), 'languageId'); ?>
			<?= $this->form()->selectStart('languageId', $this->getLanguages(), NULL, TRUE)->addAttribute('ID','languageId'); ?>
		</div>

		<div class="btn" style="margin-bottom:15px;">
			<?= $this->form()->submit('addDomain', $this->_('Page/ManageHostname/Add'))->addAttribute('ID','submit')->addClass('btn-s');?>
		</div>
	<? endif; ?>

	<h3><?= $this->_('Page/ManageHostname/EditCurrentDomains'); ?></h3>

	<? if(!$this->hosts->hasRows()) : ?>
		<div>
			<?= $this->_('Page/ManageHostname/NoDomainsAdded'); ?>
		</div>
	<? else: ?>
		<ul>
		<? foreach($this->hosts->getRows() as $host) : ?>
			<li style="margin-bottom:8px;"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/world.png" alt="" style="vertical-align:middle;" /> <?= (!$host->getHostname()) ? $this->_('Page/ManageHostname/All') : $host->getHostname(); ?> (<?= $host->getLanguage()->getName(); ?>) - <a href="javascript:;" onclick="editHost('<?= $host->getNodeHostID(); ?>');"><?= $this->_('Page/ManageHostname/Edit')?></a> | <a href="javascript:;" onclick="deleteHost('<?= $host->getNodeHostID(); ?>');"><?= $this->_('Page/ManageHostname/Delete'); ?></a></li>
		<? endforeach;?>
		</ul>
	<? endif;?>
<?= $this->form()->end();?>

<div class="options">
	<a href="javascript:dialog.closeActive();" class="btn"><?= $this->_('Page/ManageHostname/Close'); ?></a>
</div>

<script type="text/javascript">
	var editHost = function(nodeHostId) {
		$('form[name="managehosts"] #postback').val('1');
		$('form[name="managehosts"] #nodeHostId').val(nodeHostId);
		$('form[name="managehosts"] #submit').trigger('click');
	};

	var deleteHost = function(nodeHostId) {
		if(confirm('<?= $this->_('Page/ManageHostname/DeleteDomainConfirm'); ?>')) {
			$('form[name="managehosts"] #postback').val('1');
			$('form[name="managehosts"] #nodeHostId').val(nodeHostId);
			$('form[name="managehosts"] #delete').val('1');
			$('form[name="managehosts"] #submit').trigger('click');
		}
	};
</script>