<? /* @var $this \CMS\Widget\Dialog\Icons */ ?>

<div class="icons">
	<? if(count($this->icons) > 0)?>
	<ul>
		<? foreach($this->icons as $icon) : ?>
		<li>
			<a href="javascript:;" onclick="chooseIcon(this);" data-value="<?= $icon; ?>"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/<?= $icon; ?>" alt="" style="width:16px;height:16px;" /></a>
		</li>
		<? endforeach; ?>
	</ul>
</div>

<div style="margin-top:15px;text-align:right;">
	<a href="javascript:;" onclick="cancel();" class="btn-s"><?= $this->_('Icons/Cancel'); ?></a>
	<a href="javascript:;" onclick="dialog.closeActive();" class="btn-s"><?= $this->_('Icons/OK'); ?></a>
</div>

<script type="text/javascript">

	function cancel() {
		$('.icons li.active').removeClass('active');
		dialog.closeActive();
	};

	function chooseIcon(el) {
		el = $(el);
		$('.icons li.active').removeClass('active');
		el.parent().addClass('active');
	};
</script>