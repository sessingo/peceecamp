<? /* @var $this \CMS\Widget\Dialog\Macro\Create */ ?>

<?= $this->form()->start('createMacro')->addAttribute('class', 'form'); ?>
	<?= $this->showErrors('createMacro'); ?>
	<div class="input">
		<?= $this->form()->label($this->_('Settings/Macro/Title'), 'title'); ?>
		<?= $this->form()->input('title', 'text')->addAttribute('id', 'title');?>
	</div>

	<div class="btn">
		<?= $this->form()->submit('submit', $this->_('Settings/Macro/Create'));?>
	</div>
<?= $this->form()->end();?>