<? /* @var $this \CMS\Widget\Dialog\Macro\Propertyedit */ ?>

<?= $this->form()->start('propertyEdit')->addAttribute('class', 'form'); ?>

	<p>
		<?= $this->_('Settings/Macro/PropertiesDescription')?>
	</p>

	<?= $this->showErrors(); ?>

	<div class="input">
		<?= $this->form()->label($this->_('Settings/Macro/PropertyMethod'), 'method'); ?>
		<?= $this->module->getControlID(); ?>
	</div>

	<div class="input">
		<?= $this->form()->label($this->_('Settings/Macro/PropertyName'), 'name'); ?>
		<?= $this->form()->input('name','text', $this->module->getName(), TRUE);?>
	</div>

	<div class="input">
		<?= $this->form()->label($this->_('Settings/Macro/Widget'), 'widget'); ?>
		<?= $this->form()->selectStart('module', new \CMS\Dataset\DatasetModules($this->_('Property/Module/Choose')), \Pecee\Str::GetFirstOrValue($this->data->module, $this->module->getModule()), TRUE)->addAttribute('ID', 'module'); ?>

		<? if($this->moduleContent) : ?>
			<div style="padding: 30px 0;clear:both;">
				<?= $this->moduleContent; ?>
			</div>
		<? endif; ?>
	</div>
	<?= $this->form()->input('postback', 'hidden', '')->addAttribute('ID', 'postback'); ?>
	<div class="options">
		<?= $this->form()->submit('submit', $this->_('Settings/Macro/Save'))->addAttribute('ID','submit');?>
	</div>

	<script>
		$(document).ready(function() {
			$('#module').live('change', function() {
				$('#postback').val('1');
				$('#submit').click();
			});
		});
	</script>

<?= $this->form()->end();?>