<? /* @var $this \CMS\Widget\Dialog\Macro\Properties */ ?>

<?= $this->form()->start('properties')->addAttribute('class', 'form'); ?>

	<p>
		<?= $this->_('Settings/Macro/PropertiesDescription')?>
	</p>

	<?= $this->showErrors('properties'); ?>

	<table>
		<? foreach($this->properties as $property) : ?>
		<tr>
			<td style="width:10px;">
				<?= $this->form()->bool('methods[]', $property->name, TRUE, $property->name)->addAttribute('ID', $property->name)->setChecked($this->hasProperty($property->name)); ?>
			</td>
			<td>
				<label for="<?= $property->name; ?>"><?= $property->name; ?></label>
			</td>
		</tr>
		<? endforeach; ?>
	</table>

	<div class="options">
		<?= $this->form()->submit('submit', $this->_('Settings/Macro/Save'));?>
	</div>
<?= $this->form()->end();?>