<? /* @var $this \CMS\Widget\Dialog\Macro\Editoradd   */ ?>
<?= $this->form()->start('insert-macro', 'post', $this->getRoute(NULL, NULL, NULL, array(), TRUE))->addClass('form')->addAttribute('ID','insert-macro'); ?>
	<?= $this->form()->selectStart('nodeId', $this->widgets, $this->data->nodeId, TRUE)->addAttribute('ID','widgets')->addAttribute('style','width:100%;'); ?>
	<div style="margin-top:20px;">
		<? if($this->macro && $this->macro->hasRow() && $this->macro->getModules()->hasRows()) : ?>
				<? foreach($this->macro->getModules()->getRows() as $property) : ?>
				<div>
					<?= $this->renderModule($property); ?>
				</div>
				<? endforeach; ?>
		<? endif; ?>
		<div class="clear"></div>
	</div>
	<?= $this->form()->input('postback', 'hidden', '0')->addAttribute('ID', 'postback'); ?>
	<?= $this->form()->input('update', 'hidden', NULL, TRUE)->addAttribute('ID', 'postback'); ?>
	<div class="options">
		<?= $this->form()->submit('submit', $this->_('Settings/Macro/Save'))->addAttribute('id','submit');?>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#widgets').live('change', function() {
				$('#postback').val('1');
				$('#submit').trigger('click');
			});

			<? if($this->macro && $this->macro->hasRow() && $this->isPostBack() && !$this->data->postback) : ?>

				var html = '<div:macro widget="<?= urlencode($this->macro->getWidget()); ?>" ';
				html += 'data-id="<?= $this->macro->getNodeID(); ?>" data-data="<?= $this->getObject(); ?>" ';
				html += 'style="display:block;background-color:#EEE;margin:10px;font-size:12px;font-weight:bold;padding:5px;border:3 px solid #CCC;text-align:center;"';
				html += '><?= $this->macro->getTitle(); ?></div:macro>';

				var e = jQuery.Event('<?= $this->jsEvent; ?>', {
			        html: html,
			        window: window
			    });

			    $(document).trigger(e);
			    dialog.closeActive();

			<? else: ?>
			$('.js-editor').redactor({
				toolbarFixedBox: false,
				minHeight: 100,
				maxHeight: 300,
				focus: true,
				autoresize: false,
				imageUpload: admin.options.imageUploadUrl,
				fileUpload: admin.options.fileUploadUrl,
				imageGetJson: admin.options.imageGetJsonUrl
			});
			<? endif; ?>
		});
	</script>
<?= $this->form()->end(); ?>