<? /* @var $this \CMS\Widget\Admin\User\UserChoose */ ?>

<div style="margin:8px 0;">

	<? if(!$this->page || $this->page && !$this->page->hasRow()) : ?>
		<span id="pagename_<?= $this->name; ?>"></span>
		<span id="remove_link_<?= $this->name; ?>" style="display:none;">
			<a href="javascript:;" onclick="removeLink<?= md5($this->name); ?>(this);"><?= $this->_('User/Remove'); ?></a>
		</span>
	<? else: ?>
		<span id="pagename_<?= $this->name; ?>"><?= $this->page->getTitle(); ?></span>
		<span id="remove_link_<?= $this->name; ?>">
			<a href="javascript:;" onclick="removeLink<?= md5($this->name); ?>(this);"><?= $this->_('User/Remove'); ?></a> -
		</span>
	<? endif; ?>

	<a href="javascript:;" onclick="chooseUser<?= md5($this->name); ?>();" id="choosepage_<?= $this->name; ?>"><?= $this->_('User/ChooseUser'); ?></a>
	<?= $this->form()->input($this->name, 'hidden', $this->value, TRUE)->addAttribute('id', $this->name)?>
	<script type="text/javascript">
		function chooseUser<?= md5($this->name); ?>() {
			parent.dialog.show({
				url: '<?= \PC\Router::GetRoute(NULL, NULL, array('dialog','view','user','choose'));?>?nodeId=<?= $this->defaultNode?>',
				type: dialog.dialogTypes.ajax,
				onLoad: function() {
					var t = parent.$('#userchoose').tree({
						sourceUrl: '<?= \PC\Router::GetRoute(NULL, NULL, array('user','tree')); ?>',
						imageUrl: '/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/',
						setCookie: false
					});
					t.onSelectPage(function(e, nodeId, name,ui) {
						<? if($this->properties) :?>
						if(nodeId > 0) {
							var properties = [<?= $this->properties;?>];

							var propertyId = parseInt(ui.parents('li:first').attr('data-propertyId'));
							if($.inArray(propertyId, properties) > -1) {
								$('#pagename_<?= $this->name;?>').html(name);
								$('#<?= $this->name; ?>').val(nodeId);
								$('#remove_link_<?= $this->name; ?>').show();
								parent.dialog.closeActive();
							}
						}
						<? else: ?>
						if(nodeId > 0) {
							$('#pagename_<?= $this->name;?>').html(name);
							$('#<?= $this->name; ?>').val(nodeId);
							$('#remove_link_<?= $this->name; ?>').show();
							parent.dialog.closeActive();
						}
						<? endif;?>
					});
					<? if($this->properties) :?>
						t.onCallback(function(el,p) {
							var properties = [<?= $this->properties;?>];
							if(p != null && p.find('li').length > 0) {
								p.find('li').each(function() {
									var propertyId = parseInt($(this).attr('data-propertyId'));
									if($.inArray(propertyId, properties) == -1) {
										$(this).find('a:first').addClass('disabled');
									}
								});
							}

						});
					<? endif;?>
					t.refreshNode(0);
				}
			});
		}

		function removeLink<?= md5($this->name); ?>(el) {
			$('#<?= $this->name; ?>').val('');
			$('#pagename_<?= $this->name; ?>').html('');
			$(el).parent().hide();
		};
	</script>
</div>