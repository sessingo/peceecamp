<? /* @var $this \CMS\Widget\Admin\User\UserHome */ ?>
<div class="headline">
	<ul class="hor-nav">
		<li><a href="<?= $this->getRoute(NULL, 'user'); ?>" class="active"><?= $this->_('User/Users')?></a></li>
		<? if(\Pecee\Model\User\ModelUser::Current()->getAdminLevel() == \PC\Helper::USER_TYPE_ADMIN) :?>
		<li><a href="<?= $this->getRoute(NULL, 'userproperty'); ?>"><?= $this->_('User/Structure')?></a></li>
		<? endif; ?>
	</ul>
	<ul>
		<li>
			<?= $this->form()->start('search', 'get', $this->getRoute(NULL, 'user', array('search'))); ?>
				<?= $this->form()->input('query', 'text')->addAttribute('placeholder', $this->_('User/Search'))->addAttribute('ID','query')->addAttribute('autocomplete','off'); ?>
			<?= $this->form()->end(); ?>
		</li>
		<li>
			<a href="#" data-href="<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'user','create')); ?>" class="js-dialog btn-s"><?= $this->_('User/New'); ?></a>
		</li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>
	<div id="loader"></div>
	<ol id="userbrowser" class="treeview" style="float:none;">
		<li id="node_0">
			<div>
				<div class="hitarea <?= (in_array('0', $this->activeNodes) ? 'expanded' : 'collapsed')?>"></div>
				<a href="javascript:;" class="js-root" data-id="0" style="font-weight:bold;"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/folder.png" alt="" /> <?= $this->plugin->application->Name; ?></a>
				<? if($this->users && $this->users->hasRows()): ?>
				<ol<?= (in_array('0', $this->activeNodes) ? '' : ' style="display:none;"')?>>
					<? foreach($this->users->getRows() as $p) : ?>
					<li id="node_<?= $p->getNodeID(); ?>" data-accepts="<?= (is_array($p->getProperty()->getProperties())) ? join(',', $p->getProperty()->getProperties()) : ''?>" data-propertyId="<?= $p->getProperty()->getNodeID();?>">
						<div>
							<div class="hitarea <?= (in_array($p->getNodeID(), $this->activeNodes) ? 'expanded' : 'collapsed'); ?>"></div><a href="<?= $this->getRoute(NULL, 'user', array('edit', $p->getNodeID()));?>" data-id="<?= $p->getNodeID(); ?>" class="js-user"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/<?= $p->getProperty()->getIcon(); ?>" alt="" /><?= $p->getTitle();?></a>
						</div>
						<?= $this->loopPages($p)?>
					</li>
					<? endforeach; ?>
				</ol>
			<? endif;?>
			</div>
		</li>
	</ol>

	<div class="clear"></div>

	<script type="text/javascript">

		$(document).ready(function() {

			$('.ctn').disableSelection();

			var t = $('#userbrowser').tree({
				cookieName: 'UserActiveNodes',
				sourceUrl: '<?= $this->getRoute(NULL, 'user',array('tree'))?>',
				editUrl: '<?= $this->getRoute(NULL, 'user', array('edit'));?>',
				imageUrl: '/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/'
			});

			var search = null;
			var searchTimer = null;

			$('#query').bind('keyup', function() {
				var self=$(this);
				self.parents('form:first').find('ul').remove();
				clearTimeout(searchTimer);
				searchTimer = setTimeout(function() {
					if(self.val() != '') {
						if(search != null) {
							search.abort();
							search = null;
						}
						search = $.getJSON('<?= $this->getRoute(NULL, 'user', array('search')); ?>?query='+self.val(), function(r) {
							self.parents('form:first').find('ul').remove();
							if(r.nodes != null && r.nodes.length > 0) {
								var el = $('<ul/>');
								for(var i=0;i<r.nodes.length;i++) {
									var page = r.nodes[i];
									el.append('<li><a href="<?= $this->getRoute(NULL, 'user', array('edit'));?>'+ page.id +'"> <img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/'+page.icon+'" alt="" /> '+ page.title +'</a></li>');
								}
								self.parents('form:first').append(el);
							}
							search = null;
						});
					}
				}, 500);
			});

			$(document).bind('click', function() {
				$('#query').parents('form:first').find('ul').remove();
			});

			$('ol#userbrowser').nestedSortable({
	            handle: '> div',
	            items: 'ol > li',
	            toleranceElement: '> div',
	            protectRoot: true,
	            tolerance: 'pointer',
	            tabSize : 20,
	            expandOnHover: 700,
	            revert: 100,
	            isAllowed: function(item,parent) {
	            	var accepts = $(parent).attr('data-accepts');
					accepts = (accepts != null) ? accepts.split(',') : '';
					return ($(parent).find('a:first').data('id') == '0' || $.inArray($(item).attr('data-propertyId'), accepts) > -1);
	            },
	            stop: function(e, ui) {
	            	$('#loader').show();
	            	var list = $('ol#userbrowser').nestedSortable('serialize');
	                var el = $(ui.item);
	                var parent = el.parents('li:first').find('a');
	            	$.post('<?= $this->getRoute(NULL, 'user',array('setparent')); ?>?parentNodeId='+parent.data('id'), list, function(r) {
	            		$('#loader').hide();
						if(r.result == null || !r.result) {
							alert('<?= $this->_('User/ErrorUpdating'); ?>');
						}
	            	});
	            }
	        });

			$('.treeview li a.js-root').contextmenu([
				{
	            	name: '<?= $this->_('User/New');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/user_add.png',
	                callback: function(activeElement) {
	                    dialog.show({
	                    	url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'user', 'create')); ?>',
	                        type: dialog.dialogTypes.ajax,
	                        onLoad: function() {
	                        	$('select').ddslick({ imagePosition: "left" });
	                        }
						});
					}
				},
				{
	            	name: '<?= $this->_('Page/ClearCache');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/database_refresh.png',
	                callback: function(activeElement) {
	                	$('#loader').show();
	                	$.getJSON('<?= $this->getRoute(NULL, NULL, array('file','clearcache.json')) ?>', function(r) {
	                		$('#loader').hide();
	                	});
					}
	            },
				{
	            	name: '<?= $this->_('User/Refresh');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/arrow_refresh.png',
	                callback: function(activeElement) {
	                	t.refreshNode($(activeElement).data('id'));
					}
	            }
			]);

			$('.treeview li a.js-user').contextmenu([
				{
					name: '<?= $this->_('User/New');?>',
					image: '/plugin/CMS/admin/gfx/ico/user_add.png',
				    callback: function(activeElement) {
				    	var parent = $(activeElement);
				        dialog.show({
				        	url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'user', 'create')); ?>?parentNodeId='+parent.data('id'),
				            type: dialog.dialogTypes.ajax,
				            onLoad: function() {
				            	$('select').ddslick({ imagePosition: "left" });
				            }
						});
					}
				},
				{
	            	name: '<?= $this->_('User/Edit');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/user_edit.png',
	                callback: function(activeElement) {
	                	var parent = $(activeElement);
	                	top.location.href='<?= $this->getRoute(null, 'user', array('edit'));?>'+parent.data('id');
					}
	            },
	            {
	            	name: '<?= $this->_('User/Delete');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/user_delete.png',
	                callback: function(activeElement) {
	                	$('#loader').show();
	                	var parent = $(activeElement);
		                admin.dialog.confirm('<?= $this->_('User/ConfirmDelete');?>', '<?= $this->_('User/ConfirmDeleteText');?>', function(r) {
		                	$('#loader').hide();
							if(r) {
								$.post('<?= $this->getRoute(NULL, 'user', array('deletePage'))?>', 'nodeId='+parent.data('id'), function(r) {
									if(r == null || r.result != null && !r.result) {
										alert('<?= $this->_('User/ErrorDeleting');?>');
									} else {
										parent.parents('li:first').hide(100);
									}
								});
							}
		                });
					}
	            },
	            {
	            	name: '<?= $this->_('User/Refresh');?>',
	            	cookieName: 'UserActiveNodes',
	            	image: '/plugin/CMS/admin/gfx/ico/arrow_refresh.png',
	                callback: function(activeElement) {
	                	t.refreshNode($(activeElement).data('id'));
					}
	            }
			]);
		});
	</script>
</div>