<? /* @var $this \CMS\Widget\Admin\User\Property\PropertyEdit */ ?>
<div class="headline">
	<ul class="hor-nav">
		<li><?= $this->tab->button('settings', $this->_('Property/Settings')); ?></li>
		<li><?= $this->tab->button('structure', $this->_('Property/Structure')); ?></li>
		<li><?= $this->tab->button('tabs', $this->_('Property/Tabs')); ?></li>
		<li><?= $this->tab->button('modules', $this->_('Property/Modules')); ?></li>
	</ul>
	<ul>
		<li>
			<a href="#" data-href="<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'user', 'property', 'tab', 'create'), array('nodeId' => $this->property->getNodeID())); ?>" class="js-dialog btn-s"><?= $this->_('Property/NewTab'); ?></a>
		</li>
		<li>
			<a href="#" data-href="<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'user', 'property','module', 'create'), array('nodeId' => $this->property->getNodeID())); ?>" class="js-dialog btn-s"><?= $this->_('Property/NewModule'); ?></a>
		</li>
	</ul>
</div>

<div class="ctn">

	<?= $this->showFlash(); ?>

	<?= $this->form()->start('edit-page', 'post')->addClass('form'); ?>

		<?= $this->tab->start('settings', TRUE); ?>
			<div class="input">
				<?= $this->form()->label($this->_('Property/Title'), 'title'); ?>
				<?= $this->form()->input('title', 'text', $this->property->getTitle(), TRUE)->addAttribute('id', 'title');?>
			</div>
			<div class="input">
				<?= $this->form()->label($this->_('Property/Alias'), 'alias'); ?>
				<?= $this->form()->input('alias', 'text', $this->property->getAlias(), TRUE)->addAttribute('id', 'alias');?>
			</div>
			<div class="input">
				<?= $this->form()->label($this->_('Property/Icon'), 'icon'); ?>
				<div class="js-icon" style="margin-top:8px;">
					<a href="#"><?= $this->_('Property/ChooseIcon'); ?></a>
					<span id="icon">
						<? if($this->property->getIcon()) : ?>
						<img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/<?= $this->property->getIcon(); ?>" alt="" style="vertical-align:middle;margin-left:8px;margin-top: -4px;" />
						<? endif; ?>
					</span>
					<?= $this->form()->input('icon', 'hidden', $this->property->getIcon()); ?>
				</div>
			</div>
		<?= $this->tab->end(); ?>

		<?= $this->tab->start('modules'); ?>
			<? if(count($this->property->getModules()) == 0) : ?>
				<?= $this->_('Property/Module/NoModules');?>
			<? else: ?>
				<table class="dataTable js-modules">
					<thead>
						<tr>
							<th style="width:160px;"><?= $this->_('Property/Module/Name'); ?></th>
							<th>
								<?= $this->_('Property/Module/Tab'); ?>
							</th>
							<th style="width:160px;">
								<?= $this->_('Property/Module/ID')?>
							</th>
							<th style="width:200px;"><?= $this->_('Property/Module/Type'); ?></th>
							<th style="width:100px;" class="center"><?= $this->_('Property/Module/Actions');?></th>
						</tr>
					</thead>
					<tbody>
						<? foreach($this->property->getModules() as $key=>$module) : ?>
						<tr class="<?= ($key%2==0) ? 'odd' : 'even' ?> gradeX<?= ($module->getNodeID() != $this->property->getNodeID()) ? ' disabled' : ''; ?>" data-id="<?= $module->getPropertyModuleID(); ?>">
							<td>
								<?= $module->getName(); ?>
							</td>
							<td>
								<?= $module->getTab()->getName(); ?>
							</td>
							<td>
								<?= $module->getControlID(); ?>
							</td>
							<td>
								<?= $module->getModule(); ?>
							</td>
							<td class="center">
								<div class="actions">
									<? if($module->getNodeID() == $this->property->getNodeID()) : ?>
										<a href="#" data-href="<?= $this->getRoute(NULL, 'dialog', array('view', 'user', 'property', 'module', 'edit'), array('propertyModuleId' => $module->getPropertyModuleID()));?>" class="js-dialog"><?= $this->_('Property/Module/Edit'); ?></a>
										<a href="<?= $this->getRoute(NULL, 'userproperty', array('deletemodule', $module->getPropertyModuleID()));?>" onclick="return confirm('<?= $this->_('Property/Module/ConfirmDelete');?>');"><?= $this->_('Property/Module/Delete'); ?></a>
									<? endif;?>
								</div>
							</td>
						</tr>
						<? endforeach; ?>
					</tbody>
				</table>
			<? endif; ?>
		<?= $this->tab->end();?>

		<?= $this->tab->start('structure'); ?>
			<div style="margin-bottom:10px;">
				<?= $this->_('Property/AllowedProperties')?>
			</div>
			<ol id="properties" class="treeview" style="float:none;">
				<li id="node_0" class="js-root">
					<div>
						<div class="hitarea expanded"></div>
						<a href="javascript:;" class="js-folder" data-id="0" style="font-weight:bold;"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/folder.png" alt="" /> <?= $this->plugin->application->Name; ?></a>
						<?= $this->loopProperties(0)?>
					</div>
				</li>
			</ol>
		<?= $this->tab->end(); ?>

		<?= $this->tab->start('tabs'); ?>
			<? if(count($this->property->getTabs()) == 0) : ?>
				<?= $this->_('Property/Tab/NoTabs');?>
			<? else: ?>
				<table class="dataTable js-tabs">
					<thead>
						<tr>
							<th><?= $this->_('Property/Tab/Name'); ?></th>
							<th style="width:100px;"><?= $this->_('Property/Tab/Actions');?></th>
						</tr>
					</thead>
					<tbody>
						<? foreach($this->property->getTabs() as $key=>$tab) : ?>
						<tr class="<?= ($key%2==0) ? 'odd' : 'even' ?> gradeX<?= ($tab->getNodeID() != $this->property->getNodeID()) ? ' disabled' : ''; ?>" data-id="<?= $tab->getPropertyTabID(); ?>">
							<td>
								<?= $tab->getName(); ?>
							</td>
							<td class="center">
								<div class="actions">
									<? if($tab->getNodeID() == $this->property->getNodeID()) : ?>
										<a href="#" data-href="<?= $this->getRoute(NULL, 'dialog', array('view', 'user', 'property', 'tab', 'edit'), array('propertyTabId' => $tab->getPropertyTabID()));?>" class="js-dialog"><?= $this->_('Property/Tab/Edit'); ?></a>
										<a href="<?= $this->getRoute(NULL, 'userproperty', array('deletetab', $tab->getPropertyTabID()));?>" onclick="return confirm('<?= $this->_('Property/Tab/ConfirmDelete');?>');"><?= $this->_('Property/Tab/Delete'); ?></a>
									<? endif;?>
								</div>
							</td>
						</tr>
						<? endforeach; ?>
					</tbody>
				</table>
			<? endif; ?>
		<?= $this->tab->end();?>

		<div class="clear"></div>
		<div style="padding:8px 0px;margin-top:25px;border-top: 1px solid #dfdfdf;text-align:right;">
			<?= $this->form()->submit('save', $this->_('Property/SaveChanges'))?>
		</div>

	<?= $this->form()->end(); ?>

	<script type="text/javascript">
		$(document).ready(function() {
			var t = $('#properties').tree({
				setCookie: false,
				onSelectPage: function(e, id, text, ui) {
					if(ui.data('type') == 'property') {
						if(ui.hasClass('active')) {
							ui.removeClass('active');
							ui.parents('li:first').find('input[type="checkbox"]').attr('checked', false);
						} else {
							ui.addClass('active');
							ui.parents('li:first').find('input[type="checkbox"]').attr('checked', true);
						}
					}
				}
			});
			$('.js-icon a').bind('click', function(e) {
				e.preventDefault();
				dialog.show({
                	url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'icons')); ?>',
                    type: dialog.dialogTypes.ajax,
                    onClose: function(r) {
						var active = $('.dialog .icons ul li.active a');
						if(active.length > 0) {
							var icon = active.data('value');
							$('.js-icon span').html('<img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/'+icon+'" alt="" style="vertical-align:middle;margin-left:8px;margin-top: -4px;" />');
							$('.js-icon input').val(icon);
						}
                    }
				});
			});

			var fixHelper = function(e, tr) {
			    var $originals = tr.children();
			    var $helper = tr.clone();
			    $helper.children().each(function(index)
			    {
			      $(this).width($originals.eq(index).width())
			    });
			    return $helper;
			};

			$('.js-modules tbody').sortable({
				helper: fixHelper,
				cancel: '.disabled',
				stop: function(ui, el) {
					var item = $(el.item);
					var obj = { ids: [] };
					item.parents('tbody:first').find('tr').removeClass('even').removeClass('odd').each(function(i) {
						if((i+1) % 2 == 0) {
							$(this).addClass('even');
						} else {
							$(this).addClass('odd');
						}
						obj.ids.push($(this).data('id'));
					});

					$.post('<?= $this->getRoute(NULL, 'userproperty',array('ordermodules'), array('nodeId' => $this->property->getNodeID())); ?>', $.param(obj), function(r) {
						if(r.result == null || !r.result) {
							alert('<?= $this->_('Property/ErrorUpdating'); ?>');
						}
	            	});
				}
			}).disableSelection();

			$('.js-tabs tbody').sortable({
				helper: fixHelper,
				cancel: '.disabled',
				stop: function(ui, el) {
					var item = $(el.item);
					var obj = { ids: [] };
					item.parents('tbody:first').find('tr').removeClass('even').removeClass('odd').each(function(i) {
						if((i+1) % 2 == 0) {
							$(this).addClass('even');
						} else {
							$(this).addClass('odd');
						}
						obj.ids.push($(this).data('id'));
					});

					$.post('<?= $this->getRoute(NULL, 'userproperty',array('ordertabs'), array('nodeId' => $this->property->getNodeID())); ?>', $.param(obj), function(r) {
						if(r.result == null || !r.result) {
							alert('<?= $this->_('Property/ErrorUpdating'); ?>');
						}
	            	});
				}
			}).disableSelection();

			$('a.pecee-tablist').bind('click', function(e) {
				$.cookie('ActiveTabId', $(this).data('id'));
			});

			var activeTabId = $.cookie('ActiveTabId');
			if(activeTabId) {
				$('div.pecee-tablist').hide();
				var el = $('div.pecee-tablist[data-id="'+activeTabId+'"]');
				if(el.length > 0) {
					el.show();
					$('a.pecee-tablist').removeClass('active');
					$('a.pecee-tablist[data-id="'+activeTabId+'"]').addClass('active');
				}
			};
		});
	</script>
</div>