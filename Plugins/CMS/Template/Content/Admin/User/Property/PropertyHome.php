<? /* @var $this \CMS\Widget\Admin\User\Property\PropertyHome */ ?>
<div class="headline">
	<ul class="hor-nav">
		<li><a href="<?= $this->getRoute(NULL, 'user'); ?>"><?= $this->_('User/Users')?></a></li>
		<? if(\Pecee\Model\User\ModelUser::Current()->getAdminLevel() == \PC\Helper::USER_TYPE_ADMIN) :?>
		<li><a href="<?= $this->getRoute(NULL, 'userproperty'); ?>" class="active"><?= $this->_('User/Structure')?></a></li>
		<? endif; ?>
	</ul>
	<ul>
		<li>
			<a href="#" data-href="<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'user','create')); ?>" class="js-dialog btn-s"><?= $this->_('User/New'); ?></a>
		</li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>
	<div id="loader"></div>
	<ol id="browser" class="treeview" style="float:none;">
		<li id="node_0" class="js-root">
			<div>
				<div class="hitarea <?= (in_array('0', $this->activeNodes) ? 'expanded' : 'collapsed')?>"></div>
				<a href="javascript:;" class="js-folder" data-id="0" style="font-weight:bold;"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/folder.png" alt="" /> <?= $this->plugin->application->Name; ?></a>
				<? if($this->pages && $this->pages->hasRows()): ?>
				<ol class="js-page"<?= (in_array('0', $this->activeNodes) ? '' : ' style="display:none;"')?>>
					<? foreach($this->pages->getRows() as $p) : ?>
					<li id="node_<?= $p->getNodeID(); ?>" data-id="<?= $p->getNodeID(); ?>">
						<div>
							<div class="hitarea <?= (in_array($p->getNodeID(), $this->activeNodes) ? 'expanded' : 'collapsed'); ?>"></div><a href="<?= $this->getRoute(NULL, 'userproperty', array('edit', $p->getNodeID())); ?>" data-id="<?= $p->getNodeID(); ?>"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/<?= $p->getIcon(); ?>" alt="" /><?= $p->getTitle();?></a>
						</div>
						<?= $this->loopPages($p)?>
					</li>
					<? endforeach; ?>
				</ol>
			<? endif;?>
			</div>
		</li>
	</ol>

	<div class="clear"></div>

	<script type="text/javascript">

		$(document).ready(function() {

			$('.ctn').disableSelection();

			$('.treeview').tree({
				cookieName: 'UserPropertyNodes',
				sourceUrl: null,
				editUrl: null,
				imageUrl: '/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/'
			});

			$('ol#browser').nestedSortable({
	            handle: '> div',
	            items: 'ol > li',
	            toleranceElement: '> div',
	            protectRoot: true,
	            tolerance: 'pointer',
	            tabSize: 25,
	            tabSize : 20,
	            expandOnHover: 700,
	            revert: 100,
	            stop: function(e, ui) {
	            	$('#loader').show();
	            	var list = $('ol#browser').nestedSortable('serialize');
	                var el = $(ui.item);
	                var parent = el.parents('li:first').find('a');
	                $.post('<?= $this->getRoute(NULL, 'user',array('setparent')); ?>?parentNodeId='+parent.data('id'), list, function(r) {
	            		$('#loader').hide();
						if(r.result == null || !r.result) {
							alert('<?= $this->_('Property/ErrorUpdating'); ?>');
						}
	            	});
	            }
	        });

			$('.treeview li.js-root a').contextmenu([
				{
			    	name: '<?= $this->_('Property/NewProperty');?>',
			    	image: '/plugin/CMS/admin/gfx/ico/cog_add.png',
			        callback: function(activeElement) {
				        dialog.show({
					        url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'user', 'property','create')); ?>',
					        type: dialog.dialogTypes.ajax,
					        onLoad: function() {
					        	$('select').ddslick({ imagePosition: "left" });
							}
				        });
					}
				}
			]);

			$('ol.js-page > li a').contextmenu([
				{
					name: '<?= $this->_('Property/NewProperty');?>',
					image: '/plugin/CMS/admin/gfx/ico/cog_add.png',
				    callback: function(activeElement) {
				    	var parent = $(activeElement);
		                dialog.show({
		                	url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'user', 'property', 'create')); ?>?nodeId=' + parent.data('id'),
		                    type: dialog.dialogTypes.ajax,
		                    onLoad: function() {
		                    	$('select').ddslick({ imagePosition: "left" });
		                    }
						});
					}
				},
				{
	            	name: '<?= $this->_('Property/EditProperty');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/cog_edit.png',
	                callback: function(activeElement) {
	                	var parent = $(activeElement);
	                	top.location.href='<?= $this->getRoute(NULL, 'userproperty', array('edit'));?>'+parent.data('id');
					}
	            },
	            {
	            	name: '<?= $this->_('Property/DeleteProperty');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/cog_delete.png',
	                callback: function(activeElement) {
	                	var parent = $(activeElement);
		                admin.dialog.confirm('<?= $this->_('Property/ConfirmDelete');?>', '<?= $this->_('Property/ConfirmDeleteText');?>', function(r) {
							if(r) {
								$.post('<?= $this->getRoute(NULL, 'userproperty', array('delete'))?>', 'nodeId='+parent.data('id'), function(r) {
									if(r == null || r.result != null && !r.result) {
										alert('<?= $this->_('Property/ErrorDeleting');?>');
									} else {
										parent.parents('li:first').hide(100);
									}
								});
							}
		                });
					}
	            }
			]);
		});
	</script>
</div>