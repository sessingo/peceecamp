<? /* @var $this \CMS\Widget\Admin\User\UserEdit */ ?>
<div class="headline">
	<ul class="hor-nav">
		<? foreach(array_keys($this->modules) as $i=>$tab) : ?>
			<li><?= $this->tab->button('tab_'.$i, $tab); ?></li>
		<? endforeach; ?>
		<li><?= $this->tab->button('history', $this->_('User/History')); ?></li>
		<li><?= $this->tab->button('properties', $this->_('User/Properties')); ?></li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash();?>
	<?= $this->form()->start('edit-user', 'post', NULL, \Pecee\UI\Form\Form::FORM_ENCTYPE_FORM_DATA)
		->addAttribute('class', 'form')
		->addAttribute('ID', 'edit-user'); ?>

		<? $i=0;?>
		<? foreach(array_keys($this->modules) as $tab) : ?>
			<?= $this->tab->start('tab_'.$i, ($i==0)); ?>
				<? foreach($this->modules[$tab] as $widget) : ?>
				<?= $widget; ?>
				<? endforeach;?>
			<? $i++;?>
			<?= $this->tab->end('tab_'.$i); ?>
		<? endforeach;?>

		<?= $this->tab->start('properties', (count($this->page->getProperty()->getTabs())==0));?>
		<div class="input">
			<?= $this->form()->label($this->_('User/Name'), 'title'); ?>
			<?= $this->form()->input('title', 'text', $this->page->getTitle(), TRUE)->addAttribute('ID', 'title'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('User/Password'), 'title'); ?>
			<?= $this->form()->input('password', 'password')->addAttribute('ID', 'title'); ?>
		</div>
		<div class="input">
			<label><?= $this->_('User/Type'); ?></label>
			<span><?= $this->page->getProperty()->getAlias(); ?></span>
		</div>
		<div class="input">
			<label><?= $this->_('User/PubDate'); ?></label>
			<span><?= $this->page->getPubDate(); ?></span>
		</div>
		<div class="input">
			<label><?= $this->_('User/ChangedDate'); ?></label>
			<span><?= $this->page->getChangedDate(); ?></span>
		</div>
		<?= $this->tab->end(); ?>

		<?= $this->tab->start('history');?>
			<? if(!$this->history->hasRows()) : ?>
				<div style="padding:10px;">
					<?= $this->_('User/NoHistory')?>
				</div>
			<? else: ?>

				<table class="dataTable">
					<thead>
						<tr>
							<th class="center"><?= $this->_('User/Date'); ?></th>
							<th class="center"><?= $this->_('User/Author');?></th>
							<th style="width:100px;" class="center"><?= $this->_('User/Actions');?></th>
						</tr>
					</thead>
					<tbody>
						<? foreach($this->history->getRows() as $key=>$history) : ?>
						<tr class="<?= ($key%2==0) ? 'odd' : 'even' ?> gradeX">
							<td class="center">
								<?= $history->getPubDate();?>
							</td>
							<td class="center">
								<?= \Pecee\Model\User\ModelUser::GetByUserID($history->getUserID())->getUsername(); ?>
							</td>
							<td class="center">
								<div class="actions">
									<a href="<?= $this->getRoute(NULL, NULL, NULL, array('draftId' => $history->getNodeID(TRUE)), TRUE);?>"><?= $this->_('User/Edit')?></a> -
									<a href="<?= $this->getRoute(NULL, 'page', array('viewdraft', $history->getNodeID(TRUE)));?>" rel="new"><?= $this->_('User/View')?></a>
								</div>
							</td>
						</tr>
						<? endforeach; ?>
					</tbody>
				</table>
			<? endif;?>
		<?= $this->tab->end(); ?>

		<div class="clear"></div>
		<div style="padding:8px 0px;margin-top:15px;border-top: 1px solid #dfdfdf;text-align:right;">
			<div id="loader" style="display:inline-block;background:url(/plugin/CMS/admin/gfx/loader.gif) no-repeat;background-position:50% 50%;height:33px;width:33px;display:none;width:100px;margin-bottom:20px;"></div>
			<div id="buttons">
				<?= $this->form()->input('save', 'button', $this->_('User/Save'))->addAttribute('id','page-save'); ?>
				<?= $this->form()->input('publish', 'button', $this->_('User/SaveAndPublish'))->addAttribute('id','page-publish'); ?>
			</div>
		</div>

	<?= $this->form()->end(); ?>

	<script type="text/javascript">
		$(document).ready(function() {
			var saveTimer = null;
			var draftId = '<?= $this->getParam('draftId'); ?>';
			var showLoader = function() {
				$('#loader').show();
				$('#buttons').hide();
			};

			var hideLoader = function() {
				$('#loader').hide();
				$('#buttons').show();
			};

			var saveDraft = function(callback) {
				$.ajax({
					url: '<?= $this->getRoute(NULL, 'page', array('savedraft', $this->page->getNodeID())); ?>?draftId=' + draftId,
					dataType: 'json',
					type: 'POST',
					data: $('#edit-user').serializeArray(),
					success: function(r) {
						if(r != null && !r.result) {
							alert(r.msg);
						}
						if(callback != null) {
							draftId = r.draftId;
							callback(r);
						}
					}
				});
			};

			var originalForm = $('form').serialize();
			var editMode = true;
			$(window).bind('beforeunload', function(){
				if(originalForm != $('form').serialize() && editMode) {
					return '<?= $this->_('User/PageLeaveUnsaved');?>';
				}
			});

			var autoSave = function() {
				saveTimer = setTimeout(function() {
					clearTimeout(saveTimer);
					saveTimer = null;
					saveDraft(function() {
						autoSave();
					});
				}, 60000);
			};

			<? if(!$this->getDisableHistory()) : ?>
			autoSave();
			<? endif; ?>

			$('#page-save').bind('click', function(e) {
				e.preventDefault();
				clearTimeout(saveTimer);
				saveTimer = null;
				showLoader();
				saveDraft(function() {
					hideLoader();
					autoSave();
				});
			});

			$('#page-publish').bind('click', function(e) {
				e.preventDefault();
				draftId = '';
				editMode = false;
				showLoader();
				//saveDraft(function() {
					$('#edit-user').submit();
				//});
			});

			$('.js-editor').redactor({
				toolbarFixedBox: true,
				minHeight: 100,
				autoresize: true,
				imageUpload: '<?= $this->getRoute(NULL, 'file', array('upload'), array('type' => 'image', 'id' => session_id()));?>',
				fileUpload: '<?= $this->getRoute(NULL, 'file', array('upload'), array('id' => session_id()));?>',
				imageGetJson: '<?= $this->getRoute(NULL, 'file', array('getimages'), array('id' => session_id()));?>'
			});

			$('#activeFrom,#activeTo').datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: 'dd-mm-yy'
			});
		});
	</script>
</div>