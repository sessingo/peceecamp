<? /* @var $this \CMS\Widget\Admin\Settings\SettingsHome */ ?>
<table style="width:100%">
	<tr>
		<td style="width:210px;">
			<ol id="settings" class="treeview" style="float:none;">
				<li>
					<div>
						<div class="hitarea expanded"></div>
						<a href="javascript:;" class="js-root" style="font-weight:bold;"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/wrench.png" alt="" /> <?= $this->_('Settings/Settings')?></a>
						<ol>
							<li>
								<div>
									<a href="javascript:;" data-url="<?= $this->getRoute(NULL, NULL, array('iframe','view','settings','site'));?>"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/house.png" alt="" /> <?= $this->_('Settings/Site')?></a>
								</div>
							</li>
							<li>
								<div>
									<a href="javascript:;" data-url="<?= $this->getRoute(NULL, NULL, array('iframe','view','settings','history'));?>"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/book_open.png" alt="" /> <?= $this->_('Settings/History/History')?></a>
								</div>
							</li>
							<li>
								<div>
									<a href="javascript:;" data-url="<?= $this->getRoute(NULL, NULL, array('iframe','view','settings','files'));?>"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/application_cascade.png" alt="" /> <?= $this->_('Settings/Files')?></a>
								</div>
							</li>
							<li>
								<div>
									<a href="javascript:;" data-url="<?= $this->getRoute(NULL, NULL, array('iframe','view','settings','thumbnails'));?>"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/image.png" alt="" /> <?= $this->_('Settings/Thumbnails/Thumbnails')?></a>
								</div>
							</li>
							<li>
								<div>
									<a href="javascript:;" data-url="<?= $this->getRoute(NULL, NULL, array('iframe','view','settings','cache'));?>"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/chart_bar.png" alt="" /> <?= $this->_('Settings/Cache')?></a>
								</div>
							</li>
							<li>
								<div>
									<a href="javascript:;" data-url="<?= $this->getRoute(NULL, NULL, array('iframe','view','settings','themes'));?>"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/color_swatch.png" alt="" /> <?= $this->_('Settings/Themes/Themes')?></a>
								</div>
							</li>
							<li>
								<div>
									<a href="javascript:;" data-url="<?= $this->getRoute(NULL, NULL, array('iframe','view','settings','variables'));?>"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/text_allcaps.png" alt="" /> <?= $this->_('Settings/SiteVariables')?></a>
								</div>
							</li>
						</ol>
					</div>
				</li>
				<li>
					<div>
						<div class="hitarea collapsed"></div>
						<a href="javascript:;" style="font-weight:bold;" class="js-macro-root"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/cog.png" alt="" /> <?= $this->_('Settings/Macro/Macros')?></a>
						<? if($this->macros && $this->macros->hasRows()): ?>
							<ol>
							<? foreach($this->macros->getRows() as $m) : ?>
							<li>
								<div>
									<a href="javascript:;" data-url="<?= $this->getRoute(NULL, NULL, array('iframe','view','settings','macro'), array('nodeId' => $m->getNodeID()));?>" data-id="<?= $m->getNodeID(); ?>" class="js-macro"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/cog_go.png" alt="" /><?= $m->getTitle();?></a>
								</div>
							</li>
							<? endforeach; ?>
						</ol>
						<? endif; ?>
					</div>
				</li>
				<li>
					<div>
						<div class="hitarea collapsed"></div>
						<a href="javascript:;" data-url="<?= $this->getRoute(NULL, NULL, array('iframe','view','settings','rewrite'));?>" style="font-weight:bold;"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/link.png" alt="" /> <?= $this->_('Settings/UrlRewrite/UrlRewrite')?></a>
					</div>
				</li>
				<li>
					<div>
						<div class="hitarea collapsed"></div>
						<a href="javascript:;" data-url="<?= $this->getRoute(NULL, NULL, array('iframe','view','settings','cleanup'));?>" style="font-weight:bold;"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/wand.png" alt="" /> <?= $this->_('Settings/Cleanup/Cleanup')?></a>
					</div>
				</li>
				<?php /*<li>
					<div>
						<div class="hitarea collapsed"></div>
						<a href="javascript:;" data-url="<?= $this->getRoute(NULL, NULL, array('iframe','view','settings','backuprestore'));?>" style="font-weight:bold;"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/arrow_refresh_small.png" alt="" /> <?= $this->_('Settings/BackupRestore/BackupRestore'); ?></a>
					</div>
				</li>*/ ?>
			</ol>
		</td>
		<td>
			<script>
				$(document).ready(function() {
					$('#settings').disableSelection();
					$('#settings').tree({
						setCookie: false,
						onSelectPage: function(e) {
							var el = $(e.target);
							var url = el.data('url');

							if(url != null) {
								$('#settings a.active').removeClass('active');
								el.addClass('active');
								$('#frame').attr('src', url);
							}
						}
					});
				});

				$('.treeview li a.js-macro-root').contextmenu([
				{
					name: '<?= $this->_('Settings/Macro/New');?>',
					image: '/plugin/CMS/admin/gfx/ico/cog_add.png',
					callback: function(activeElement) {
						dialog.show({
							url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'macro','create')); ?>',
							type: dialog.dialogTypes.ajax,
							onLoad: function() {
								$('select').ddslick({ imagePosition: "left" });
							}
						});
					}
				}
				]);

				$('.treeview li a.js-macro').contextmenu([
				{
					name: '<?= $this->_('Settings/Macro/Edit');?>',
					image: '/plugin/CMS/admin/gfx/ico/cog_edit.png',
					callback: function(activeElement) {
						$('#settings a.active').removeClass('active');
						var parent = $(activeElement);
						$('#frame').attr('src', parent.data('url'));
						parent.addClass('active');
					}
				},
				{
					name: '<?= $this->_('Settings/Macro/Delete');?>',
					image: '/plugin/CMS/admin/gfx/ico/cog_delete.png',
					callback: function(activeElement) {
						$('#loader').show();
						var parent = $(activeElement);
						admin.dialog.confirm('<?= $this->_('Settings/Macro/ConfirmDelete');?>', '<?= $this->_('Settings/Macro/ConfirmDeleteText');?>', function(r) {
							$('#loader').hide();
							if(r) {
								$.post('<?= $this->getRoute(NULL, 'macro', array('delete.json'))?>', 'nodeId='+parent.data('id'), function(r) {
									if(r == null || r.result != null && !r.result) {
										alert('<?= $this->_('Settings/Macro/ErrorDeleting');?>');
									} else {
										parent.parents('li:first').hide(100);
									}
								});
							}
						});
					}
				}
				]);
			</script>
			<iframe src="about:blank" id="frame" frameborder="0" onload="admin.utils.setFrameHeight();" style="height:100%;width:100%;"></iframe>
		</td>
	</tr>
</table>

