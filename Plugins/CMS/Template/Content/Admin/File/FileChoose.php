<? /* @var $this \CMS\Widget\Admin\File\FileChoose */ ?>

<div style="margin:8px 0;">

	<? if(!$this->page || $this->page && !$this->page->hasRow()) : ?>
		<span id="foldername_<?= $this->name; ?>"></span>
		<span id="remove_link_<?= $this->name; ?>">
			<a href="javascript:;" onclick="removeLink<?= md5($this->name); ?>(this);" style="display:none;"><?= $this->_('File/Remove'); ?></a>
		</span>
	<? else: ?>
		<span id="foldername_<?= $this->name; ?>"><?= $this->page->getTitle(); ?></span>
		<span id="remove_link_<?= $this->name; ?>">
			<a href="javascript:;" onclick="removeLink<?= md5($this->name); ?>(this);"><?= $this->_('File/Remove'); ?></a> -
		</span>
	<? endif; ?>

	<a href="javascript:;" onclick="chooseFile<?= md5($this->name); ?>();" id="choosefile_<?= $this->name; ?>"><?= $this->_('File/ChooseFile'); ?></a>
	<?= $this->form()->input($this->name, 'hidden', $this->value, TRUE)->addAttribute('id', $this->name)?>
	<script type="text/javascript">
		function chooseFile<?= md5($this->name); ?>() {
			parent.dialog.show({
				url: '<?= \PC\Router::GetRoute(NULL, NULL, array('dialog','view','file','choose'));?>?nodeId=<?= $this->defaultNode; ?>',
				type: dialog.dialogTypes.ajax,
				onLoad: function() {
					var t = parent.$('#filechoose').tree({
						sourceUrl: '<?= $this->getRoute(NULL, 'file',array('tree'))?>',
						imageUrl: '/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/',
						setCookie: false
					});
					t.onSelectPage(function(e, nodeId, name) {
						if(nodeId > 0) {
							$('#foldername_<?= $this->name;?>').html(name);
							$('#<?= $this->name; ?>').val(nodeId);
							$('#remove_link_<?= $this->name; ?>').show();
							parent.dialog.closeActive();
						}
					});
					t.refreshNode(0);
				}
			});
		}

		function removeLink<?= md5($this->name); ?>(el) {
			$('#<?= $this->name; ?>').val('');
			$('#foldername_<?= $this->name; ?>').html('');
			$(el).parent().hide();
		};
	</script>
</div>