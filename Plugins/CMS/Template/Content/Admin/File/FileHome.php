<? /* @var $this \CMS\Widget\Admin\File\FileHome */ ?>
<div class="headline">
	<ul class="hor-nav">
		<li><a href="<?= $this->getRoute(NULL, 'file'); ?>" class="active"><?= $this->_('File/Files')?></a></li>
	</ul>
	<ul>
		<li>
			<?= $this->form()->start('search', 'get', $this->getRoute(NULL, 'file', array('search'))); ?>
				<?= $this->form()->input('query', 'text')->addAttribute('placeholder', $this->_('File/Search'))->addAttribute('ID','query')->addAttribute('autocomplete','off'); ?>
			<?= $this->form()->end(); ?>
		</li>
		<li>
			<a href="#" data-href="<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'file', 'folder', 'create')); ?>" class="js-dialog btn-s"><?= $this->_('File/NewFolder'); ?></a>
		</li>
		<li>
			<a href="#" data-href="<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'file', 'upload')); ?>" class="js-dialog btn-s"><?= $this->_('File/UploadFile'); ?></a>
		</li>
	</ul>
</div>

<div class="ctn">

	<?= $this->showFlash(); ?>

	<div id="loader"></div>
	<ol id="filebrowser" class="treeview" style="float:none;">
		<li id="node_0">
			<div>
				<div class="hitarea <?= (in_array('0', $this->activeNodes) ? 'expanded' : 'collapsed')?>"></div>
				<a href="javascript:;" class="js-root" data-id="0" style="font-weight:bold;"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/folder.png" alt="" /> <?= $this->plugin->application->Name; ?></a>
				<? if($this->folders && $this->folders->hasRows()): ?>
				<ol<?= (in_array('0', $this->activeNodes) ? '' : ' style="display:none;"')?>>
					<? foreach($this->folders->getRows() as $f) : ?>
					<li id="node_<?= $f->getNodeID(); ?>">
						<div>
							<div class="hitarea <?= (in_array($f->getNodeID(), $this->activeNodes) ? 'expanded' : 'collapsed'); ?>"></div><a href="javascript:;" data-id="<?= $f->getNodeID(); ?>" class="js-folder"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/folder.png" alt="" /><?= $f->getTitle();?></a>
						</div>
						<?= $this->loopFolders($f)?>
					</li>
					<? endforeach; ?>
					<? if($this->files->hasRows()) : ?>
						<? foreach($this->files->getRows() as $file) : ?>
							<li id="file_<?= $file->getNodeID();?>" class="no-nesting">
								<div>
									<a href="<?= $file->getUrl(); ?>" class="js-file js-overlay" data-id="<?= $file->getNodeID();?>"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/application.png" alt="" /><?= $file->getTitle();?></a>
								</div>
							</li>
						<? endforeach; ?>
					<? endif; ?>
				</ol>
			<? endif;?>
			</div>
		</li>
	</ol>

	<div class="clear"></div>

	<script type="text/javascript">

		$(document).ready(function() {

			$('.ctn').disableSelection();

			var t = $('#filebrowser').tree({
				cookieName: 'FileActiveNodes',
				classname: 'js-file',
				sourceUrl: '<?= $this->getRoute(NULL, 'file',array('tree'))?>',
				imageUrl: '/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/',
				onSelectPage: function(e,nodeId, txt) {
					var el = $('a[data-id="'+nodeId+'"]');
					if(el.length > 0) {
						if(el.hasClass('js-file')){
							e.preventDefault();
							window.open(el.attr('href'));
						}
					}
				}
			});

			var search = null;
			var searchTimer = null;

			$('#query').bind('keyup', function() {
				var self=$(this);
				self.parents('form:first').find('ul').remove();
				clearTimeout(searchTimer);
				searchTimer = setTimeout(function() {
					if(self.val() != '') {
						if(search != null) {
							search.abort();
							search = null;
						}
						search = $.getJSON('<?= $this->getRoute(NULL, 'file', array('search')); ?>?query='+self.val(), function(r) {
							self.parents('form:first').find('ul').remove();
							if(r.files != null && r.files.length > 0) {
								var el = $('<ul/>');
								for(var i=0;i<r.files.length;i++) {
									var file = r.files[i];
									el.append('<li><a href="/'+ file.url +'" rel="new"> <img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/'+file.icon+'" alt="" /> '+ file.title +'</a></li>');
								}
								self.parents('form:first').append(el);
							}
							search = null;
						});
					}
				}, 500);
			});

			$(document).bind('click', function() {
				$('#query').parents('form:first').find('ul').remove();
			});

			$('ol#filebrowser').nestedSortable({
	            handle: '> div',
	            items: 'ol > li',
	            toleranceElement: '> div',
	            protectRoot: true,
	            tolerance: 'pointer',
	            tabSize: 25,
	            tabSize : 20,
	            disableNesting: 'no-nesting',
	            expandOnHover: 700,
	            revert: 100,
	            stop: function(e, ui) {
	            	$('#loader').show();
	            	var list = $('ol#filebrowser').nestedSortable('serialize');
	                var el = $(ui.item);
	                var parent = el.parents('li:first').find('a');
	            	$.post('<?= $this->getRoute(NULL, 'file',array('setparent')); ?>?parentNodeId='+parent.data('id'), list, function(r) {
	            		$('#loader').hide();
						if(r.result == null || !r.result) {
							alert('<?= $this->_('File/ErrorUpdating'); ?>');
						}
	            	});
	            }
	        });

			$('.treeview a.js-root').contextmenu([
				{
					name: '<?= $this->_('File/UploadFile');?>',
					image: '/plugin/CMS/admin/gfx/ico/application_add.png',
				    callback: function(activeElement) {
				    	var parent = $(activeElement);
				        dialog.show({
				        	url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'file', 'upload')); ?>?parentNodeId=' + parent.data('id'),
				            type: dialog.dialogTypes.ajax,
				            onLoad: function() {
				            	$('select').ddslick({ imagePosition: "left" });
				            }
						});
					}
				},
				{
	            	name: '<?= $this->_('File/NewFolder');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/folder_add.png',
	                callback: function(activeElement) {
	                	var parent = $(activeElement);
	                    dialog.show({
	                    	url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'file', 'folder', 'create')); ?>?parentNodeId='+ parent.data('id'),
	                        type: dialog.dialogTypes.ajax,
	                        onLoad: function() {
	                        	$('select').ddslick({ imagePosition: "left" });
	                        }
						});
					}
				},
				{
	            	name: '<?= $this->_('File/ClearCache');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/database_refresh.png',
	                callback: function(activeElement) {
	                	$('#loader').show();
	                	$.getJSON('<?= $this->getRoute(NULL, NULL, array('file','clearcache.json')) ?>', function(r) {
	                		$('#loader').hide();
	                	});
					}
	            },
				{
	            	name: '<?= $this->_('File/Refresh');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/arrow_refresh.png',
	                callback: function(activeElement) {
	                	t.refreshNode($(activeElement).data('id'));
					}
	            }
			]);

			$('.treeview a.js-folder').contextmenu([
			{
				name: '<?= $this->_('File/UploadFile');?>',
				image: '/plugin/CMS/admin/gfx/ico/application_add.png',
			    callback: function(activeElement) {
			    	var parent = $(activeElement);
			        dialog.show({
			        	url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'file', 'upload')); ?>?parentNodeId=' + parent.data('id'),
			            type: dialog.dialogTypes.ajax,
			            onLoad: function() {
			            	$('select').ddslick({ imagePosition: "left" });
			            }
					});
				}
			},
			{
				name: '<?= $this->_('File/NewFolder');?>',
				image: '/plugin/CMS/admin/gfx/ico/folder_add.png',
				callback: function(activeElement) {
					var parent = $(activeElement);
					dialog.show({
						url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'file', 'folder', 'create')); ?>?parentNodeId='+ parent.data('id'),
						type: dialog.dialogTypes.ajax,
						onLoad: function() {
							$('select').ddslick({ imagePosition: "left" });
						}
					});
				}
			},
			{
				name: '<?= $this->_('File/EditFolder');?>',
				image: '/plugin/CMS/admin/gfx/ico/folder_edit.png',
				callback: function(activeElement) {
					var parent = $(activeElement);
					dialog.show({
						url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'file', 'folder', 'edit')); ?>?nodeId=' + parent.data('id'),
						type: dialog.dialogTypes.ajax,
						onLoad: function() {
							$('select').ddslick({ imagePosition: "left" });
						}
					});
				}
			},
			{
				name: '<?= $this->_('File/DeleteFolder');?>',
				image: '/plugin/CMS/admin/gfx/ico/folder_delete.png',
				callback: function(activeElement) {
					var parent = $(activeElement);
	                admin.dialog.confirm('<?= $this->_('File/ConfirmDeleteFolder');?>', '<?= $this->_('File/ConfirmDeleteFolderText');?>', function(r) {
						if(r) {
							$.post('<?= $this->getRoute(NULL, 'file', array('deleteFolder'))?>', 'nodeId='+parent.data('id'), function(r) {
								if(r == null || r.result != null && !r.result) {
									alert('<?= $this->_('File/ErrorDeleting');?>');
								} else {
									parent.parents('li:first').hide(100);
								}
							});
						}
	                });
				}
			},
			{
            	name: '<?= $this->_('File/Refresh');?>',
				image: '/plugin/CMS/admin/gfx/ico/arrow_refresh.png',
                callback: function(activeElement) {
                	t.refreshNode($(activeElement).data('id'));
				}
            }
		]);

			$('.treeview a.js-file').contextmenu([
				{
					name: '<?= $this->_('File/Edit');?>',
					image: '/plugin/CMS/admin/gfx/ico/application_edit.png',
					callback: function(activeElement) {
						var parent = $(activeElement);
						dialog.show({
							url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'file', 'edit')); ?>?nodeId=' + parent.data('id'),
							type: dialog.dialogTypes.ajax,
							onLoad: function() {
								$('select').ddslick({ imagePosition: "left" });
							}
						});
					}
				},
	            {
	            	name: '<?= $this->_('File/View');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/application_go.png',
	                callback: function(activeElement) {
	                	var parent = $(activeElement);
	                	window.open(parent.attr('href'));
					}
	            },
	            {
	            	name: '<?= $this->_('File/DeleteFile');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/application_delete.png',
	                callback: function(activeElement) {
	                	var parent = $(activeElement);
		                admin.dialog.confirm('<?= $this->_('File/ConfirmDeleteFile');?>', '<?= $this->_('File/ConfirmDeleteFileText');?>', function(r) {
							if(r) {
								$.post('<?= $this->getRoute(NULL, 'file', array('deleteFile'))?>', 'nodeId='+parent.data('id'), function(r) {
									if(r == null || r.result != null && !r.result) {
										alert('<?= $this->_('File/ErrorDeleting');?>');
									} else {
										parent.parents('li:first').hide(100);
									}
								});
							}
		                });
					}
	            }
			]);
		});
	</script>
</div>