<? /* @var $this \CMS\Widget\Admin\Iframe\Settings\Cleanup */ ?>

<div class="headline">
	<ul class="hor-nav">
		<li><a href="javascript:;" class="active"><?= $this->_('Settings/Cleanup/Cleanup'); ?></a></li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>

	<div id="analyse-ctn">
		<a href="#" class="btn-s js-cleanup-analyse"><?= $this->_('Settings/Cleanup/AnalyseNodes'); ?></a>
		<div class="js-loader" style="display:none;">
			<img src="<?= $this->getRessource('admin/gfx/loader.gif')?>" alt ="" style="vertical-align:middle;"/>
			<?= $this->_('Settings/Cleanup/AnalysingPleaseWait')?>
		</div>
	</div>

	<div id="result-ctn" style="display:none;">

		<p>
			<?= $this->_('Settings/Cleanup/AnalyseComplete')?>
		</p>

		<table class="dataTable" style="margin-bottom:15px;">
			<tbody>
				<tr>
					<td class="center">
						<?= $this->_('Settings/Cleanup/NodesDeletedParent')?>
					</td>
					<td class="center">
						<span id="deletedParent"><?= $this->_('Settings/Cleanup/NoNodesFound')?></span>
					</td>
				</tr>
				<tr>
					<td class="center">
						<?= $this->_('Settings/Cleanup/NodesInvalidProperty')?>
					</td>
					<td class="center">
						<span id="deletedProperty"><?= $this->_('Settings/Cleanup/NoNodesFound')?></span>
					</td>
				</tr>
				<tr>
					<td class="center">
						<?= $this->_('Settings/Cleanup/NodeDraftNoConnection')?>
					</td>
					<td class="center">
						<span id="deletedDraft"><?= $this->_('Settings/Cleanup/NoNodesFound')?></span>
					</td>
				</tr>
				<tr>
					<td class="center">
						<?= $this->_('Settings/Cleanup/NodeDataNoConnection')?>
					</td>
					<td class="center">
						<span id="lostNodeData"><?= $this->_('Settings/Cleanup/NoNodesFound')?></span>
					</td>
				</tr>
			</tbody>
		</table>

		<a href="#" class="btn-s js-cleanup-start" style="display:none;"><?= $this->_('Settings/Cleanup/StartCleanup')?></a>

		<div class="js-loader" style="display:none;">
			<img src="<?= $this->getRessource('admin/gfx/loader.gif')?>" alt ="" style="vertical-align:middle;"/>
			<?= $this->_('Settings/Cleanup/CleaningPleaseWait')?>
		</div>
	</div>

	<script>
		$(document).ready(function() {
			$('.js-cleanup-start').bind('click', function(e) {
				e.preventDefault();
				var self = $(this);
				self.hide();
				$('.js-loader').show();

				$.getJSON('<?= $this->getRoute(NULL, NULL, array('cleanup', 'cleanup.json')); ?>', function(r) {
					self.show();
					$('.js-loader').hide();
					alert('<?= $this->_('Settings/Cleanup/CleanupComplete');?>');
					top.location.reload(true);
				});
			});

			$('.js-cleanup-analyse').bind('click', function(e) {
				e.preventDefault();
				var self = $(this);
				self.hide();
				$('.js-loader').show();
				$.getJSON('<?= $this->getRoute(NULL, NULL, array('cleanup', 'analyse.json')); ?>', function(r) {
					self.show();
					$('.js-loader').hide();
					if(r != null) {
						$('#analyse-ctn').hide();
						$('#result-ctn').show();
						$('#total').text(r.total);

						if(r.total > 0) {
							$('.js-cleanup-start').show();
						}

						if(r.deletedParent.length > 0) {
							$('#deletedParent').text(r.deletedParent.join(', '));
						}

						if(r.deletedProperty.length > 0) {
							$('#deletedProperty').text(r.deletedProperty.join(', '));
						}

						$('#deletedDraft').text(r.deletedDraft);
						$('#lostNodeData').text(r.lostNodeData);
						parent.admin.utils.setFrameHeight();
					}
				});
			});
		});
	</script>
</div>