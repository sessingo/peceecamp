<? /* @var $this \CMS\Widget\Admin\Iframe\Settings\History */ ?>

<div class="headline">
	<ul class="hor-nav">
		<li><a href="javascript:;" class="active"><?= $this->_('Settings/History/History'); ?></a></li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>
	<?= $this->form()->start('edit-settings')->addAttribute('class', 'form'); ?>
		<div class="input">
			<?= $this->form()->label($this->_('Settings/History/HistoryDisable'), 'disableHistory'); ?>
			<?= $this->form()->bool('disableHistory', $this->settings->getDisableHistory(), TRUE)->addAttribute('id', 'disableCache');?>
			<?= $this->validationFor('disableHistory'); ?>
		</div>

		<div class="input" style="margin-left:180px;">
			<a href="#" class="btn-s js-clear-history" onclick="return confirm('<?= $this->_('Settings/History/ConfirmClearHistory');?>');"><?= $this->_('Settings/History/ClearHistory')?></a>
			<span id="loader" style="display:none;"><img src="/gfx/admin/spinner.gif" alt="" /></span>
		</div>

		<?= $this->form()->submit('submit', $this->_('Settings/Update'));?>
	<?= $this->form()->end(); ?>
</div>
<script>
	$(document).ready(function() {
		$('a.js-clear-history').bind('click', function(e) {
			e.preventDefault();
			$(this).hide();
			var self = $(this);
			$('#loader').show();
			$.getJSON('<?= $this->getRoute(NULL, NULL, array('page', 'cleardrafts'));?>', function(r) {
				$('#loader').hide();
				self.show();
			});
		});
	});
</script>