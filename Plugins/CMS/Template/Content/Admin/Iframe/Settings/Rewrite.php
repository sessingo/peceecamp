<? /* @var $this \CMS\Widget\Admin\Iframe\Settings\Rewrite */ ?>

<div class="headline">
	<ul class="hor-nav">
		<li><a href="javascript:;" class="active"><?= $this->_('Settings/UrlRewrite/UrlRewrite'); ?></a></li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>

	<?= $this->form()->start('rewrite'); ?>
	<table>
		<tr>
			<td style="width:100%;vertical-align:top;padding:0;">
				<? if($this->rewrite && $this->rewrite->hasRow()) : ?>
				<table style="width:100%;min-width:400px;">
					<tr>
						<td>
							<?= $this->form()->input('originalUrl', 'text', $this->rewrite->getOriginalUrl())
								->addAttribute('placeholder', $this->_('Settings/UrlRewrite/OriginalUrl'))
								->addAttribute('style','width:100%;')
								->addAttribute('autocomplete','off'); ?>
						</td>
						<td>
							<?= $this->form()->input('UrlRewrite', 'text', $this->rewrite->getRewriteUrl())
								->addAttribute('placeholder', $this->_('Settings/UrlRewrite/UrlRewrite'))
								->addAttribute('style','width:100%;')
								->addAttribute('autocomplete','off'); ?>

							<?= $this->widget(new \CMS\Widget\Admin\Page\PageChoose('nodeId', $this->rewrite->getNodeID()))?>
						</td>
						<td style="width:50px;vertical-align:top;">
							<?= $this->form()->input('host', 'text', $this->rewrite->getHost(), TRUE)->addAttribute('placeholder', $this->_('Settings/UrlRewrite/Host'))?>
						</td>
						<td style="width:150px;vertical-align:top;">
							<label>
								<?= $this->form()->bool('regex', $this->rewrite->getRegex(), TRUE, TRUE);?>
								<?= $this->_('Settings/UrlRewrite/RegularExpression')?>
							</label>
						</td>
					</tr>
				</table>
				<? else : ?>
				<table style="width:100%;min-width:400px;">
					<tr>
						<td>
							<?= $this->form()->input('originalUrl', 'text')
								->addAttribute('placeholder', $this->_('Settings/UrlRewrite/OriginalUrl'))
								->addAttribute('style','width:100%;')
								->addAttribute('autocomplete','off'); ?>
						</td>
						<td>
							<?= $this->form()->input('UrlRewrite', 'text')
								->addAttribute('placeholder', $this->_('Settings/UrlRewrite/UrlRewrite'))
								->addAttribute('style','width:100%;')
								->addAttribute('autocomplete','off'); ?>
							<?= $this->widget(new \CMS\Widget\Admin\Page\PageChoose('nodeId'))?>
						</td>
						<td style="width:50px;vertical-align:top;">
							<?= $this->form()->input('host', 'text', NULL, TRUE)->addAttribute('placeholder', $this->_('Settings/UrlRewrite/Host'))?>
						</td>
						<td style="width:150px;vertical-align:top;">
							<label>
								<?= $this->form()->bool('regex', FALSE, TRUE, TRUE);?>
								<?= $this->_('Settings/UrlRewrite/RegularExpression')?>
							</label>
						</td>
					</tr>
				</table>
				<? endif; ?>
			</td>
			<td style="text-align:right;vertical-align:top;">
				<?= $this->form()->submit('submit', $this->_('Settings/UrlRewrite/Save'))->addClass('btn-s'); ?>
			</td>
		</tr>
	</table>

	<div style="border-top:1px dotted #CCC;padding-top:20px;margin-top:10px;">

		<? if(!$this->rewrites->hasRows()) : ?>
			<div style="text-align:center;padding:10px;">
				<?= $this->_('Settings/UrlRewrite/NoRulesCreated')?>
			</div>
		<? else: ?>
			<table class="dataTable">
				<thead>
					<tr>
						<th>
							<?= $this->_('Settings/UrlRewrite/OriginalUrl')?>
						</th>
						<th>
							<?= $this->_('Settings/UrlRewrite/UrlRewrite')?>
						</th>
						<th class="center">
							<?= $this->_('Settings/UrlRewrite/Host')?>
						</th>
						<th class="center" style="width:100px;">
							<?= $this->_('Settings/UrlRewrite/Actions')?>
						</th>
					</tr>
				</thead>
				<tbody>
					<? foreach($this->rewrites->getRows() as $rewrite) : ?>
					<tr>
						<td>
							<?= $rewrite->getOriginalUrl(); ?>
						</td>
						<td>
							<? if($rewrite->getNodeID()) : ?>
								<a href="<?= $rewrite->getNode()->getSlug(); ?>" rel="new"><?= $rewrite->getNode()->getTitle(); ?></a>
							<? else: ?>
								<?= $rewrite->getRewriteUrl(); ?>
							<? endif; ?>
						</td>
						<td class="center">
							<?= $rewrite->getHost(); ?>
						</td>
						<td class="center">
							<a href="<?= $this->getRoute(NULL, NULL, NULL, array('rewriteId' =>$rewrite->getRewriteID()), TRUE)?>"><?= $this->_('Settings/UrlRewrite/Edit')?></a> -
							<a href="<?= $this->getRoute(NULL, NULL, array('url','delete',$rewrite->getRewriteID()))?>" onclick="return confirm('<?= $this->_('Settings/UrlRewrite/ConfirmDelete'); ?>');"><?= $this->_('Settings/UrlRewrite/Delete')?></a>
						</td>
					</tr>
					<? endforeach; ?>
				</tbody>
			</table>
		<? endif; ?>
	</div>

	<?= $this->form()->end(); ?>
</div>