<? /* @var $this \CMS\Widget\Admin\Iframe\Settings\Themes */ ?>

<div class="headline">
	<ul class="hor-nav">
		<li><a href="javascript:;" class="active"><?= $this->_('Settings/Themes/Themes'); ?></a></li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>
	<?= $this->form()->start('edit-settings')->addAttribute('class', 'form'); ?>
		<table>
			<tr>
				<td style="width:90%;">
					<?= $this->form()->input('themeHost[]', 'text')->addAttribute('placeholder', $this->_('Settings/Themes/Host'))->addAttribute('style','width:100%;'); ?>
				</td>
				<td>
					<?= $this->form()->selectStart('themeTheme[]', $this->themes);?>
				</td>
				<td>
					<?= $this->form()->submit('submit', $this->_('Settings/Themes/Add'));?>
				</td>
			</tr>
		</table>
		<div style="border-top:1px solid #CCC;padding-top:20px;margin-top:10px;margin-bottom:20px;">
			<table class="dataTable">
				<thead>
					<tr>
						<th><?= $this->_('Settings/Themes/Host')?></th>
						<th><?= $this->_('Settings/Themes/Theme')?></th>
						<th><?= $this->_('Settings/Themes/Actions')?></th>
					</tr>
				</thead>
				<tbody>
					<? if(count($this->settings->getThemes()) == 0) : ?>
						<tr>
							<td colspan="3" style="text-align:center;padding:10px;">
								<?= $this->_('Settings/Themes/NoThemes')?>
							</td>
						</tr>
					<? else: ?>
						<? foreach($this->settings->getThemes() as $host=>$theme) : ?>
						<tr>
							<td>
								<?= $host; ?>
								<?= $this->form()->input('themeHost[]', 'hidden', $host); ?>
							</td>
							<td>
								<?= $theme; ?>
								<?= $this->form()->input('themeTheme[]', 'hidden', $theme); ?>
							</td>
							<td>
								<a href="javascript:;" onclick="$(this).parents('tr:first').remove();">Slet</a>
							</td>
						</tr>
						<? endforeach; ?>
					<? endif; ?>
				</tbody>
			</table>
		</div>

		<?= $this->form()->submit('submit', $this->_('Settings/Update'));?>
	<?= $this->form()->end(); ?>
</div>