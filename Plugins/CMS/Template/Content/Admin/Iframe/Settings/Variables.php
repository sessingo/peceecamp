<? /* @var $this \CMS\Widget\Admin\Iframe\Settings\Variables */ ?>

<div class="headline">
	<ul class="hor-nav">
		<li><a href="javascript:;" class="active"><?= $this->_('Settings/SiteVariables'); ?></a></li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>
	<?= $this->form()->start('edit-settings')->addAttribute('class', 'form'); ?>
		<table class="js-site-variables" style="margin-bottom:20px;">
			<thead>
				<tr>
					<th>
						<?= $this->_('Settings/Name')?>
					</th>
					<th>
						<?= $this->_('Settings/Data')?>
					</th>
					<th>
						<?= $this->_('Settings/Actions')?>
					</th>
				</tr>
			</thead>
			<tbody>
				<? foreach($this->settings->getSiteData() as $key=>$value) : ?>
				<tr>
					<td>
						<?= $this->form()->input('variableName[]', 'text', $key)->addAttribute('placeholder', $this->_('Settings/Name'))->addAttribute('style','width:250px;');?>
					</td>
					<td>
						<?= $this->form()->textarea('variableData[]', 10, 10, $value)->addAttribute('placeholder', $this->_('Settings/Data'))->addAttribute('style','height:30px;width:300px;')->addAttribute('class','js-grow');?>
					</td>
					<td style="vertical-align:middle;">
						<a href="#" class="js-variable-remove"><?= $this->_('Settings/Remove')?></a>
					</td>
				</tr>
				<? endforeach; ?>
				<tr>
					<td>
						<?= $this->form()->input('variableName[]', 'text')->addAttribute('placeholder', $this->_('Settings/Name'))->addAttribute('style','width:250px;');?>
					</td>
					<td>
						<?= $this->form()->textarea('variableData[]', 10, 10)->addAttribute('placeholder', $this->_('Settings/Data'))->addAttribute('style','height:30px;width:300px;')->addAttribute('class','js-grow');?>
					</td>
					<td style="vertical-align:middle;">
						<a href="#" class="js-variable-remove"><?= $this->_('Settings/Remove')?></a>
					</td>
				</tr>
			</tbody>
		</table>
		<?= $this->form()->submit('submit', $this->_('Settings/Update'));?>
	<?= $this->form()->end(); ?>
</div>

<script type="text/javascript">
$(document).ready(function() {

	$('.js-site-variables tr:last input:first').live('keydown', function() {
		if($('.js-site-variables tr:last input:first').val() == '') {
			var n = $(this).parents('tr:first').clone();
			n.find('input').val('');
			$('.js-site-variables tbody').append(n);
			parent.admin.utils.setFrameHeight();
		}
	});

	$('.js-site-variables a.js-variable-remove').live('click', function(e) {
		e.preventDefault();
		if($(this).parents('tbody:first').find('tr').length > 1) {
			$(this).parents('tr:first').remove();
			parent.admin.utils.setFrameHeight();
		}
	});
});
</script>