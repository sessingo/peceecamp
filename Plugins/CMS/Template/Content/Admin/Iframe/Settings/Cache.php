<? /* @var $this \CMS\Widget\Admin\Iframe\Settings\Cache */ ?>

<div class="headline">
	<ul class="hor-nav">
		<li><a href="javascript:;" class="active"><?= $this->_('Settings/Cache'); ?></a></li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>
	<?= $this->form()->start('edit-settings')->addAttribute('class', 'form'); ?>
		<div class="input">
			<?= $this->form()->label($this->_('Settings/CacheDisabled'), 'disableCache'); ?>
			<?= $this->form()->bool('disableCache', $this->settings->getDisableCache(), TRUE)->addAttribute('id', 'disableCache');?>
			<?= $this->validationFor('disableCache'); ?>
		</div>

		<? if($this->getCacheType() == \PC\Cache::TYPE_FILE) : ?>
		<div class="input" style="margin-left:180px;">
			<a href="javascript:;" class="btn-s js-clean-cache"><?= $this->_('Settings/CacheCleanup')?></a>
			<span id="loader" style="display:none;"><img src="/gfx/admin/spinner.gif" alt="" /></span>
		</div>
		<? endif;?>

		<div class="btn">
			<?= $this->form()->submit('submit', $this->_('Settings/Update'));?>
		</div>
	<?= $this->form()->end(); ?>
</div>

<script>
	$(document).ready(function() {
		$('a.js-clean-cache').bind('click', function(e) {
			e.preventDefault();
			$(this).hide();
			var self = $(this);
			$('#loader').show();
			$.getJSON('<?= $this->getRoute(NULL, NULL, array('settings', 'cleanupCache.json'));?>', function(r) {
				$('#loader').hide();
				self.show();
			});
		});
	});
</script>