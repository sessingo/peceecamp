<? /* @var $this \CMS\Widget\Admin\Iframe\Settings\Thumbnails */ ?>

<div class="headline">
	<ul class="hor-nav">
		<li><a href="javascript:;" class="active"><?= $this->_('Settings/Thumbnails/Thumbnails'); ?></a></li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>
	<?= $this->form()->start('edit-settings')->addAttribute('class', 'form'); ?>
		<table class="js-site-variables" style="margin-bottom:20px;">
			<thead>
				<tr>
					<th>
						<?= $this->_('Settings/Thumbnails/Name')?>
					</th>
					<th>
						<?= $this->_('Settings/Thumbnails/Width')?>
					</th>
					<th>
						<?= $this->_('Settings/Thumbnails/Height')?>
					</th>
					<th>
						<?= $this->_('Settings/Thumbnails/Actions')?>
					</th>
				</tr>
			</thead>
			<tbody>
				<? foreach($this->settings->getThumbnails() as $key=>$width) : ?>
				<tr>
					<td>
						<?= $this->form()->input('name[]', 'text', $this->getValue($width))->addAttribute('placeholder', $this->_('Settings/Thumbnails/Name'))->addAttribute('style','width:250px;');?>
					</td>
					<td>
						<?= $this->form()->input('width[]', 'text', $this->getValue($width,0))->addAttribute('placeholder', $this->_('Settings/Thumbnails/Width'))->addAttribute('style','width:250px;');?>
					</td>
					<td>
						<?= $this->form()->input('height[]', 'text', $this->getValue($width,1))->addAttribute('placeholder', $this->_('Settings/Thumbnails/Height'))->addAttribute('style','width:250px;');?>
					</td>
					<td style="vertical-align:middle;">
						<a href="#" class="js-variable-remove"><?= $this->_('Settings/Thumbnails/Remove')?></a>
					</td>
				</tr>
				<? endforeach; ?>
				<tr>
					<td>
						<?= $this->form()->input('name[]', 'text')->addAttribute('placeholder', $this->_('Settings/Thumbnails/Name'))->addAttribute('style','width:250px;');?>
					</td>
					<td>
						<?= $this->form()->input('width[]', 'text')->addAttribute('placeholder', $this->_('Settings/Thumbnails/Width'))->addAttribute('style','width:250px;');?>
					</td>
					<td>
						<?= $this->form()->input('height[]', 'text')->addAttribute('placeholder', $this->_('Settings/Thumbnails/Height'))->addAttribute('style','width:250px;');?>
					</td>
					<td style="vertical-align:middle;">
						<a href="#" class="js-variable-remove"><?= $this->_('Settings/Thumbnails/Remove')?></a>
					</td>
				</tr>
			</tbody>
		</table>
		<?= $this->form()->submit('submit', $this->_('Settings/Update'));?>
	<?= $this->form()->end(); ?>
</div>

<script type="text/javascript">
$(document).ready(function() {

	$('.js-site-variables tr:last input:first').live('keydown', function() {
		if($('.js-site-variables tr:last input:first').val() == '') {
			var n = $(this).parents('tr:first').clone();
			n.find('input').val('');
			$('.js-site-variables tbody').append(n);
			parent.admin.utils.setFrameHeight();
		}
	});

	$('.js-site-variables a.js-variable-remove').live('click', function(e) {
		e.preventDefault();
		if($(this).parents('tbody:first').find('tr').length > 1) {
			$(this).parents('tr:first').remove();
			parent.admin.utils.setFrameHeight();
		}
	});
});
</script>