<? /* @var $this \CMS\Widget\Admin\Iframe\Settings\File */ ?>

<div class="headline">
	<ul class="hor-nav">
		<li><a href="javascript:;" class="active"><?= $this->_('Settings/Files'); ?></a></li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>
	<?= $this->form()->start('edit-settings')->addAttribute('class', 'form'); ?>
		<div class="input">
			<?= $this->form()->label($this->_('Settings/FileUrl'), 'fileUrl'); ?>
			<?= $this->form()->input('fileUrl', 'text', $this->settings->getFileUrl(), TRUE)->addAttribute('id', 'fileUrl');?>
			<?= $this->validationFor('fileUrl'); ?>
		</div>

		<div class="input">
			<?= $this->form()->label($this->_('Settings/FileStorage'), 'fileStoragePath'); ?>
			<?= $this->form()->input('fileStoragePath', 'text', $this->settings->getFileStoragePath(), TRUE)->addAttribute('id', 'fileStoragePath');?>
			<?= $this->validationFor('fileStoragePath'); ?>
		</div>

		<div class="input">
			<?= $this->form()->label($this->_('Settings/MaxFilesize'), 'maxFilesize'); ?>
			<?= $this->form()->input('maxFilesize', 'text', $this->settings->getMaxFilesize(), TRUE)->addAttribute('id', 'maxFilesize');?>
			<?= $this->validationFor('maxFilesize'); ?>
		</div>

		<div class="input">
			<?= $this->form()->label($this->_('Settings/FileTypes'), 'filetypes'); ?>
			<?= $this->form()->input('filetypes', 'text', $this->settings->getFileTypes(FALSE), TRUE)->addAttribute('id', 'filetypes');?>
			<?= $this->validationFor('filetypes'); ?>
		</div>

		<div class="input">
			<?= $this->form()->label($this->_('Settings/CreateThumbs'), 'createThumbs'); ?>
			<?= $this->form()->bool('createThumbs', $this->settings->getCreateThumbs(), TRUE)->addAttribute('id', 'createThumbs');?>
			<?= $this->validationFor('createThumbs'); ?>
		</div>

		<div class="btn">
			<?= $this->form()->submit('submit', $this->_('Settings/Update'));?>
		</div>
	<?= $this->form()->end(); ?>
</div>