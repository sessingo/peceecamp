<? /* @var $this \CMS\Widget\Admin\Iframe\Settings\Macro */ ?>
<div class="headline">
	<ul class="hor-nav">
		<li><a href="<?= $this->getRoute(NULL, NULL, NULL, NULL, TRUE); ?>" class="active"><?= $this->macro->getTitle(); ?></a></li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>
	<? if($this->macro && $this->macro->hasRow()) : ?>
		<?= $this->form()->start('edit-macro')->addClass('form')->addAttribute('ID','macro-form'); ?>
		<div class="input">
			<?= $this->form()->label($this->_('Settings/Macro/Title'), 'title'); ?>
			<?= $this->form()->input('title', 'text', $this->macro->getTitle(), TRUE)->addAttribute('ID', 'title'); ?>
		</div>

		<div class="input">
			<span style="float:left;"><?= $this->_('Settings/Macro/Widget'); ?></span>

			<ul style="margin-left:180px;">
			<? foreach($this->macros as $i=>$value) : ?>
				<li style="margin-bottom:5px;">
					<?= $this->form()->input('widget', 'radio', $value['value'], TRUE)->setChecked(($value['value'] == $this->macro->getWidget()))->addAttribute('id','widget_'.$i); ?>
					<?= $this->form()->label($value['name'], 'widget_'.$i)->addAttribute('style','margin-left:10px;');?>
				</li>
			<? endforeach; ?>
			</ul>
			<? if($this->macro->getWidget()) : ?>
			<div style="clear:both;margin-left:180px;padding-top:15px;">
				<a href="#" data-href="<?= $this->getRoute(NULL, NULL, array('dialog','view','macro','properties'), array('nodeId' => $this->macro->getNodeID()));?>" class="js-dialog"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/application_form_edit.png" alt="application_form_edit" style="margin-right:5px;float:left;"/> Browse properties</a>
			</div>
			<? endif; ?>
		</div>

		<? if($this->macro->getModules()->hasRows()) : ?>
			<h3 style="margin-top:25px;"><?= $this->_('Settings/Macro/Properties'); ?></h3>

			<table style="width:100%;" class="dataTable">
				<thead>
					<tr>
						<th style="width:30%;">
							<?= $this->_('Settings/Macro/PropertyName')?>
						</th>
						<th>
							Modul
						</th>
						<th class="center">
							<?= $this->_('Settings/Macro/PropertyMethod')?>
						</th>
						<th style="text-align:right;">

						</th>
					</tr>
				</thead>
				<tbody>
					<? foreach($this->macro->getModules()->getRows() as $module) : ?>
					<tr>
						<td style="padding-top:15px;">
							<?= $this->form()->input('macroPropertyDeleted['.$module->getPropertyModuleID().']', 'hidden', FALSE, TRUE)->addClass('js-property-delete'); ?>
							<?= $module->getName(); ?>
						</td>
						<td>
							<?= \Pecee\Str::GetFirstOrValue($module->getModule(), $this->_('Settings/Macro/NoModuleChosen'));?>
						</td>
						<td  class="center">
							<?= $module->getControlID(); ?>
						</td>
						<td style="width:20%;text-align:center;">
							<a href="#" class="btn-s js-property-edit-btn" data-id="<?= $module->getPropertyModuleID(); ?>"><?= $this->_('Settings/Macro/Edit')?></a>
							<a href="#" class="btn-s js-property-delete-btn"><?= $this->_('Settings/Macro/Delete')?></a>
						</td>
					</tr>
					<? endforeach; ?>
				</tbody>
			</table>
		<? endif; ?>

		<div style="margin-top:50px;">
			<?= $this->form()->submit('save', $this->_('Settings/Macro/Save'))->addAttribute('ID','submit'); ?>
		</div>
		<?= $this->form()->end();?>
	<? endif; ?>

	<script type="text/javascript">
		$(document).ready(function() {
			$('a.js-property-edit-btn').bind('click', function(e) {
				e.preventDefault();
				var self = $(this);
				parent.dialog.show({
					url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'macro','propertyedit'), array('nodeId' => $this->macro->getNodeID())); ?>&moduleId=' + self.data('id'),
					type: parent.dialog.dialogTypes.ajax,
					onLoad: function() {
						$('select').ddslick({ imagePosition: "left" });
					}
				});
			});

			$('a.js-property-delete-btn').bind('click', function(e) {
				e.preventDefault();
				$(this).parents('tr:first').find('input.js-property-delete').val('1');
				$('#submit').click();
			});
		});
	</script>
</div>