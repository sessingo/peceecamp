<? /* @var $this \CMS\Widget\Admin\Iframe\Settings\Site */ ?>

<div class="headline">
	<ul class="hor-nav">
		<li><a href="javascript:;" class="active"><?= $this->_('Settings/Site'); ?></a></li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>
	<?= $this->form()->start('edit-settings')->addAttribute('class', 'form'); ?>
		<div class="input">
			<?= $this->form()->label($this->_('Settings/Frontpage'), 'frontpageNodeId'); ?>
			<?= $this->widget(new \CMS\Widget\Admin\Page\PageChoose('frontpageNodeId', $this->settings->getFrontpageNodeId())); ?>
			<?= $this->validationFor('frontpageNodeId'); ?>
		</div>

		<div class="input">
			<?= $this->form()->label($this->_('Settings/PageNotFound'), '404nodeId'); ?>
			<?= $this->widget(new \CMS\Widget\Admin\Page\PageChoose('404nodeId', $this->settings->get404NodeId())); ?>
			<?= $this->validationFor('404nodeId'); ?>
		</div>

		<div class="btn">
			<?= $this->form()->submit('submit', $this->_('Settings/Update'));?>
		</div>
	<?= $this->form()->end(); ?>
</div>
