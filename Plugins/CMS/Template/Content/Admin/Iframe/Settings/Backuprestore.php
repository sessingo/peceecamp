<? /* @var $this \CMS\Widget\Admin\Iframe\Settings\Backuprestore */ ?>

<div class="headline">
	<ul class="hor-nav">
		<li><a href="javascript:;" class="active"><?= $this->_('Settings/BackupRestore/RestoreBackup')?></a></li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>
	<?= $this->form()->start('restore-backup', 'post', NULL, \Pecee\UI\Form\Form::FORM_ENCTYPE_FORM_DATA)->addAttribute('class', 'form'); ?>
		<div style="padding-bottom:10px;margin-bottom:10px;border-bottom:1px dashed #CCC;font-weight:bold;">
			<?= $this->_('Settings/BackupRestore/ChooseRestoreFile')?>
		</div>
		<?= $this->form()->input('file', 'file'); ?>
		<?= $this->form()->submit('submit', $this->_('Settings/BackupRestore/Restore'));?>
	<?= $this->form()->end(); ?>
</div>
