<? /* @var $this \CMS\Widget\Admin\Language\LanguageHome */ ?>
<div class="headline">
	<ul class="hor-nav">
		<li><a href="<?= $this->getRoute(NULL, 'language'); ?>" class="active"><?= $this->_('Language/Language')?></a></li>
	</ul>
	<ul>
		<li>
			<?= $this->form()->start('search', 'get', $this->getRoute(NULL, 'page', array('search'))); ?>
				<?= $this->form()->input('query', 'text')->addAttribute('placeholder', $this->_('Language/Search'))->addAttribute('ID','query')->addAttribute('autocomplete','off'); ?>
			<?= $this->form()->end(); ?>
		</li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>

	<div id="loader"></div>

	<ol id="languages" class="treeview">
		<li id="node_0">
			<div>
				<div class="hitarea <?= (in_array('0', $this->activeLanguage) ? 'expanded' : 'collapsed')?>"></div>
				<a href="javascript:;" data-id="0" class="js-root" style="font-weight:bold;"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/folder.png" alt="" /> <?= $this->_('Language/Languages'); ?></a>
				<? if($this->languages && $this->languages->hasRows()): ?>
				<ol class="js-language"<?= (in_array('0', $this->activeLanguage)) ? '' : ' style="display:none;"'?>>
					<? foreach($this->languages->getRows() as $language) : ?>
					<li>
						<div>
							<a href="javascript:;" data-id="<?= $language->getLanguageID(); ?>" class="js-page"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/database_table.png" alt="" /><?= $language->getName();?></a>
						</div>
					</li>
					<? endforeach; ?>
				</ol>
			<? endif;?>
			</div>
		</li>
	</ol>

	<ol id="languageKeys" class="treeview">
		<li id="node_0">
			<div>
				<div class="hitarea <?= (in_array('0', $this->activeKeys) ? 'expanded' : 'collapsed')?>"></div>
				<a href="javascript:;" class="js-root js-folder" data-id="0" style="font-weight:bold;"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/folder.png" alt="" /> <?= $this->_('Language/Key/Dictionary')?></a>
				<?= $this->loopKeys()?>
			</div>
		</li>
	</ol>

	<div class="clear"></div>

	<script type="text/javascript">

		$(document).ready(function() {

			$('.ctn').disableSelection();

			$('ol#languages').tree({
				cookieName: 'LanguageActive',
				sourceUrl: null,
				editUrl: null,
				imageUrl: '/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/'
			});

			$('ol#languageKeys').tree({
				cookieName: 'LanguageKeysActive',
				sourceUrl: null,
				editUrl: null,
				imageUrl: '/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/'
			});

			var search = null;
			var searchTimer = null;

			$('#query').bind('keyup', function() {
				var self=$(this);
				self.parents('form:first').find('ul').remove();
				clearTimeout(searchTimer);
				searchTimer = setTimeout(function() {
					if(self.val() != '') {
						if(search != null) {
							search.abort();
							search = null;
						}
						search = $.getJSON('<?= $this->getRoute(NULL, 'language', array('search')); ?>?query='+self.val(), function(r) {
							self.parents('form:first').find('ul').remove();
							if(r.keys != null && r.keys.length > 0) {
								var el = $('<ul/>');
								for(var i=0;i<r.keys.length;i++) {
									var key = r.keys[i];
									el.append('<li><a href="<?= $this->getRoute(NULL, 'language', array('edit'));?>'+ key.id +'"> <img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/note.png" alt="" /> '+ key.name +'</a></li>');
								}
								self.parents('form:first').append(el);
							}
							search = null;
						});
					}
				}, 500);
			});

			$(document).bind('click', function() {
				$('#query').parents('form:first').find('ul').remove();
			});

			$('ol#languageKeys').nestedSortable({
	            handle: '> div',
	            items: 'ol > li',
	            toleranceElement: '> div',
	            protectRoot: true,
	            tolerance: 'pointer',
	            tabSize : 20,
	            expandOnHover: 700,
	            revert: 100,
	            isAllowed: function(item,parent) {
		            var result = true;
	            	$(parent).find('a').each(function() {
						if($(item).find('a').text() == $(this).text()) {
							result = false;
						}
	            	});
	            	return result;
	            },
	            stop: function(e, ui) {
	            	$('#loader').show();
	                var el = $(ui.item);
	                var parent = el.parents('li:first').find('a');

					var data = {
						languageKeyId: el.find('a').data('id'),
						parentLanguageKeyId: parent.data('id')
					};

					if(data.languageKeyId != data.parentLanguageKeyId) {
		            	$.post('<?= $this->getRoute(NULL, 'language',array('setparent.json')); ?>?parentLanguageKeyId=', data, function(r) {
		            		$('#loader').hide();
		            	});
					}
	            }
	        });

			$('ol#languages a.js-root').contextmenu([
				{
						name: '<?= $this->_('Language/NewLanguage'); ?>',
						image: '/plugin/CMS/admin/gfx/ico/database_add.png',
						callback: function(activeElement) {
						dialog.show({
							url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'language','create')); ?>',
							type: dialog.dialogTypes.ajax,
							onLoad: function() {
								$('select').ddslick({ imagePosition: "left" });
							}
						});
					}
				}
			]);

			$('ol#languages ol.js-language > li a').contextmenu([
				{
					name: '<?= $this->_('Language/EditLanguage'); ?>',
					image: '/plugin/CMS/admin/gfx/ico/database_edit.png',
					callback: function(activeElement) {
						var parent = $(activeElement);
						dialog.show({
							url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'language', 'edit')); ?>?languageId=' + parent.data('id'),
							type: dialog.dialogTypes.ajax,
							onLoad: function() {
								$('select').ddslick({ imagePosition: "left" });
							}
						});
					}
				},
				{
	            	name: '<?= $this->_('Language/DeleteLanguage'); ?>',
	            	image: '/plugin/CMS/admin/gfx/ico/database_delete.png',
	                callback: function(activeElement) {
	                	var parent = $(activeElement);
		                admin.dialog.confirm('<?= $this->_('Language/ConfirmDelete');?>', '<?= $this->_('Language/ConfirmDeleteText');?>', function(r) {
							if(r) {
								$.post('<?= $this->getRoute(NULL, 'language', array('deletelang.json'))?>', 'languageId='+parent.data('id'), function(r) {
									if(r == null || r.result != null && !r.result) {
										alert('<?= $this->_('Language/ErrorDeleting');?>');
									} else {
										parent.parents('li:first').hide(100);
									}
								});
							}
		                });
					}
	            }
			]);

			$('ol#languageKeys a.js-root').contextmenu([
				{
			    	name: '<?= $this->_('Language/Key/NewKey'); ?>',
			    	image: '/plugin/CMS/admin/gfx/ico/note_add.png',
			        callback: function(activeElement) {
				        dialog.show({
					        url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'language', 'key','create')); ?>',
					        type: dialog.dialogTypes.ajax,
					        onLoad: function() {
					        	$('select').ddslick({ imagePosition: "left" });
							}
				        });
					}
				}
			]);

			$('ol#languageKeys ol.js-page > li a').contextmenu([
				{
					name: '<?= $this->_('Language/Key/NewKey'); ?>',
					image: '/plugin/CMS/admin/gfx/ico/note_add.png',
				    callback: function(activeElement) {
				    	var parent = $(activeElement);
		                dialog.show({
		                	url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'language', 'key', 'create')); ?>?parentLanguageKeyId=' + parent.data('id'),
		                    type: dialog.dialogTypes.ajax,
		                    onLoad: function() {
		                    	$('select').ddslick({ imagePosition: "left" });
		                    }
						});
					}
				},
				{
	            	name: '<?= $this->_('Language/Key/EditKey'); ?>',
	            	image: '/plugin/CMS/admin/gfx/ico/note_edit.png',
	                callback: function(activeElement) {
	                	var parent = $(activeElement);
	                	top.location.href='<?= $this->getRoute(NULL, 'language', array('edit'));?>'+parent.data('id');
					}
	            },
	            {
	            	name: '<?= $this->_('Language/Key/DeleteKey'); ?>',
	            	image: '/plugin/CMS/admin/gfx/ico/note_delete.png',
	                callback: function(activeElement) {
	                	var parent = $(activeElement);
		                admin.dialog.confirm('<?= $this->_('Language/Key/ConfirmDelete');?>', '<?= $this->_('Language/Key/ConfirmDeleteText');?>', function(r) {
							if(r) {
								$.post('<?= $this->getRoute(NULL, 'language', array('deleteKey.json'))?>', 'languageKeyId='+parent.data('id'), function(r) {
									if(r == null || r.result != null && !r.result) {
										alert('<?= $this->_('Language/Key/ErrorDeleting');?>');
									} else {
										parent.parents('li:first').hide(100);
									}
								});
							}
		                });
					}
	            }
			]);
		});
	</script>
</div>