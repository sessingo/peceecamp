<? /* @var $this \CMS\Widget\Admin\Language\KeyEdit */ ?>
<div class="headline">
	<ul class="hor-nav">
		<li><?= $this->tab->button('texts', $this->_('Language/Key/Texts')); ?></li>
		<li><?= $this->tab->button('properties', $this->_('Language/Key/Properties')); ?></li>
	</ul>
</div>

<div class="ctn">

	<?= $this->showFlash(); ?>

	<?= $this->form()->start('edit-page', 'post')->addClass('form'); ?>

		<?= $this->tab->start('texts', TRUE); ?>
			<? foreach($this->languages->getRows() as $i=>$language) : ?>
				<h3><?= $language->getName(); ?></h3>
				<div class="input">
				<?= $this->form()->textarea('text['.$language->getAlias().']', 5, 5, $this->key->getText($language->getAlias()),TRUE)->addAttribute('style','width:100%;height:150px;')->addAttribute('class', 'js-grow'); ?>
				</div>
			<? endforeach; ?>
		<?= $this->tab->end(); ?>

		<?= $this->tab->start('properties'); ?>
		<div class="input">
			<?= $this->form()->label($this->_('Language/Key/Name'), 'name'); ?>
			<?= $this->form()->input('name', 'text', $this->key->getName(), TRUE)->addAttribute('ID', 'name'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Language/Key/Key'), 'key'); ?>
			<?= $this->form()->input('key', 'text', $this->key->getFullKey(), TRUE)->addAttribute('ID', 'key')->addAttribute('readonly','readonly'); ?>
		</div>
		<div class="input">
			<label><?= $this->_('Language/Key/PubDate'); ?></label>
			<span><?= $this->key->getPubDate(); ?></span>
		</div>
		<div class="input">
			<label><?= $this->_('Language/Key/ChangedDate'); ?></label>
			<span><?= $this->key->getChangedDate(); ?></span>
		</div>
		<?= $this->tab->end(); ?>

		<div class="clear"></div>
		<div style="padding:8px 0px;margin-top:25px;border-top: 1px solid #dfdfdf;text-align:right;">
			<?= $this->form()->submit('save', $this->_('Language/Key/SaveChanges'))?>
		</div>
	<?= $this->form()->end(); ?>
</div>