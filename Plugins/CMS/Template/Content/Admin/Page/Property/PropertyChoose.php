<? /* @var $this \CMS\Widget\Admin\Page\Property\PropertyChoose */ ?>

<div style="margin:8px 0;">

	<? if(!$this->page || $this->page && !$this->page->hasRow()) : ?>
		<span id="propertyname_<?= $this->name; ?>"></span>
	<? else: ?>
		<span id="propertyname_<?= $this->name; ?>"><?= $this->getName(); ?></span>
	<? endif; ?>

	<a href="javascript:;" onclick="chooseProperty<?= md5($this->name); ?>();" id="chooseproperty_<?= $this->name; ?>"><?= $this->_('Property/Choose/ChooseProperties'); ?></a>
	<?= $this->form()->input($this->name, 'hidden', $this->value, TRUE)->addAttribute('id', $this->name)?>
	<script type="text/javascript">
		function chooseProperty<?= md5($this->name); ?>() {
			parent.dialog.show({
				url: '<?= \PC\Router::GetRoute(NULL, NULL, array('dialog','view','page','property', 'choose'));?>?nodeId=<?= $this->defaultNode?>',
				type: dialog.dialogTypes.ajax,
				onLoad: function() {
					var t = parent.$('#propertychoose').tree({
						sourceUrl: '<?= \PC\Router::GetRoute(NULL, NULL, array('pageproperty','tree')); ?>',
						imageUrl: '/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/',
						setCookie: false
					});
					t.onSelectPage(function(e, nodeId, name, ui) {
						<? if($this->selectMultiple) :?>

							var checkbox = ui.parents('li:first').find('input[type="checkbox"]:first');

							if(ui.hasClass('active')) {
								ui.removeClass('active');
								checkbox.attr('checked', false);
							} else {
								ui.addClass('active');
								checkbox.attr('checked', true);
							}


							if(nodeId > 0) {
								var names = [];
								var ids = [];

								$('#propertychoose li a.active').each(function() {
									names.push($(this).text());
									ids.push($(this).attr('data-id'));
								});

								$('#propertyname_<?= $this->name;?>').html(names.join(', '));
								$('#<?= $this->name; ?>').val(ids.join(','));
							}
						<? else: ?>

							if(nodeId > 0) {
								$('#propertyname_<?= $this->name;?>').html(name);
								$('#<?= $this->name; ?>').val(nodeId);
								parent.dialog.closeActive();
							}

						<? endif; ?>
					});
					<? if($this->selectMultiple) :?>
						t.onCallback(function(e, nodeId, name, ui) {
							var ids = $('#<?= $this->name; ?>').val();
							if(ids.length > 0) {
								ids = ids.split(',');
								if(ids.length > 0) {
									$('#propertychoose li a').each(function() {
										var id = $(this).attr('data-id');
										if($.inArray(id, ids) > -1) {
											$(this).addClass('active');
											if($(this).parent().find('input[type="checkbox"]').length == 0) {
												$(this).parent().append('<input type="checkbox" value="'+id+'" checked="checked" name="selectedIds[]" style="display:none;"/>');
											}
										}
									});
								}
							}
						});
					<? endif; ?>
					t.refreshNode(0);
				}
			});
		}

	</script>
</div>