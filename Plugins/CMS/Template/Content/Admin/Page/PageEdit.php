<? /* @var $this \CMS\Widget\Admin\Page\PageEdit */ ?>
<div class="headline">
	<ul class="hor-nav">
		<? foreach(array_keys($this->modules) as $i=>$tab) : ?>
			<li><?= $this->tab->button('tab_'.$i, $tab); ?></li>
		<? endforeach; ?>
		<li><?= $this->tab->button('meta', $this->_('Page/Meta')); ?></li>
		<li><?= $this->tab->button('history', $this->_('Page/History')); ?></li>
		<li><?= $this->tab->button('properties', $this->_('Page/Properties')); ?></li>
	</ul>

	<ul>
		<li><a href="javascript:;" class="btn-s" id="view-page-mobile"><?= $this->_('Page/ViewMobile')?></a></li>
		<li><a href="javascript:;" class="btn-s" id="view-page"><?= $this->_('Page/ViewPage')?></a></li>
	</ul>
</div>
<div class="ctn">
	<?= $this->showFlash();?>
	<?= $this->form()->start('edit-page', 'post', NULL, \Pecee\UI\Form\Form::FORM_ENCTYPE_FORM_DATA)->addAttribute('id', 'edit-page')->addAttribute('class', 'form'); ?>

		<? $i=0;?>
		<? foreach(array_keys($this->modules) as $tab) : ?>
			<?= $this->tab->start('tab_'.$i, ($i==0)); ?>
				<? foreach($this->modules[$tab] as $widget) : ?>
				<?= $widget; ?>
				<? endforeach;?>
			<? $i++;?>
			<?= $this->tab->end('tab_'.$i); ?>
		<? endforeach;?>

		<?= $this->tab->start('properties', (count($this->page->getProperty()->getTabs())==0));?>
		<div class="input">
			<?= $this->form()->label($this->_('Page/Title'), 'title'); ?>
			<?= $this->form()->input('title', 'text', $this->page->getTitle(), TRUE)->addAttribute('ID', 'title'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Page/Url'), 'slug'); ?>

			<?= $this->form()->input('slug', 'text', $this->page->getSlug(), TRUE)->addAttribute('ID', 'slug'); ?>

			<? if(count($this->getSlugs()) > 0) : ?>
			<div style="margin-left:180px;margin-top:40px;clear:both;">
				<ul>
				<? foreach($this->getSlugs() as $slug) : ?>
					<li><a href="http://<?= $slug; ?>" rel="new"><?= $slug; ?></a></li>
				<? endforeach;?>
				</ul>
			</div>
			<? endif; ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Page/Widget'), 'template'); ?>
			<?= $this->form()->selectStart('widget', $this->widgets,$this->page->getWidget(), TRUE)->addAttribute('id', 'template');?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Page/ActiveFrom'), 'activeFrom'); ?>
			<?= $this->form()->input('activeFrom', 'text', $this->page->getActiveFrom(), TRUE)->addAttribute('ID', 'activeFrom'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Page/ActiveTo'), 'activeTo'); ?>
			<?= $this->form()->input('activeTo', 'text', $this->page->getActiveTo(), TRUE)->addAttribute('ID', 'activeTo'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Page/VisibleInMenu'), 'visibleInMenu'); ?>
			<?= $this->form()->bool('visibleInMenu', TRUE, TRUE)->addAttribute('ID', 'visibleInMenu')->setChecked($this->page->getVisibleInMenu()); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Page/Published'), 'active'); ?>
			<?= $this->form()->bool('active', TRUE, TRUE)->addAttribute('ID', 'active')->setChecked($this->page->getActive()); ?>
		</div>
		<div class="input">
			<label><?= $this->_('Page/Type'); ?></label>
			<span><?= $this->page->getProperty()->getAlias(); ?></span>
		</div>
		<div class="input">
			<label><?= $this->_('Page/PubDate'); ?></label>
			<span><?= $this->page->getPubDate(); ?></span>
		</div>
		<div class="input">
			<label><?= $this->_('Page/ChangedDate'); ?></label>
			<span><?= $this->page->getChangedDate(); ?></span>
		</div>
		<?= $this->tab->end(); ?>

		<?= $this->tab->start('meta');?>
		<div class="input">
			<?= $this->form()->label($this->_('Page/Title'), 'metaTitle'); ?>
			<?= $this->form()->input('metaTitle', 'text', $this->page->getMetaTitle(), TRUE)->addAttribute('ID', 'metaTitle'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Page/Description'), 'metaDescription'); ?>
			<?= $this->form()->input('metaDescription', 'text', $this->page->getMetaDescription(), TRUE)->addAttribute('ID', 'metaDescription'); ?>
		</div>
		<div class="input">
			<?= $this->form()->label($this->_('Page/Tags'), 'metaTags'); ?>
			<?= $this->form()->input('metaTags', 'text', $this->page->getMetaKeywords(), TRUE)->addAttribute('ID', 'metaTags'); ?>
		</div>
		<?= $this->tab->end(); ?>

		<?= $this->tab->start('history');?>
			<? if(!$this->history->hasRows()) : ?>
				<div style="padding:10px;">
					<?= $this->_('Page/NoHistory')?>
				</div>
			<? else: ?>

				<table class="dataTable">
					<thead>
						<tr>
							<th class="center"><?= $this->_('Page/Date'); ?></th>
							<th class="center"><?= $this->_('Page/Author');?></th>
							<th style="width:100px;" class="center"><?= $this->_('Page/Actions');?></th>
						</tr>
					</thead>
					<tbody>
						<? foreach($this->history->getRows() as $key=>$history) : ?>
						<tr class="<?= ($key%2==0) ? 'odd' : 'even' ?> gradeX">
							<td class="center">
								<?= $history->getPubDate();?>
							</td>
							<td class="center">
								<?= \Pecee\Model\User\ModelUser::GetByUserID($history->getUserID())->getUsername(); ?>
							</td>
							<td class="center">
								<div class="actions">
									<a href="<?= $this->getRoute(NULL, NULL, NULL, array('draftId' => $history->getNodeID(TRUE)), TRUE);?>"><?= $this->_('Page/Edit')?></a> -
									<a href="<?= $this->getRoute(NULL, 'page', array('viewdraft', $history->getNodeID(TRUE)));?>" rel="new"><?= $this->_('Page/View')?></a>
								</div>
							</td>
						</tr>
						<? endforeach; ?>
					</tbody>
				</table>
			<? endif;?>
		<?= $this->tab->end(); ?>

		<div class="clear"></div>
		<div style="padding:8px 0px;margin-top:15px;border-top: 1px solid #dfdfdf;text-align:right;">
			<div id="loader" style="display:inline-block;background:url(/plugin/CMS/admin/gfx/loader.gif) no-repeat;background-position:50% 50%;height:33px;width:33px;display:none;width:100px;margin-bottom:20px;"></div>
			<div id="buttons">
				<?= $this->form()->input('save', 'button', $this->_('Page/Save'))->addAttribute('id','page-save'); ?>
				<?= $this->form()->input('publish', 'button', $this->_('Page/SaveAndPublish'))->addAttribute('id','page-publish'); ?>
			</div>
		</div>

	<?= $this->form()->end(); ?>

	<script type="text/javascript">
		$(document).ready(function() {
			var saveTimer = null;
			var draftId = '<?= $this->getParam('draftId'); ?>';
			var showLoader = function() {
				$('#loader').show();
				$('#buttons').hide();
			};

			var hideLoader = function() {
				$('#loader').hide();
				$('#buttons').show();
			};

			var saveDraft = function(callback) {
				$.ajax({
					url: '<?= $this->getRoute(NULL, 'page', array('savedraft', $this->page->getNodeID())); ?>?draftId=' + draftId,
					dataType: 'json',
					type: 'POST',
					data: $('#edit-page').serializeArray(),
					success: function(r) {
						if(r != null && !r.result) {
							alert(r.msg);
						}
						if(callback != null) {
							draftId = r.draftId;
							callback(r);
						}
					}
				});
			};

			var originalForm = $('form').serialize();
			var editMode = true;
			$(window).bind('beforeunload', function(){
				if(originalForm != $('form').serialize() && editMode) {
					return '<?= $this->_('Page/PageLeaveUnsaved');?>';
				}
			});

			var autoSave = function() {
				saveTimer = setTimeout(function() {
					clearTimeout(saveTimer);
					saveTimer = null;
					saveDraft(function() {
						autoSave();
					});
				}, 60000);
			};

			<? if(!$this->getDisableHistory()) : ?>
			autoSave();
			<? endif; ?>

			var slugTimer = null;
			$('#title').bind('keyup',function() {
				var self = $(this);
				clearTimeout(slugTimer);
				slugTimer = setTimeout(function() {
					showLoader();
					if(self.val() != '') {
						$.ajax({
							url: '<?= $this->getRoute(NULL, 'page', array('generateSlug.json'));?>',
							dataType: 'json',
							type: 'POST',
							data: { nodeId: '<?= $this->page->getNodeID(); ?>', title: self.val() },
							success: function(r) {
								hideLoader();
								if(r.result && r.slug) {
									$('#slug').val(r.slug);
								}
							}
						});
					}
				},800);
			});

			$('#page-save').bind('click', function(e) {
				e.preventDefault();
				clearTimeout(saveTimer);
				saveTimer = null;
				showLoader();
				saveDraft(function() {
					hideLoader();
					autoSave();
				});
			});

			$('#page-publish').bind('click', function(e) {
				e.preventDefault();
				draftId = '';
				editMode = false;
				showLoader();
				//saveDraft(function() {
					$('#edit-page').submit();
				//});
			});

			$('#view-page').bind('click',function(e) {
				e.preventDefault();
				showLoader();
				saveDraft(function(r) {
					if(r != null) {
						window.open('<?= $this->getRoute(NULL, 'page', array('viewdraft'));?>' + r.draftId + '/');
					}
				});
				hideLoader();
			});

			$('#view-page-mobile').bind('click',function(e) {
				e.preventDefault();
				showLoader();
				saveDraft(function(r) {
					if(r != null) {
						window.open('<?= $this->getRoute(NULL, 'page', array('viewdraft'));?>' + r.draftId + '?mobile=true');
					}
					hideLoader();
				});
			});

			$('.js-editor').redactor({
				plugins: ['macro','fullscreen'],
				autoresize: true,
				toolbarFixedBox: false,
				minHeight: 100,
				height: 100,
				maxHeight: 300,
				imageUpload: '<?= $this->getRoute(NULL, 'file', array('upload'), array('type' => 'image', 'id' => session_id()));?>',
				fileUpload: '<?= $this->getRoute(NULL, 'file', array('upload'), array('id' => session_id()));?>',
				imageGetJson: '<?= $this->getRoute(NULL, 'file', array('getimages'), array('id' => session_id()));?>'
			});

			$('#activeFrom,#activeTo').datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: 'dd-mm-yy'
			});

		});
	</script>
</div>