<? /* @var $this \CMS\Widget\Admin\Page\PageHome */ ?>

<div class="headline">
	<ul class="hor-nav">
		<li><a href="<?= $this->getRoute(NULL, ''); ?>" class="active"><?= $this->_('Page/Pages')?></a></li>
		<? if(\Pecee\Model\User\ModelUser::Current()->getAdminLevel() == \PC\Helper::USER_TYPE_ADMIN) :?>
		<li><a href="<?= $this->getRoute(NULL, 'pageproperty'); ?>"><?= $this->_('Page/Structure')?></a></li>
		<? endif; ?>
	</ul>
	<ul>
		<li>
			<?= $this->form()->start('search', 'get', $this->getRoute(NULL, 'page', array('search'))); ?>
				<?= $this->form()->input('query', 'text')->addAttribute('placeholder', $this->_('Page/Search'))->addAttribute('ID','query')->addAttribute('autocomplete','off'); ?>
			<?= $this->form()->end(); ?>
		</li>
		<li>
			<a href="/<?= $this->plugin->application->getSlug(); ?>" class="btn-s" rel="new"><?= $this->_('Page/ViewSite'); ?></a>
		</li>
		<li>
			<a href="#" data-href="<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'page','create')); ?>" class="js-dialog btn-s"><?= $this->_('Page/NewPage'); ?></a>
		</li>
	</ul>
</div>

<div class="ctn">
	<?= $this->showFlash(); ?>

	<div id="loader"></div>
	<ol id="pages" class="treeview">
		<li id="node_0">
			<div>
				<div class="hitarea <?= (in_array('0', $this->activeNodes) ? 'expanded' : 'collapsed')?>"></div>
				<a href="javascript:;" data-id="0" class="js-root" style="font-weight:bold;"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/folder.png" alt="" /> <?= $this->plugin->application->Name; ?></a>
				<? if($this->pages && $this->pages->hasRows()): ?>
				<ol<?= (in_array('0', $this->activeNodes) ? '' : ' style="display:none;"')?>>
					<? foreach($this->pages->getRows() as $p) : ?>
					<li id="node_<?= $p->getNodeID(); ?>" data-accepts="<?= (is_array($p->getProperty()->getProperties())) ? join(',', $p->getProperty()->getProperties()) : ''?>" data-propertyId="<?= $p->getProperty()->getNodeID();?>">
						<div>
							<div class="hitarea <?= (in_array($p->getNodeID(), $this->activeNodes) ? 'expanded' : 'collapsed'); ?>"></div><a href="<?= $this->getRoute(NULL, 'page', array('edit', $p->getNodeID()));?>" data-id="<?= $p->getNodeID(); ?>" data-slug="<?= $p->getSlug(); ?>" class="js-page"><img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/<?= $p->getProperty()->getIcon(); ?>" alt="" /><?= $p->getTitle();?></a>
						</div>
						<?= $this->loopPages($p)?>
					</li>
					<? endforeach; ?>
				</ol>
			<? endif;?>
			</div>
		</li>
	</ol>

	<div class="clear"></div>

	<script type="text/javascript">
		$(document).ready(function() {

			$('.ctn').disableSelection();

			var t = $('#pages').tree({
				cookieName: 'PageActiveNodes',
				sourceUrl: '<?= $this->getRoute(NULL, 'page',array('tree'))?>',
				editUrl: '<?= $this->getRoute(NULL, 'page', array('edit'));?>',
				imageUrl: '/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/'
			});

			var search = null;
			var searchTimer = null;

			$('#query').bind('keyup', function() {
				var self=$(this);
				self.parents('form:first').find('ul').remove();
				clearTimeout(searchTimer);
				searchTimer = setTimeout(function() {
					if(self.val() != '') {
						if(search != null) {
							search.abort();
							search = null;
						}
						search = $.getJSON('<?= $this->getRoute(NULL, 'page', array('search')); ?>?query='+self.val(), function(r) {
							self.parents('form:first').find('ul').remove();
							if(r.nodes != null && r.nodes.length > 0) {
								var el = $('<ul/>');
								for(var i=0;i<r.nodes.length;i++) {
									var page = r.nodes[i];
									el.append('<li><a href="<?= $this->getRoute(NULL, 'page', array('edit'));?>'+ page.id +'"> <img src="/plugin/<?= $this->plugin->name; ?>/admin/gfx/ico/'+page.icon+'" alt="" /> '+ page.title +'</a></li>');
								}
								self.parents('form:first').append(el);
							}
							search = null;
						});
					}
				}, 500);
			});

			$(document).bind('click', function() {
				$('#query').parents('form:first').find('ul').remove();
			});

			$('ol#pages').nestedSortable({
	            handle: '> div',
	            items: 'ol > li',
	            toleranceElement: '> div',
	            protectRoot: true,
	            tolerance: 'pointer',
	            tabSize : 20,
	            expandOnHover: 700,
	            revert: 100,
	            isAllowed: function(item,parent) {
	            	var accepts = $(parent).attr('data-accepts');
					accepts = (accepts != null) ? accepts.split(',') : '';
		            return ($(parent).find('a:first').data('id') == '0' || $.inArray($(item).attr('data-propertyId'), accepts) > -1);
	            },
	            stop: function(e, ui) {
	            	$('#loader').show();
	            	var list = $('ol#pages').nestedSortable('serialize');
	                var el = $(ui.item);
	                var parent = el.parents('li:first').find('a');
	            	$.post('<?= $this->getRoute(NULL, 'page',array('setparent')); ?>?parentNodeId='+parent.data('id'), list, function(r) {
	            		$('#loader').hide();
						if(r.result == null || !r.result) {
							alert('<?= $this->_('Page/ErrorUpdating'); ?>');
						}
	            	});
	            }
	        });

			$('.treeview li a.js-root').contextmenu([
				{
	            	name: '<?= $this->_('Page/NewPage');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/page_add.png',
	                callback: function(activeElement) {
	                    dialog.show({
	                    	url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'page','create')); ?>',
	                        type: dialog.dialogTypes.ajax,
	                        onLoad: function() {
	                        	$('select').ddslick({ imagePosition: "left" });
	                        }
						});
					}
				},
				{
	            	name: '<?= $this->_('Page/ClearCache');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/database_refresh.png',
	                callback: function(activeElement) {
	                	$('#loader').show();
	                	$.getJSON('<?= $this->getRoute(NULL, NULL, array('page','clearcache.json')) ?>', function(r) {
	                		$('#loader').hide();
	                	});
					}
	            },
				{
	            	name: '<?= $this->_('Page/Refresh');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/arrow_refresh.png',
	                callback: function(activeElement) {
	                	t.refreshNode($(activeElement).data('id'));
					}
	            }
			]);

			$('.treeview li a.js-page').contextmenu([
				{
					name: '<?= $this->_('Page/NewPage');?>',
					image: '/plugin/CMS/admin/gfx/ico/page_add.png',
				    callback: function(activeElement) {
				    	var parent = $(activeElement);
		                dialog.show({
		                	url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'page','create')); ?>?parentNodeId=' + parent.data('id'),
		                    type: dialog.dialogTypes.ajax,
		                    onLoad: function() {
		                    	$('select').ddslick({ imagePosition: "left" });
		                    }
						});
					}
				},
				{
	            	name: '<?= $this->_('Page/EditPage');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/page_edit.png',
	                callback: function(activeElement) {
	                	var parent = $(activeElement);
	                	top.location.href='<?= $this->getRoute(null, 'page', array('edit'));?>'+parent.data('id');
					}
	            },
	            {
	            	name: '<?= $this->_('Page/DeletePage');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/page_delete.png',
	                callback: function(activeElement) {
	                	$('#loader').show();
	                	var parent = $(activeElement);
		                admin.dialog.confirm('<?= $this->_('Page/ConfirmDeletePage');?>', '<?= $this->_('Page/ConfirmDeletePageText');?>', function(r) {
		                	$('#loader').hide();
							if(r) {
								$.post('<?= $this->getRoute(NULL, 'page', array('deletePage'))?>', 'nodeId='+parent.data('id'), function(r) {
									if(r == null || r.result != null && !r.result) {
										alert('<?= $this->_('Page/ErrorDeleting');?>');
									} else {
										parent.parents('li:first').hide(100);
									}
								});
							}
		                });
					}
	            },
	            {
	            	name: '<?= $this->_('Page/ViewPage');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/page_go.png',
	                callback: function(activeElement) {
	                	var parent = $(activeElement);
		                window.open('http://<?= $_SERVER['HTTP_HOST']; ?><?= $this->plugin->application->Slug;?>'+parent.data('slug'));
					}
	            },
	            {
	            	name: '<?= $this->_('Page/Publish');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/page_save.png',
	                callback: function(activeElement) {
	                	var parent = $(activeElement);
	                	 dialog.show({
		                	url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'page','publish')); ?>?nodeId=' + parent.data('id'),
		                    type: dialog.dialogTypes.ajax,
		                    onLoad: function() {
		                    	$('select').ddslick({ imagePosition: "left" });
		                    }
						});
					}
	            },
	            {
	            	name: 'Manage hostnames',
	            	image: '/plugin/CMS/admin/gfx/ico/world.png',
	                callback: function(activeElement) {
	                	var parent = $(activeElement);
	                	 dialog.show({
		                	url: '<?= $this->getRoute(NULL, NULL, array('dialog', 'view', 'page','hosts')); ?>?nodeId=' + parent.data('id'),
		                    type: dialog.dialogTypes.ajax,
		                    onLoad: function() {
		                    	$('select').ddslick({ imagePosition: "left" });
		                    }
						});
					}
	            },
	            {
	            	name: '<?= $this->_('Page/Refresh');?>',
	            	image: '/plugin/CMS/admin/gfx/ico/arrow_refresh.png',
	                callback: function(activeElement) {
	                	t.refreshNode($(activeElement).data('id'));
					}
	            }
			]);
		});
	</script>
</div>