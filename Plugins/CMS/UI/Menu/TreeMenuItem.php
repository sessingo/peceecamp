<?php
namespace CMS\UI\Menu;
class TreeMenuItem {
	protected $data;
	protected $currentItem;

	/**
	 * Add menu
	 * @param \CMS\UI\Menu\TreeMenu  $menu
	 * @return \CMS\UI\Menu\TreeMenuItem
	 */
	public function addMenu(\CMS\UI\Menu\TreeMenu $menu){
		$this->currentItem['menu'] = $menu;
		return $this;
	}

	/**
	 * Add new item
	 *
	 * @param string $title
	 * @param string $value
	 * @return \CMS\UI\Menu\TreeMenuItem
	 */
	public function addItem($title, $value, $image, $description=NULL) {
		$this->moveItem();
		$this->currentItem['title'] = $title;
		$this->currentItem['value'] = $value;
		$this->currentItem['image'] = '/plugin/'. \PC\Plugin::Instance()->getPlugin()->name .'/admin/gfx/ico/' .$image;
		$this->currentItem['description'] = $description;
		$this->currentItem['attributes']=NULL;
		$this->currentItem['linkAttributes']=NULL;
		return $this;
	}

	public function getTitle() {
		return isset($this->currentItem['title']) ? $this->currentItem['title'] : NULL;
	}

	public function getValue() {
		return isset($this->currentItem['value']) ? $this->currentItem['value'] : NULL;
	}

	public function getImage() {
		return isset($this->currentItem['image']) ? $this->currentItem['image'] : NULL;
	}

	public function setTitle($title) {
		$this->currentItem['title'] = $title;
		return $this;
	}

	public function setValue($value) {
		$this->currentItem['value'] = $value;
		return $this;
	}

	public function setImage($value) {
		$this->currentItem['image'] = $value;
		return $this;
	}

	public function getDescription() {
		return isset($this->currentItem['description']) ? $this->currentItem['description'] : NULL;
	}

	/**
	 * @return \Pecee\UI\Menu\Menu
	 */
	public function getMenu() {
		return isset($this->currentItem['menu']) ? $this->currentItem['menu'] : NULL;
	}

	/**
	 * Moves current element to data array.
	 */
	private function moveItem() {
		if( $this->currentItem ) {
			$this->data[] = $this->currentItem;
			$this->currentItem = array();
		}
	}

	/**
	 * Adds attribute to item.
	 *
	 * @param string $name
	 * @param string $value
	 * @return \CMS\UI\Menu\TreeMenuItem
	 */
	public function addAttribute( $name,$value ) {
		if( isset($this->currentItem['attributes'][$name]) ) {
			$tmp = $this->currentItem['attributes'][$name];
			unset($this->currentItem['attributes'][$name]);
			$this->currentItem['attributes'][$name] = $tmp . ' ' . $value;
			return $this;
		}
		$this->currentItem['attributes'][$name] = $value;
		return $this;
	}

	public function removeAttribute($name) {
		if(isset($this->currentItem['attributes'][$name])) {
			unset($this->currentItem['attributes'][$name]);
		}
	}

	/**
	 * Adds attribute to item.
	 *
	 * @param string $name
	 * @param string $value
	 * @return \CMS\UI\Menu\TreeMenuItem
	 */
	public function addLinkAttribute($name,$value) {
		if(isset($this->currentItem['linkAttributes'][$name])) {
			$tmp = $this->currentItem['linkAttributes'][$name];
			unset($this->currentItem['linkAttributes'][$name]);
			$this->currentItem['linkAttributes'][$name] = $tmp . ' ' . $value;
			return $this;
		}
		$this->currentItem['linkAttributes'][$name] = $value;
		return $this;
	}

	/**
	 * Add form content
	 * @param \Pecee\UI\Html\Html $element
	 * @return \CMS\UI\Menu\TreeMenuItem
	 */
	public function addContent(\Pecee\UI\Html\Html $element) {
		$this->currentItem['content'][] = $element;
		return $this;
	}

	/**
	 * Get form content, if any
	 * @return \Pecee\UI\Html\Html|null
	 */
	public function getContent() {
		return $this->currentItem['content'];
	}

	public function removeLinkAttribute($name) {
		if(isset($this->currentItem['linkAttributes'][$name])) {
			unset($this->currentItem['linkAttributes'][$name]);
		}
	}

	/**
	 * Add class to item
	 * @param string $name
	 * @return \CMS\UI\Menu\TreeMenuItem
	 */
	public function addClass($name) {
		$this->addAttribute('class', $name);
		return $this;
	}

	public function getItems() {
		$this->moveItem();
		return $this->data;
	}
}