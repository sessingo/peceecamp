<?php
namespace CMS\UI\Menu;
class TreeMenu {
	protected $items;
	protected $class;
	protected $attributes;
	protected $content;
	public function __construct() {
		$this->attributes=array();
		$this->content=array();
		return $this;
	}
	public function getItems() {
		return $this->items;
	}
	/**
	 * Get active tab by index.
	 * @param int $index
	 * @return \CMS\UI\Menu\TreeMenuItem
	 */
	public function getItem($index) {
		if($this->hasItem($index)) {
			return $this->items[$index];
		}
		return NULL;
	}

	/**
	 * Returns first item.
	 * @return \CMS\UI\Menu\TreeMenuItem|null
	 */
	public function getFirst() {
		if(count($this->items) > 0) {
			foreach($this->items as $item) {
				return $item;
			}
		}
		return NULL;
	}

	/**
	 * Returns last item.
	 * @return \CMS\UI\Menu\TreeMenuItem|null
	 */
	public function getLast() {
		if(count($this->items) > 0) {
			return $this->items[count($this->items)-1];
		}
		return NULL;
	}

	/**
	 * Check if the item-index exists.
	 * @param int $index
	 * @return bool
	 */
	public function hasItem($index) {
		return isset($this->items[$index]);
	}

	public function hasItems() {
		return (count($this->items) > 0);
	}

	/**
	 * Add form content
	 * @param \Pecee\UI\Html\Html $element
	 */
	public function addContent(\Pecee\UI\Html\Html $element) {
		$this->content[] = $element;
	}

	/**
	 * Add form content
	 * @param \CMS\UI\Menu\TreeMenu $element
	 */
	public function addMenu(\Pecee\UI\Menu\Menu $element) {
		$this->content[] = $element;
	}

	/**
	 * Get form content, if any
	 * @return \Pecee\UI\Html\Html|null
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * Add new item
	 *
	 * @param string $title
	 * @param string $value
	 * @return \CMS\UI\Menu\TreeMenuItem
	 */
	public function addItem($title, $value, $image, $description=NULL) {
		$item = new \CMS\UI\Menu\TreeMenuItem();
		$item->addItem($title, $value, $image, $description);
		$this->items[] = $item;
		return $item;
	}

	/**
	 * Add new item
	 *
	 * @param \CMS\UI\Menu\TreeMenuItem $item
	 * @return \CMS\UI\Menu\TreeMenuItem
	 */
	public function addItemObject(\CMS\UI\Menu\TreeMenuItem $item) {
		$this->items[] = $item;
		return $item;
	}


	/**
	 * Add new item to given index
	 * @param int $index
	 * @param string $title
	 * @param string $value
	 * @param \CMS\UI\Menu\TreeMenuItem $description
	 */
	public function addItemToIndex($index, $title, $value, $description=NULL) {
		$item = new \CMS\UI\Menu\TreeMenuItem();
		$item->addItem( $title, $value, $description );
		$this->items[$index] = $item;
		return $item;
	}

	/**
	 * Set item-class
	 * @param string $name
	 * @return \CMS\UI\Menu\TreeMenu
	 */
	public function setClass( $name ) {
		$this->class = $name;
		return $this;
	}
	public function addAttribute($name, $value) {
		$this->attributes[$name] = $value;
	}

	public function addClass($class) {
		$this->addAttribute('class', $class);
	}

	protected function getAttributes($attributes) {
		if(is_array($attributes) && count($attributes) > 0) {
			$out = array();
			/* Run through each attribute */
			foreach($attributes as $attr=>$v) {
				$out[] = ' ' . $attr . '="'.$v.'"';
			}
			return join($out, NULL);
		}
		return '';
	}

	/**
	 * Write html
	 * @return string
	 */
	public function __toString() {
		$o = array();
		if(count($this->items) > 0) {
			$o[] = '<ol'.(($this->class) ? ' class="'.$this->class.'"' : '');
			if(count($this->attributes) > 0) {
				$o[]=$this->getAttributes($this->attributes);
			}
			$o[] = '>';
			foreach($this->items as $item) {
				foreach($item->getItems() as $key=>$i) {
					/* Write html */
					$o[] = sprintf('<li%1$s><div><div class="hitarea collapsed"></div><a href="%2$s" title="%4$s"%5$s><img src="%6$s" alt="" />%3$s</a>',
									$this->getAttributes($i['attributes']),
									$i['value'],
									$i['title'],
									htmlspecialchars($i['description']),
									$this->getAttributes($i['linkAttributes']),
									$i['image']);
					if(isset($i['content']) && is_array($i['content'])) {
						foreach($i['content'] as $c) {
							$o[]=$c->__toString();
						}
					}
					if(isset($i['menu'])) {
						$o[]=$i['menu']->__toString();
					}
					if(isset($this->content[$key]) > 0) {
						$o[]=$this->content[$key]->__toString();
					}
					$o[]='</div></li>';
				}
			}

			$o[] = '</ol>';
			return join($o, '');
		}
		return '';
	}
}