<?php
namespace CMS;
class Module {
	public static function GetModules() {
		$modules = array();

		$plugin = \PC\Plugin::Instance()->getByName('CMS');

		// List all modules from theme
		$modulePath = $plugin->themesdir . DIRECTORY_SEPARATOR . $plugin->application->Theme.'/Widget/Module/';
		if(is_dir($modulePath)) {
			if((($handle = opendir($modulePath)) !== FALSE)) {
				while (false !== ($entry = readdir($handle))) {
					if(strpos(strtolower($entry), '.php') > -1 && strpos(strtolower($entry), '.svn') === FALSE) {
						$name = explode('.',$entry);
						$widget = '\\CMS\\'.$plugin->application->Theme.'\\Widget\\Module\\' . ucfirst($name[0]);
						if(class_exists($widget)) {
							$class = new \ReflectionClass($widget);
							if(!$class->isAbstract()) {
								$w = new $widget();
								$modules[$w->getName()] = (object)array('name' => $w->getName(), 'widget' => $widget);
							}
						}
					}
				}
				closedir($handle);
			}
		}

		// List all modules from default path
		if((($handle = opendir($plugin->path . '/Widget/Module/')) !== FALSE)) {
			while (false !== ($entry = readdir($handle))) {
				if(strpos(strtolower($entry), '.php') > -1 && strpos(strtolower($entry), '.svn') === FALSE) {
					$name = explode('.',$entry);
					$widget = '\\CMS\\Widget\\Module\\' . ucfirst($name[0]);
					if(class_exists($widget)) {
						$class = new \ReflectionClass($widget);
						if(!$class->isAbstract()) {
							$w = new $widget();
							$modules[$w->getName()] = (object)array('name' => $w->getName(), 'widget' => $widget);
						}
					}
				}
			}
			closedir($handle);
		}

		ksort($modules);

		return (object)$modules;
	}

	public static function GetWidgets($path = '') {
		$templates = array();
		$plugin = \PC\Plugin::Instance()->getByName('CMS');
		$p = $plugin->themesdir . DIRECTORY_SEPARATOR . $plugin->application->Theme .'/Widget/' . $path;
		if(($handle = opendir($p)) !== FALSE) {
			while (false !== ($entry = readdir($handle))) {
				if(strpos(strtolower($entry), '.php') > -1 && strpos(strtolower($entry), '.svn') === FALSE) {
					$name = explode('.',$entry);
					$widget = ucfirst($name[0]);
					$widget = str_replace(DIRECTORY_SEPARATOR, '\\', $path) . $widget;
					$widget = sprintf('\\CMS\\%s\\Widget\\%s', $plugin->application->Theme, $widget);
					if(class_exists($widget)) {
						$class = new \ReflectionClass($widget);
						if(!$class->isAbstract()) {
							$templates[] = $widget;
						}
					}
				}

				if(!in_array($entry, array('.', '..','Macro','Module')) && is_dir($p . DIRECTORY_SEPARATOR .$entry)) {
					$templates = array_merge(self::GetWidgets($path . $entry . DIRECTORY_SEPARATOR), $templates);
				}
			}
			closedir($handle);
		}
		return $templates;
	}
}