<?php
/* @var $this \PC\Plugin
 *
 * GLOBAL PLUGIN CONFIGURATION FILE
 * This file is executed when first time the plugin is initialized.
 *
 */

/* - CONFIGURATION START - */

/* - CONFIGURATION END - */

if($this->getIsAdmin()) {
	$menu=\PC\UI\Plugin\PluginMenu::Instance();
	$menu->addItem($this->_('Page/Pages'), $this->getRoute($this->plugin->application->ApplicationID, ''));
	$menu->addItem($this->_('File/Files'), $this->getRoute($this->plugin->application->ApplicationID, 'file'));
	$menu->addItem($this->_('User/Users'), $this->getRoute($this->plugin->application->ApplicationID, 'user'));
	$user = \Pecee\Model\User\ModelUser::Current();
	if($user && $user->getAdminLevel() > \PC\Helper::USER_TYPE_EDITOR) {
		$menu->addItem($this->_('Language/Language'), $this->getRoute($this->plugin->application->ApplicationID, 'language'));
		$menu->addItem($this->_('Settings/Settings'), $this->getRoute($this->plugin->application->ApplicationID, 'settings'));
	}
} else {
	/* @var $modelsettings \CMS\Model\ModelSettings */
	$modelsettings = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelSettings');

	/* @var $modelpage \CMS\Model\ModelPage */
	$modelpage = \PC\ObjectManager::GetInstance()->get('\CMS\Model\ModelPage');

	$settings = $modelsettings::GetInstance();

	if($settings->getUsingHostnames()) {
		\Pecee\Debug::Instance()->add('CMS Plugin: get root node by hostname');
		$root = $modelpage::GetByHostname($_SERVER['HTTP_HOST']);
		if($root->hasRow()) {
			$settings->setRootNodeId($root->getNodeID());
			$settings->setLanguage($root->getLanguageAlias());
			$settings->setRootSlug(trim($root->getSlug(),'/'));
		}
	}
}