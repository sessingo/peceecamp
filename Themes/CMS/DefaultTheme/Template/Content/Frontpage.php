<? /* @var $this \CMS\DefaultTheme\Widget\Frontpage */ ?>

<?= $this->widget(new \CMS\DefaultTheme\Widget\Snippet\Slider($this->page->getNodeID())); ?>

<section id="main">
	<div class="main-inner wrapper clearfix">
		<div class="iconbox box-color box-nonspaced box-one-fourth box-scale">
			<div class="box-inner clearfix">
				<div class="icon"><img style="width:45px;height:45px;" src="<?= $this->getRessource('img/icons/responsive-icon.png')?>" alt="Responsive" /></div>
				<h3><strong>Responsive</strong></h3>
				<div class="seperator"></div>
				<p>This theme is responsive, so it will looks awesome on all mobile devices.</p>
				<a class="link-moreinfo" href="#">more Info</a>
			</div>
		</div>

		<div class="iconbox box-color box-nonspaced box-one-fourth box-scale" style="background: #328c9a;">
			<div class="box-inner clearfix">
				<div class="icon"><img style="width:45px;height:45px;" src="<?= $this->getRessource('img/icons/color-icon.png')?>" alt="Color" /></div>
				<h3><strong>Unlimited Color</strong></h3>
				<div class="seperator"></div>
				<p>You can change the different colors easily through the css files.</p>
				<a class="link-moreinfo" href="#">more Info</a>
			</div>
		</div>

		<div class="iconbox box-color box-nonspaced box-one-fourth box-scale"  style="background: #409fad;">
			<div class="box-inner clearfix">
				<div class="icon"><img style="width:45px;height:45px;" src="<?= $this->getRessource('img/icons/retina-icon.png')?>" alt="Retina" /></div>
				<h3><strong>Retina Ready</strong></h3>
				<div class="seperator"></div>
				<p>Yalu is ready for retina and looks fantastic on high-resolution displays.</p>
				<a class="link-moreinfo" href="#">more Info</a>
			</div>
		</div>

		<div class="iconbox box-color box-nonspaced box-one-fourth box-scale box-last"  style="background: #52b0be;">
			<div class="box-inner clearfix">
				<div class="icon"><img style="width:45px;height:45px;" src="<?= $this->getRessource('img/icons/font-icon.png')?>" alt="Font" /></div>
				<h3><strong>Google Fonts</strong></h3>
				<div class="seperator"></div>
				<p>We love Fonts.  Choose from the Google font library to suit your needs.</p>
				<a class="link-moreinfo" href="#">more Info</a>
			</div>
		</div>
		<div class="clear"></div>

		<!-- <div class="highlight-field highlight-grey-bordered clearfix">
			<div class="highlight-content left-float">
				<h2 class="colored"><strong>Yalu Html Template for only 15$</strong></h2>
				<h6 class="subtitle">Yalu is the perfect Multi-Pupose Template</h6>
			</div>

			<div class="highlight-button right-float">
				<a class="y-button default-button1 medium-button" href="http://themeforest.net/item/yalu-creative-multipurpose-template-html/4569177">Buy this Template</a>
			</div>
		</div> -->

		<div class="column one-half">
			<h4 class="title"><span><strong>Seneste nyt</strong></span></h4>
			<? if($this->posts->hasRows()) : ?>
			<div id="latest-blog">
				<? foreach($this->posts->getRows() as $post) : ?>
				<div class="blog-entry blog-latest-entry clearfix">
					<div class="blog-left">
						<div class="blog-date"><span class="day"><?= date('d', strtotime($post->getPubDate()));?></span>
						<span class="month"><?= date('m', strtotime($post->getPubDate()));?></span></div>
					</div>

					<div class="blog-right">
						<div class="blog-headline">
							<h3><a href="<?= $post->getSlug();?>"><strong><?= $post->getTitle(); ?></strong></a></h3>
							<ul class="blog-meta clearfix">
								<li class="meta-author">Af Simon Sessingø</li>
								<li class="meta-category"><?= $post->getParent()->getTitle(); ?></li>
								<li class="meta-category"><?= date('d-m-Y H:i', strtotime($post->getPubDate()));?></li>
							</ul>
						</div>
						<div class="blog-intro">
							<p>
								<?= nl2br(\Pecee\Str::SubStr($post->getData('teaser'), 110), TRUE); ?>
							</p>
						</div>
						<div class="blog-readmore"><a class="link-moreinfo" href="<?= $post->getSlug();?>">Læs mere</a></div>
					</div>
				</div> <!-- END .blog-entry -->
				<? endforeach; ?>
			</div>
			<? endif; ?>
		</div> <!-- END .one-half -->

		<div class="column one-half last-col">
			<h4 class="title"><span><strong>Hvad kan vi</strong></span></h4>

			<div class="accordion">
				<div class="toggle">
					<div class="toggle-title clearfix">
						<div class="toggle-icon"><span></span></div>
						<div class="toggle-name">
							Vi udvikler løsninger på kryds af platforme
						</div>
					</div>
					<div class="toggle-inner">
						Hva end du ønsker at dit næste projekt skal være på iPhone, Android eller web
						står vi klar med en løsning der matcher netop dit behov.
					</div>
				</div>

				<div class="toggle">
					<div class="toggle-title clearfix">
						<div class="toggle-icon"><span></span></div>
						<div class="toggle-name">Integrationer med de nyeste teknologier</div>
					</div>
					<div class="toggle-inner">
						Vi forstår at udnytte potentialet med de nyeste teknologier. Om du ønsker en
						Facebook, Twitter eller Google+ integration, bygger vi skræddersyede løsninger
						der bruger de nyeste teknologier.
					</div>
				</div>

				<div class="toggle">
					<div class="toggle-title clearfix">
						<div class="toggle-icon"><span></span></div>
						<div class="toggle-name">Vores CMS kan ALT</div>
					</div>
					<div class="toggle-inner">
						Det er slut med at gå på kompromis med funktionalitet. Vi har bygget et CMS-system
						der er baseret på verdens mest anvendte Content Management System, Umbraco.
						Så det er kun fantasien der sætter grænser.
					</div>
				</div>
			</div>

		</div> <!-- END .one-half -->

		<div class="clear"></div>

	</div> <!-- END .main-inner -->

</section>
<!-- END #main  -->