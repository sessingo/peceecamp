<? /* @var $this \CMS\DefaultTheme\Widget\Contact */ ?>

<section id="main">
	<div id="map" class="header-map" style="height: 400px;"></div>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript">
		function mapinitialize() {
			var latlng = new google.maps.LatLng(55.70539,12.527118);
			var myOptions = {
				zoom: 18,
				center: latlng,
				scrollwheel: true,
				scaleControl: false,
				disableDefaultUI: false,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			var map = new google.maps.Map(document.getElementById("map"),myOptions);

			var image = "/plugin/CMS/theme/DefaultTheme/img/map-pin.png";
			var marker = new google.maps.Marker({
				map: map,
				icon: image,
				position: map.getCenter()
			});

			var contentString = '<b>Pecee</b><br/>Theklavej 29B<br/>2400 København NV';
			var infowindow = new google.maps.InfoWindow({
				content: contentString
			});

			google.maps.event.addListener(marker, 'click', function() {
			  infowindow.open(map,marker);
			});


		}
		mapinitialize();
	</script>
	<div class="main-inner wrapper clearfix">
		<div class="column two-third">
			<h4 class="title">
				<span><strong>Kontakt os</strong></span>
			</h4>

			<?= $this->form()->start('contact'); ?>
				<?= $this->showFlash(); ?>
				<div class="form-row clearfix">
					<label for="name" class="req">Navn *</label>
					<div class="form-value">
						<?= $this->form()->input('name', 'text'); ?>
					</div>
				</div>

				<div class="form-row clearfix">
					<label for="email" class="req">E-mail *</label>
					<div class="form-value">
						<?= $this->form()->input('email', 'text'); ?>
					</div>
				</div>

				<div class="form-row clearfix textbox">
					<label for="message" class="req">Besked *</label>
					<div class="form-value">
						<?= $this->form()->textarea('message', 15, 50)?>
					</div>
				</div>

				<div class="form-row form-submit">
					<?= $this->form()->submit('submit', 'Send');?>
				</div>
			<?= $this->form()->end(); ?>
		</div>
		<!-- END .column -->

		<div class="column one-third last-col">
			<h4 class="title">
				<span><strong>Office</strong></span>
			</h4>
			<p>
				<strong>Pecee</strong><br/>
				Theklavej 29b<br/>
				2400 København NV<br/>
				<br/> <strong>Telefon</strong>: 60 84 76 79<br/>
			</p>
			<br/>
			<br/>
		</div>
		<!-- END .column -->

		<div class="clear"></div>

	</div>
	<!-- END .main-inner -->

</section>
<!-- END #main  -->