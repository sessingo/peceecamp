<? /* @var $this \CMS\DefaultTheme\Widget\Blog\Post */ ?>

<section id="main">
	
	<div id="page-title">

		<div class="wrapper clearfix">
			<div class="title-name left-float">
				<h3><strong><?= $this->page->getTitle();?></strong></h3>
				<ul class="blog-meta clearfix">
					<li class="meta-author">Af Simon Sessingø</li>
					<li class="meta-category"><?= $this->page->getParent()->getTitle(); ?></li>
					<li class="meta-category"><?= date('d-m-Y H:i', strtotime($this->page->getPubDate()));?></li>
				</ul>
			</div>
		</div>
	</div>
	
	<div class="main-inner wrapper clearfix">
		
		<div class="main-content left-float">
			<div id="blog-single" class="clearfix">		
				<div class="entry-content blog-content clearfix">
					<div class="blog-text">
						<p style="font-weight:bold;"><?= $this->page->getData('teaser'); ?></p>
						<?= $this->page->getData('content');?>
					</div>
				</div> <!-- END .entry-content -->
			</div>
		</div> <!-- END .main-content -->
		
		<aside class="right-float">
		
			<div class="widget">
				<h4 class="title"><span><strong>Kategorier</strong></span></h4>
				<ul class="list-arrow">
					<li><a href="/blog">Alle</a></li>
					<? if($this->categories->hasRows()) : ?>
						<? foreach($this->categories->getRows() as $category) : ?>
							<li><a href="<?= $category->getSlug(); ?>"><?= $category->getTitle();?></a></li>
						<? endforeach; ?>
					<? endif; ?>
				</ul>
			</div>
		
			<?= $this->getData('sidebarContent'); ?>	
		</aside>
		
	</div> <!-- END .main-inner -->
		
</section> <!-- END #main  -->