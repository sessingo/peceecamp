<? /* @var $this \CMS\DefaultTheme\Widget\Blog\Home */ ?>

<section id="main">
	<div id="page-title">
		<div class="wrapper clearfix">
			<div class="title-name left-float">
				<h3><strong>Blog</strong></h3>
				<h6 class="subtitle">De seneste nyheder</h6>
			</div>

			<div class="breadcrumb right-float">
				Hjem &raquo; Blog
			</div>

		</div>
	</div> <!-- END .slider -->

	<div class="main-inner wrapper clearfix">

		<div class="main-content left-float">
			<? if($this->posts->hasRows()) : ?>
			<div id="blog-entries" class="clearfix">
				<? foreach($this->posts->getRows() as $post) : ?>
				<div class="blog-entry clearfix">
					<div class="blog-left">
						<div class="blog-date"><span class="day"><?= date('d', strtotime($post->getPubDate()));?></span>
						<span class="month"><?= \CMS\DefaultTheme\Helper::GetMonth($post->getPubDate());?></span></div>
					</div>
					<div class="blog-right">
						<div class="blog-headline">
							<h3><a href="<?= $post->getSlug();?>"><strong><?= $post->getTitle(); ?></strong></a></h3>
							<ul class="blog-meta clearfix">
								<li class="meta-author">Af Simon Sessingø</li>
								<li class="meta-category"><?= $post->getParent()->getTitle(); ?></li>
								<li class="meta-category"><?= date('d-m-Y H:i', strtotime($post->getPubDate()));?></li>
							</ul>
						</div>
						<div class="blog-intro">
							<p>
								<?= nl2br($post->getData('teaser'), TRUE); ?>
							</p>
						</div>
						<div class="blog-readmore"><a class="link-moreinfo" href="<?= $post->getSlug();?>">Læs mere</a></div>
					</div>
				</div>
				<? endforeach; ?>
				<ul id="entries-pagination" class="clearfix">
					<? if($this->pageIndex > 0) : ?>
						<li class="prev"><a href="<?= \PC\Router::GetRoute(NULL, NULL, NULL, array('page' => ($this->pageIndex-1)))?>">Forrige</a></li>
					<? endif; ?>
					<? if($this->page->getMaxPages() > ($this->pageIndex+1)) : ?>
						<li class="next"><a href="<?= \PC\Router::GetRoute(NULL, NULL, NULL, array('page' => ($this->pageIndex+1)));?>">Næste</a></li>
					<? endif; ?>
				</ul> <!-- END #entries-pagination -->

			</div>  <!-- END #blog-entries -->
			<? endif; ?>
		</div> <!-- END .main-content -->

		<aside class="right-float">

			<div class="widget">
				<h4 class="title"><span><strong>Kategorier</strong></span></h4>
				<ul class="list-arrow">
					<li><a href="/blog">Alle</a></li>
					<? if($this->categories->hasRows()) : ?>
						<? foreach($this->categories->getRows() as $category) : ?>
							<li><a href="<?= $category->getSlug(); ?>"><?= $category->getTitle();?></a></li>
						<? endforeach; ?>
					<? endif; ?>
				</ul>
			</div>


			<?= $this->getData('sidebarContent'); ?>
		</aside>

	</div> <!-- END .main-inner -->

</section> <!-- END #main  -->