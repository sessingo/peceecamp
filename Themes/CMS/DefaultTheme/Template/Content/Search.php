<? /* @var $this \CMS\DefaultTheme\Widget\Search */ ?>

<div class="wrapper">
	<section class="search">
		
		<h3 class="title"><span><strong>Søg</strong></span></h3>
		

		<?= $this->form()->start('search', 'get'); ?>
			<div class="column four-fifth">
				<?= $this->form()->input('query', 'text', $this->query)
					->addAttribute('autocomplete','off')
					->addAttribute('placeholder', 'Indtast søgeord...')
					->addAttribute('style', 'width:100%;');?>
			</div>
			<div class="column one-fifth last-col">
				<?= $this->form()->submit('submit', 'Søg')->addAttribute('style', 'width:100%;'); ?>
			</div>
		<?= $this->form()->end();?>
		<div class="column one-full results">
			<? if($this->query) : ?>
				<? if(!$this->nodes || !$this->nodes->hasRows()) : ?>
					<? if(!$this->zip) : ?>
					<div class="no-results">
						<h3>Ingen sider fundet med "<?= $this->query; ?>"</h3>
					</div>
					<? endif; ?>
				<? else: ?>
					<ul>
						<? foreach($this->nodes->getRows() as $node) : ?>
						<li>
							<h3><a href="<?= $node->getSlug(); ?>" title="<?= $node->getMetaDescription(); ?>"><?= $node->getTitle(); ?></a></h3>
							<p>
								<?= \Pecee\Str::SubStr(strip_tags($node->getData('content')), 250);?>
							</p>
							
							<div class="info">
								<a href="<?= $node->getSlug(); ?>" title="<?= $node->getMetaDescription(); ?>" class="url">pecee.dk<?= $node->getSlug();?></a>
							</div>
						</li>
						<? endforeach;?>
					</ul>
					<ul id="entries-pagination" class="clearfix">
						<? if($this->pageIndex > 0) : ?>
							<li class="prev"><a href="<?= \PC\Router::GetRoute(NULL, NULL, NULL, array('page' => ($this->pageIndex-1)))?>">Forrige</a></li>
						<? endif; ?>
						<? if($this->page->getMaxPages() > ($this->pageIndex+1)) : ?>
							<li class="next"><a href="<?= \PC\Router::GetRoute(NULL, NULL, NULL, array('page' => ($this->pageIndex+1)));?>">Næste</a></li>
						<? endif; ?>
					</ul>
				<? endif; ?>
			<? endif; ?>
		</div>
		
	</section>
</div>