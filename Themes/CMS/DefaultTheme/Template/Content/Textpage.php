<? /* @var $this \CMS\DefaultTheme\Widget\Textpage */ ?>

<section id="main">

	<div id="page-title">

		<div class="wrapper clearfix">
			<div class="title-name left-float">
				<h3><strong><?= $this->page->getTitle(); ?></strong></h3>
				<h6 class="subtitle"><?= $this->page->getData('subHeader'); ?></h6>
			</div>

			<div class="breadcrumb right-float">
				<?= join(' &raquo; ', $this->getLevel()); ?>
			</div>

		</div>

	</div> <!-- END .slider -->

	<div class="main-inner wrapper clearfix">

		<div class="main-content right-float">
			<div class="column one-full">
				<?= $this->getData('content');?>
			</div>
		</div> <!-- END .main-content -->

		<aside class="left-float">

			<div class="widget">
				<?php /*<h4 class="title"><span><strong><?= $this->pageSubMenuTitle; ?></strong></span></h4> */ ?>
				<?= $this->pageSubMenu; ?>
			</div>

		</aside>

	</div> <!-- END .main-inner -->

</section> <!-- END #main  -->