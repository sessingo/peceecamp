<? /* @var $this \CMS\DefaultTheme\Widget\Snippet\Slider */ ?>
<? if($this->slides && $this->slides->hasRows()) : ?>
<!-- REVOLUTION SLIDER -->
<div id="main-slider" class="fullwidthbanner-container" style="overflow:hidden;">
	<div class="fullwidthbanner">
		<ul style="visibility:hidden;">
			<? foreach($this->slides->getRows() as $slide) : ?>
				<? if(strtolower($slide->getData('styling')) == 'right') : ?>
					<li data-transition="fade" data-masterspeed="600">
						<img src="<?= $this->getFile($slide->getData('slideImage'))->getThumbnail('slider'); ?>" alt="">

						<!-- THE CAPTIONS IN THIS SLDIE -->
						<div class="caption sfr yalu-text-h2" data-x="530" data-y="130" data-speed="800" data-start="100" data-easing="easeOutExpo">
							<?= $slide->getData('slideHeader1'); ?>
						</div>

						<div class="caption sfr yalu-text-h7" data-x="530" data-y="220" data-speed="800" data-start="600" data-easing="easeOutExpo">
							<?= $slide->getData('slideHeader2'); ?>
						</div>

						<div class="caption sfb" data-x="530" data-y="310" data-speed="400" data-start="1000" data-easing="easeOutExpo">
							<?= $slide->getData('slideLink'); ?>
						</div>
					</li>
				<? else: ?>

					<li data-transition="fade" data-masterspeed="600">
						<img src="<?= $this->getFile($slide->getData('slideImage'))->getThumbnail('slider'); ?>" alt="">

						<!-- THE CAPTIONS IN THIS SLDIE -->
						<div class="caption sfl yalu-text-h2" data-x="15" data-y="130" data-speed="800" data-start="200" data-easing="easeOutExpo">
						 	<?= $slide->getData('slideHeader1'); ?>
						 </div>

						<div class="caption sfl yalu-text-h6" data-x="15" data-y="220" data-speed="800" data-start="600" data-easing="easeOutExpo">
							 <?= $slide->getData('slideHeader2'); ?>
						</div>

						<div class="caption sfb" data-x="15" data-y="320" data-speed="400" data-start="1000" data-easing="easeOutExpo">
							<?= $slide->getData('slideLink'); ?>
						</div>
					</li>
				<? endif; ?>
			<? endforeach; ?>
		</ul>
	</div>
</div> <!-- END #main-slider -->
<!-- REVOLUTION SLIDER -->

<script type="text/javascript">

	/*----------------------------------------------
		   R E V O L U T I O N   S L I D E R
	------------------------------------------------*/
	$(document).ready(function() {
		$('.fullwidthbanner').revolution(
		{
			delay:9000,
			startheight:500,
			startwidth:940,

			hideThumbs:10,

			thumbWidth:100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
			thumbHeight:50,
			thumbAmount:5,

			navigationType:"none",				// bullet, thumb, none
			navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none

			navigationStyle:"custom",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom

			navigationHAlign:"center",				// Vertical Align top,center,bottom
			navigationVAlign:"bottom",				// Horizontal Align left,center,right
			navigationHOffset:0,
			navigationVOffset:20,

			soloArrowLeftHalign:"left",
			soloArrowLeftValign:"center",
			soloArrowLeftHOffset:0,
			soloArrowLeftVOffset:0,

			soloArrowRightHalign:"right",
			soloArrowRightValign:"center",
			soloArrowRightHOffset:0,
			soloArrowRightVOffset:0,

			touchenabled:"on",						// Enable Swipe Function : on/off
			onHoverStop:"on",						// Stop Banner Timet at Hover on Slide on/off

			stopAtSlide:-1,
			stopAfterLoops:-1,

			shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
			fullWidth:"on"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
		});
	});
</script>
<? endif; ?>