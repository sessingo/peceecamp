<? /* @var $this \CMS\DefaultTheme\Widget\Snippet\TagCloud */ ?>
<? if(count($this->tags) > 0) : ?>
<div class="widget">
	<h4 class="title"><strong>Tags</strong></h4>
	<div class="tag-list">
		<? foreach(array_keys($this->tags) as $tag) : ?>
		<a href="<?= $this->searchPage->getSlug()?>?query=<?= $tag; ?>" title="Søg efter: <?= $tag;?>"><?= $tag;?></a> 
		<? endforeach;?>
	</div>
</div>
<? endif; ?>