<? /* @var $this \CMS\DefaultTheme\Widget\Macro\Textbox */ ?>

<div class="widget">
	<h4 class="title"><span><strong><?= $this->title; ?></strong></span></h4>
	<p><?= $this->content; ?></p>
</div>