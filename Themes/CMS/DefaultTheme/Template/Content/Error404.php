<? /* @var $this \CMS\DefaultTheme\Widget\Error404 */ ?>

<section id="main">
	<div id="page-title">
		<div class="wrapper clearfix">
			<div class="title-name left-float">
				<h3><strong>Siden findes ikke</strong></h3>
			</div>
		</div>
	</div> <!-- END .slider -->
	
	<div class="main-inner wrapper clearfix">
		<div id="notfound-404">
			<h2><strong>404</strong></h2>
			<h4 class="subtitle">Siden du leder efter findes ikke.</h4>
		</div>
	</div> <!-- END .main-inner -->
</section> <!-- END #main  -->