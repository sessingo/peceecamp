<? /* @var $this \CMS\DefaultTheme\Widget\WidgetAbstract */
echo $this->getSite()->getDocType(); ?>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="da-DK"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="da-DK"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="da-DK"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="da-DK"> <!--<![endif]-->
	<head>
		<?= $this->printHeader(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

		<link rel="stylesheet" id="default-style-css"  href="<?= $this->getRessource('css/style.css');?>" type="text/css" media="all" />
		<link rel="stylesheet" id="fancybox-style-css"  href="<?= $this->getRessource('css/fancybox.css');?>" type="text/css" media="all" />
		<link rel="stylesheet" id="flexslider-style-css"  href="<?= $this->getRessource('css/flexslider.css');?>" type="text/css" media="all" />
		<link rel="stylesheet" id="jplayer-style-css"  href="<?= $this->getRessource('data/jplayer/jplayer.css');?>" type="text/css" media="all" />
		<link rel="stylesheet" id="isotope-style-css"  href="<?= $this->getRessource('css/isotope.css');?>" type="text/css" media="all" />
		<link rel="stylesheet" id="rs-style-css" href="<?= $this->getRessource('data/rs-plugin/css/settings.css');?>" type="text/css"  media="all" />
		<link rel="stylesheet" id="mqueries-style-css"  href="<?= $this->getRessource('css/mqueries.css');?>" type="text/css" media="all" />

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="<?= $this->getRessource('js/jquery.modernizr.min.js')?>"></script>

	</head>
	<body>
		<div id="page-content">
			<header>
			  <div class="header-inner wrapper clearfix">
		       	<div id="logo" class="left-float">
		            	<a href="/" class="logotype"><img src="<?= $this->getRessource('img/logo.png')?>" alt="Logo"></a>
		          </div>

		          <div class="menu right-float clearfix">
					  <nav id="main-nav">
							<?= $this->menu;?>
		           	  </nav>
		          </div>

		        </div>
		    </header>
		    <!-- END header -->

			<?= $this->getContentHtml(); ?>

			<footer>
		    	<div class="footer-inner wrapper clearfix">

		        	<div class="column one-fourth">
		            	<?= $this->getData('footerText');?>
		            </div>

		            <div class="column one-fourth">
		            	<?= $this->widget(new \CMS\DefaultTheme\Widget\Snippet\TagCloud()); ?>
		          	</div>
		            <div class="column one-fourth">
		            	<div class="widget">

		            	</div>
		          </div>
		            <div class="column one-fourth last-col">
		            	<div class="widget">
		            		<h4 class="title"><strong>Søg</strong></h4>
		                    <form role="search" method="get" id="searchform" action="<?= $this->searchPage->getSlug()?>" >
		                        <div><label class="screen-reader-text" for="s">Search for:</label>
		                        <input type="text" value="" name="query" id="s" />
		                        <input type="submit" id="searchsubmit" value="Search" />
		                        </div>
		                    </form>
		            	</div>
		            </div>
		        </div>

		        <div class="copyright">
		        	<div class="wrapper clearfix">
		                <div class="left-float">&copy; <?= date('Y'); ?> Pecee</div>
		                <div class="right-float"></div>
		            </div>
		        </div>
		    </footer> <!-- END #footer -->

		</div> <!-- END #page -->

		<script type="text/javascript" src="<?= $this->getRessource('js/jquery.easing.1.3.js')?>"></script>
		<script type="text/javascript" src="<?= $this->getRessource('js/jquery.easing.compatibility.js')?>"></script>
		<script type="text/javascript" src="<?= $this->getRessource('js/jquery.fancybox.pack.js')?>"></script>
		<script type="text/javascript" src="<?= $this->getRessource('js/jquery.flexslider-min.js')?>"></script>
		<script type="text/javascript" src="<?= $this->getRessource('data/jplayer/jquery.jplayer.min.js')?>"></script>
		<script type="text/javascript" src="<?= $this->getRessource('js/jquery.isotope.min.js')?>"></script>
		<script type="text/javascript" src="<?= $this->getRessource('data/rs-plugin/js/jquery.themepunch.plugins.min.js')?>"></script>
		<script type="text/javascript" src="<?= $this->getRessource('data/rs-plugin/js/jquery.themepunch.revolution.min.js')?>"></script>
		<script type="text/javascript" src="<?= $this->getRessource('js/script.js')?>"></script>
	</body>
</html>
