<?php
namespace CMS\DefaultTheme\Model;
class ModelExample extends \Pecee\Model\Model {

	public function __construct() {
		/*
		 * Here we tell the model what table we are using
		 * and what default values the columns have.
		 */
		parent::__construct('ExampleTable', array('ExampleTableID' => NULL, 'Name' => '', 'CreatedDate' => time()));
	}

	public function getCurrentTime() {
		return time();
	}

	public function save() {
		// Do stuff before saving
		parent::save();
	}

	public function delete() {
		// Do stuff before deleting
		parent::delete();
	}

	public function update() {
		// Do stuff before deleting
		parent::update();
	}


	/**
	 * This method will execute the sql query and fetch all items from the
	 * query. Please look in the Example controller to see how the object is
	 * used. The object will have all fieldtables automacticlly like ->getName() or ->name;
	 *
	 * @param integer $id
	 */
	public static function GetById($id) {
		return self::FetchAll('SELECT * FROM `ExampleTable` WHERE `ExampleTableID` = %s', $id);
	}
}