<?php
namespace CMS\DefaultTheme\Controller;
class ControllerExample extends \PC\Controller\ControllerAbstract {

	/**
	 * NOTE: The default name of the controller is Default.
	 * This is used to simulate an index controller
	 */

	/**
	 * Method called indexView will allways be executed
	 * when no method is defined in the request.
	 *
	 * For example: /example will execute this method
	 */
	public function indexView() {
		echo 'Hello world!';
	}

	/**
	 * This method will be executed when the
	 * request is coming from /example/getpersons.json
	 *
	 * The method will handle headsers automaticlly and as long
	 * as you remember to use $this->asJson() the controller
	 * will always output the result as json.
	 */
	public function getPersonsJson() {
		$result = array('Persons' => array('Jakob', 'Simon', 'Aija'));
		$this->asJSON($result);
	}

	/**
	 * This method will be executed when the
	 * request is coming from /example/animals/
	 *
	 * See the params? If the param is set to be nullable
	 * the param is optional and the controller will not
	 * fail if no param is delivered from the request
	 */
	public function animalsView($requiredParam, $optionalParam = NULL) {
		$this->renderAnimals($requiredParam);
	}

	/**
	 * Protected and private methods cannot be accessed
	 * via any request. Use them to seperate functionality
	 * so you can easily extend the controller and overwrite
	 * the protected methods.
	 */
	protected function renderAnimals($index, $optionalParam = NULL) {
		$animals = array('dog','horse','rabbit','cat','unicorn');
		return (isset($animals[$index]) ? $animals[$index] : 'Animal not found!') . ' - optional param was: ' . $optionalParam;
	}

	protected function modelExample() {
		/**
		 * We call our static method which will return \Pecee\Model\Model
		 * which basiclly is a collection of \CMS\DefaultTheme\Model\ModelExample with
		 * more information like the sql-query etc.
		 */
		$items = \CMS\DefaultTheme\Model\ModelExample::GetById(12);

		// die(var_dump($items)); // This will give you an idea about what the object is capable off.

		// Check if theres any items
		if($items->hasRow()) {
			/* @var $item \CMS\DefaultTheme\Model\ModelExample */
			foreach($items as $item) {
				echo $item->getCurrentTime(); // will output current time
				echo $item->getTitle(); // Will output the title

				// Edit example
				$item->setCreatedDate(time());
				$item->update();
			}
		}


		// Saving new item example
		$item = new \CMS\DefaultTheme\Model\ModelExample();
		$item->setName('MyName');
		$item->save();
	}
}