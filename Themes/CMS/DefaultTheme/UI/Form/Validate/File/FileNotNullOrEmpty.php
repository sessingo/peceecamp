<?php
namespace CMS\DefaultTheme\UI\Form\Validate\File;
class FileNotNullOrEmpty extends \Pecee\UI\Form\Validate\ValidateFile {
	protected $msg;
	public function __construct($msg) {
		$this->msg = $msg;
	}

	public function validate() {
		return (!empty($this->fileName) && $this->fileSize > 0 && $this->fileError == 0);
	}
	public function getErrorMessage() {
		return $this->msg;
	}
}