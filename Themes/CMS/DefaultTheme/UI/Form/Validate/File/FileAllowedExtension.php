<?php
namespace CMS\DefaultTheme\UI\Form\Validate\File;
class FileAllowedExtension extends \Pecee\UI\Form\Validate\ValidateFile {
	protected $extensions;
	protected $msg;

	public function __construct($msg, array $extensions) {
		$this->msg = $msg;
		$this->extensions=$extensions;
	}

	public function validate() {
		$ext=\Pecee\IO\File::GetExtension($this->fileName);
		return (in_array($ext, $this->extensions));
	}

	public function getErrorMessage() {
		return $this->msg;
	}
}