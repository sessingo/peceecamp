<?php
namespace CMS\DefaultTheme\UI\Form\Validate;
class ValidateNotNullOrEmpty extends \Pecee\UI\Form\Validate\ValidateInput {
	protected $msg;
	public function __construct($msg) {
		$this->msg=$msg;
	}

	public function validate() {
		return (!empty($this->value));
	}
	public function getErrorMessage() {
		return $this->msg;
	}
}