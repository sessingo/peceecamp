<?php
namespace CMS\DefaultTheme\UI\Form\Validate;
class ValidateEmail extends \Pecee\UI\Form\Validate\ValidateInput {
	protected $msg;
	public function __construct($msg) {
		$this->msg = $msg;
	}
	public function validate() {
		return (\Pecee\Util::is_email($this->value));
	}
	public function getErrorMessage() {
		return $this->msg;
	}
}