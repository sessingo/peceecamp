<?php
namespace CMS\DefaultTheme\UI\Form\Validate;
class ValidateInteger extends \Pecee\UI\Form\Validate\ValidateInput {
	protected $msg;
	public function __construct($msg=NULL) {
		$this->msg=$msg;
	}

	public function validate() {
		return (\Pecee\Integer::is_int($this->value));
	}
	public function getErrorMessage() {
		return $this->msg;
	}
}