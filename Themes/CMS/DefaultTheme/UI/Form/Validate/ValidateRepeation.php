<?php
namespace CMS\DefaultTheme\UI\Form\Validate;
class ValidateRepeation extends \Pecee\UI\Form\Validate\ValidateInput {
	protected $compareValue;
	protected $caseSensitive;
	protected $msg;

	public function __construct($msg, $compareValue, $caseSensitive = true) {
		$this->compareValue = $compareValue;
		$this->caseSensitive = $caseSensitive;
		$this->msg = $msg;
	}
	public function validate() {
		if(!$this->caseSensitive) {
			return ((bool)strtolower($this->compareValue) == strtolower($this->value));
		}
		return ((bool)$this->compareValue == $this->value);
	}
	public function getErrorMessage() {
		return $this->msg;;
	}
}