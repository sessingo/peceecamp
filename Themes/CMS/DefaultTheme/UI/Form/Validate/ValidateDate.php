<?php
namespace CMS\DefaultTheme\UI\Form\Validate;
class ValidateDate extends \Pecee\UI\Form\Validate\ValidateInput {
	protected $allowEmpty;
	protected $msg;
	public function __construct($msg,$allowEmpty=false) {
		$this->allowEmpty=$allowEmpty;
		$this->msg=$msg;
	}

	public function validate() {
		return ($this->allowEmpty && empty($this->value) || \Pecee\Date::IsValid($this->value));
	}
	public function getErrorMessage() {
		return $this->msg;
	}
}