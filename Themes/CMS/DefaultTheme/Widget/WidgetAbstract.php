<?php
namespace CMS\DefaultTheme\Widget;
abstract class WidgetAbstract extends \CMS\Widget\WidgetAbstract {
	protected $menu;
	protected $searchPage;
	public function __construct() {
		parent::__construct();

		$this->getSite()->setTitle('PeceeCamp');
		$this->searchPage = \CMS\Model\ModelPage::GetByPropertyAlias('searchpage')->getFirstOrDefault();
	}

	protected function prependSiteTitle($title) {
		parent::prependSiteTitle($title, ' | ');
	}

	public function renderPage() {
		$this->menu = new \Pecee\UI\Menu\Menu();
		$this->menu->addClass('clearfix');
		$childs = \CMS\Model\ModelPage::GetByPropertyAlias('site')->getFirstOrDefault()->getChilds(TRUE);
		$hasActive = FALSE;
		if($childs->hasRows()) {
			foreach($childs->getRows() as $child) {
				if($child->getVisibleInMenu()) {
					$this->menu->addItem($child->getTitle(), $child->getSlug(), $child->getMetaDescription());

					if(strstr(\PC\Router::GetCurrentRoute(TRUE,FALSE), $child->getSlug()) !== FALSE)  {
						$this->menu->getLast()->addClass('current-menu-item');
					}
				}
			}
		}

		if(\PC\Router::GetCurrentRoute(TRUE,FALSE) == '/') {
			$this->menu->getFirst()->addClass('current-menu-item');
		}
	}

	public function showFlash($formName=NULL) {
		$o=$this->showMessages('error', $formName);
		$o.=$this->showMessages('warning', $formName);
		$o.=$this->showMessages('info', $formName);
		$o.=$this->showMessages('success', $formName);
		return $o;
	}

	public function showMessages($type, $formName = NULL) {
		if($this->hasMessages($type)) {
			$class=array('info' => 'alert-info', 'note' => 'alert-note', 'error' => 'alert-error', 'success' => 'alert-confirm');
			$o = sprintf('<div class="alert %s"><div class="alert-icon"><span class="icon"></span></div><div class="alert-message">', $class[$type]);
			$msg=array();
			/* @var $error \Pecee\UI\Form\FormMessage */
			foreach($this->getMessages($type) as $error) {
				if(is_null($formName) || $error->getForm() == $formName) {
					$msg[] = sprintf('%s', $error->getMessage());
				}
			}

			if(count($msg) > 0) {
				$o .= join('<br/>', $msg) . '</div><a href="#" class="alert-close">x</a></div>';
				return $o;
			}
		}
		return '';
	}
}