<?php
namespace CMS\DefaultTheme\Widget;
class Search extends \CMS\DefaultTheme\Widget\WidgetAbstract {
	protected $zip;
	protected $city;
	protected $query;
	protected $nodes;
	protected $pageIndex;
	public function __construct() {
		parent::__construct();
		$this->getSite()->addWrappedCss('search.css');

		$this->query = $this->getParam('query');
		$this->pageIndex = $this->getParam('page', 0);

		if($this->query) {
			$this->nodes = \CMS\Model\ModelPage::Get(NULL, NULL, TRUE)->where(array('Title', 'content'), $this->query, '*')->skip(5*$this->pageIndex)->limit(5);
		}
	}

	public function renderPage() {
		parent::renderPage();
		$this->prependSiteTitle('Søgning efter ' . $this->query . ' - ');
	}

	public function onSiteRender() {

	}
}