<?php
namespace CMS\DefaultTheme\Widget;
class Error404 extends \CMS\DefaultTheme\Widget\WidgetAbstract {
	public function __construct() {
		parent::__construct();
		$this->prependSiteTitle('Siden blev ikke fundet');
	}

	public function onSiteRender() {

	}
}