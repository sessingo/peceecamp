<?php
namespace CMS\DefaultTheme\Widget\Snippet;
use Pecee\Str;

class TagCloud extends \CMS\DefaultTheme\Widget\Snippet\SnippetAbstract {
	protected $tags;
	protected $searchPage;
	public function __construct($limit = 20, $ignoreLevel = 5) {
		parent::__construct();
		$this->tags=array();
		$this->searchPage = \CMS\Model\ModelPage::GetByPropertyAlias('searchpage')->getFirstOrDefault();
		$posts = \CMS\Model\ModelPage::GetByPropertyAlias('blogPost');

		$words = array();
		if($posts->hasRows()) {
			foreach($posts->getRows() as $post) {
				/*foreach(\Pecee\Str::ParseTagString(strip_tags($post->getData('teaser'))) as $word) {
					$words[] = $word;
				}*/

				foreach(Str::ParseTagString(strip_tags($post->getData('content'))) as $word) {
					$words[] = $word;
				}
			}
		}

		if(count($words) > 0) {
			foreach($words as $word) {
				if(strlen($word) > 5) {
					if(isset($this->tags[strtolower($word)])) {
						$this->tags[strtolower($word)] = $this->tags[strtolower($word)]+1;
					} else {
						$this->tags[strtolower($word)] = 1;
					}
				}
			}
		}

		ksort($this->tags);

		if(count($this->tags) > $limit) {
			$this->tags = array_slice($this->tags, $ignoreLevel, $limit);
		}
	}

	public function setSize($size) {
		$this->size = $size;
	}
}