<?php
namespace CMS\DefaultTheme\Widget\Snippet;
abstract class SnippetAbstract extends \CMS\Widget\WidgetAbstract {
	public function __construct() {
		parent::__construct();
		$this->setTemplate(NULL);
	}

	public function setPage() {

	}

	public function renderPage() {

	}

	public function onSiteRender() {

	}
}