<?php
namespace CMS\DefaultTheme\Widget\Snippet;
class Slider extends \CMS\DefaultTheme\Widget\Snippet\SnippetAbstract {
	protected $nodeId;
	protected $slides;
	public function __construct($nodeId) {
		parent::__construct();
		$this->nodeId = $nodeId;
		$this->setTemplate(NULL);

		$node = \CMS\Model\ModelPage::GetByNodeID($nodeId);
		if($node->hasRow()) {
			$this->slides = $node->getChildsOfType('slide',TRUE,NULL);
		}
	}

	public function onSiteRender() {

	}
}