<?php
namespace CMS\DefaultTheme\Widget;
class Frontpage extends \CMS\DefaultTheme\Widget\WidgetAbstract {
	protected $posts;
	public function __construct() {
		parent::__construct();
		$this->prependSiteTitle('Forside');
		$this->posts = \CMS\Model\ModelPage::GetByPropertyAlias('blogPost',TRUE)->order('PubDate')->limit(1);
	}

	public function onSiteRender() {

	}

	protected function getImage(\CMS\Model\ModelPage &$case) {
		$imageFolder = $case->getData('images');

		if($imageFolder) {
			$folder = \CMS\Model\ModelFolder::GetByNodeID($imageFolder);
			$images = $folder->getFiles();
			if($images->hasRows()) {
				return $images->getFirstOrDefault()->getThumbnail('small');
			}
		} else {
			return \CMS\Model\ModelFile::GetByNodeID(168)->getThumbnail('small');
		}
	}
}