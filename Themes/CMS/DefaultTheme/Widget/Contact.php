<?php
namespace CMS\DefaultTheme\Widget;
class Contact extends \CMS\DefaultTheme\Widget\WidgetAbstract {
	public function __construct() {
		parent::__construct();
		$this->prependSiteTitle('Kontakt');
	}

	public function onSiteRender() {
		if($this->isPostBack()) {
			$this->addInputValidation('name', new \CMS\DefaultTheme\UI\Form\Validate\ValidateNotNullOrEmpty('Du skal udfylde feltet navn'));
			$this->addInputValidation('email', new\CMS\DefaultTheme\UI\Form\Validate\ValidateEmail('Den indtastede email er ikke gyldig'));
			$this->addInputValidation('message', new \CMS\DefaultTheme\UI\Form\Validate\ValidateNotNullOrEmpty('Besked skal udfyldes'));

			if(!$this->hasErrors()) {
				$email = $this->page->getData('email');
				if($email) {
					\Pecee\Util::SendMail($email, 'Besked fra KBH-Online.dk',
							sprintf("Navn: %s\nEmail:%s\n\nBesked: %s\n",
									$this->data->name,
									$this->data->email,
									$this->data->message), 'noreply@pecee.dk', $this->data->email);
					$this->setMessage($this->_('Din besked er blevet sendt'), 'success');
					\PC\Router::Refresh();
				}
			}
		}
	}
}