<?php
namespace CMS\DefaultTheme\Widget;
class Textpage extends \CMS\DefaultTheme\Widget\WidgetAbstract {
	protected $pageSubMenuTitle;
	protected $pageSubMenu;
	public function __construct() {
		parent::__construct();
	}

	public function onSiteRender() {

		$this->prependSiteTitle($this->page->getTitle());

		$this->pageSubMenu = new \Pecee\UI\Menu\Menu();
		$this->pageSubMenu->addClass('list-arrow');

		$parent = $this->page->getParent();
		if($parent && $parent->hasRow() && $parent->getProperty()->getAlias() == 'page') {
			$this->pageSubMenuTitle = $parent->getTitle();
			$childs = $parent->getChilds();
		} else {
			$this->pageSubMenuTitle = $this->page->getTitle();
			$childs = $this->page->getChilds();
		}

		if($childs && $childs->hasRows()) {
			$menu = new \Pecee\UI\Menu\Menu();
			foreach($childs->getRows() as $child) {

				if(!$child->getData('hideSidebar') && $child->getVisibleInMenu() &&
						in_array($child->getProperty()->getAlias(), array('page', 'contactPage', 'faqPage'))) {
							$this->pageSubMenu->addItem($child->getTitle(), $child->getSlug());

							if(\PC\Router::GetCurrentRoute(TRUE,FALSE) == $child->getSlug())  {
								$this->pageSubMenu->getLast()->addClass('active');
							}
						}
			}
		}
	}

	protected function getLevel() {
		$level = array('<a href="'.$this->page->getSlug().'">'.$this->page->getTitle().'</a>');
		$parent = $this->page->getParent();
		while($parent->hasRow() && $parent->getProperty()->getAlias() == 'page') {
			$level[] = '<a href="'.$parent->getSlug().'">'.$parent->getTitle().'</a>';
			$parent = $parent->getParent();
		}
		return array_reverse($level);
	}
}