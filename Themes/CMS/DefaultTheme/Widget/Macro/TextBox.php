<?php
namespace CMS\DefaultTheme\Widget\Macro;
class TextBox extends \CMS\Widget\Macro\MacroAbstract {
	protected $title;
	protected $content;
	public function __construct() {
		parent::__construct();
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle($title) {
		$this->title = $title;
	}

	public function getContent() {
		return $this->content;
	}

	public function setContent($content) {
		$this->content = $content;
	}

	public function onSiteRender() {

	}

}