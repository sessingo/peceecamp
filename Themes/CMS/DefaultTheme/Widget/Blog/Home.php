<?php
namespace CMS\DefaultTheme\Widget\Blog;
class Home extends \CMS\DefaultTheme\Widget\Blog\BlogAbstract {
	protected $posts;
	protected $pageIndex;
	protected $maxPosts;
	public function __construct() {
		parent::__construct();
		$this->maxPosts =  10;
		$this->pageIndex = $this->getParam('page',0);
	}

	public function onSiteRender() {
		if($this->page->getProperty()->getAlias() == 'blogCategory'){
			$this->posts = $this->page->getChildsOfType('blogPost',TRUE)->order('PubDate')->skip(($this->maxPosts * $this->pageIndex))->limit($this->maxPosts);
			$this->prependSiteTitle('Blog: ' . $this->page->getTitle());
		} else {
			$this->prependSiteTitle('Blog: Alle');
			$this->posts = \CMS\Model\ModelPage::GetByPropertyAlias('blogPost',TRUE)->order('PubDate')->skip(($this->maxPosts * $this->pageIndex))->limit($this->maxPosts);
		}

		$this->posts->setPage($this->pageIndex);
	}
}