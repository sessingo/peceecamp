<?php
namespace CMS\DefaultTheme\Widget\Blog;
class Post extends \CMS\DefaultTheme\Widget\Blog\BlogAbstract {
	public function __construct() {
		parent::__construct();
	}

	public function onSiteRender() {

	}

	protected function setFacebookMeta() {
		if($this->page->getData('image')) {
			$meta = new \Pecee\UI\Html\HtmlMeta('http://' . $_SERVER['HTTP_HOST'] . $this->getFile($this->page->getData('image'))->getUrl());
			$meta->addAttribute('property', 'og:image');
			$this->getSite()->addHeader($meta);
		}

		$this->prependSiteTitle($this->page->getTitle());

		$meta = new \Pecee\UI\Html\HtmlMeta($this->page->getTitle());
		$meta->addAttribute('property', 'og:title');
		$this->getSite()->addHeader($meta);

		if($this->page->getData('teaser')) {
			$meta = new \Pecee\UI\Html\HtmlMeta($this->page->getData('teaser'));
			$meta->addAttribute('property', 'og:description');
			$this->getSite()->addHeader($meta);

			$this->getSite()->setDescription($this->page->getData('teaser'));
		}
	}
}