<?php
namespace CMS\DefaultTheme\Widget\Blog;
abstract class BlogAbstract extends \CMS\DefaultTheme\Widget\WidgetAbstract {
	protected $categories;
	public function __construct() {
		parent::__construct();
		$this->categories = \CMS\Model\ModelPage::GetByPropertyAlias('blogCategory',TRUE);
	}
}