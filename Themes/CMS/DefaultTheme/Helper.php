<?php
namespace CMS\DefaultTheme;
class Helper {

	public static function GetMonth($date) {
		$month = date('j', strtotime($date));
		$map = array('1' => 'Jan', '2' => 'Feb', '3' => 'Mar', '4' => 'Apr', '5' => 'Maj',
						'6' => 'Jun', '7' => 'Jul', '8' => 'Aug', '9' => 'Sep', '10' => 'Okt',
						'11' => 'Nov', '12' => 'Dec');

		return $map[$month];
	}

}