<?php
namespace Pecee\Controller;
abstract class Controller {
	public function __construct() {
		\Pecee\Debug::Instance()->add('START CONTROLLER ' . get_class($this));
		/* Register user activity here, otherwise we can't catch the exception */
		/*if(\Pecee\Model\User\ModelUser::IsLoggedIn()) {
			\Pecee\Model\User\ModelUser::Current()->registerActivity();
		}
		$this->disableAutoMagicQuotes();*/
	}

	/*protected function disableAutoMagicQuotes() {
		if (get_magic_quotes_gpc()) {
			$process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
			while ((list($key, $val) = each($process)) != FALSE) {
				foreach ($val as $k => $v) {
					unset($process[$key][$k]);
					if (is_array($v)) {
						$process[$key][stripslashes($k)] = $v;
						$process[] = &$process[$key][stripslashes($k)];
					} else {
						$process[$key][stripslashes($k)] = stripslashes($v);
					}
				}
			}
			unset($process);
		}
	}*/
	public function hasParam($param) {
		return (isset($_GET[$param]));
	}
	public function getParam($param,$default=null){
		return $this->hasParam($param) ? $_GET[$param] : $default;
	}
	public function isPostback() {
		return \Pecee\UI\ResponseData\ResponseDataPost::IsPostBack();
	}
	public function getPost($index,$default=null) {
		if(isset($_POST[$index])) {
			return $_POST[$index];
		}
		foreach($_POST as $key=>$post) {
			if(strpos($key, '_') > -1) {
				$key = explode('_',$key);
				if($key[1] == $index) {
					return $post;
				}
			}
		}
		return $default;
	}
	public function asJSON(array $array) {
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($array);
		die();
	}
	/**
	 * Translates message
	 * @param string $messageID
	 * @param array $args
	 * @return Language
	 */
	protected function _($key, $args = NULL) {
		if (!is_array($args)) {
			$args = func_get_args();
			$args = array_slice($args, 1);
		}
		return \Pecee\Language::Instance()->_($key, $args);
	}
}