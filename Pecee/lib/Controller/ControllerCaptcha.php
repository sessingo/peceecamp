<?php
namespace Pecee\Controller;
class ControllerCaptcha extends \Pecee\Controller\Controller {
	public function showView($captchaName) {
		if(\Pecee\Session::Instance()->exists($captchaName . '_data')) {
			$captcha = \Pecee\Session::Instance()->get($captchaName . '_data');
			if($captcha instanceof \Pecee\UI\Form\FormCaptcha) {
				$captcha->showCaptcha();
				\Pecee\Session::Instance()->destroy($captchaName . '_data');
			}
		}
	}
}