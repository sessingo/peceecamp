<?php
namespace Pecee\Controller;
class ControllerCss extends \Pecee\Controller\File\ControllerFile {
	public function __construct() {
		parent::__construct(\Pecee\Controller\File\ControllerFile::TYPE_CSS);
	}
}