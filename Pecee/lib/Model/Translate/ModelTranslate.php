<?php
namespace Pecee\Model\Translate;
class ModelTranslate {
	const CACHE_EXPIRE_MINUTES = 500;
	const CACHE_KEY = 'LANG';
	
	private static $instance;
	protected $translated;
	protected $original;
	protected $defaultLocale;
	protected $path;
	protected $cachedEnabled;
	protected $translationMode;
	
	/**
	 * @return \Pecee\Model\Translate\ModelTranslate
	 */
	public static function GetInstance() {
		if(!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	public function __construct() {
		$this->translated=array();
		$this->original=array();
		$this->defaultLocale = \Pecee\Locale::Instance()->getDefaultLocale();
		$this->cachedEnabled = \Pecee\Registry::GetInstance()->get(\Pecee\Language::SETTINGS_CACHE_ENABLE, TRUE);
		$this->path = \Pecee\Router::GetInstance()->getPath(TRUE);
		
		if($this->defaultLocale != \Pecee\Locale::Instance()->getLocale()) {
			/* Restore cache */
			if($this->cachedEnabled) {
				/* @var $cache \Pecee\Model\Translate\ModelTranslateObject */
				$cache = $this->getCache();
				if($cache) {
					$this->original = $cache->original;
					$this->translated = $cache->translated;
				} else {
					$this->getFromDB();
				}
			} else {
				$this->getFromDB();
			}
		}
	}
	
	private function getFromDB() {
		$results = \Pecee\DB\DB::GetInstance()->listTableArray('SELECT `OriginalText`, `TranslatedText` FROM `Language` WHERE `Locale` = %s AND `PageCode` = %s', 
		null, null, \Pecee\Locale::Instance()->getLocale(), md5($this->path));
		if($results) {
			foreach($results as $result) {
				$this->original[] = $result->OriginalText;
				$this->translated[] = $result->TranslatedText;
			}
			/* Save in cache */
			if($this->cachedEnabled) {
				$this->addCache();
			}
		}
	}
	
	public function lookup($text) {
		if($this->defaultLocale != \Pecee\Locale::Instance()->getLocale()) {
			if($this->original) {					
				$key = array_search($text, $this->original, true);
				if(is_int($key)) {
					return $this->translated[$key];
				}
			}
			// If the translated text dosnt exist in the database, then add it...
			return $this->save($text);
		}
		return $text;
	}
	private function save($text) {
		$translated = $this->translate($text);
		if(empty($translated)) {
			throw new ModelTranslateException(sprintf('An error occured: failed to translate "%s", from %s to %s', $text, $this->defaultLocale, \Pecee\Locale::Instance()->getLocale()));
		}
		\Pecee\DB\DB::GetInstance()->nonQuery('INSERT INTO `Language`(`OriginalText`, `TranslatedText`, `Locale`, `PageCode`, `Path`) VALUES(%s, %s, %s, %s, %s)', $text, $translated, \Pecee\Locale::Instance()->getLocale(), md5($this->path), $this->path);
		$this->original[] = $text;
		$this->translated[] = $translated;
		/* Save in cache */
		if($this->cachedEnabled) {
			$this->addCache();
		}
		return $translated;
	}
	
	private function translate($text) {
		if(\Pecee\Registry::GetInstance()->get(\Pecee\Language::SETTINGS_AUTO_TRANSLATE,FALSE)) {
			die('oversæt');
			$translate = new \Pecee\Service\Google\Translate\GoogleTranslate($this->defaultLanguage, \Pecee\Locale::Instance()->getLocale());
			return $translate->translateWord($text);
		}
		return $text;
	}
	
	private function getObject() {
		if( count($this->original) > 0 && count($this->translated) > 0 ) {
			$object = new \Pecee\Model\Translate\ModelTranslateObject();
			$object->original = $this->original;
			$object->translated = $this->translated;
			return $object;
		}
		return NULL;
	}
	
	private function addCache() {
		$object = $this->getObject();
		if($object) {
			if(\Pecee\Registry::GetInstance()->get(\Pecee\Service\Memcache\Memcache::SETTINGS_HOST, false)) {
				\Pecee\Service\Memcache\Memcache::GetInstance()->set($this->getCacheKey(), $object, null, self::CACHE_EXPIRE_MINUTES);
			} else {
				\Pecee\Model\ModelCache::SetCache($this->getCacheKey(), $object, self::CACHE_EXPIRE_MINUTES);
			}
		}
	}
	private function getCache() {
		$object = (\Pecee\Registry::GetInstance()->get(\Pecee\Service\Memcache\Memcache::SETTINGS_HOST, false)) ? \Pecee\Service\Memcache\Memcache::GetInstance()->get($this->getCacheKey()) : \Pecee\Model\ModelCache::Get($this->getCacheKey());
		return ($object instanceof ModelTranslateObject) ? $object : NULL;
	}
	private function getCacheKey() {
		return join('_', array(self::CACHE_KEY, md5($this->defaultLocale . \Pecee\Locale::Instance()->getLocale() . $this->path)));
	}
}