<?php 
namespace Pecee\Model\User;
class ModelUserData extends \Pecee\Model\Model {
	public function __construct($userId='', $key='', $value='') {
		parent::__construct('UserData', array('UserID' => $userId, 'Key' => $key, 'Value' => $value));
	}
	public function save() {
		if(self::Scalar('SELECT `Key` FROM `UserData` WHERE `Key` = %s AND `UserID` = %s', $this->Key, $this->UserID)) {
			parent::update();
		} else {
			parent::save();
		}
	}

	public static function RemoveAll($UserID) {
		self::NonQuery('DELETE FROM `UserData` WHERE `UserID` = %s', array($UserID));
	}
	public static function GetByUserID($UserID) {
		return self::FetchAll('SELECT * FROM `UserData` WHERE `UserID` = %s', array($UserID));
	}
}