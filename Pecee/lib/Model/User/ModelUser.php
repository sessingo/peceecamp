<?php
namespace Pecee\Model\User;
class ModelUser extends \Pecee\Model\ModelData {
	// Errors
	const ERROR_TYPE_BANNED = 'USER_ERROR_BANNED';
	const ERROR_TYPE_INVALID_USER = 'USER_ERROR_INVALID_USER';
	const ERROR_TYPE_INVALID_LOGIN = 'USER_ERROR_INVALID_LOGIN';
	const ERROR_TYPE_EXISTS = 'USER_ERROR_EXISTS';
	
	const ORDER_ID_DESC = 'u.`UserID` DESC';
	const ORDER_ID_ASC = 'u.`UserID` ASC';
	const ORDER_LASTACTIVITY_ASC = 'u.`LastActivity` DESC';
	const ORDER_LASTACTIVITY_DESC = 'u.`LastActivity` ASC';
	const TICKET_AUTH_KEY = 'TicketUserLoginKey';
	
	protected static $instance;
	
	public static $ORDERS=array(self::ORDER_ID_ASC, self::ORDER_ID_DESC, self::ORDER_LASTACTIVITY_ASC, self::ORDER_LASTACTIVITY_DESC);
	public function __construct($username = NULL, $password = NULL, $email = NULL) {
		parent::__construct('User',  array('UserID' => NULL, 'Username' => $username, 'Password' => md5($password), 
											'AdminLevel' => 0, 'LastActivity' => \Pecee\Date::ToDateTime(), 
											'Deleted' => FALSE));
		$this->setEmail($email);
	}
	
	public function setEmail($email) {
		$this->data->email=$email;
	}
	
	public function getEmail() {
		return $this->data->email;
	}
	
	public function save() {
		$user = self::GetByUsername($this->Username);
		if($user->hasRow()) {
			throw new ModelUserException(sprintf('The username %s already exists', $this->data->username), self::ERROR_TYPE_EXISTS);
		}
		parent::save();
	}
	
	public function updateData() {
		if($this->data) {
			/* Remove all fields */
			ModelUserData::RemoveAll($this->UserID);
			foreach($this->data->getData() as $key=>$value) {
				$data=new ModelUserData($this->UserID, $key, $value);
				$data->save();
			}
		}
	}
		
	protected function fetchData($row) {
		$data = ModelUserData::GetByUserID($row->UserID);
		if($data->hasRows()) {
			foreach($data->getRows() as $d) {
				$row->setDataValue($d->getKey(), $d->getValue());
			}
		}
	}
	
	public function update() {
		return parent::update();
	}
	
	public function delete() {
		//\Pecee\Model\User\ModelUserData::RemoveAll($this->UserID);
		$this->Deleted=TRUE;
		return parent::update();
	}
	
	
	public static function IsLoggedIn() {
		return \Pecee\Cookie::Exists('ticket');
	}
	
	public function signOut() {
		if(\Pecee\Cookie::Exists('ticket')) {
			\Pecee\Cookie::Delete('ticket');
		}
	}
	
	public function exist() {
		return $this->scalar('SELECT u.`Username` FROM `User` u WHERE u.`Username` = %s && u.`Deleted` = 0 LIMIT 1', $this->Username);
	}
	
	public function registerActivity() {
		if($this->IsLoggedIn()) {
			self::NonQuery('UPDATE `User` SET `LastActivity` = NOW() WHERE `UserID` = %s', $this->UserID);
		}
	}
	
	public function trackBadLogin() {
		self::NonQuery('INSERT INTO UserIncorrectLogin(`IPAddress`, `RequestTime`, `Username`) VALUES(%s, %s, %s)', \Pecee\Server::GetRemoteAddr(), time(), $this->Username);
	}
	
	protected static function CheckBadLogin() {
		$trackQuery = self::FetchOne('SELECT `RequestTime`, COUNT(`RequestTime`) AS "CountRequestTime" FROM UserIncorrectLogin WHERE IpAddress = %s AND Active = 1 GROUP BY `IpAddress` ORDER BY RequestTime DESC', \Pecee\Server::GetRemoteAddr());
		if($trackQuery->hasRow()) { 
			$lastLoginTimeStamp = $trackQuery->RequestTime;
			$countRequestsFromIP = $trackQuery->CountRequestTime;
			$lastLoginMinutesAgo = round((time()-$lastLoginTimeStamp)/60);
			return ($lastLoginMinutesAgo < 30 && $countRequestsFromIP > 20);
		}
		return FALSE;
	}
	
	protected function resetBadLogin() {
		self::NonQuery('UPDATE UserIncorrectLogin SET Active = 0 WHERE `IPAddress` = %s', \Pecee\Server::GetRemoteAddr());
	}
	
	protected function signIn($cookieExp){
		$user = array($this->UserID, $this->Password, md5(microtime()), $this->Username, $this->AdminLevel);
		$ticket = \Pecee\Mcrypt::Encrypt(join('|',$user), self::GenerateLoginKey() );
		\Pecee\Cookie::Create('ticket', $ticket, $cookieExp);
	}
	
	/**
	 * Set timeout on user session
	 * @param int $minutes
	 */
	public function setTimeout($minutes) {
		$this->signIn(time()+60*$minutes);
	}
	
	/**
	 * Sets users password and encrypts it.
	 * @param string $string
	 */
	public function setPassword($string) {
		$this->Password=md5($string);
	}
	
	/**
	 * Get current user
	 * @return self
	 */
	public static function Current($setData=FALSE) {
		if(!is_null(self::$instance)) {
			return self::$instance;
		}
		if(self::IsLoggedIn()){
			$ticket = \Pecee\Cookie::Get('ticket');
			if(trim($ticket) != ''){
				$ticket = \Pecee\Mcrypt::Decrypt($ticket, self::GenerateLoginKey() );
				$user = explode('|', $ticket);
				if(is_array($user)) {
					if($setData) {
						self::$instance = self::GetByUserID($user[0]);
					} else {
						$caller=get_called_class();
						$obj=new $caller();
						$obj->setRow('UserID', $user[0]);
						$obj->setRow('Password', $user[1]);
						$obj->setRow('Username', $user[3]);
						$obj->setRow('AdminLevel', $user[4]);
						return $obj;
					}
				}
			}
		}
		return self::$instance;
	}
	
	protected static function GenerateLoginKey() {
		return substr(md5(md5(self::TICKET_AUTH_KEY)), 0, 15);
	}
	
	public static function Get($keyword=NULL, $adminLevel=NULL, $deleted=NULL, $order=NULL, $rows=NULL, $page=NULL) {
		$order=(is_null($order) || !in_array($order, self::$ORDERS)) ? self::ORDER_ID_DESC : $order;
		$where=array('1=1');
		if(!is_null($adminLevel)) {
			$where[]=\Pecee\DB\DB::FormatQuery('u.AdminLevel=%s', array($adminLevel));
		}
		if(!is_null($deleted)) {
			$where[]=\Pecee\DB\DB::FormatQuery('u.Deleted=%s', array($deleted));
		}
		if(!is_null($keyword)) {
			$where[]='Username LIKE \'%%'.\Pecee\DB\DB::Escape($keyword).'%%\'';
		}
		return self::FetchPage('u.UserID', 'SELECT u.* FROM User u WHERE ' . join(' && ', $where) . ' ORDER BY '.$order, $rows, $page);
	}
	
	/**
	 * Get user by user id.
	 * @param int $UserID
	 * @return self
	 */
	public static function GetByUserID($userId) {
		return self::FetchOne('SELECT u.* FROM User u WHERE u.UserID = %s', array($userId));
	}
	
	public static function GetByUserIDs(array $userIds) {
		return self::FetchAll('SELECT u.* FROM User u WHERE u.UserID IN ('.\Pecee\DB\DB::JoinArray($userIds).')' );
	}
	
	public static function GetByUsernameOrEmail($query, $rows = 10, $page = 0) {
		return self::FetchPage('u.UserID', 'SELECT u.* FROM `User` u JOIN `UserData` ud ON(ud.`UserID` = u.`UserID`) WHERE (ud.`Key` = \'email\' && ud.`Value` LIKE %s || u.`Username` LIKE %s) && u.`Deleted` = 0', $rows, $page, $query, $query);
	}
	
	public static function GetByUsername($username) {
		return self::FetchOne('SELECT u.* FROM `User` u WHERE u.`Username` = %s && u.`Deleted` = 0', $username);
	}
	
	public static function GetByEmail($email) {
		return self::FetchOne('SELECT u.* FROM `User` u JOIN `UserData` ud ON(ud.`UserID` = u.`UserID`) WHERE ud.`Key` = \'email\' && ud.`Value` = %s && u.`Deleted` = 0', $email);
	}
	
	public function auth() {
		return self::Authenticate($this->Username, $this->Password, FALSE);
	}
	
	public static function AuthenticateByEmail($email, $password, $remember=FALSE) {
		if(self::CheckBadLogin()) {
			return self::ERROR_TYPE_BANNED;
		}
		$user = self::FetchOne('SELECT u.`UserID`, u.`Username`, u.`Password`, u.`AdminLevel` FROM `User` u JOIN `UserData` ud ON(ud.`UserID` = u.`UserID`) WHERE u.`Deleted` = 0 && ud.`Key` = \'email\' && ud.`Value` = %s', $email);
		if(!$user->hasRows()) {
			return self::ERROR_TYPE_INVALID_USER;
		}
		// Incorrect user login (track bad request).
		if(strtolower($user->getEmail()) != strtolower($email) || $user->Password != md5($password) && $user->Password != $password) {
			$user->trackBadLogin();
			return self::ERROR_TYPE_INVALID_LOGIN;
		}
		$user->resetBadLogin();
		$user->signIn(($remember) ? NULL : 0);
		return $user;
	}
	
	public static function Authenticate($username, $password, $remember = FALSE) {
		if(self::CheckBadLogin()) {
			return self::ERROR_TYPE_BANNED;
		}
		$user = self::FetchOne('SELECT u.* FROM `User` u WHERE u.`Deleted` = 0 && u.`Username` = %s', $username);
		if(!$user->hasRows()) {
			return self::ERROR_TYPE_INVALID_USER;
		}
		// Incorrect user login (track bad request).
		if(strtolower($user->Username) != strtolower($username) || $user->Password != md5($password) && $user->Password != $password) {
			$user->trackBadLogin();
			return self::ERROR_TYPE_INVALID_LOGIN;
		}
		$user->resetBadLogin();
		$user->signIn(($remember) ? NULL : 0);
		return $user;
	}
}