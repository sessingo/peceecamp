<?php
namespace Pecee\Model\User;
class ModelUserReset extends \Pecee\Model\Model {
	public function __construct($UserID = NULL) {
		parent::__construct('UserPasswordReset', array('UserID' => $UserID, 'Key' => md5(uniqid()), 'Date' => time()));
	}
	
	public static function GetByKey($key) {
		return self::FetchOne('SELECT * FROM `UserPasswordReset` WHERE `Key` = %s', array($key));
	}
	
	public static function Confirm($Key, $Password) {
		$reset = self::FetchOne('SELECT * FROM `UserPasswordReset` WHERE `Key` = %s', $Key);
		if($reset->hasRow()) {
			$reset->delete();
			self::NonQuery('DELETE FROM `UserPasswordReset` WHERE `Key` = %s LIMIT 1', $Key);
			self::NonQuery('UPDATE `User` SET `Password` = %s WHERE `UserID` = %s LIMIT 1', md5($Password), $reset->getUserID());
			return $reset->getUserID();
		}
		return NULL;
	}
}