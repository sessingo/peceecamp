<?php
namespace Pecee\Model;
abstract class Model extends ModelRow implements IModel {
	protected $table;
	protected $query;
	protected $countField;

	/**
	 * Model constructor.
	 * Takes argument $rows - used if you need to save() or update() the model.
	 * First argument in array of $rows is always the primary key of the table.
	 * Example:
	 * array('PrimaryKey' => 'PrimaryKeyValue', 'SecondField' => '')
	 * @param null|string $table
	 * @param array|null $rows
	 */
	public function __construct($table=NULL, $rows=NULL) {
		/* Try to figure out table itself */
		if(is_null($table)) {
			$this->table = str_replace('_', '', substr(get_called_class(), strlen('model')));
		} else {
			$this->table = $table;
		}

		$rows = (is_null($rows)) ? array('*') : $rows;
		$this->results['data']['rows']=$rows;
		$this->fields=$rows;
		//$this->originalFields=$rows;
	}

	/**
	 * Counts fieldname in the database, giving the
	 * number of rows in the table with the specified fieldname.
	 *
	 * @param string $fieldName
	 * @param string $tableName
	 * @return int
	 */
	public static function Count($fieldName, $tableName, $where = '', $args = NULL) {
		$args = (is_null($args) || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 3));
		return \Pecee\DB\DB::GetInstance()->count($fieldName, $tableName, $where, $args);
	}

	/**
	 * Returns maximum rows by given fieldname.
	 *
	 * @param string $fieldName
	 * @param string $tableName
	 * @return int
	 */
	public static function Max($fieldName, $tableName, $where = '', $args = NULL) {
		$args = (is_null($args) || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 3));
		return \Pecee\DB\DB::GetInstance()->max($fieldName, $tableName, $where, $args);
	}

	/**
	 * Save item
	 * @see Pecee\Model\ModelRow::save()
	 * @return self
	 */
	public function save() {
		if(!$this->hasRows() || !is_array($this->getRows())) {
			throw new ModelException('Table rows missing from constructor.');
		}
		$fields = array_keys($this->getRows());
		$values = array_values($this->getRows());
		$sql = sprintf('INSERT INTO `%s`(%s) VALUES (%s);', $this->table, \Pecee\DB\DB::JoinArray($fields,TRUE), \Pecee\DB\DB::FormatQuery(\Pecee\DB\DB::JoinValues($values, ', '), $values));
		$out = $this->insert($sql,NULL);
		$this->results['data']['rows'][$fields[0]] = $out->getInsertId();
		return $out;
	}

	public function delete() {
		if(!$this->hasRows() || !is_array($this->getRows())) {
			throw new ModelException('Table rows missing from constructor.');
		}
		$primaryValue = array_values($this->getRows());
		$primaryKey = array_keys($this->getFields());
		if($primaryKey[0] && $primaryValue[0]) {
			$sql = sprintf('DELETE FROM `%s` WHERE `%s` = %s;', $this->table, $primaryKey[0], \Pecee\DB\DB::FormatQuery('%s', array($primaryValue[0])));
			return $this->affectedRows($sql);
		}
		return NULL;
	}

	public function exists() {
		$primaryValue = array_values($this->getRows());
		$primaryKey = array_keys($this->getFields());
		if($primaryKey[0] && $primaryValue[0]) {
			$sql = sprintf('SELECT %s FROM `%s` WHERE `%s` = %s;', $primaryKey[0], $this->table, $primaryKey[0], \Pecee\DB\DB::FormatQuery('%s', array($primaryValue[0])));
			return self::Scalar($sql);
		}
		return FALSE;
	}

	public function update() {
		if(!$this->hasRows() || !is_array($this->getRows())) {
			throw new ModelException('Table rows missing from constructor.');
		}
		$primaryValue = array_values($this->getRows());
		$primaryKey = array_keys($this->getFields());

		if(isset($primaryKey[0]) && isset($primaryValue[0])) {
			$concat=array();
			foreach($this->getFields() as $key=>$val) {
				$val = $this->getRow($key);
				$concat[]=\Pecee\DB\DB::FormatQuery('`'.$key.'` = %s', array($val));
			}
			$sql = sprintf('UPDATE `%s` SET %s WHERE `%s` = \'%s\' LIMIT 1;', $this->table, join(', ', $concat), $primaryKey[0], \Pecee\DB\DB::Escape($primaryValue[0]));
			return $this->Query($sql);
		}
	}

	protected function getCountSql($sql) {
		$sql = (strripos($sql, 'LIMIT') <= 1 && strripos($sql, 'LIMIT') > -1) ? substr($sql, 0, strripos($sql, 'LIMIT')) : $sql;
		$sql = (strripos($sql, 'OFFSET') <= 1 && strripos($sql, 'OFFSET') > -1) ? substr($sql, 0, strripos($sql, 'OFFSET')) : $sql;
		return sprintf('SELECT COUNT(*) as `Total` FROM (%s) as `CountedResult`', $sql);
	}

	public static function Query($query, $rows = NULL, $page = NULL, $countField = NULL, $args = NULL) {
		/* $var $model Model */
		$model = self::OnCreateModel();
		$model->setCountField($countField);
		$results=array();
		$fetchPage=false;
		$args = (is_null($args) || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 4));
		if(!is_null($rows)){
			$page = (is_null($page)) ? 0 : $page;
			$countSql=$model->getCountSql(\Pecee\DB\DB::FormatQuery($query, $args));
			$query .= sprintf(' LIMIT %s, %s',($page*$rows), $rows);
			$fetchPage=true;
		}
		$sql = \Pecee\DB\DB::FormatQuery($query, $args);
		$query = \Pecee\DB\DB::GetInstance()->query($sql);
		if($query) {
			$results['data']['numFields'] = isset($query->field_count) ? $query->field_count : 0;
			$results['data']['numRows']=isset($query->num_rows) ? $query->num_rows : 0;
			$results['insertId']=isset($query->insert_id) ? $query->insert_id : NULL;
			$results['affectedRows']=isset($query->affected_rows) ? $query->affected_rows : 0;
			$results['query'][]=$sql;
			if($results['data']['numRows'] > 0) {
				while(($row = $query->fetch_assoc()) != FALSE) {
					$obj = self::OnCreateModel();
					foreach($row as $key=>$value) {
						$obj->setRow($key, $value);
					}
					$results['data']['rows'][]=$obj;
				}
			}
			if($fetchPage) {
				$results['query'][] = $countSql;
				$maxRows = \Pecee\DB\DB::GetInstance()->scalar($countSql);
				$results['data']['page']=$page;
				$results['data']['rowsPerPage']=$rows;
				$results['data']['maxRows']=intval($maxRows);
			}
			$model->setResults($results);
		}
		return $model;
	}

	/**
	 * Returns model instance
	 * @return self
	 */
	protected static function OnCreateModel() {
		$caller=get_called_class();
		return new $caller();
	}

	public static function FetchAll($query, $args = NULL) {
		$args = (is_null($args) || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 1));
		return self::Query($query, NULL, NULL, NULL, $args);
	}

	public static function FetchAllPage($countField, $query, $startIndex=0, $rows=5, $args=NULL) {
		$args = (is_null($args) || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 4));
		$model=self::OnCreateModel();
		$model->setCountField($countField);
		$maxRows = \Pecee\DB\DB::GetInstance()->scalar($model->getCountSql($query));
		$query=$query.' LIMIT ' . $startIndex . ',' . $rows;
		$model = self::Query($query, NULL, NULL, NULL, $args);
		$results=$model->getResults();
		$results['data']['rowsPerPage']=$rows;
		$results['data']['maxRows']=intval($maxRows);
		$results['data']['hasNext']=($rows+$startIndex < intval($maxRows));
		$results['data']['hasPrevious']=($startIndex>0);
		$model->setResults($results);
		return $model;
	}

	public static function FetchPage($countField, $query, $rows = 10, $page = 0, $args=NULL) {
		$args = (is_null($args) || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 4));
		return self::Query($query, $rows, $page, $countField, $args);
	}

	public static function FetchOne($query, $args=NULL) {
		$args = (is_null($args) || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 1));
		$model =  self::query($query . ((stripos($query, 'LIMIT') > 0) ? '' : ' LIMIT 1'), NULL, NULL, NULL, $args);
		if($model->hasRows()){
			$results = $model->getResults();
			if(isset($results['data']['rows'])) {
				return $results['data']['rows'][0];
			}
		}

		return $model;
	}

	public static function Insert($query, $args = NULL) {
		$args = (is_null($args) || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 1));
		$model=self::OnCreateModel();
		$model->setResults(array('insertId' => \Pecee\DB\DB::GetInstance()->insert($query, $args)));
		return $model;
	}

	public static function AffectedRows($query, $args = NULL) {
		$args = (is_null($args) || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 1));
		$model=self::OnCreateModel();
		$model->setResults(array('affectedRows' => \Pecee\DB\DB::GetInstance()->affectedRows($query, $args)));
		return $model;
	}

	public static function NonQuery($query, $args = NULL) {
		$args = (is_null($args) || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 1));
		\Pecee\DB\DB::GetInstance()->nonQuery($query, $args);
	}

	public static function Scalar($query, $args = NULL) {
		$args = (is_null($args) || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 1));
		return \Pecee\DB\DB::GetInstance()->scalar($query, $args);
	}

	protected function parseJsonData($data) {
		return (\Pecee\Integer::is_int($data)) ? intval($data) : $data;
	}

	protected function parseJsonRow($row) {
		$results=array();
		$originalKeys=array_keys($this->fields);
		foreach($row->getRows() as $k=>$r) {
			if(in_array($k, array_map('strtolower', $originalKeys), FALSE)) {
				$k=array_search($k, array_map('strtolower', $originalKeys));
				$k=$originalKeys[$k];
			}
			$results[$k]=$this->parseJsonData($r);
		}
		return $results;
	}

	public function getAsJsonObject(){
		$arr=array('rows' => NULL);
		$arr=array_merge($arr, (array)$this->results['data']);
		if($this->hasRow()){
			$rows=$this->results['data']['rows'];
			if($rows && is_array($rows)) {
				foreach($rows as $key=>$row){
					if($row instanceof \Pecee\Model\Model) {
						$rows[$key]=$this->parseJsonRow($row);
					} else {
						$rows[$key]=$this->parseJsonData($row);
					}
				}
			}
			if(count($this->getResults()) == 1) {
				return $rows;
			}
			$arr['rows']=$rows;
		}
		return $arr;
	}

	public function setCountField($countField) {
		$this->countField=$countField;
	}

	public function getCountField() {
		return $this->countField;
	}

	public function hasRows() {
		return (isset($this->results['data']['rows']) && count($this->results['data']['rows']) > 0);
	}

	public function hasRow() {
		return (isset($this->results['data']['rows']));
	}

	/**
	 * Get row
	 * @param int $index
	 * @return self
	 */
	public function getRow($index) {
		return ($this->hasRows()) ? $this->results['data']['rows'][$index] : NULL;
	}

	public function setRows(array $rows) {
		if(!isset($this->results['data']['numRows'])) {
			$this->results['data']['numRows'] = count($rows);
		}
		$this->results['data']['rows'] = $rows;
	}

	/**
	 * Get rows
	 * @return self
	 */
	public function getRows() {
		return ($this->hasRows()) ? $this->results['data']['rows'] : NULL;
	}

	public function getMaxRows() {
		return isset($this->results['data']['maxRows']) ? $this->results['data']['maxRows'] : 0;
	}

	public function setMaxRows($rows) {
		$this->results['data']['maxRows'] = $rows;
	}

	public function setNumRow($numRows) {
		$this->results['data']['numRows'] = $numRows;
	}

	public function getNumRows() {
		return isset($this->results['data']['numRows']) ? $this->results['data']['numRows'] : 0;
	}

	public function getNumFields( ){
		return isset($this->results['data']['numFields']) ? $this->results['data']['numFields'] : 0;
	}

	public function getMaxPages() {
		return ($this->getMaxRows() && $this->getNumRows()) ? ceil($this->getMaxRows()/$this->getNumRows()) : 0;
	}

	public function setPage($page) {
		$this->results['data']['page'] = $page;
	}

	public function getPage() {
		return isset($this->results['data']['page']) ? $this->results['data']['page'] : 0;
	}

	public function setResults($results) {
		$this->results = $results;
	}

	public function getResults() {
		return $this->results;
	}

	public function hasNext() {
		return ($this->getPage()+1 < $this->getMaxPages());
	}

	public function hasPrevious() {
		return ($this->getPage() > 0);
	}

	public function getInsertId() {
		return $this->results['insertId'];
	}
}
