<?php
namespace Pecee\Model;
class ModelRow {
	protected $table;
	protected $fields;
	protected $info;
	protected $results;

	public function setRow($key, $value) {
		$this->results['data']['rows'][$key] = $value;
	}
	public function set($name, $value) {
		$this->__set($name, $value);
	}
	public function __get($name) {
		return (isset($this->results['data']['rows'][$name])) ? $this->results['data']['rows'][$name] : NULL;
	}
	public function __set($name, $value) {
		if(array_key_exists($name, $this->fields)) {
			if(is_null($this->fields[$name]) && $value == '') {
				$value = NULL;
			}
			$this->results['data']['rows'][$name] = $value;
		} else {
			throw new ModelException(sprintf('Unknown field %s in table %s', $name, $this->table));
		}
	}
	public function hasFields() {
		return (is_array($this->fields) && count($this->fields) > 0);
	}
	public function getFields() {
		return $this->fields;
	}
	public function setFields(array $fields) {
		$this->fields=$fields;
	}
	/**
	 * Sets post data from post variable.
	 * @param array $data
	 */
	public function setPostData($data){
		if($data && count($data) > 0) {
			foreach($data as $key=>$value){
				$this->__set($key, $value);
			}
		}
	}
	public function __call($name, $args=NULL) {
		if(!method_exists($this, $name)){
			$index = substr($name, 3, strlen($name));
			switch(strtolower(substr($name, 0, 3))){
				case 'get':
					return (isset($this->results['data']['rows']) && array_key_exists($index, $this->results['data']['rows'])) ? $this->results['data']['rows'][$index] : NULL;
					break;
				case 'set':
					$this->__set($index, $args[0]);
					return;
					break;
			}
			$debug=debug_backtrace();
			throw new ModelException(sprintf('Unknown method: %s in %s on line %s', $name, $debug[0]['file'], $debug[0]['line']));
		}
	}
}