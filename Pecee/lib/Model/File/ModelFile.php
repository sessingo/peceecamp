<?php
namespace Pecee\Model\File;
class ModelFile extends \Pecee\Model\ModelData {
	const ORDER_DATE_ASC = 'f.`CreatedDate` ASC';
	const ORDER_DATE_DESC = 'f.`CreatedDate` DESC';
	
	public static $ORDERS = array(self::ORDER_DATE_ASC, self::ORDER_DATE_DESC);
	
	public function __construct($name='',$path='') {
		$fullPath=NULL;
		if(!empty($name) && !empty($path)) {
			$fullPath=$path.(\Pecee\IO\File::HasEndingDirectorySeperator($path) ? '' : DIRECTORY_SEPARATOR) . $name;
		}
		parent::__construct('File', array('FileID' => \Pecee\Guid::Create(), 'Filename' => $name, 'OriginalFilename' => '', 'Path' => $path, 
											'Type' => ($fullPath && is_file($fullPath)) ? \Pecee\IO\File::GetMimeType($fullPath) : '', 
											'Bytes' => ($fullPath && is_file($fullPath)) ? filesize($fullPath) : 0, 'CreatedDate' => \Pecee\Date::ToDateTime()));
	}
	
	public function setFilename($filename) {
		$this->Filename = $filename;
	}
	
	public function setOriginalFilename($filename) {
		$this->OriginalFilename = $filename;
	}
	
	public function setPath($path) {
		$this->Path = $path;
	}
	
	public function setType($type) {
		$this->Type = $type;
	}
	
	public function setBytes($bytes) {
		$this->Bytes = $bytes;
	}
	
	public function setCreatedDate($datetime) {
		$this->CreatedDate = $datetime;
	}
	
	public function updateData() {
		if($this->data) {
			/* Remove all fields */
			ModelFileData::RemoveAll($this->FileID);
			foreach($this->data->getData() as $key=>$value) {
				$data=new ModelFileData($this->FileID, $key, $value);
				$data->save();
			}
		}
	}
		
	protected function fetchData($row) {
		$data = ModelFileData::GetFileId($row->FileID);
		if($data->hasRows()) {
			foreach($data->getRows() as $d) {
				$row->setDataValue($d->getKey(), $d->getValue());
			}
		}
	}
	
	public function getFullPath() {
		return $this->Path . (\Pecee\IO\File::HasEndingDirectorySeperator($this->Path) ? '' : DIRECTORY_SEPARATOR) . $this->Filename;
	}
	
	/**
	 * Get file by file id.
	 * @param string $fileId
	 * @return self
	 */
	public static function GetById($fileId){
		return self::FetchOne('SELECT * FROM `File` WHERE `FileID` = %s', array($fileId));
	}
	
	public static function Get($order=NULL, $rows=NULL, $page=NULL){
		$order = (in_array($order, self::$ORDERS)) ? $order : self::ORDER_DATE_DESC;
		return self::FetchPage('f.`FileID`', 'SELECT f.* FROM `File` f ORDER BY ' .$order, $rows=NULL,$page=NULL);
	}
}