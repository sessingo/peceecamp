<?php 
namespace Pecee\Model\File;
class ModelFileData extends \Pecee\Model\Model {
	public function __construct($fileId='', $key='', $value='') {
		parent::__construct('FileData', array('FileID' => $fileId, 'Key' => $key, 'Value' => $value));
	}
	public function save() {
		if(self::Scalar('SELECT `Key` FROM `FileData` WHERE `Key` = %s AND `FileID` = %s', $this->Key, $this->FileID)) {
			parent::update();
		} else {
			parent::save();
		}
	}

	public static function RemoveAll($fileId) {
		self::NonQuery('DELETE FROM `FileData` WHERE `FileID` = %s', array($fileId));
	}
	
	public static function GetFileId($fileId) {
		return self::FetchAll('SELECT * FROM `FileData` WHERE `FileID` = %s', array($fileId));
	}
}