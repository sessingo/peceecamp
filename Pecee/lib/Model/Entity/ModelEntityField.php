<?php
namespace Pecee\Model\Entity;
class ModelEntityField extends \Pecee\Model\Model {
	public function __construct($entityId='', $name = '', $value = '') {
		parent::__construct('EntityField', array('EntityID' => $entityId, 'Name' => $name, 'Value' => $value));
	}
	public function save() {
		if(self::Scalar('SELECT `Name` FROM `EntityField` WHERE `Name` = %s AND `EntityID` = %s', $this->EntityID, $this->Name)) {
			parent::update();
		} else {
			parent::save();
		}
	}
	
	public static function Clear($entityId) {
		self::NonQuery('DELETE FROM `EntityField` WHERE `EntityID` = %s', array($entityId));
	}
	
	public static function GetByEntityID($EntityId) {
		return self::FetchAll('SELECT * FROM `EntityField` WHERE `EntityID` = %s', array($EntityId));
	}
}