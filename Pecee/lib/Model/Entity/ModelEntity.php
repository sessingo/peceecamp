<?php
namespace Pecee\Model\Entity;
use Pecee\Boolean;

class ModelEntity extends ModelEntityCore {
	const ORDER_ID_DESC = 'e.`EntityID` DESC';
	const ORDER_DATE_DESC = 'IFNULL(e.`ActiveFrom`, e.`PubDate`) DESC';
	const ORDER_DATE_ASC = 'e.`PubDate` ASC';
	const ORDER_TITLE_DESC = 'e.`Title` DESC';
	const ORDER_TITLE_ASC = 'e.`Title` ASC';
	const ORDER_CATEGORY_DESC = 'e.`EntityCategoryID` DESC';
	const ORDER_CATEGORY_ASC = 'e.`EntityCategoryID` ASC';
	
	public static $orders=array(self::ORDER_ID_DESC,self::ORDER_DATE_ASC,self::ORDER_DATE_DESC,self::ORDER_TITLE_ASC,self::ORDER_TITLE_DESC, self::ORDER_CATEGORY_DESC, self::ORDER_CATEGORY_ASC);
	
	public function __construct($title='', $entityCategoryId=0) {
		parent::__construct('Entity', 
		array('EntityID' => '', 
							'EntityCategoryID' => $entityCategoryId, 
							'Title' => $title, 
							'Content' => '', 
							'PubDate' => \Pecee\Date::ToDateTime(), 
							'ActiveFrom' => NULL, 
							'ActiveTo' => NULL, 
							'Active' => TRUE));
	}
	
	public function setActiveFrom($activeFrom) {
		$this->ActiveFrom = (\Pecee\Date::IsValid($activeFrom)) ? $activeFrom : NULL;
	}
	
	public function setActiveTo($activeTo) {
		$this->ActiveTo = (\Pecee\Date::IsValid($activeTo)) ? $activeTo : NULL;
	}
	
	public function getEntityCategory() {
		return ModelEntityCategory::GetByEntityCategoryID($this->EntityCategoryID);
	}
	
	public function setData($name,$value) {
		$this->data->$name = $value;
	}
	
	public function exists() {
		return self::Scalar('SELECT EntityID FROM `Entity` WHERE EntityID = %s', $this->EntityID);
	}
	
	/**
	 * Get entity by entity id.
	 * @param string $EntityID
	 * @return self
	 */
	public static function GetByEntityID($EntityID, $Active=NULL) {
		$where='e.`EntityID` = %s';
		if(!is_null($Active)) {
			$where.=' AND e.`Active` = ' . Boolean::Parse($Active,0);
		}
		return self::FetchOne('SELECT e.* FROM `Entity` e WHERE ' . $where, $EntityID);
	}
	
	/**
	 * Get entity by entity category id.
	 * @param string $EntityCategoryID
	 * @param string $EntityCategoryID
	 * @param int|null $Rows
	 * @param int|null $Page
	 * @return self
	 */
	public static function GetByEntityCategoryID($EntityCategoryID, $Rows=15, $Page=0) {
		return self::FetchPage('e.EntityID', 'SELECT e.* FROM `Entity` e WHERE e.`EntityCategoryID` = %s', $Rows, $Page, $EntityCategoryID);
	}
	
	/**
	 * Get entities.
	 * @param string|null $Query
	 * @param bool|null $Active
	 * @param int|null $Rows
	 * @param int|null $Page
	 * @return self
	 */
	public static function Get($Query=NULL, $Active=NULL, $EntityCategoryID=NULL, $ParentEntityCategoryID=NULL, $Order=NULL, $Rows=15, $Page=0) {
		$where=array('1=1');
		if(!is_null($Active)) {
			$where[] = \Pecee\DB\DB::FormatQuery('e.`Active` = %s', array(Boolean::Parse($Active)));
			$where[] = \Pecee\DB\DB::FormatQuery('(ISNULL(e.`ActiveFrom`) && ISNULL(e.`ActiveTo`) || e.`ActiveFrom` <= NOW() && (e.`ActiveTo` >= NOW() || ISNULL(e.`ActiveTo`)))');
		}
		if(!is_null($EntityCategoryID)) {
			$where[] = \Pecee\DB\DB::FormatQuery('e.`EntityCategoryID` = %s', array($EntityCategoryID));
		}
		if(!is_null($ParentEntityCategoryID)) {
			$where[] = "(c.`Path` LIKE '%".\Pecee\DB\DB::Escape($ParentEntityCategoryID)."%' OR e.`EntityCategoryID` = '".\Pecee\DB\DB::Escape($ParentEntityCategoryID)."') ";
		}
		if(!is_null($Query)) {
			$where[] = sprintf('(e.`Title` LIKE \'%s\' OR e.`Content` LIKE \'%s\')', '%'.\Pecee\DB\DB::Escape($Query).'%', '%'.\Pecee\DB\DB::Escape($Query).'%');
		}
		$Order=(!is_null($Order) && in_array($Order, self::$orders)) ? $Order : self::ORDER_DATE_DESC;
		return self::FetchPage('e.EntityID', 'SELECT e.* FROM `Entity` e LEFT OUTER JOIN `EntityCategory` c ON(c.`EntityCategoryID` = e.`EntityCategoryID`) WHERE ' . join(' && ', $where) . ' ORDER BY ' . $Order, $Rows, $Page);
	}
	
	/**
	 * Get entities.
	 * @param string|null $Query
	 * @param bool|null $Active
	 * @param int|null $Rows
	 * @param int|null $Page
	 * @return self
	 */
	public static function GetMultipleRows($Query=NULL, $Active=NULL, $EntityCategoryID=NULL, $ParentEntityCategoryID=NULL, $Order=NULL, $StartIndex=0, $Rows=15) {
		$where=array('1=1');
		if(!is_null($Active)) {
			$where[] = \Pecee\DB\DB::FormatQuery('e.`Active` = %s', Boolean::Parse($Active));
			$where[] = \Pecee\DB\DB::FormatQuery('(ISNULL(e.`ActiveFrom`) && ISNULL(e.`ActiveTo`) || e.`ActiveFrom` <= NOW() && (e.`ActiveTo` >= NOW() || ISNULL(e.`ActiveTo`)))');
		}
		if(!is_null($EntityCategoryID)) {
			$where[] = \Pecee\DB\DB::FormatQuery('e.`EntityCategoryID` = %s', array($EntityCategoryID));
		}
		if(!is_null($ParentEntityCategoryID)) {
			$where[] = "(c.`Path` LIKE '%".\Pecee\DB\DB::Escape($ParentEntityCategoryID)."%' OR e.`EntityCategoryID` = '".\Pecee\DB\DB::Escape($ParentEntityCategoryID)."')";
		}
		if(!is_null($Query)) {
			$where[] = sprintf('(e.`Title` LIKE \'%s\' OR e.`Content` LIKE \'%s\')', '%'.\Pecee\DB\DB::Escape($Query).'%', '%'.\Pecee\DB\DB::Escape($Query).'%');
		}
		$Order=(!is_null($Order) && in_array($Order, self::$orders)) ? $Order : self::ORDER_DATE_DESC;
		return self::FetchRows('e.EntityID','SELECT e.* FROM `Entity` e LEFT OUTER JOIN `EntityCategory` c ON(c.`EntityCategoryID` = e.`EntityCategoryID`) WHERE ' . join(' && ', $where) . ' ORDER BY ' . $Order, $StartIndex, $Rows);
	}
}