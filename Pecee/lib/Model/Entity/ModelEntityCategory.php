<?php
namespace Pecee\Model\Entity;
class ModelEntityCategory extends ModelEntityCore {
	/* Orders */
	const ORDER_PUBDATE_DESC='`PubDate` DESC';
	const ORDER_PUBDATE_ASC='`PubDate` ASC';
	const ORDER_SORT_DESC='`Order` DESC';
	const ORDER_SORT_ASC='`Order` ASC';
	const ORDER_TITLE_DESC='`Title` DESC';
	const ORDER_TITLE_ASC='`Title` ASC';
	
	public static $orders=array(self::ORDER_PUBDATE_ASC,self::ORDER_PUBDATE_DESC,self::ORDER_SORT_ASC,
								self::ORDER_SORT_DESC, self::ORDER_TITLE_ASC, self::ORDER_TITLE_DESC);
	
	public function __construct($title='') {
		parent::__construct('EntityCategory', 
					array('EntityCategoryID' => '', 
							'ParentEntityCategoryID' => '',
							'Path' => '',
							'Title' => $title, 
							'Description' => NULL, 
							'Order' => 0,
							'PubDate' => \Pecee\Date::ToDateTime()));
	}
	
	protected function calculatePath() {
		$path=array($this->EntityCategoryID);
		$fetchingPath=TRUE;
		if($this->ParentEntityCategoryID) {
			$parent=self::GetByEntityCategoryID($this->ParentEntityCategoryID);
			$i=0;
			while($fetchingPath) {
				if($parent->hasRow()) {
					$path[]=$parent->getEntityCategoryID();
					$p=$parent->getParentEntityCategoryID();
					if(!empty($p)) {
						$parent=self::GetByEntityCategoryID($parent->getParentEntityCategoryID());
					} else {
						$fetchingPath=FALSE;
					}
					$i++;
				} else {
					$fetchingPath=FALSE;
				}
			}
			
			if($i==0) {
				$path[]=$this->ParentEntityCategoryID;
			}
		}
		$this->Path=join('>', array_reverse($path));
	}
	
	public function getParent() {
		return self::GetByEntityCategoryID($this->getParentEntityCategoryID());
	}

	public function exists() {
		return self::Scalar('SELECT `EntityCategoryID` FROM `EntityCategory` WHERE `EntityCategoryID` = %s', $this->EntityCategoryID);
	}
	
	public function save() {
		$this->calculatePath();
		parent::save();
	}
	
	public function update() {
		$this->calculatePath();
		return parent::update();
	}
	
	/**
	 * Get entity by entity category id.
	 * @param string $EntityCategoryID
	 * @return \Pecee\Model\Entity\ModelEntityCategory
	 */
	public static function GetByEntityCategoryID($EntityCategoryID) {
		return self::FetchOne('SELECT * FROM `EntityCategory` WHERE `EntityCategoryID` = %s', $EntityCategoryID);
	}
	/**
	 * Get entity by entity category ids.
	 * @param array $EntityCategoryIDs
	 * @return \Pecee\Model\Entity\ModelEntityCategory
	 */
	public static function GetByEntityCategoryIDs(array $EntityCategoryIDs, $Order=NULL) {
		$Order=(is_null($Order) || !in_array($Order, self::$orders)) ? self::ORDER_SORT_ASC : $Order;
		return self::FetchAll('SELECT * FROM `EntityCategory` WHERE `EntityCategoryID` IN  ('. \Pecee\DB\DB::JoinArray($EntityCategoryIDs) .') ORDER BY '. $Order);
	}
	
	/**
	 * Get entities
	 * @param string|null $Query
	 * @param string|null $ParentEntityCategoryID
	 * @param int|null $Rows
	 * @param int|null $Page
	 * @return \Pecee\Model\Entity\ModelEntityCategory
	 */
	public static function Get($Query=NULL, $ParentEntityCategoryID=NULL, $Order=NULL, $Rows=15, $Page=0) {
		$where=array('1=1');
		if(!is_null($ParentEntityCategoryID)) {
			$where[] = \Pecee\DB\DB::FormatQuery('`ParentEntityCategoryID` = %s', array($ParentEntityCategoryID));
		}
		if(!is_null($Query)) {
			$where[] = sprintf('(`Title` LIKE \'%s\' OR `Description` LIKE \'%s\')', '%'.\Pecee\DB\DB::Escape($Query).'%', '%'.\Pecee\DB\DB::Escape($Query).'%');
		}
		$Order=(is_null($Order) || !in_array($Order, self::$orders)) ? self::ORDER_SORT_ASC : $Order;
		return self::FetchPage('EntityCategoryID', 'SELECT * FROM `EntityCategory` WHERE ' . join('  &&', $where) . ' ORDER BY ' . $Order, $Rows, $Page);
	}
	
	/**
	 * Get entities
	 * @param string|null $Query
	 * @param string|null $ParentEntityCategoryID
	 * @param int|null $Rows
	 * @param int|null $Page
	 * @return \Pecee\Model\Entity\ModelEntityCategory
	 */
	public static function GetChildren($Query=NULL, $ParentEntityCategoryID=NULL, $Order=NULL, $Rows=15, $Page=0) {
		$where=array('1=1');
		if(!is_null($ParentEntityCategoryID)) {
			$where[] = "`Path` LIKE '%".\Pecee\DB\DB::Escape($ParentEntityCategoryID)."%' AND `EntityCategoryID` != '".\Pecee\DB\DB::Escape($ParentEntityCategoryID)."'";
		}
		if(!is_null($Query)) {
			$where[] = sprintf('(`Title` LIKE \'%s\' OR e.`Content` LIKE \'%s\')', '%'.\Pecee\DB\DB::Escape($Query).'%', '%'.\Pecee\DB\DB::Escape($Query).'%');
		}
		$Order=(is_null($Order) || !in_array($Order, self::$orders)) ? self::ORDER_SORT_ASC : $Order;
		return self::FetchPage('EntityCategoryID', 'SELECT * FROM `EntityCategory` WHERE ' . join(' && ', $where) . ' ORDER BY ' . $Order, $Rows, $Page);
	}
}