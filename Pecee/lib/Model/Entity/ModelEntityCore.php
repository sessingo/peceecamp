<?php
namespace Pecee\Model\Entity;
abstract class ModelEntityCore extends \Pecee\Model\Model {
	public $data;
	public function __construct($table, $rows) {
		parent::__construct($table,$rows);
		$this->data = new \Pecee\Util\Map();
	}
	
	public function getAsJsonObject(){
		$arr=array('rows' => NULL);
		$arr=array_merge($arr, (array)$this->results['data']);
		if($this->hasRow()){
			$rows=$this->results['data']['rows'];
			if($rows && is_array($rows)) {
				$addData = FALSE;
				foreach($rows as $key=>$row){
					if($row instanceof \Pecee\Model\Model) {
						$rows[$key]=$this->parseJsonRow($row);
						$rows[$key]['data'] = $this->parseJsonData($row->data->getData());
					} else {
						$rows[$key]=$this->parseJsonData($row);
						$addData = TRUE;
					}
				}
				if($addData) {
					$rows['data'] = $this->parseJsonData($this->data->getData());
				}
			}
			if(count($this->getResults()) == 1) {
				return $rows;
			}
			$arr['rows']=$rows;
		}
		return $arr;
		
	}
	
	public function updateFields() {
		if($this->data) {
			/* Remove all fields */
			$entityId=($this instanceof ModelEntity) ? $this->EntityID : $this->EntityCategoryID;
			ModelEntityField::Clear($entityId);
			if(count($this->data->getData()) > 0) {
				foreach($this->data->getData() as $name=>$value) {
					$field=new ModelEntityField($entityId, $name, $value);
					$field->save();
				}
			}
		}
	}
	
	public function save() {
		if($this instanceof ModelEntity) {
			$this->EntityID = parent::save()->getInsertId();
		} else {
			$this->EntityCategoryID = parent::save()->getInsertId();
		}
		$this->updateFields();
	}
	
	public function update() {
		$this->updateFields();
		parent::update();
	}
	
	public function delete() {
		$entityId=($this instanceof ModelEntity) ? $this->EntityID : $this->EntityCategoryID;
		ModelEntityField::Clear($entityId);
		parent::delete();
	}
	
	protected function fetchField($row) {
		$entityId=($this instanceof ModelEntity) ? $row->EntityID : $row->EntityCategoryID;
		$fields = ModelEntityField::GetByEntityID($entityId);
		if($fields->hasRows()) {
			foreach($fields->getRows() as $field) {
				$name=$field->getName();
				$row->data->$name = $field->getValue();
			}
		}
	}
	
	protected function setEntityFields($single=false) {
		if($single && $this->hasRow()) {
			$this->fetchField($this);
		} else {
			if($this->hasRows()) {
				foreach($this->getRows() as $row) {
					$this->fetchField($row);
				}
			}
		}
	}
	
	public static function FetchPage($countField, $query, $rows = 10, $page = 0, $args=NULL) {
		$args = (!$args || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 4));
		$model = parent::fetchPage($countField, $query, $rows, $page, $args);
		$model->setEntityFields();
		return $model;
	}
	
	public static function FetchRows($countField, $query, $startIndex=0, $rows = 10, $args = NULL) {
		$args = (!$args || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 4));
		$model = parent::fetchAllPage($countField, $query, $startIndex, $rows, $args);
		$model->setEntityFields();
		return $model;
	}
	
	public static function FetchOne($query, $args=NULL) {
		$args = (!$args || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 1));
		$model = parent::fetchOne($query, $args);
		$model->setEntityFields(TRUE);
		return $model;
	}
}