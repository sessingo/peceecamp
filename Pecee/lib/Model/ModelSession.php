<?php
namespace Pecee\Model;
class ModelSession extends \Pecee\Model\Model {
	public function __construct($name='', $value='') {
		parent::__construct('Session', array('Name' => $name, 'Value' => $value, 'Time' => time()));
	}
	
	public function save() {
		self::NonQuery('DELETE FROM `Session` WHERE `Time` <= %s', (time()-(60*30)));
		$session = $this->Get($this->Name)->hasRows();
		if($session->hasRows()) {
			$session->Time = time();
			$session->update();
		} else {
			parent::save();
		}
	}
	
	/**
	 * Get Session by key
	 * @param string $key
	 * @return \Pecee\Model\ModelSession
	 */
	public static function Get($name) {
		return self::FetchOne('SELECT * FROM `Session` WHERE `Name` = %s', $name);
	}
}