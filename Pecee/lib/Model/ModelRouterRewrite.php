<?php
namespace Pecee\Model;
class ModelRouterRewrite extends Model {
	public function __construct() {
		parent::__construct('Rewrite', array('RewriteID' => '', 'OriginalUrl' => '', 'RewriteUrl' => '', 
												'Host' => NULL, 'Regex' => NULL, 'Order' => 0));
	}
	
	public function exists() {
		return (self::Scalar('SELECT `OriginalPath` FROM `Rewrite` WHERE `OriginalPath` = %s', $this->OriginalPath));
	}
	
	/**
	 * Get rewrite by originalpath
	 * @param string $originalPath
	 * @return \Pecee\Model\ModelRouterRewrite
	 */
	public static function GetByOriginalUrl($originalUrl, $host = NULL) {
		$where = array('1=1');
		if(!is_null($host)) {
			$where[] = \Pecee\DB\DB::FormatQuery('`Host` = %s', array($host));
		}
		return self::FetchOne('SELECT * FROM `Rewrite` WHERE `OriginalUrl` = %s && ' . join(' && ', $where), $originalUrl);
	}
	
	public static function GetByRewritePath($rewriteUrl) {
		return self::FetchOne('SELECT * FROM `Rewrite` WHERE `RewriteUrl` = %s', $rewriteUrl);
	}
	
	public static function Get($rows = NULL, $page = NULL) {
		return self::FetchPage('OriginalUrl', 'SELECT * FROM `Rewrite` ORDER BY `Order` ASC, `RewriteID` DESC', $rows, $page);
	}
	
	public static function GetByRewriteID($rewriteId) {
		return self::FetchOne('SELECT * FROM `Rewrite` WHERE `RewriteID` = %s', $rewriteId);
	}
}