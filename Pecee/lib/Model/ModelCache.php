<?php
namespace Pecee\Model;
class ModelCache extends Model {
	// Depricated in near future
	public function __construct($key='', $data='', $expireMinutes='') {
		parent::__construct('Cache', array('Key' => $key, 'Data' => $data, 'ExpireDate' => time()+($expireMinutes*60)));
	}
	
	/**
	 * Checks expiration date for given cache key.
	 *
	 * @param string $cacheKey
	 * @return bool
	 */
	protected static function IsExpired($expireDate) {
		return ($expireDate && $expireDate > 0 && $expireDate <= time());
	}
	
	/**
	 * Clear all cache elements.
	 */
	public static function ClearCache() {
		self::NonQuery('TRUNCATE `Cache`');
	}
	
	public static function SetCache($key, $data, $expire) {
		if(!\Pecee\Integer::is_int($expire)) throw new \InvalidArgumentException('Unknown datatype for expirationMinutes. Must be INT or nummeric string.');
		$expirationDate = time() + ($expire*60);
		self::RemoveCache($key);
		$model = new self($key, serialize($data), $expire);
		return ($model->save());
	}
	
	public static function RemoveCache($key) {
		self::NonQuery('DELETE FROM `Cache` WHERE `Key` = %s', $key);
	}
	
	public static function Get($key) {
		$model = self::FetchOne('SELECT * FROM `Cache` WHERE `Key` = %s', $key);
		if($model->hasRow()) {
			if(self::IsExpired($model->getExpireDate())){
				$model->delete();
			} else {
				return unserialize($model->getData());
			}
		}
		return NULL;
	}
}