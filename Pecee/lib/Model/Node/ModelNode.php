<?php
namespace Pecee\Model\Node;
use Pecee\Boolean;

class ModelNode extends \Pecee\Model\Model {
	const ORDER_ID_DESC = 'n.`NodeID` DESC';
	const ORDER_CHANGED_DESC = 'IFNULL(n.`ChangedDate`, IFNULL(n.`ActiveFrom`, n.`PubDate`)) DESC';
	const ORDER_CHANGED_ASC = 'IFNULL(n.`ChangedDate`, IFNULL(n.`ActiveFrom`, n.`PubDate`)) ASC';
	const ORDER_DATE_DESC = 'IFNULL(n.`ActiveFrom`, n.`PubDate`) DESC';
	const ORDER_DATE_ASC = 'n.`PubDate` ASC';
	const ORDER_TITLE_DESC = 'n.`Title` DESC';
	const ORDER_TITLE_ASC = 'n.`Title` ASC';
	const ORDER_PARENT_DESC = 'n.`ParentNodeID` DESC';
	const ORDER_PARENT_ASC = 'n.`ParentNodeID` ASC';
	const ORDER_ORDER_DESC = 'n.`ORDER` DESC';
	const ORDER_ORDER_ASC = 'n.`ORDER` ASC';

	public static $orders=array(self::ORDER_ID_DESC,self::ORDER_DATE_ASC,
								self::ORDER_DATE_DESC,self::ORDER_TITLE_ASC,
								self::ORDER_TITLE_DESC, self::ORDER_PARENT_DESC,
								self::ORDER_PARENT_ASC, self::ORDER_ORDER_ASC, self::ORDER_ORDER_DESC);

	public $data;
	protected $parent, $next, $prev;
	protected $childs;
	public function __construct() {
		parent::__construct('Node', array('NodeID' => '', 'ParentNodeID' => 0, 'Path' => NULL,
										'Type' => NULL, 'Title' => NULL, 'Content' => NULL,
										'PubDate' => \Pecee\Date::ToDateTime(), 'ChangedDate' => NULL,
										'ActiveFrom' => NULL, 'ActiveTo' => NULL, 'Active' => TRUE,
										'Level' => 0, 'Order' => 0));
		$this->data = new \Pecee\Util\Map();
	}

	protected function calculatePath() {
		$path=array('0');
		$fetchingPath=TRUE;
		if($this->ParentNodeID) {
			$parent=self::GetByNodeID($this->ParentNodeID);
			$i=0;
			while($fetchingPath) {
				if($parent->hasRow()) {
					$path[]=$parent->getNodeID();
					$p=$parent->getParentNodeID();
					if(!empty($p)) {
						$parent=self::GetByNodeID($parent->getParentNodeID());
					} else {
						$fetchingPath=FALSE;
					}
					$i++;
				} else {
					$fetchingPath=FALSE;
				}
			}

			if($i==0) {
				$path[]=$this->ParentNodeID;
			}
		}
		$this->Path=join('>', $path);
		$this->Level=count($path);
	}

	public function removeData($name) {
		unset($this->data->$name);
	}

	public function setData($name,$value) {
		$this->data->$name = $value;
	}

	public function getData($name) {
		return $this->data->$name;
	}

	public function getNext() {
		if(!$this->next) {
			$parentNodeId = 0;
			if($this->ParentNodeID) {
				$parentNodeId = self::GetByNodeID($this->ParentNodeID);
				if($parentNodeId->hasRow()) {
					$parentNodeId = $parentNodeId->getNodeID();
				}
			}

			$where=array('n.`Active` = 1');
			$where[] = \Pecee\DB\DB::FormatQuery('(ISNULL(n.`ActiveFrom`) && ISNULL(n.`ActiveTo`) || n.`ActiveFrom` <= NOW() && (n.`ActiveTo` >= NOW() || ISNULL(n.`ActiveTo`)))');
			$where[] = "n.`ParentNodeID` = '".\Pecee\DB\DB::Escape($parentNodeId)."'";
			$where[] = "n.`Path` LIKE '%>".\Pecee\DB\DB::Escape($parentNodeId).">%'";
			$where[] = 'n.`Order` > ' . $this->Order;

			$this->next = self::FetchOne('SELECT n.* FROM `Node` n WHERE ' . join(' && ', $where));
		}
		return $this->next;
	}

	public function getPrev() {
		if(!$this->prev) {
			$parentNodeId = 0;
			if($this->ParentNodeID) {
				$parentNodeId = self::GetByNodeID($this->ParentNodeID);
				if($parentNodeId->hasRow()) {
					$parentNodeId = $parentNodeId->getNodeID();
				}
			}

			$where=array('n.`Active` = 1');
			$where[] = \Pecee\DB\DB::FormatQuery('(ISNULL(n.`ActiveFrom`) && ISNULL(n.`ActiveTo`) || n.`ActiveFrom` <= NOW() && (n.`ActiveTo` >= NOW() || ISNULL(n.`ActiveTo`)))');
			$where[] = "n.`ParentNodeID` = '".\Pecee\DB\DB::Escape($parentNodeId)."'";
			$where[] = "n.`Path` LIKE '%>".\Pecee\DB\DB::Escape($parentNodeId).">%'";
			$where[] = 'n.`Order` < ' . $this->Order;

			$this->prev = self::FetchOne('SELECT n.* FROM `Node` n WHERE ' . join(' && ', $where));
		}
		return $this->prev;
	}

	/**
	 * Get childs
	 * @param string $alias
	 * @param string $recursive
	 * @param string $order
	 * @return self
	 */
	public function getChildsOfType($alias, $recursive=TRUE, $order = NULL) {
		$out = array();
		if($recursive) {
			$pages = self::Get(NULL, NULL, NULL, NULL, $this->getNodeID(), $order);
		} else {
			$pages =  self::Get(NULL, NULL, NULL, $this->getNodeID(), NULL, $order, NULL, NULL);
		}
		if($pages->hasRows()) {
			foreach($pages->getRows() as $page) {
				if($page->getProperty()->hasRow() && $page->getProperty()->getAlias() == $alias) {
					$out[] = $page;
				}
				/*if($recursive) {
					$tmp = $page->getChildsOfType($alias, $recursive, $order);
					if($tmp->hasRows()) {
						$out = array_merge($out, $tmp->getRows());
					}
				}*/
			}
		}
		$result = get_called_class();
		$result = new $result();
		$result->setRows($out);
		return $result;
	}

	public function setChilds($childs) {
		$this->childs = $childs;
	}

	public function getChilds() {
		if(!$this->parent) {
			$this->parent = self::Get(NULL, NULL, NULL, $this->getNodeID(), NULL, NULL, NULL, NULL);
		}
		return $this->parent;
	}

	public function updateFields() {
		if($this->data) {
			/* Remove all fields */
			ModelNodeData::Clear($this->NodeID);
			if(count($this->data->getData()) > 0) {
				foreach($this->data->getData() as $key=>$value) {
					$field=new ModelNodeData();
					$field->setNodeID($this->NodeID);
					$field->setKey($key);
					$field->setValue($value);
					$field->save();
				}
			}
		}
	}

	public function save() {
		$this->calculatePath();
		$this->NodeID = parent::save()->getInsertId();
		$this->updateFields();
	}

	public function update() {
		$this->ChangedDate = \Pecee\Date::ToDateTime();
		$this->calculatePath();
		$this->updateFields();
		parent::update();
	}

	public function delete() {
		// Delete childs
		$childs = $this->getChilds();
		if($childs->hasRows()) {
			foreach($childs->getRows() as $child) {
				$child->delete(FALSE);
			}
		}

		ModelNodeData::Clear($this->NodeID);
		parent::delete();
	}

	public function exists() {
		return self::Scalar('SELECT `NodeID` FROM `Node` WHERE `NodeID` = %s', $this->NodeID);
	}

	protected function fetchField($row) {
		$data = ModelNodeData::GetByNodeID($row->NodeID);
		if($data->hasRows()) {
			foreach($data->getRows() as $field) {
				$key=$field->getKey();
				$row->data->$key = $field->getValue();
			}
		}
	}

	protected function setEntityFields($single=false) {
		if($single && $this->hasRow()) {
			$this->fetchField($this);
		} else {
			if($this->hasRows()) {
				$nodeIds = array();
				foreach($this->getRows() as $row) {
					$nodeIds[] = $row->getNodeID();
				}

				if(count($nodeIds) > 0) {
					$nodeData = array();
					$data = ModelNodeData::GetByNodeIDs($nodeIds);
					if($data->hasRows()) {
						foreach($data->getRows() as $data) {
							$nodeData[$data->getNodeID()][] = $data;
						}
					}

					foreach($this->getRows() as $row) {
						if(isset($nodeData[$row->getNodeID()])) {
							foreach($nodeData[$row->getNodeID()] as $field) {
								$key=$field->getKey();
								$row->data->$key = $field->getValue();
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Order by key
	 * @param string $key
	 * @param string $direction
	 * @return self
	 */
	public function order($key, $direction = 'DESC') {
		if($this->hasRows()) {
			$rows = array();
			foreach($this->getRows() as $row) {
				$k = (isset($row->fields[$key])) ? $row->__get($key) : $row->data->$key;
				$k = ($k == 'Tjs=') ? \Pecee\String\Encoding::Base64Decode($k) : $k;
				$rows[$k] = $row;
			}
			if(strtolower($direction) == 'asc') {
				ksort($rows);
			} else {
				krsort($rows);
			}

			$this->setRows(array_values($rows));
		}

		return $this;
	}

	/**
	 * Get first or default value
	 * @param string $default
	 * @return self
	 */
	public function getFirstOrDefault($default=NULL) {
		if($this->hasRows()) {
			return $this->getRow(0);
		}
		return $default;
	}

	/**
	 * Skip number of rows
	 * @param int $number
	 * @return self
	 */
	public function skip($number) {
		if($this->hasRows() && $number > 0) {
			$out = array_splice($this->getRows(), $number);
			$this->setRows($out);
		}
		return $this;
	}

	/**
	 * Limit the output
	 * @param int $limit
	 * @return self
	 */
	public function limit($limit) {
		$out = array();
		if($this->hasRows()) {
			foreach($this->getRows() as $i=>$row) {
				if($i < $limit) {
					$out[] = $row;
				}
			}
		}
		$this->setRows($out);
		$this->setNumRow($limit);
		return $this;
	}

	/**
	 * Filter elements
	 * @param string $key
	 * @param string $value
	 * @param string $delimiter
	 * @return self
	 */
	public function where($key, $value, $delimiter = '=') {
		$out = array();
		if($this->hasRows()) {
			foreach($this->getRows() as $row) {
				$keys = (is_array($key)) ? $key : array($key);
				foreach($keys as $_key) {
					$k = (array_key_exists($_key, $row->fields)) ? $row->__get($_key) : $row->data->$_key;
					$k = (strpos($k, 'Tjs=') == '1') ? \Pecee\String\Encoding::Base64Decode($k) : $k;

					if($delimiter == '>') {
						if($k > $value) {
							if(!in_array($row, $out)) {
								$out[] = $row;
							}
						}
					} elseif($delimiter == '<') {
						if($k < $value) {
							if(!in_array($row, $out)) {
								$out[] = $row;
							}
						}
					} elseif($delimiter == '>=') {
						if($k >= $value) {
							if(!in_array($row, $out)) {
								$out[] = $row;
							}
						}
					} elseif($delimiter == '<=') {
						if($k <= $value) {
							if(!in_array($row, $out)) {
								$out[] = $row;
							}
						}
					} elseif($delimiter == '!=') {
						if($k != $value) {
							if(!in_array($row, $out)) {
								$out[] = $row;
							}
						}
					} elseif($delimiter == '*') {
						if(strtolower($k) == $value || strstr(strtolower($k), strtolower($value)) !== FALSE) {
							if(!in_array($row, $out)) {
								$out[] = $row;
							}
						}
					} else {
						if($k == $value) {
							if(!in_array($row, $out)) {
								$out[] = $row;
							}
						}
					}
				}
			}
		}
		$this->setMaxRows(count($out));
		$this->setRows($out);
		return $this;
	}

	/**
	 * Get entity by entity id.
	 * @param string $EntityID
	 * @return self
	 */
	public static function GetByNodeIDs(array $nodeIds, $active=NULL, $rows=NULL,$page=NULL) {
		$where='n.`NodeID` IN('.\Pecee\DB\DB::JoinArray($nodeIds).')';
		if(!is_null($active)) {
			$where.=' AND n.`Active` = ' . Boolean::Parse($active,0);
		}
		return self::FetchPage('n.`NodeID`', 'SELECT n.* FROM `Node` n WHERE ' . $where . ' ORDER BY n.`Order` ASC', $rows, $page);
	}

	/**
	 * Get entity by entity id.
	 * @param string $EntityID
	 * @return self
	 */
	public static function GetByNodeID($nodeId, $active=NULL) {
		$where='n.`NodeID` = %s';
		if(!is_null($active)) {
			$where.=' AND n.`Active` = ' . Boolean::Parse($active,0);
		}
		return self::FetchOne('SELECT n.* FROM `Node` n WHERE ' . $where, array($nodeId));
	}

	/**
	 * Get entities.
	 * @param string|null $Query
	 * @param bool|null $Active
	 * @param int|null $Rows
	 * @param int|null $Page
	 * @return self
	 */
	public static function GetByPath($type=NULL, $query=NULL, $active=NULL, $parentNodeId=NULL, $order=NULL, $rows=NULL, $page=NULL) {
		$where=array('1=1');
		if(!is_null($active)) {
			$where[] = \Pecee\DB\DB::FormatQuery('n.`Active` = %s', array(Boolean::Parse($active)));
			$where[] = \Pecee\DB\DB::FormatQuery('(ISNULL(n.`ActiveFrom`) && ISNULL(n.`ActiveTo`) || n.`ActiveFrom` <= NOW() && (n.`ActiveTo` >= NOW() || ISNULL(n.`ActiveTo`)))');
		}
		if(!is_null($parentNodeId)) {
			if(empty($parentNodeId)) {
				$where[] = "(n.`Path` IS NULL OR n.`ParentNodeID` IS NULL)";
			} else {
				$where[] = "(n.`Path` LIKE '%".\Pecee\DB\DB::Escape($parentNodeId)."%'') ";
			}
		}
		if(!is_null($type)) {
			$where[] =  'n.`Type` = \''.$type.'\'';
		}
		if(!is_null($query)) {
			$where[] = sprintf('(n.`Title` LIKE \'%s\' OR n.`Content` LIKE \'%s\')', '%'.\Pecee\DB\DB::Escape($query).'%', '%'.\Pecee\DB\DB::Escape($query).'%');
		}
		$order=(!is_null($order) && in_array($order, self::$orders)) ? $order : self::ORDER_DATE_DESC;
		return self::FetchPage('n.NodeID', 'SELECT n.* FROM `Node` n WHERE ' . join(' && ', $where) . ' ORDER BY ' . $order, $rows, $page);
	}

	/**
	 * Get entities.
	 * @param string|null $Query
	 * @param bool|null $Active
	 * @param int|null $Rows
	 * @param int|null $Page
	 * @return self
	 */
	public static function Get($type=NULL, $query=NULL, $active=NULL, $parentNodeId=NULL, $path=NULL, $order=NULL, $rows=NULL, $page=NULL) {
		$where=array('1=1');
		if(!is_null($active)) {
			$where[] = \Pecee\DB\DB::FormatQuery('n.`Active` = %s', array(Boolean::Parse($active)));
			$where[] = \Pecee\DB\DB::FormatQuery('(ISNULL(n.`ActiveFrom`) && ISNULL(n.`ActiveTo`) || n.`ActiveFrom` <= NOW() && (n.`ActiveTo` >= NOW() || ISNULL(n.`ActiveTo`)))');
		}
		if(!is_null($parentNodeId)) {
			$where[] = "n.`ParentNodeID` = '".\Pecee\DB\DB::Escape($parentNodeId)."'";
		}

		if(!is_null($path)) {
			$where[] = "n.`Path` LIKE '>%".\Pecee\DB\DB::Escape($path)."'";
		}

		if(!is_null($type)) {
			if(is_array($type)) {
				$where[] =  'n.`Type` IN ('.\Pecee\DB\DB::JoinArray($type).')';
			} else {
				$where[] =  'n.`Type` = \''.$type.'\'';
			}

		}
		if(!is_null($query)) {
			$where[] = sprintf('(n.`Title` LIKE \'%s\' OR n.`Content` LIKE \'%s\')', '%'.\Pecee\DB\DB::Escape($query).'%', '%'.\Pecee\DB\DB::Escape($query).'%');
		}
		$order=(!is_null($order) && in_array($order, self::$orders)) ? $order : self::ORDER_ORDER_ASC;
		return self::FetchPage('n.NodeID', 'SELECT n.* FROM `Node` n WHERE ' . join(' && ', $where) . ' ORDER BY ' . $order, $rows, $page);
	}

	public static function FetchPage($countField, $query, $rows = 10, $page = 0, $args=NULL) {
		$args = (!$args || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 4));
		$model = parent::FetchPage($countField, $query, $rows, $page, $args);
		$model->setEntityFields();
		return $model;
	}

	public static function FetchRows($countField, $query, $startIndex=0, $rows = 10, $args = NULL) {
		$args = (!$args || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 4));
		$model = parent::FetchAllPage($countField, $query, $startIndex, $rows, $args);
		$model->setEntityFields();
		return $model;
	}

	public static function FetchOne($query, $args=NULL) {
		$args = (!$args || is_array($args) ? $args : \Pecee\DB\DB::ParseArgs(func_get_args(), 1));
		$model = parent::FetchOne($query, $args);
		$model->setEntityFields(TRUE);
		return $model;
	}

}
