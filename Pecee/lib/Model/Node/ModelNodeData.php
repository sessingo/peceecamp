<?php
namespace Pecee\Model\Node;
class ModelNodeData extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('NodeData', array('NodeID' => '', 'Key' => '', 'Value' => ''));
	}
	public function save() {
		if(self::Scalar('SELECT `Key` FROM `NodeData` WHERE `Key` = %s AND `NodeID` = %s', $this->NodeID, $this->Key)) {
			parent::update();
		} else {
			parent::save();
		}
	}
	
	public static function Clear($nodeId) {
		self::NonQuery('DELETE FROM `NodeData` WHERE `NodeID` = %s', array($nodeId));
	}
	
	public static function GetByNodeIDs(array $nodeIds) {
		return self::FetchAll('SELECT * FROM `NodeData` WHERE `NodeID` IN('.\Pecee\DB\DB::JoinArray($nodeIds).')');
	}
	
	public static function GetByNodeID($nodeId) {
		return self::FetchAll('SELECT * FROM `NodeData` WHERE `NodeID` = %s', array($nodeId));
	}
}