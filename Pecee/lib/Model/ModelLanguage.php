<?php
namespace Pecee\Model;
class ModelLanguage extends \Pecee\Model\Model {
	public function __construct() {
		parent::__construct('Language', array('LanguageID' => '', 'OriginalText' => '', 'TranslatedText' => '', 
												'Locale' => '', 'PageCode' => '', 'Path' => ''));
	}
	
	public static function GetPages($rows=15, $page=0) {
		return self::FetchPage('`LanguageID`', 'SELECT * FROM `Language` GROUP BY `Path` ORDER BY `Path` ASC', $rows, $page);
	}
	
	public static function GetByPath($path, $locale=null, $rows=NULL, $page=NULL) {
		$where=array(sprintf("`Path` = '%s'", \Pecee\DB\DB::Escape($path)));
		if(!is_null($locale)) {
			$where[]=sprintf("`Locale` = '%s'", \Pecee\DB\DB::Escape($locale));
		}
		return self::FetchPage('`LanguageID`', 'SELECT * FROM `Language` WHERE ' . join(' AND ', $where), $rows, $page);
	}
	
	public static function GetById($languageId) {
		return self::FetchOne('SELECT * FROM `Language` WHERE `LanguageID` = %s', $languageId);
	}
}