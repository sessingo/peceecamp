<?php
namespace Pecee\Session;
class SessionMessage {
	protected $session;
	protected $messages;
	protected static $instance;
	const KEY='MSG';
	
	public static function Instance() {
		if(is_null(self::$instance)) {
			self::$instance=new self();
		}
		return self::$instance;
	}
	
	public function __construct() {
		$this->parseMessages();
	}
	
	protected function parseMessages() {
		$this->messages= \Pecee\Session::Instance()->get(self::KEY, NULL);
	}
	
	protected function saveMessages() {
		\Pecee\Session::Instance()->set(self::KEY, $this->messages);
	}
	
	public function set(\Pecee\UI\Form\FormMessage $message, $type=NULL) {
		// Ensure no double posting
		if(isset($this->messages[$type]) && is_array($this->messages[$type])) {
			if(!in_array($message, $this->messages[$type])) {
				$this->messages[$type][] = $message;
				$this->saveMessages();
			}
		} else {
			$this->messages[$type][] = $message;
			$this->saveMessages();
		}
	}
	
	/**
	 * Get messages
	 * @param string|NULL $type
	 * @param string|NULL $default
	 * @return \Pecee\UI\Form\FormMessage|NULL
	 */
	public function get($type=NULL, $default=NULL) {
		if(!is_null($type)) {
			return (isset($this->messages[$type])) ? $this->messages[$type] : $default;
		}
		return $this->messages;
	}
	
	/**
	 * Checks if there's any messages
	 * @param string|null $type
	 * @return boolean
	 */
	public function hasMessages($type=NULL) {
		if(!is_null($type)) {
			return (isset($this->messages[$type]) && count($this->messages[$type]) > 0);
		}
		return (count($this->messages) > 0);
	}
	
	public function clear($type=NULL) {
		if(!is_null($type)) {
			unset($this->messages[$type]);
			$this->saveMessages();
		} else {
			\Pecee\Session::Instance()->destroy(self::KEY);
		}
	}
}