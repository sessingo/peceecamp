<?php
namespace Pecee\IO;
class File {

	/**
	 * Recursivly delete folder from filesystem
	 * @param unknown_type $dir
	 * @return boolean
	 */
	public static function DeleteTree($path) {
		$path = self::HasEndingDirectorySeperator($path) ? $path : $path . DIRECTORY_SEPARATOR;
		$files = glob($path . '*', GLOB_MARK);
	    foreach($files as $file){
	        if(substr($file, -1) == DIRECTORY_SEPARATOR)
	            self::DeleteTree($file);
	        else
	            @unlink($file);
	    }
	    @rmdir($path);
	}

	public static function PathExist($path) {
		$inc = explode(PATH_SEPARATOR, get_include_path());
		foreach($inc as $prefix){
			if(substr($prefix,-1) == DIRECTORY_SEPARATOR)
				$prefix = substr($prefix,0,-1);
			$p = sprintf("%s%s%s", $prefix, DIRECTORY_SEPARATOR, $path);
			if(file_exists($p))
				return($p);
		}
		return FALSE;
	}

	public static function RemoteFileSize($url) {
		$headers = get_headers($url, 1);
		if (isset($headers['Content-Length'])) return $headers['Content-Length'];
		if (isset($headers['Content-length'])) return $headers['Content-length'];
		$c = curl_init();
		curl_setopt_array($c, array(CURLOPT_URL => $url,
									CURLOPT_RETURNTRANSFER => TRUE,
									CURLOPT_HTTPHEADER => array('User-Agent: Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3'),));
		curl_exec($c);
		return curl_getinfo($c, CURLINFO_SIZE_DOWNLOAD);
	}
	public static function RemoteExist($url) {
		$curl = curl_init($url);
	    curl_setopt($curl, CURLOPT_NOBODY, TRUE);
	    $result = curl_exec($curl);
	    if ($result) {
	        //if request was ok, check response code
	        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	        if ($statusCode == 200) {
	            return TRUE;
	        }
	    }
	    curl_close($curl);
	    return FALSE;
	}
	public static function GetMimeType($file) {
		if(\Pecee\Url::IsValid($file)) {
			$ch = curl_init($file);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
			curl_setopt($ch, CURLOPT_HEADER, TRUE);
			curl_setopt($ch, CURLOPT_NOBODY, TRUE);

			curl_exec($ch);
			return curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
		} else if (function_exists('mime_content_type')){
			return mime_content_type($file);
		} else if (function_exists('finfo_file')){
			$finfo = finfo_open(FILEINFO_MIME);
		    $mtype = finfo_file($finfo, $file);
		    finfo_close($finfo);
		    return $mtype;
		}
		return 'application/force-download';
	}
	public static function GetExtension($path) {
		return pathinfo($path, PATHINFO_EXTENSION);
	}
	public static function HasEndingDirectorySeperator($path) {
		return (strlen($path) > 0 && $path[strlen($path)-1] == DIRECTORY_SEPARATOR);
	}
	/**
	 * Creates all folders of a specified path if they not exists.
	 * @param string $path
	 */
	public static function CreatePath($path) {
		$path=explode(DIRECTORY_SEPARATOR, $path);
		$root=$path[0];
		unset($path[0]);
		foreach($path as $p) {
			$root=$root. DIRECTORY_SEPARATOR . $p;
			if(!is_dir($root) && !is_file($root)) {
				@mkdir($root);
				@chmod($root, 0777);
			}
		}
	}

	public static function Move($source, $destination) {
		if(is_dir($source)) {
			if(!is_dir($destination)) {
				mkdir($destination);
			}
			$files = scandir($source);
			foreach($files as $file) {
				if($file != "." && $file != "..") {
					self::Move("$source/$file", "$destination/$file");
				}
			}
		} elseif(file_exists($source)) {
			copy($source, $destination);
		}
	}
}