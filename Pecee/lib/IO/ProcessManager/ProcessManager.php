<?php
namespace Pecee\IO\ProcessManager;
class Processmanager{
	public $maxProcesses;
	protected $debug;
	public $sleepTimeout;
	protected $running;
	protected $scripts; 
	protected $processesRunning = 0;
	
	public function __construct() {
		$this->debug = FALSE;
		$this->maxProcesses = 3;
		$this->processesRunning = 0;
		$this->running = array();
		$this->scripts = array();
	}
	
	public function addScript($script, $max_execution_time = 300){
		$this->scripts[] = array("script_name"=>$script, "max_execution_time"=>$max_execution_time);
	}
	
	public function exec(){
		$i = 0;
		for(;;) {
			// Fill up the slots
			while(($this->processesRunning < $this->maxProcesses) and ($i < count($this->scripts))) {
				if($this->debug) {
					//ob_start();
					//echo "Adding script: " . $this->scripts[$i]["script_name"] . chr(10);
					//ob_flush();
					//flush();
				}
				
				$this->running[] = & new \Pecee\IO\ProcessManager\Process($this->scripts[$i]["script_name"], $this->scripts[$i]["max_execution_time"]);
				$this->processesRunning ++;
				$i ++;
			}
			
			// Check if done
			if(($this->processesRunning == 0) and ($i >= count($this->scripts))) {
				break;
			}
			
			// sleep, this duration depends on your script execution time, the
			// longer execution time, the longer sleep time
			sleep($this->sleepTimeout);
			
			// check what is done
			foreach($this->running as $key=>$val) {
				if(!$val->isRunning() || $val->isOverExecuted()) {
					proc_close($val->resource);
					unset($this->running[$key]);
					$this->processesRunning --;
				} else {
					sleep(10);
				}
			}
		}
	}
}