<?php
namespace Pecee\IO\ProcessManager;
class Process{
	public $resource;
	public $pipes;
	public $script;
	public $max_execution_time;
	public $start_time;
	function __construct(&$executable, $max_execution_time){
		$this->script = $executable;
		$this->max_execution_time = $max_execution_time;
		$descriptorspec = array( 0 => array('pipe','r'),
									1 => array('pipe','w'),
									2 => array('pipe','w'));
		
		$this->resource = popen($executable, 'r');
		$this->start_time = time();
	}
	
	// is still running?
	function isRunning(){
		$status = proc_get_status($this->resource);
		return $status["running"];
	}
	
	// long execution time, proccess is going to be killer
	function isOverExecuted(){
		return ($this->start_time + $this->max_execution_time < time());
	}
}