<?php
namespace Pecee;
class Boolean {
	public static function Parse($str, $default=FALSE) {
		$bool=filter_var($str, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
		return is_null($bool) ? $default : $bool;
	}
}