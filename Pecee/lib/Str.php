<?php
namespace Pecee;
class Str {
	public static function removeNewlines($input){
		return preg_replace('/[\n]/', ' ',$input); 
	}
	public static function removeTabs($input) {
		return preg_replace('/[\t]/','',$input); 
	}
	public static function removeEnters($input) {
		return preg_replace('/[\r]/','',$input); 
	}
	public static function GetFirstOrValue($value,$defaultValue=NULL){
		return ($value && !empty($value)) ? trim($value) : $defaultValue;
	}
	public static function RemoveSlashes($string) {
		return str_replace('\\"', '"', str_replace("\\'", "'", $string));		
	}
	public static function isUTF8($str) {
        return ($str === mb_convert_encoding(mb_convert_encoding($str, "UTF-32", "UTF-8"), "UTF-8", "UTF-32")) ? TRUE : FALSE;
    }
	public static function StripTags($string,$allowedTags = '') {
		$tagsAr = explode(',',str_ireplace(array('<','>'),'',$allowedTags));
		preg_match_all('/<([\!A-Z0-9\-\_]*)[^>]*>/is',$string,$matches);
		$tagsProcessed = array();
		foreach($matches[1] as $tag) {
			if (!in_array($tag,$tagsAr) && !in_array($tag,$tagsProcessed)) {
				$tagsProcessed[] = $tag;
				$string = preg_replace('/<'.$tag.'[^>]*>[^<]*<\/'.$tag.'>/is',' ', $string);
				$string = preg_replace('/<'.$tag.'[^>]*>[^<]*</is',' <', $string);
				$string = preg_replace('/>[^>]*<\/'.$tag.'>/is','> ', $string);
				$string = preg_replace('/<'.$tag.'[^>]*\/>/is',' ', $string);
				$string = preg_replace('/<'.$tag.'[^>]*>/is',' ', $string);
				$string = preg_replace('/<\/'.$tag.'>/is',' ', $string);
				$string = preg_replace('/[ \t]{2,}/is','', $string);
				$string = str_replace("\r","", $string);
				$string = str_replace("\n\n","\n", $string);
			}
        }
        return $string;
    }
	
	/**
	 * Parses a String of Tags
	 *
	 * Tags are space delimited. Either single or double quotes mark a phrase.
	 * Odd quotes will cause everything on their right to reflect as one single
	 * tag or phrase. All white-space within a phrase is converted to single
	 * space characters. Quotes burried within tags are ignored! Duplicate tags
	 * are ignored, even duplicate phrases that are equivalent.
	 *
	 * Returns an array of tags.
	 */
	public static function ParseTagString($sTagString) {
		$arTags = array();		// Array of Output
		$cPhraseQuote = null;	// Record of the quote that opened the current phrase
		$sPhrase = null;		// Temp storage for the current phrase we are building
		
		// Define some constants
		static $sTokens = " \r\n\t-,.?()0123456789'/\"'\\;:";	// Space, Return, Newline, Tab
		static $sQuotes = "'\"";		// Single and Double Quotes
		$sToken = null;
		// Start the State Machine
		do {
			// Get the next token, which may be the first
			$sToken = isset($sToken)? strtok($sTokens) : strtok($sTagString, $sTokens);
			
			// Are there more tokens?
			if ($sToken === false) {
				// Ensure that the last phrase is marked as ended
				$cPhraseQuote = null;
			} else {		
				// Are we within a phrase or not?
				if ($cPhraseQuote !== null) {
					// Will the current token end the phrase?
					if (substr($sToken, -1, 1) === $cPhraseQuote) {
						// Trim the last character and add to the current phrase, with a single leading space if necessary
						if (strlen($sToken) > 1) $sPhrase .= ((strlen($sPhrase) > 0)? ' ' : null) . substr($sToken, 0, -1);
						$cPhraseQuote = null;
					} else {
						// If not, add the token to the phrase, with a single leading space if necessary
						$sPhrase .= ((strlen($sPhrase) > 0)? ' ' : null) . $sToken;
					}
				} else {
					// Will the current token start a phrase?
					if (strpos($sQuotes, $sToken[0]) !== false) {
						// Will the current token end the phrase?
						if ((strlen($sToken) > 1) && ($sToken[0] === substr($sToken, -1, 1))) {
							// The current token begins AND ends the phrase, trim the quotes
							$sPhrase = substr($sToken, 1, -1);
						} else {
							// Remove the leading quote
							$sPhrase = substr($sToken, 1);
							$cPhraseQuote = $sToken[0];
						}
					} else {
						$sPhrase = $sToken;
					}
				}
			}
			
			// If, at this point, we are not within a phrase, the prepared phrase is complete and can be added to the array
			if (($cPhraseQuote === null) && ($sPhrase != null)) {
				$sPhrase = strtolower($sPhrase);
				if (!in_array($sPhrase, $arTags)) $arTags[] = $sPhrase;
				$sPhrase = null;
			}
		}
		while ($sToken !== false);	// Stop when we receive FALSE from strtok()
		return $arTags;
	}
	
	/**
	 * Substring's a given input and add's '...' if the string
	 * is longer than $maxLength
	 *
	 * @param string $text
	 * @param string $maxLength
	 * @return string
	 */
	public static function SubWord($text, $maxLength, $end='...') {	
		if (strlen($text) > $maxLength) { 
			$whitespaceposition = @strpos($text," ",($maxLength-20)); 
			return substr($text, 0, $whitespaceposition).$end; 
		} 
		return $text;
	}
	
	public static function SubStr($text, $maxLength, $end='...', $encoding = 'UTF-8') {
		if(strlen($text) > $maxLength) {
			$output = mb_substr($text, 0, $maxLength, $encoding);
			if(strlen($text) > $maxLength)
				$output .= $end;
			return $output;
		}
		return $text;
	}
	
	public static function WordWrap($text, $maxLength=25) {
		if(!\Pecee\Integer::is_int($maxLength)) throw new \Exception("Unknown datatype for maxLength. Must be INT or nummeric string.");
		if(!empty($text)) return preg_replace('/([^\s]{'.$maxLength.'})(?=[^\s])/', '$1', $text);
		return $text;
	}
	
	/**
	 * Searches text for given string and returns a span with given className.
	 *
	 * @param string $searchFor
	 * @param string $input
	 * @param string $hightLightClassName
	 * @param bool $specific
	 * @return string
	 */
	
	public static function HighLightWord($searchFor, $input, $hightLightClassName, $specific=false) {
		$words = explode(' ', $input);
		$output = '';
		foreach($words as $word) {
			if(strtolower($searchFor) == strtolower($word) || $specific == false && strstr(strtolower($word), strtolower($searchFor))) {
				$output .= '<span class="'.$hightLightClassName.'">'.$word.'</span> ';
			} else {
				$output .= $word.' ';
			}
		}
		return $output;
	}
	
	public static function HtmlEntities($Value) {
		if( is_array($Value) ) {
			$newArr = array();
			foreach( $Value as $key=>$v ) {
				$newArr[$key] = htmlentities($v, null, 'UTF-8');
			}
			return $newArr;
		}
		return htmlentities($Value, ENT_QUOTES, 'UTF-8');
	}
	
	public static function HtmlEntitiesDecode($Value) {
		if( is_array($Value) ) {
			$newArr = array();
			foreach( $Value as $key=>$v ) {
				$newArr[$key] = html_entity_decode($v, null, 'UTF-8');
			}
			return $newArr;
		}
		return html_entity_decode($Value, ENT_QUOTES, 'UTF-8');
	}
	
	/**
	 * Spots links in a context, and make them clickable.
	 * @param string $var
	 * @return string
	 */
	public static function MakeLink($text, $format1='<a href="\\0" rel="nofollow" title="">\\0</a>', 
											$format2='<a href="http://\\2" title="" rel="nofollow">\\2</a>', 
											$format3='<a href="mailto:\\1" title="">\\1</a>') {
		// match protocol://address/path/
		$text = preg_replace("/[a-zA-Z]+:\\/\\/([.\\/-]?[a-zA-Z0-9_\\/-\\/&\\/%\\/?\\/=])*/is", $format1, $text);
		// match www.something
		$text = preg_replace("/(^|\\s)(www\\.([a-zA-Z0-9_\\/-\\/.])*)/is", $format2, $text);
		// match me@something.com
		return preg_replace('/([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})/is', $format3, $text);
	}
	
	public static function CleanXML($xml) {
		$tidy_options = array(
                'input-xml'    => true,
                'output-xml'   => true,
                'indent'       => true,
                'wrap'         => false,
        );

		$tidy = new \tidy();
		$tidy->parseString(trim($xml), $tidy_options);
		$tidy->cleanRepair();
		return $tidy;
	}
	
	public static function Normalize($string,$html = true) {
		//Replace odd white spaces with normal white space...
		$str = str_replace(chr(194).chr(160),chr(32),$string);
		$str = str_replace("\t","    ",$str);
		if (strstr($str,chr(13))) {
			if (strstr($str,chr(10)))
				$str = str_replace (chr(13),'',$str);
			else
				$str = str_replace (chr(13),chr(10),$str);
		}
		if ($html)
			$str = str_replace(chr(10),'<br/>',$str);
		return $str;
	}
}