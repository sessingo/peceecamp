<?php
namespace Pecee;
class Browser {
	const TYPE_CHROME_ALL = 'Chrome';
	const TYPE_CHROME_V1 = '(Chrome\/1\.*)';
	const TYPE_CHROME_V2 = '(Chrome\/2\.*)';
	const TYPE_CHROME_V3 = '(Chrome\/3\.*)';
	const TYPE_CHROME_V4 = '(Chrome\/4\.*)';
	const TYPE_CHROME_V5 = '(Chrome\/5\.*)';
	const TYPE_CHROME_V6 = '(Chrome\/6\.*)';
	const TYPE_CHROME_V7 = '(Chrome\/7\.*)';
	const TYPE_CHROME_V8 = '(Chrome\/8\.*)';
	const TYPE_CHROME_V9 = '(Chrome\/9\.*)';
	const TYPE_CHROME_V10 = '(Chrome\/10\.*)';
	const TYPE_CHROME_V11 = '(Chrome\/11\.*)';
	const TYPE_CHROME_V12 = '(Chrome\/12\.*)';
	const TYPE_CHROME_V13 = '(Chrome\/13\.*)';
	const TYPE_CHROME_V14 = '(Chrome\/14\.*)';
	const TYPE_CHROME_V15 = '(Chrome\/15\.*)';
	const TYPE_CHROME_V16 = '(Chrome\/16\.*)';
	const TYPE_CHROME_V17 = '(Chrome\/17\.*)';
	const TYPE_CHROME_V18 = '(Chrome\/18\.*)';
	const TYPE_FIREFOX_ALL = 'firefox';
	const TYPE_FIREFOX_V1 = 'firefox_1';
	const TYPE_FIREFOX_V2 = 'firefox_2';
	const TYPE_FIREFOX_V3 = 'firefox_3';
	const TYPE_FIREFOX_V4 = 'firefox_4';
	const TYPE_FIREFOX_V5 = 'firefox_5';
	const TYPE_FIREFOX_V6 = 'firefox_6';
	const TYPE_FIREFOX_V7 = 'firefox_7';
	const TYPE_FIREFOX_V8 = 'firefox_8';
	const TYPE_FIREFOX_V9 = 'firefox_9';
	const TYPE_FIREFOX_V10 = 'firefox_10';
	const TYPE_FIREFOX_V11 = 'firefox_11';
	const TYPE_INTERNET_EXPLORER_ALL = 'MSIE';
	const TYPE_INTERNET_EXPLORER_V5 = 'MSIE 5';
	const TYPE_INTERNET_EXPLORER_V6 = 'MSIE 6';
	const TYPE_INTERNET_EXPLORER_V7 = 'MSIE 7';
	const TYPE_INTERNET_EXPLORER_V8 = 'MSIE 8';
	const TYPE_INTERNET_EXPLORER_V9 = 'MSIE 9';
	const TYPE_WINDOWS_MOBILE = '';
	const TYPE_APPLE_IPHONE = 'iPhone';
	const TYPE_APPLE_IPAD = 'iPad';
	const TYPE_ANDROID = 'Android';
	protected static $types = array(self::TYPE_CHROME_V18,
									self::TYPE_CHROME_V17,
									self::TYPE_CHROME_V16,
									self::TYPE_CHROME_V15,
									self::TYPE_CHROME_V14,
									self::TYPE_CHROME_V13,
									self::TYPE_CHROME_V12,
									self::TYPE_CHROME_V11,
									self::TYPE_CHROME_V10,
									self::TYPE_CHROME_V9,
									self::TYPE_CHROME_V8,
									self::TYPE_CHROME_V7,
									self::TYPE_CHROME_V6,
									self::TYPE_CHROME_V5,
									self::TYPE_CHROME_V4,
									self::TYPE_CHROME_V3,
									self::TYPE_CHROME_V2,
									self::TYPE_CHROME_V1,
									self::TYPE_CHROME_ALL,
									self::TYPE_FIREFOX_V11,
									self::TYPE_FIREFOX_V10,
									self::TYPE_FIREFOX_V9,
									self::TYPE_FIREFOX_V8,
									self::TYPE_FIREFOX_V7,
									self::TYPE_FIREFOX_V6,
									self::TYPE_FIREFOX_V5,
									self::TYPE_FIREFOX_V4,
									self::TYPE_FIREFOX_V3,
									self::TYPE_FIREFOX_V2,
									self::TYPE_FIREFOX_V1,
									self::TYPE_FIREFOX_ALL,
									self::TYPE_INTERNET_EXPLORER_V5,
									self::TYPE_INTERNET_EXPLORER_V6,
									self::TYPE_INTERNET_EXPLORER_V7,
									self::TYPE_INTERNET_EXPLORER_V8,
									self::TYPE_INTERNET_EXPLORER_V9,
									self::TYPE_INTERNET_EXPLORER_ALL,
									self::TYPE_APPLE_IPHONE,
									self::TYPE_APPLE_IPAD,
									self::TYPE_ANDROID );
									
	public static function Match( array $types ) {
		return (in_array(self::GetType(), $types));
	}
	public static function MatchString($string, array $types) {
		return (in_array(self::GetTypeByString($string), $types));
	}
	public static function GetLanguage() {
		return (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) ? substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) : NULL;
	}
	public static function GetLocale() {
		return (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : NULL;
	}
	public static function GetLanguageAndCountry() {
		return (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) ? substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, strpos($_SERVER['HTTP_ACCEPT_LANGUAGE'], ',')) : NULL;
	}
	public static function GetUserAgent() {
		return $_SERVER['HTTP_USER_AGENT'];
	}
	public static function GetType() {
		return self::GetTypeByString(self::GetUserAgent());
	}
	public static function GetTypeByString($string) {
		foreach(self::$types as $type) {
			if(preg_match('/' . $type . '/is', $string)) {
				return $type;
			}
		}
		return null;
	}
}