<?php
namespace Pecee;
class Util {	
	/**
	 * Converts html to rgb
	 * Taken directly from:
	 * http://php.dtbaker.com.au/post/transform_hex_html_colors_to_rbg_colors
	 *
	 * @param string $color
	 * @param bool $returnstring
	 * @return array|false
	 */
	public static function html2rgb($color,$returnstring=false) {
		if($color[0] == '#') {
	    	$color = substr($color, 1);
	    }
	    if(strlen($color) == 6) {
	    	list($r, $g, $b) = array($color[0].$color[1], $color[2].$color[3], $color[4].$color[5]);
	    } elseif(strlen($color) == 3) {
	    	list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
	    } else {
	        return false;
	    }
	    // use this for normal range 0 to 255 eg: (0, 255, 50)
	    $key = 1; 
	    $r = hexdec($r)*$key;
	    $g = hexdec($g)*$key;
	    $b = hexdec($b)*$key;
	    return ($returnstring) ? "{rgb $r $g $b}" : array($r, $g, $b);
	}
	
	/**
	 * Returns weather the $value is a valid email.
	 * @param string $value
	 * @return bool
	 */
	public static function is_email($email) {
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}
	
	/**
	 * Check if content is valid XML.
	 *
	 * @param string $content
	 * @return bool
	 */
	public static function is_valid_xml($content) {
		if(!@simplexml_load_string($content))
			return false;
		return true;
	}

	public static function CalculateRating($ratingsTotal, $ratingVotes) {
		if(!\Pecee\Integer::is_int($ratingsTotal) || !\Pecee\Integer::is_int($ratingVotes)) return false;
		if($ratingVotes == 0) return 0;
		return round($ratingsTotal/$ratingVotes, 1);
	}
	
	private static function SendOut($receptiant, $subject, $content, $plainText, $fromEmail, $replyToEmail) {
		$headers = 'From: '.$fromEmail . "\r\n" . 'Reply-To: '. $replyToEmail . "\r\n";
		if(!$plainText) {
			// To send HTML mail, the Content-type header must be set
			$headers .= 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset='.\Pecee\UI\Site::GetInstance()->getCharset() . "\r\n";
		}
		mail($receptiant, $subject, $content, $headers);
	}

	/**
	 * Function to send email(s)
	 *
	 * @param array or string $receptiant
	 * @param string $subject
	 * @param string $content
	 * @param bool $plainText
	 */	
	public static function SendMail($receptiants, $subject, $content, $fromEmail, $replyToEmail=NULL, $plainText=TRUE) {
		if(!$replyToEmail) {
			$replyToEmail = $fromEmail;
		}
		if(is_array($receptiants)) {
			foreach($receptiants as $receptiant) {
				if(\Pecee\Util::is_email($receptiant)) {
					self::SendOut($receptiant, $subject, $content, $plainText, $fromEmail, $replyToEmail);
				}
			}
			return TRUE;
		} else {
			if(\Pecee\Util::is_email($receptiants)) {
				self::SendOut($receptiants, $subject, $content, $plainText, $fromEmail, $replyToEmail);
				return TRUE;
			}
		}
		return FALSE;
	}
	
	public static function GetTypeOf( $object ) {
		$typeof=NULL;
		if(is_array($object)) {
			foreach($object as $o) {
				$typeof=class_parents($o);
				break;
			}
		} else {
			$typeof=class_parents($object);
		}
		if( is_array($typeof) ) {
			$typeof=array_values($typeof);
			return $typeof[0];
		}
		return $typeof;
	}
}