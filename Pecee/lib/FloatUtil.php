<?php
namespace Pecee;
class FloatUtil {
	public static function is_float($val) {
		return is_float(filter_var($val, FILTER_VALIDATE_FLOAT));
	}
	
	public static function ParseFloat($val) {
		return (self::is_float($val)) ? $val : floatval(str_replace(array('.', ','), array('', '.'), $val));
	}
}