<?php
namespace Pecee;
class Date {
	public static function IsValid( $str ) {
		$stamp = strtotime($str);
		if (!is_numeric($stamp)){
			return FALSE;
		}
		$month = date('m', $stamp);
		$day = date('d', $stamp);
		$year  = date( 'Y', $stamp);
		if (checkdate($month, $day, $year)) {
			return TRUE;
		}
		return FALSE;
	}
	
	public static function GetMinutesAgo($time) {
		return (round(((time() - $time) / 60)));
	}
	
	/**
	 * Returns age by given string date.
	 *
	 * @param int $dateTime
	 * @return int
	 */
	public static function GetAge($dateTime) {
		$d = date('d', $dateTime);
		$m = date('m', $dateTime);
		$Y = date('Y', $dateTime);
	    return( date('md') < $m.$d ? date('Y')-$Y-1 : date('Y')-$Y );
	}
	
	public static function FromToDateTime($dateTime) {
		return self::ToDateTime(strtotime($dateTime));
	}
	
	public static function ToDate($unixTime=NULL) {
		return date("Y-m-d", (is_null($unixTime) ? time() : $unixTime));
	}
	
	public static function ToDateTime($unixTime=NULL) {
		return date("Y-m-d H:i:s", (is_null($unixTime) ? time() : $unixTime));
	}
	
	/**
	 * Get days until date
	 * @param int $unixTime
	 * @return number
	 */
	public static function DaysUntil($unixTime) {
		$today = time();
		$difference = $unixTime - $today;
		if ($difference < 0) { 
			$difference = 0; 
		}
		return ceil($difference/60/60/24);
	}
	
	/**
	 * Parse date to unix-time, weather it is dc time or MS datetime.
	 * @param string $Date
	 * @return int
	 */
	public static function ParseDate($Date) {
    	// first see if strtotime manages it.
    	$matches = array();
   		$timestamp = strtotime($Date);  // strtotime() returns -1 on failure
        /*
         * You might think from the php docs at http://us4.php.net/strtotime
		 * that php strtotime uses gnu strtotime. It doesn't. Rather, it implements its own grammar at: 
		 * http://cvs.php.net/php-src/ext/standard/parsedate.y
		 * In http://cvs.php.net/php-src/ext/standard/
		 * As you can see there, the support for parsing ISO8601 dates is poor prior to v1.52 of Mar 1 05:42:27 2004
		 *
		 * As an example feed that has dates with timezone offsets, see
		 * http://www.rpgdot.com/team/rss/rss13.xml
		 * For example, it has dc:date of "2004-06-15T09:55+06:00".
         */
        // remove numerical timezone, and parse that ourselves
        if(preg_match('/(.*)([\+\-]\d\d):?(\d\d)/', $Date, $matches)) {
            $timestamp = strtotime($matches[1]);
            $secs = 0 + $matches[2] * 3600 + $matches[3] * 60;
            // timezone offsets are local time minus UTC. so UTC = localtime - offset.
            $timestamp -= $secs;
        }
        return $timestamp;
	}
	
}