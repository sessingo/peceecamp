<?php
namespace Pecee\UI;
class Site {
	
	const DOCTYPE_HTML_5 = '<!DOCTYPE html>';
	const DOCTYPE_XHTML_DEFAULT = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
	const DOCTYPE_XHTML_STRICT = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
	
	const CHARSET_UTF8 = 'UTF-8';
	
	// Settings
	public static $docTypes = array(self::DOCTYPE_XHTML_DEFAULT, self::DOCTYPE_XHTML_STRICT);
	private static $instance;
	
	const LOCATION_LEFT = 'LEFT';
	const LOCATION_RIGHT = 'RIGHT';
	const LOCATION_BOTTOM = 'BOTTOM';
	const LOCATION_TOP_HEADER = 'TOP_HEADER';
	const LOCATION_TOP = 'TOP';
	const LOCATION_TOP_RIGHT = 'TOP_RIGHT';
	const LOCATION_TOP_LEFT = 'TOP_LEFT';
	
	protected $docType;
	protected $charset;
	protected $title;
	protected $description;
	protected $keywords;
	protected $header = array();
	protected $js = array();
	protected $css = array();
	protected $jsPath;
	protected $cssPath;
	protected $jsFilesWrapped = array();
	protected $cssFilesWrapped = array();
	protected $location = array();
	protected $debug;
	protected $locale;
	
	/**
	 * Get new instance
	 * @return \Pecee\UI\Site
	 */
	public static function GetInstance() {
		if(is_null(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	public function __construct() {
		// Load default settings
		$this->docType = self::DOCTYPE_HTML_5;
		$this->charset = self::CHARSET_UTF8;
		$this->jsPath = 'js/';
		$this->cssPath = 'css/';
		$this->keywords = array();
		$this->debug = \Pecee\Auth::GetInstance()->hasAdminIP();
		$this->locale = \Pecee\Locale::Instance();
	}
	
	/**
	 * Add widget to location
	 * @param \Pecee\UI\Menu\Menu $widget
	 * @param string $location
	 */
	public function addToLocation(\Pecee\UI\Menu\Menu $widget, $location) {
		$this->location[$location] = $widget;
	}
	
	/**
	 * Get location content.
	 * @param string $location
	 * @return string
	 */
	public function LOCATION($location) {
		return isset($this->location[$location]) ? $this->location[$location] : NULL;
	}
	
	public function getTitle() {
		return $this->title;
	}
	
	public function setTitle($title) {
		$this->title = $title;
	}
	
	public function getDescription() {
		return $this->description;
	}
	
	public function setDescription($description) {
		$this->description = $description;
	}
	
	public function setDocType($doctype) {
		if(!in_array($doctype, self::$docTypes))
			throw new \InvalidArgumentException('Unknown doctype.');
		$this->docType = $doctype;
	}
	public function getDocType() {
		return $this->docType;
	}
	
	public function getCharset() {
		return $this->charset;
	}
	public function addWrappedJs($filename) {
		if(!in_array($filename, $this->jsFilesWrapped)) {
			$this->jsFilesWrapped[] = $filename;
		}
	}
	public function removeWrappedJs($filename) {
		if(in_array($filename, $this->jsFilesWrapped)) {
			$key = array_search($filename, $this->jsFilesWrapped);
			unset($this->jsFilesWrapped[$key]);
		}
	}
	
	public function removeWrappedCss($filename) {
		if(in_array($filename, $this->cssFilesWrapped)) {
			$key = array_search($filename, $this->cssFilesWrapped);
			unset($this->cssFilesWrapped[$key]);
		}
	}
	
	public function addWrappedCss($filename) {
		if(!in_array($filename, $this->cssFilesWrapped)) {
			$this->cssFilesWrapped[] = $filename;
		}
	}
	
	public function setDebug($debug) {
		$this->debug = $debug;
	}
	
	public function getDebug() {
		return $this->debug;
	}
	
	public function addCss($path) {
		$css = new Html\HtmlLink($path, 'stylesheet', 'text/css');
		if(!in_array($css, $this->css)) {
			$this->css[] = $css;
		}
	}
	public function addJs($path) {
		$js = new Html\HtmlScript($path);
		if(!in_array($js, $this->js)) {
			$this->js[] = $js;
		}
	}
	public function clearCss() {
		$this->cssFilesWrapped=array();
	}
	public function clearJs() {
		$this->jsFilesWrapped=array();
	}
	public function setCssPath($path) {
		$this->cssPath = $path;
	}
	public function setJsPath($path) {
		$this->jsPath = $path;
	}
	public function getCssPath() {
		return $this->cssPath;
	}
	public function getJsPath() {
		return $this->jsPath;
	}
	
	public function setKeywords(array $keywords) {
		$this->keywords = $keywords;
		return $this;
	}
	
	public function getKeywords() {
		return $this->keywords;
	}
	
	public function addMeta($name, $content) {
		$meta=new \Pecee\UI\Html\HtmlMeta($content);
		$meta->addAttribute('name', $name);
		return $this->addHeader($meta);
	}
	
	public function addHeader(\Pecee\UI\Html\Html $el) {
		$this->header[] = $el;
		return $this;
	}
	
	public function getJsFilesWrapped() {
		return $this->jsFilesWrapped;
	}
	
	public function getCssFilesWrapped() {
		return $this->cssFilesWrapped;
	}
	
	public function getJs() {
		return $this->js;
	}
	
	public function getCss() {
		return $this->css;
	}
	
	public function getHeader() {
		return $this->header;
	}
	
}