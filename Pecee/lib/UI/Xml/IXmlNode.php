<?php
namespace Pecee\UI\Xml;
interface IXmlNode {
	public function __toString();
	/**
	 * @param XmlNode $parent
	*/
	public function setParent($parent);
	/**
	 * @return XmlNode
	*/
	public function getParent();
}