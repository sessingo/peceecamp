<?php 
namespace Pecee\UI;
class Paging {
	
	public static function DisplayNumberedPaging($maxNumbers = 3, $pageIndex, $numberOfPages, $querystringFormat = '?page=%1$d' ) {	
		$return = null;
		for($i=max($pageIndex-$maxNumbers,0);$i<$pageIndex;$i++) {
			$return .= '<a class="previous" href="'.sprintf($querystringFormat,$i).'">' .($i+1) . '</a> ';
		}
		$return .= '<span class="current">' . ($pageIndex+1) .'</span> ';
		for($i=$pageIndex+1;$i<min($numberOfPages, ($pageIndex+$maxNumbers+1));$i++) {
			$return .= '<a class="next" href="'.sprintf($querystringFormat,$i).'">' . ($i+1) . '</a> ';
		}
		return $return;
	}
	
	public static function DisplayPaging( $pageIndex, $numberOfPages, $querystringFormat = '?page=%1$d', $prevText = 'Forrige', $nextText = 'Næste' ){
		$prevPageIndex = ($pageIndex - 1);
		if( $prevPageIndex >= 0 ){
			echo '<div class="prevLink"><a href="'.sprintf($querystringFormat,$prevPageIndex).'">'.$prevText.'</a></div>';
		}
	
		$nextPageIndex = ($pageIndex + 1);
		if( $nextPageIndex < $numberOfPages ){
			echo '<div class="nextLink"><a href="'.sprintf($querystringFormat,$nextPageIndex).'">'.$nextText.'</a></div>';
		}	
	}
	
	public static function DisplayPagingWithIcons( $pageIndex, $numberOfPages, $querystringFormat = '?page=%1$d', $prevText = 'Forrige', $nextText = 'Næste', $showText=false, $className='LinkIcon' ){
		$prevPageIndex = ($pageIndex - 1);
		if( $prevPageIndex >= 0 ){
			echo '<a class="prev'.$className.'" href="'.sprintf($querystringFormat,$prevPageIndex).'"><img src="/gfx/media/pre.gif" alt="'.$prevText.'" style="vertical-align:middle;">';
			if($showText) { echo $prevText; }
			echo '</a>';
		}
	
		$nextPageIndex = ($pageIndex + 1);
		if( $nextPageIndex < $numberOfPages ){
			echo '<a class="next'.$className.'" href="'.sprintf($querystringFormat,$nextPageIndex).'">';
			if($showText) { echo $nextText; }
			echo '<img src="/gfx/media/next.gif" alt="'.$nextText.'" style="vertical-align:middle;"></a>';
		}	
	}
	
}