<?php
namespace Pecee\UI\Html;
class HtmlElement extends \Pecee\UI\Xml\XmlElement implements \Pecee\UI\Html\IHtmlNode {

	public function isContainer() {
		switch (strtolower($this->getTag())) {
			case 'div':
			case 'span':
			case 'strong':
			case 'a':
			case 'b':
			case 'em':
			case 'i':
			case 'ul':
			case 'li':
			case 'ol':
			case 'dd':
			case 'dt':
			case 'dl':
			case 'table':
			case 'tr':
			case 'thead':
			case 'tbody':
			case 'tfoot':
			case 'td':
			case 'th':
			case 'script':
			case 'title':
			case 'head':
			case 'body':
			case 'textarea':
			case 'html':
			case 'pre':
			case 'code':
			case 'h1':
			case 'h2':
			case 'h3':
			case 'h4':
			case 'h5':
			case 'p':
			case 'blink':
			case 'script':
				return true;
		}
		return false;
	}

}