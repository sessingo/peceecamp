<?php
namespace Pecee\UI\Html;
use Pecee\Str;

class HtmlTextarea extends \Pecee\UI\Html\Html {
	protected $value;
	public function __construct($name, $rows, $cols, $value=NULL) {
		parent::__construct('textarea');
		$this->value=Str::HtmlEntities($value);
		//$this->closingTag = false;
		$this->addAttribute('name', $name);
		$this->addAttribute('rows', $rows);
		$this->addAttribute('cols', $cols);
		$this->setInnerHtml($this->value);
	}
	
	public function getValue() {
		return $this->value;
	}
}