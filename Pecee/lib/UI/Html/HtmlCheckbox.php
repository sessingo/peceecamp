<?php
namespace Pecee\UI\Html;
class HtmlCheckbox extends \Pecee\UI\Html\HtmlInput {
	public function __construct($name, $value = NULL) {
		$value = (is_null($value)) ? 1 : $value;
		parent::__construct($name, 'checkbox', $value);
	}
}