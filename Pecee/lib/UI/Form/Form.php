<?php
namespace Pecee\UI\Form;
use Pecee\Boolean;
use Pecee\Str;

class Form {
	const FORM_ENCTYPE_FORM_DATA='multipart/form-data';
	protected $prefixElements;
	protected $post;
	protected $name;
	protected $indexes;
	public function __construct(\Pecee\UI\ResponseData\ResponseDataPost $post) {
		$this->prefixElements=TRUE;
		$this->post=$post;
		$this->indexes=array();
	}
	
	protected function getPostBackValue($name) {
		if(strpos($name, '[]') != false) {
			$index=$this->indexes[md5($name)];
			$newName=substr($name, 0, strpos($name, '[]'));
			if(isset($_POST[$newName][$index])) {
				return $_POST[$newName][$index];
			}
		} else {
			if(!is_null($this->post->__get($name))) {
				return $this->post->__get($name);
			}
		}
		return NULL;
	}
	
	/**
	 * Tries to make some kind of unique form input name
	 * @param string $name
	 * @return string
	 */
	public function getInputName($name) {
		if($this->prefixElements) {
			$name = ($this->name) ? sprintf('%s_%s', $this->name, $name) : $name;
		}
		$this->indexes[md5($name)]=(isset($this->indexes[md5($name)])) ? ($this->indexes[md5($name)]+1) : 0;
		return $name;
	}
	
	/**
	 * Starts new form
	 * @param string $name
	 * @param string $method
	 * @param string $action
	 * @param string $enctype
	 * @return \Pecee\UI\Html\HtmlForm
	 */
	public function start($name = NULL, $method = 'post', $action = NULL, $enctype = 'multipart/form-data') {
		$this->name=$name;
		return new \Pecee\UI\Html\HtmlForm($name, $method, $action, $enctype);
	}
	
	/**
	 * Creates new HTML input element
	 * @param string $name
	 * @param string $type
	 * @param string $value
	 * @return \Pecee\UI\Html\HtmlInput
	 */
	public function input($name, $type, $value = NULL, $forcePostBack = FALSE) {
		$name = $this->getInputName($name);
		if(is_null($value) && $this->getPostBackValue($name) || $forcePostBack && \Pecee\UI\ResponseData\ResponseDataPost::IsPostBack()) {
			$value = $this->getPostBackValue($name);
		}
		$element = new \Pecee\UI\Html\HtmlInput($name, $type, Str::HtmlEntities($value));
		// Added: if the input is an radio, then save the god damn post value :-D...
		if(strtolower($type) == 'radio' && $this->getPostBackValue($name) && $value == $this->getPostBackValue($name) ) {
			$element->addAttribute('checked', 'checked');
		}
		return $element;
	}
	
	public function showError($index) {
		$msg = $this->validationFor($index);
		if($msg) {
			return $msg->getMessage();
		}
		return NULL;
	}
	
	public function validationFor($index) {
		// TODO: Make this so it run through each message and remove the "setmultiple" functionality.
		$messages = \Pecee\Session\SessionMessage::Instance()->get(\Pecee\Widget\Widget::MSG_ERROR);
		if(count($messages) > 0) {
			/* @var $message \Pecee\UI\Form\FormMessage */
			foreach($messages as $message) {
				if($message->getIndex()==$index) {
					return $message->getMessage();
				}
			}
		}
		return NULL;
	}
	
	/**
	 * Make new captcha element
	 * @param string $name
	 * @return \Pecee\UI\Form\FormCaptcha
	 */
	public function captcha( $name ) {
		return new \Pecee\UI\Form\FormCaptcha($name);
	}
	
	/**
	 * Creates new checkbox input element
	 * @param string $name
	 * @param string $value
	 * @return \Pecee\UI\Html\HtmlCheckbox
	 */
	public function bool($name, $value = NULL, $forcePostBack=TRUE, $defaultValue = NULL) {
		$name = $this->getInputName($name);
		$v = (!is_null($defaultValue)) ? $defaultValue : TRUE;
		$element = new \Pecee\UI\Html\HtmlCheckbox($name, $v);
		if($forcePostBack && \Pecee\UI\ResponseData\ResponseDataPost::IsPostBack()) {
			if($v && $this->getPostBackValue($name) == $v) {
				$element->addAttribute('checked', 'checked');
			}
		} else {
			if(Boolean::Parse($value)) {
				$element->addAttribute('checked', 'checked');
			}
		}
		return $element;
	}
	
	/**
	 * Creates new label
	 * @param string $name
	 * @param string $for
	 * @return \Pecee\UI\Html\HtmlLabel
	 */
	public function label($value, $for=NULL) {
		return new \Pecee\UI\Html\HtmlLabel($value, $for);
	}
	
	/**
	 * Creates new HTML Select element
	 * @param string $name
	 * @param Dataset $list
	 * @return \Pecee\UI\Html\HtmlSelect
	 */
	public function selectStart($name, $data = NULL, $value=NULL, $forcePostBack=FALSE) {
		$name = $this->getInputName($name);
		$element = new \Pecee\UI\Html\HtmlSelect($name);
		if(!is_null($data)) {
			if($data instanceof \Pecee\Dataset) {
				$arr=$data->getArray();
				if(count($arr) > 0) {
					foreach($data->getArray() as $i) {
						$val=(!isset($i['value'])) ? $i['name'] : $i['value'];
						$selected=($forcePostBack && $this->getPostBackValue($name) == $val  || $this->getPostBackValue($name) == $val || !$this->getPostBackValue($name) && $value == $val || (isset($i['selected']) && $i['selected']) || !$forcePostBack && $value == $val);
						$element->addOption(new \Pecee\UI\Html\HtmlSelectOption($i['name'], $val, $selected));
					}
				}
			} elseif(is_array($data)) {
				foreach($data as $val=>$name) {
					$selected=($forcePostBack && $this->getPostBackValue($name) == $val  || $this->getPostBackValue($name) == $val || !$this->getPostBackValue($name) && $value == $val || (isset($i['selected']) && $i['selected']) || !$forcePostBack && $value == $val);
					$element->addOption(new \Pecee\UI\Html\HtmlSelectOption($name, $val, $selected));
				}
			} else {
				throw new \InvalidArgumentException('Data must be either instance of Dataset or array.');
			}
		}
		return $element;
	}
	
	/**
	 * Creates new textarea
	 * @param string $name
	 * @param int $rows
	 * @param int $cols
	 * @param string $value
	 * @return \Pecee\UI\Html\HtmlTextarea
	 */
	public function textarea($name, $rows, $cols, $value = NULL, $forcePostBack = false) {
		$name = $this->getInputName($name);
		if(!$value && $this->getPostBackValue($name) || $forcePostBack && \Pecee\UI\ResponseData\ResponseDataPost::IsPostBack()) {
			$value = $this->getPostBackValue($name);
		}
		return new \Pecee\UI\Html\HtmlTextarea($name, $rows, $cols, $value);
	}
	
	/**
	 * Creates submit element
	 * @param string $name
	 * @param string $value
	 * @return \Pecee\UI\Html\HtmlInput
	 */
	public function submit($name, $value) {
		return $this->input($name, 'submit', $value);
	}
	
	/**
	 * Ends open form
	 * @return string
	 */
	public function end() {
		return '</form>';
	}
	
	public function setPrefixElements($bool) {
		$this->prefixElements=$bool;
	}
}