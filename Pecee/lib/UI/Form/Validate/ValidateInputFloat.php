<?php
namespace Pecee\UI\Form\Validate;
use Pecee\FloatUtil;

class ValidateInputFloat extends ValidateInput {
	protected $allowEmpty;
	public function __construct($allowEmpty=FALSE) {
		$this->allowEmpty=$allowEmpty;
	}
	public function validate() {
		return ($this->allowEmpty && empty($this->value) || FloatUtil::is_float(\Pecee\Float::ParseFloat($this->value)));
	}
	public function getErrorMessage() {
		return $this->_('%s skal indeholde tal', $this->name);
	}
}