<?php
namespace Pecee\UI\Form\Validate;
class ValidateFileAllowedExtension extends ValidateFile {
	protected $extensions;
	public function __construct(array $extensions) {
		$this->extensions=$extensions;
	}
	
	public function validate() {
		$ext=\Pecee\IO\File::GetExtension($this->fileName);
		return (in_array($ext, $this->extensions));
	}
	
	public function getErrorMessage() {
		return $this->_('%s er et ugyldigt format', $this->name);
	}
}