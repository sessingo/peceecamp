<?php
namespace Pecee\UI\Form\Validate;
class ValidateInputDateTime extends ValidateInput {	
	public function validate() {
		return (\Pecee\Date::IsValid($this->value));
	}
	
	public function getErrorMessage() {
		return $this->_('%s er ikke en gyldig dato', $this->name);
	}
}