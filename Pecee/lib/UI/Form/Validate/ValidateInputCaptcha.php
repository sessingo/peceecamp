<?php
namespace Pecee\UI\Form\Validate;
class ValidateInputCaptcha extends ValidateInput {
	protected $captchaName;
	public function __construct($name) {
		$this->captchaName = $name;
	}
	public function validate() {
		$result = (\Pecee\Session::Instance()->exists($this->captchaName) && strtolower($this->value) == strtolower(\Pecee\Session::Instance()->get($this->captchaName))); 
		if($result) {
			\Pecee\Session::Instance()->destroy($this->captchaName);
		}
		return $result;
	}
	public function getErrorMessage() {
		return $this->_('Ugyldig verificeringsnøgle - prøv igen');
	}
}