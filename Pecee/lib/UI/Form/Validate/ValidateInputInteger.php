<?php
namespace Pecee\UI\Form\Validate;
class ValidateInputInteger extends ValidateInput {
	public function validate() {
		return (\Pecee\Integer::IsNummeric($this->value));
	}
	public function getErrorMessage() {
		return $this->_('%s må kun indeholde tal', $this->name);
	}
	
}