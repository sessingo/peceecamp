<?php
namespace Pecee\UI\Form\Validate;
class ValidateInputSession extends ValidateInput {
	protected $sessionName;
	public function __construct( $sessionName ) {
		$this->sessionName = $sessionName;
	}
	public function validate() {
		return ((bool)\Pecee\Session::Instance()->exists($this->sessionName));
	}
	public function getErrorMessage() {
		return $this->_('%s eksisterer ikke', $this->name);
	}
}