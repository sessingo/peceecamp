<?php
namespace Pecee\UI\Form\Validate;
class ValidateInputEmail extends ValidateInput {
	public function validate() {
		return \Pecee\Util::is_email($this->value);
	}
	public function getErrorMessage() {
		return $this->_('%s er ikke en gyldig e-mail-adresse', $this->name);
	}
}