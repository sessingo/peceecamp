<?php
namespace Pecee\UI\Form\Validate;
class ValidateInputNotEquals extends ValidateInput {
	protected $notEquals;
	protected $strict;
	protected $error;
	public function __construct($value, $strict=FALSE) {
		$this->notEquals=$value;
		$this->strict=$strict;
	}
	public function validate() {
		if(!$this->strict) {
			$this->value=strtolower($this->value);
			$this->notEquals=strtolower($this->notEquals);
		}
		if($this->value==$this->notEquals) {
			$this->error=$this->_('Du skal udfylde feltet %s', $this->name);
			return FALSE;
		}
		return TRUE;
	}
	public function getErrorMessage() {
		return $this->error;
	}
}