<?php
namespace Pecee\UI\Form\Validate;
class ValidateInputUri extends ValidateInput {
	protected $error;
	public function validate() {
		if(empty($this->value)) {
			$this->error = $this->_('%s må ikke være tom', $this->name);
			return FALSE;
		} elseif(!\Pecee\Url::IsValid($this->value)) {
			$this->error = $this->_('%s er ikke et gyldigt link', $this->name);
			return FALSE;
		}
		return TRUE;
	}
	public function getErrorMessage() {
		return $this->error;
	}
}