<?php
namespace Pecee\UI\Form\Validate;
class ValidateInputMaxLength extends ValidateInput {
	protected $maxLength;
	public function __construct($maxLength = 50) {
		$this->maxLength = $maxLength;
	}
	public function validate() {
		return !(strlen($this->value) > $this->maxLength);
	}
	public function getErrorMessage() {
		return $this->_('%s må maksimalt være %s karakterer', $this->name, $this->maxLength);
	}
}