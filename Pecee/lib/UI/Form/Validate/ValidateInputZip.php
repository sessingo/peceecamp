<?php
namespace Pecee\UI\Form\Validate;
class ValidateInputZip extends ValidateInput {
	protected $error;
	protected $length;
	public function __construct($length=4) {
		$this->length=$length;
	}
	public function validate() {
		if(\Pecee\Integer::is_int($this->value))
			$this->error = $this->_('%s må kun indeholde tal', $this->name);
		elseif(strlen($this->value) == $this->length)
			return TRUE;
		else
			$this->error = $this->_('%s skal indeholde %s karakterer', $this->name, $this->length);
		return FALSE;
	}
	public function getErrorMessage() {
		return $this->error;
	}
}