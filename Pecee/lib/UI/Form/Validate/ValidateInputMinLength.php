<?php
namespace Pecee\UI\Form\Validate;
class ValidateInputMinLength extends ValidateInput {
	protected $minimumLength;
	public function __construct($minimumLength = 5) {
		$this->minimumLength = $minimumLength;
	}
	public function validate() {
		return ((strlen($this->value) > $this->minimumLength));
	}
	public function getErrorMessage() {
		return $this->_('%s skal mindst være %s karakterer', $this->name, $this->minimumLength);
	}
}