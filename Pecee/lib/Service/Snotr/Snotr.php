<?php
namespace Pecee\Service\Snotr;
class Snotr {
	
	const ServiceUri = 'http://www.snotr.com/rss.xml';
	const WatchVideoUri = 'http://www.snotr.com/video/';
	const VideoStoragePath = 'http://videos.snotr.com/';
	const VideoThumbnailPath = 'http://videos.snotr.com/';
	
	private static function GetVideoID($url) {
		$url = str_ireplace(self::WatchVideoUri, '', $url);
		$tmp = explode('/', $url);
		return (isset($tmp[0])) ? $tmp[0] : NULL;
	}
	
	private static function GetVideoThumbnail( $VideoID ) {
		return self::VideoThumbnailPath . $VideoID . '-large.jpg';
	}
	
	public static function GetVideos() {
		$videos = \Pecee\Xml\Rss\RssParser::parseRssUri(self::ServiceUri);
		if($videos) {
			$items = array();
			/* @var $item \Pecee\Xml\Rss\RssItem */
			foreach($videos->getItems() as $item) {
				$VideoID = self::GetVideoID($item->getLink());
				$video = new SnotrItem();
				$video->id = $VideoID;
				$video->pubDate = $item->getPubDate();
				$video->title = $item->getTitle();
				$video->videoUri = self::VideoStoragePath . $VideoID . '.flv';
				$video->description = strip_tags(html_entity_decode($item->getDescription()));
				$video->videoThumbnail = self::GetVideoThumbnail($VideoID);
				$video->url = $item->getLink();
				$video->uniqueKey = md5($VideoID);
				$items[] = $video;
			}
			return $items;
		}
		return null;
	}
	
	
	
	
}