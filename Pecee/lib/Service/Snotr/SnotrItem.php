<?php
namespace Pecee\Service\Snotr;
class SnotrItem {
	public $id;
	public $pubDate;
	public $title;
	public $description;
	public $videoUri;
	public $videoThumbnail;
	public $uniqueKey;
	public $url;
}