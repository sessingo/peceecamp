<?php
namespace Pecee\Service\Yahoo;
class YahooSearchWeb {
	
	const SearchUri = 'http://boss.yahooapis.com/ysearch/web/v1/%1$s?appid=%2$s&format=xml&count=%3$s&view=keyterms&start=%4$s&region=%5$s&lang=%6$s';
	
	private $results = array();
	
	// Search settings
	private $apiKey; // Yahoo Boss Api Key
	private $keyWord;
	private $languageCode;
	private $regionCode;
	private $pageIndex;
	private $resultsPerPage;
	private $totalHits;
	private $deepHits;
	
	public function __construct($keyWord) {
		$this->keyWord = $keyWord;
		$this->pageIndex = 0;
		$this->resultsPerPage = 10;
	}
	/**
	 * Sets search language
	 *
	 * @param string $languageCode
	 * @param string $regionCode
	 */
	public function setLanguage($languageCode, $regionCode) {
		$this->languageCode = $languageCode;
		$this->regionCode = $regionCode;
	}
	
	/**
	 * Perfom search and return result array
	 *
	 * @return array
	 */
	public function doSearch() {
		
		if(!$this->apiKey)
			throw new \InvalidArgumentException('No Api-key has been defined.');
		
		$xml = @file_get_contents(sprintf(self::SearchUri, urlencode($this->keyWord), $this->apiKey, $this->resultsPerPage, $this->getPageIndex(), $this->regionCode, $this->languageCode));
		
		if(!$xml)
			throw new \ErrorException('Couldt not connect to Yahoo.');
		
		$dom = new \DOMDocument();
		$dom->loadXML($xml);
		
		$this->totalHits = $dom->getElementsByTagName('resultset_web')->item(0)->getAttribute('totalhits');
		$this->deepHits = $dom->getElementsByTagName('resultset_web')->item(0)->getAttribute('deephits');

		$results = $dom->getElementsByTagName('result');
		
		foreach($results as $key=>$result) {
			
			$title = strip_tags($result->getElementsByTagName('title')->item(0)->nodeValue);
			$url = $result->getElementsByTagName('url')->item(0)->nodeValue;
			$description = strip_tags($result->getElementsByTagName('abstract')->item(0)->nodeValue);
			$size = $result->getElementsByTagName('size')->item(0)->nodeValue;
			
			$keytermsResults = $dom->getElementsByTagName('keyterms')->item($key)->getElementsByTagName('terms')->item(0)->getElementsByTagName('term');
			
			$keyterms = array();
			
			foreach($keytermsResults as $term) {
				array_push($keyterms, utf8_decode($term->nodeValue));
			}
			
			$this->addResult(utf8_decode($title), utf8_decode($description), $url, $size, $keyterms);
			
		}
		
		return $this->results;
		
	}
	
	/**
	 * Adds result array to the main results array
	 *
	 * @param string $title
	 * @param string $description
	 * @param string $url
	 * @param int $size
	 * @param array $keyterms
	 */
	private function addResult($title, $description, $url, $size, array $keyterms) {
		$arr = array();
		$arr['Title'] = $title;
		$arr['Description'] = $description;
		$arr['Url'] = $url;
		$arr['Size'] = $size;
		$arr['Keyterms'] = $keyterms;
		array_push($this->results, $arr);
	}
	
	/**
	 * @return int
	 */
	public function getPageIndex() {
		return $this->pageIndex;
	}
	
	/**
	 * @param int $pageIndex
	 */
	public function setPageIndex($pageIndex) {
		$this->pageIndex = $pageIndex * $this->resultsPerPage;
	}
	
	/**
	 * Returns max pageindex
	 *
	 * @return int
	 */
	public function getMaxPageIndex() {
		return (int)round($this->totalHits / $this->resultsPerPage);
	}

	
	/**
	 * @param string $apiKey
	 */
	public function setApiKey($apiKey) {
		$this->apiKey = $apiKey;
	}
	
	/**
	 * @param int $resultsPerPage
	 */
	public function setResultsPerPage($resultsPerPage) {
		$this->resultsPerPage = $resultsPerPage;
	}
	
	/**
	 * @return int
	 */
	public function getDeepHits() {
		return $this->deepHits;
	}
	
	/**
	 * @return int
	 */
	public function getTotalHits() {
		return $this->totalHits;
	}

}
?>