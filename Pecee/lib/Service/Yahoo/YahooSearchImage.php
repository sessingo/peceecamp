<?php
namespace Pecee\Service\Yahoo;
class YahooSearchImage {
	
	const SearchUri = 'http://boss.yahooapis.com/ysearch/images/v1/%1$s?appid=%2$s&format=xml&filter=%3$s&dimensions=%4$s&Region=%5$s&lang=%6$s&start=%7$d&count=%8$d';
	
	private $results = array();
	
	// Search settings
	private $apiKey; // Yahoo Boss Api Key
	private $keyWord;
	private $languageCode;
	private $regionCode;
	private $pageIndex;
	private $resultsPerPage;
	private $totalHits;
	private $deepHits;
	private $imageSize;
	private $searchFilter;
	
	public function __construct($keyWord) {
		$this->keyWord = $keyWord;
		$this->pageIndex = 0;
		$this->resultsPerPage = 10;
		$this->searchFilter = 'no';
		$this->imageSize = 'all';
	}
	
	/**
	 * Perfom search and return result array
	 *
	 * @return array
	 */
	public function doSearch() {
		if(!$this->apiKey)
			throw new \InvalidArgumentException('No Api-key has been defined.');
		$xml = @file_get_contents(vsprintf(self::SearchUri, array(urlencode($this->keyWord), $this->apiKey, $this->searchFilter, $this->imageSize, $this->regionCode, $this->languageCode, $this->getPageIndex(), $this->resultsPerPage)));
		if(!$xml)
			throw new \ErrorException('Couldt not connect to Yahoo.');
		
		$dom = new \DOMDocument();
		$dom->loadXML($xml);
		
		$this->totalHits = $dom->getElementsByTagName('resultset_images')->item(0)->getAttribute('totalhits');
		$this->deepHits = $dom->getElementsByTagName('resultset_images')->item(0)->getAttribute('deephits');

		$results = $dom->getElementsByTagName('result');
		
		foreach($results as $result) {
			
			$title = strip_tags($result->getElementsByTagName('abstract')->item(0)->nodeValue);
			$url = $result->getElementsByTagName('url')->item(0)->nodeValue;
			$clickUrl = $result->getElementsByTagName('clickurl')->item(0)->nodeValue;
			$refererUrl = $result->getElementsByTagName('refererurl')->item(0)->nodeValue;
			$refererClickUrl = $result->getElementsByTagName('refererclickurl')->item(0)->nodeValue;
			$date = $result->getElementsByTagName('date')->item(0)->nodeValue;
			$filename = strip_tags($result->getElementsByTagName('filename')->item(0)->nodeValue);
			$format = $result->getElementsByTagName('format')->item(0)->nodeValue;
			$height = $result->getElementsByTagName('height')->item(0)->nodeValue;
			$width = $result->getElementsByTagName('width')->item(0)->nodeValue;
			$mimetype = $result->getElementsByTagName('mimetype')->item(0)->nodeValue;
			$size = $result->getElementsByTagName('size')->item(0)->nodeValue;
			$thumbnailHeight = $result->getElementsByTagName('thumbnail_height')->item(0)->nodeValue;
			$thumbnailWidth = $result->getElementsByTagName('thumbnail_width')->item(0)->nodeValue;
			$thumbnailUrl = $result->getElementsByTagName('thumbnail_url')->item(0)->nodeValue;
			
			$this->addResult($title, $filename, $format, $url, $clickUrl, $refererUrl, $refererClickUrl, $size, $date, $height, $width, $mimetype, $thumbnailHeight, $thumbnailWidth, $thumbnailUrl);
		}
		return $this->results;
	}
	
	/**
	 * Adds result array to the main results array
	 *
	 * @param string $title
	 * @param string $description
	 * @param string $url
	 * @param int $size
	 * @param array $keyterms
	 */
	private function addResult($title = NULL, $filename, $format, $url, $clickUrl, $refererUrl, $refererClickUrl, $size, $date, $height, $width, $mimeType, $thumbnailHeight, $thumbnailWidth, $thumbnailUrl) {
		$arr = array();
		$arr['Title'] = $title;
		$arr['Filename'] = $filename;
		$arr['Format'] = $format;
		$arr['Url'] = $url;
		$arr['ClickUrl'] = $clickUrl;
		$arr['RefererUrl'] = $refererUrl;
		$arr['RefererClickUrl'] = $refererClickUrl;
		$arr['Size'] = $size;
		$arr['Date'] = $date;
		$arr['Height'] = $height;
		$arr['Width'] = $width;
		$arr['MimeType'] = $mimeType;
		$arr['ThumbnailHeight'] = $thumbnailHeight;
		$arr['ThumbnailWidth'] = $thumbnailWidth;
		$arr['ThumbnailUrl'] = $thumbnailUrl;
		$this->results[] = $arr;
	}
	
	/**
	 * @return int
	 */
	public function getPageIndex() {
		return $this->pageIndex;
	}
	
	/**
	 * @param int $pageIndex
	 */
	public function setPageIndex($pageIndex) {
		$this->pageIndex = $pageIndex * $this->resultsPerPage;
	}
	
	/**
	 * Returns max pageindex
	 *
	 * @return int
	 */
	public function getMaxPageIndex() {
		return (int)ceil($this->totalHits / $this->resultsPerPage);
	}

	
	/**
	 * @param string $apiKey
	 */
	public function setApiKey($apiKey) {
		$this->apiKey = $apiKey;
	}
	
	/**
	 * @param int $resultsPerPage
	 */
	public function setResultsPerPage($resultsPerPage) {
		$this->resultsPerPage = $resultsPerPage;
	}
	
	/**
	 * @return int
	 */
	public function getDeepHits() {
		return $this->deepHits;
	}
	
	/**
	 * @return int
	 */
	public function getTotalHits() {
		return $this->totalHits;
	}
	
	/**
	 * @param string $imageSize
	 */
	public function setImageSize($imageSize) {
		switch(strtolower($imageSize)) {	
			case 'small':
			case 'medium':
			case 'large':
			case 'wallpaper':
			case 'widewallpaper':
                $this->imageSize = $imageSize;
			    break;
			default:
			    throw new \InvalidArgumentException('Unknown image size.');
                break;	
		}
	}
	
	/**
	 * Sets search language
	 *
	 * @param string $languageCode
	 * @param string $regionCode
	 */
	public function setLanguage($languageCode, $regionCode) {
		$this->languageCode = $languageCode;
		$this->regionCode = $regionCode;
	}
	
	/**
	 * @param string $searchFilter
	 */
	public function setSearchFilter($searchFilter) {
		$this->searchFilter = $searchFilter;
	}
	
	public function getResults() {
		return $this->results;
	}

}