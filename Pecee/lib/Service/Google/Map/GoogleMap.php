<?php
namespace Pecee\Service\Google\Map;
/**
 * @author Simon Sessingø
 * @version 1.0.5
 */
class GoogleMap {

	// MAP TYPES
	
		/**
		 * Google Maptype: Terrain
		 * 
		 * Found at:
		 * http://earth-api-samples.googlecode.com/svn/trunk/examples/terrain.html
		 */
		const MT_G_LAYER_TERRAIN = 'LAYER_TERRAIN';
	
		/**
		 * Google Maptype: Google Earth Satellite 3D Map
		 * Displays Google Earth 3D map. Required Google Earth plugin installed.
		 * 
		 * For information, please refer to:
		 * http://code.google.com/apis/maps/documentation/services.html	 *
		 */
		const MT_G_SATELLITE_3D_MAP = 'G_SATELLITE_3D_MAP';
		/**
		 * Google Maptype: Normal_map
		 * Displays the normal, default 2D tiles of Google Maps 
		 * 
		 * For information, please refer to:
		 * http://code.google.com/apis/maps/documentation/controls.html
		 */
		const MT_G_NORMAL_MAP = 'G_NORMAL_MAP';
		/**
		 * Google Maptype Hybrid: 
		 * Displays a mix of photographic tiles and a tile layer for prominent features (roads, city names)
		 * 
		 * For information, please refer to:
		 * http://code.google.com/apis/maps/documentation/controls.html
		 */
		const MT_G_HYBRID_MAP = 'G_HYBRID_MAP';
		/**
		 * Google Maptype: satellite_map
		 * Displays photographic tiles
		 * 
		 * For information, please refer to:
		 * http://code.google.com/apis/maps/documentation/controls.html
		 */
		const MT_G_SATELLITE_MAP = 'G_SATELLITE_MAP';
		
		/**
		 * Google Maptype: Psychical map
		 * Displays physical map tiles based on terrain information	
		 * 
		 * For information, please refer to:
		 * http://code.google.com/apis/maps/documentation/controls.html
		 */
		const MT_G_PHYSICAL_MAP = 'G_PHYSICAL_MAP';
	
	// CONTROL TYPES
	
		/**
		 * Google Controltype:
		 * A large pan/zoom control used on Google Maps. Appears in the top left corner of the map by default
		 * 
		 * For more information, please refer to:
		 * http://code.google.com/apis/maps/documentation/controls.html
		 */
		const CT_GLARGEMAPCONTROL = 'GLargeMapControl';
		
		/**
		 * Google Controltype:
		 * A smaller pan/zoom control used on Google Maps. Appears in the top left corner of the map by default.
		 *
		 * For more information, please refer to:
		 * http://code.google.com/apis/maps/documentation/controls.html
		 */
		const CT_GSMALLMAPCONTROL = 'GSmallMapControl';
		
		/**
		 * Google Controltype:
		 * A small zoom control (no panning controls) used in the small map blowup windows used to display driving directions steps on Google Maps.
		 * 
		 * For more information, please refer to:
		 * http://code.google.com/apis/maps/documentation/controls.html 
		 */
		const CT_GSMALLZOOMCONTROL = 'GSmallZoomControl';
		
		/**
		 * Google Controltype:
		 * A map scale
		 * 
		 * For more information, please refer to:
		 * http://code.google.com/apis/maps/documentation/controls.html
		 */
		const CT_GSCALECONTROL = 'GScaleControl';
		
		/**
		 * Google Controltype:
		 * Buttons that let the user toggle between map types (such as Map and Satellite) 
		 *
		 * For more information, please refer to:
		 * http://code.google.com/apis/maps/documentation/controls.html
		 */
		const CT_GMAPTYPECONTROL = 'GMapTypeControl';
		
		/**
		 * Google Controltype:
		 * A selection of nested buttons and menu items for placing many map type selectors
		 * 
		 * For more information, please refer to:
		 * http://code.google.com/apis/maps/documentation/controls.html
		 */
		const CT_GHIERARCHICALMAPTYPECONTROL = 'GHierarchicalMapTypeControl';
		
		/**
		 * Google Controltype:
		 * A collapsible overview map in the corner of the screen 
		 * 
		 * For more information, please refer to:
		 * http://code.google.com/apis/maps/documentation/controls.html
		 */
		const CT_GOVERVIEWMAPCONTROL = 'GOverviewMapControl';
	
	public static $maptypes = array(
						self::MT_G_HYBRID_MAP,
						self::MT_G_NORMAL_MAP,
						self::MT_G_SATELLITE_MAP,
						self::MT_G_SATELLITE_3D_MAP,
						self::MT_G_PHYSICAL_MAP,
						self::MT_G_LAYER_TERRAIN);
						
	public static $controltypes = array(
						self::CT_GHIERARCHICALMAPTYPECONTROL,
						self::CT_GLARGEMAPCONTROL,
						self::CT_GMAPTYPECONTROL,
						self::CT_GOVERVIEWMAPCONTROL,
						self::CT_GSCALECONTROL,
						self::CT_GSMALLMAPCONTROL,
						self::CT_GSMALLZOOMCONTROL);
	/**
	 * The api key for Google maps.
	 * Needed for this to work properly. If you don't have any API key yet,
	 * dont worry - go to http://code.google.com/apis/maps/signup.html and create one.
	 *
	 * @var string
	 */
	private $apiKey;
	private $address;
	private $zipCode;
	private $city;
	private $country;
	
	private $latitude;
	private $longitude;
	private $ShowWithoutCoordinates = false;
	private $canvasID;
	
	// Special settings for current map
	private $showControls = true;
	private $controlType;
	private $mapType;
	private $zoom = 16;
	private $infoWindows = array();
	private $customIcons = array();
	private $markers = array();
	private $mapHeight = '200px';
	private $mapWidth = '500px';
	
	private function generateCanvasID() {
		return substr(uniqid(), 3);
	}
	public function setShowWithoutCoordinates($ShowWithoutCoordinates) {
		$this->ShowWithoutCoordinates = $ShowWithoutCoordinates;
	}
	private function getCoordinates() {
		if(!$this->apiKey || $this->apiKey && strlen($this->apiKey) < 1) throw new \ErrorException('No apiKey has been defined.');
		$q = str_replace(' ', '_', $this->address.','.$this->zipCode.'+'.$this->city.','.$this->country);
		if(!($document = fopen('http://maps.google.com/maps/geo?q='.$q.'&output=csv&key='.$this->apiKey, 'r'))) throw new \ErrorException('Error occured while trying to connect to Google.');
		$getCsv = fread($document, 30000);
		fclose($document);
		
		// Explode the data we get back
		// [0] = code, [1] = Accuracy, [2] = Latitude, [3] = Longitude
		
		$tmp = explode(',', $getCsv);
        $this->latitude  = $tmp[2];
        $this->longitude = $tmp[3];
	}
	
	public function addCustomIcon($iconImg, $infoWindowText=null, $latitude, $longitude) {
		$arr = array();
		$arr['iconImage'] = $iconImg;
		$arr['text'] = $infoWindowText;
		$arr['latitude'] = $latitude;
		$arr['longitude'] = $longitude;
		
		$this->setCustomIcons($arr);
	}
	
	public function addInfoWindow($text, $latitude=null, $longitude=null) {
		if(!empty($text)) {
			$item = 'map.openInfoWindow('.((!empty($latitude) || !empty($longitude)) ? 'new GLatLng('.$latitude.','.$longitude.')' : 'map.getCenter()').',
			document.createTextNode("'.$text.'"));';
			$this->setInfoWindows($item);
		}
	}
	public function addMarker($Name,$latitude, $longitude) {
		$this->markers[$Name] = array($latitude,$longitude);
	}
	
	public function outputJavaScriptHeader() {
		return '<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key='.$this->apiKey.'" type="text/javascript"></script>';
	}
	
	private function outputMapCanvas() {
		return '<div id="'.$this->canvasID.'" style="width:'.$this->mapWidth.';height:'.$this->mapHeight.';"></div><script language="javascript">initialize_'.$this->canvasID.'(null,null,null);</script>';
	}
	
	private function outputJavascript() {
		$output = '<script type="text/javascript">
var map = null;
function initialize_'.$this->canvasID.'(latitude,longtitude,zoom) {
      if (GBrowserIsCompatible()) {
      		
      		if(longtitude==null||latitude==null) {
      			longtitude = '.$this->longitude.';
      			latitude = '.$this->latitude.';
      		}
      		
      		if(zoom==null) {
      			zoom = '.$this->zoom.';
      		}
      
            map = new GMap2(document.getElementById("'.$this->canvasID.'"));
            map.setCenter(new GLatLng(latitude,longtitude), zoom);';
		
			if($this->mapType) {
				$output .= 'map.setMapType('.$this->mapType.');';
			}
            
			if($this->customIcons || $this->markers) {
				foreach($this->customIcons as $icon) {
					$output .= 'var latlng = new GLatLng('.$icon['latitude'].','.$icon['longitude'].');
			         map.addOverlay(createMarker_'.$this->canvasID.'(latlng, "'.addslashes($icon['text']).'", "'.$icon['iconImage'].'"));';
				}
				foreach($this->markers as $Name=>$Coordinates) {
					$output .= 'var latlng = new GLatLng('.$Coordinates[0].','.$Coordinates[1].');
			         map.addOverlay(createMarker_'.$this->canvasID.'(latlng, "'.htmlspecialchars($Name).'","/gfx/map-pointer.png"));';
				}
			}
		
			if(count($this->infoWindows) > 0) {
				foreach($this->infoWindows as $infoWindow) {
					$output .= $infoWindow;
				}
			}
			
			if($this->showControls) {
				if( !$this->controlType ) {
					$output .= 'map.setUIToDefault()';					
				} else {
					$output .= 'map.addControl(new '.$this->controlType.'());
		        	var mapControl = new GMapTypeControl();
		       		map.addControl(mapControl);';
				}
			}
      $output .= '}';
      $output .= 'window.onunload = GUnload;
}';
	if( $this->customIcons || $this->markers ) {
      	$output .= '
        var baseIcon = new GIcon();
        baseIcon.iconAnchor = new GPoint(9, 34);
        baseIcon.infoWindowAnchor = new GPoint(40, 5);
      	
        function createMarker_'.$this->canvasID.'(point, text, image) {
          var letteredIcon = new GIcon(baseIcon);
          letteredIcon.image = image;
          markerOptions = { icon:letteredIcon };
          var marker = new GMarker(point, markerOptions);
          
          if(text) {
	          GEvent.addListener(marker, "click", function() {
	            marker.openInfoWindowHtml(text);
	          });
          }
          return marker;
        }';
      }
$output .= '</script>';
		
		return $output;
		
	}
	
	public function __construct() {
		$this->canvasID = 'canvas_'.$this->generateCanvasID();
	}
	
	public function init($address = NULL, $zipCode=null, $city=null, $country=null) {
		$this->address=$address;
		$this->zipCode=$zipCode;
		$this->city=$city;
		$this->country=$country;
		$this->getCoordinates();
		// Genreate div, map_canvas id.
		//$this->canvasID = $this->generateCanvasID();
	}
	
	public function __toString() {
		if ($this->ShowWithoutCoordinates || ($this->latitude && $this->longitude))
			return $this->outputJavaScriptHeader() . $this->outputJavascript() . $this->outputMapCanvas();
		else
			return '';
	}
	
	/**
	 * @param string $apiKey
	 */
	public function setApiKey($apiKey) {
		if(!empty($apiKey) && strlen($apiKey) > 1) 
			$this->apiKey = $apiKey;
			$this->getCoordinates();
	}
	
	/**
	 * @param bool $showControls
	 */
	public function setShowControls($showControls) {
		if($showControls == false) $this->showControls = false;
	}
	
	/**
	 * Set's the type of the map, when loading the Google map.
	 * For more information, please refer to:
	 * http://code.google.com/apis/maps/documentation/services.html
	 * @param string $mapType
	 */
	public function setMapType($mapType) {
		if (!in_array($mapType, self::$maptypes))
			throw new \InvalidArgumentException('Unknown Maptype.');
		$this->mapType = $mapType;
	}
	
	/**
	 * @param int $mapHeight
	 */
	public function setMapHeight($mapHeight) {
		$this->mapHeight = $mapHeight;
	}
	
	/**
	 * @param int $mapWidth
	 */
	public function setMapWidth($mapWidth) {
		$this->mapWidth = $mapWidth;
	}

	
	/**
	 * More information about types:
	 * http://code.google.com/apis/maps/documentation/controls.html
	 * Default: GLargeMapControl 
	 * @param string $controlType
	 */
	public function setControlType($controlType) {
		if( !in_array($controlType, self::$controltypes) )
			throw new \InvalidArgumentException('Unknown control type');
		$this->controlType = $controlType;
	}
	
	/**
	 * @param string $customIcons
	 */
	private function setCustomIcons($customIcons) {
		if(!empty($customIcons)) array_push($this->customIcons, $customIcons);
	}

	
	/**
	 * @param string $infoWindows
	 */
	private function setInfoWindows($infoWindows) {
		if(!empty($infoWindows)) array_push($this->infoWindows, $infoWindows);
	}
	
	/**
	 * @param int $zoom
	 */
	public function setZoom($zoom) {
		$this->zoom = $zoom;
	}
	
	public function setCanvasID($canvasId) {
		$this->canvasID = $canvasId;
	}
	
	/**
	 * @return string
	 */
	public function getCanvasID() {
		return $this->canvasID;
	}
}