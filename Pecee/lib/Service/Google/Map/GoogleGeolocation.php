<?php
namespace Pecee\Service\Google\Map;
class GoogleGeolocation {
	
	const serviceUrl = "http://maps.google.com/maps/geo?q=%s&output=xml&hl=da&sensor=false";
	
	protected $Address;
	protected $CountryName;
	protected $CountryNameCode;
	protected $AdministrativeArea;
	protected $LocalityName;
	protected $ThoroughfareName;
	protected $Postalcode;
	protected $Longitude;
	protected $Latitude;
	protected $Alternatives = array();
	protected $data;
	
	private static $LastCallTime;
	
	public function LookUp($query) {
		$response = utf8_encode($this->call($query));
		$dom = new \DOMDocument();
		if ($dom->loadXML($response)) {
			$xpath = new \Domxpath($dom);
			$result = $dom->getElementsByTagName('Placemark');
			foreach($result as $placemark) {
				
				$target = new self();
					
				$addressNode = $xpath->query('*',$placemark);
				$target->setAddress($addressNode->item(0)->nodeValue);
				$Coordinates = $addressNode->item(3)->firstChild->nodeValue;
				list($Longitude,$Latitude) = explode(',',$Coordinates);
				$target->setLongitude($Longitude);
				$target->setLatitude($Latitude);
				
				@$CountryNodes = $xpath->query('*',$addressNode->item(1));
				@$target->setCountryNameCode($CountryNodes->item(0)->childNodes->item(0)->nodeValue);
				@$target->setCountryName($CountryNodes->item(0)->childNodes->item(1)->nodeValue); 
				@$target->setAdministrativeArea($CountryNodes->item(0)->childNodes->item(2)->firstChild->nodeValue);
				if (@$CountryNodes->item(0)->childNodes->item(2)->childNodes)
					@$LocalityNode = $CountryNodes->item(0)->childNodes->item(2)->childNodes->item(1);
				if (@$LocalityNode) {
					@$target->setLocalityName($LocalityNode->firstChild->nodeValue);
					@$target->setThoroughfareName($LocalityNode->childNodes->item(1)->firstChild->nodeValue);
					@$target->setPostalcode($LocalityNode->childNodes->item(2)->firstChild->nodeValue);
				}
				$this->data[] = $target;
			}
		}
		return $this->data;
	}
	private function call($query) {
		/**
		 * To ensure no flooding 
		 * G_GEO_TOO_MANY_QUERIES, code: 620
		 * http://code.google.com/intl/da/apis/maps/documentation/reference.html
		 */
		if(self::$LastCallTime && (microtime() - self::$LastCallTime) < 0.1) {
			sleep(1);
		}
		self::$LastCallTime = microtime();
		return file_get_contents(sprintf(self::serviceUrl,urlencode($query)));
	}
	
	public function getData() {
		return $this->data;
	}
	
	/**
	 * @return unknown
	 */
	public function getAddress() {
		return $this->Address;
	}
	
	/**
	 * @return unknown
	 */
	public function getAdministrativeArea() {
		return $this->AdministrativeArea;
	}
	
	/**
	 * @return unknown
	 */
	public function getCountryName() {
		return $this->CountryName;
	}
	
	/**
	 * @return unknown
	 */
	public function getCountryNameCode() {
		return $this->CountryNameCode;
	}
	
	/**
	 * @return unknown
	 */
	public function getLatitude() {
		return $this->Latitude;
	}
	
	/**
	 * @return unknown
	 */
	public function getLocalityName() {
		return $this->LocalityName;
	}
	
	/**
	 * @return unknown
	 */
	public function getLongitude() {
		return $this->Longitude;
	}
	
	/**
	 * @return unknown
	 */
	public function getPostalcode() {
		return $this->Postalcode;
	}
	
	/**
	 * @return unknown
	 */
	public function getThoroughfareName() {
		return $this->ThoroughfareName;
	}
	
	/**
	 * @param unknown_type $Address
	 */
	public function setAddress($Address) {
		$this->Address = $Address;
	}
	
	/**
	 * @param unknown_type $AdministrativeArea
	 */
	public function setAdministrativeArea($AdministrativeArea) {
		$this->AdministrativeArea = $AdministrativeArea;
	}
	
	/**
	 * @param unknown_type $CountryName
	 */
	public function setCountryName($CountryName) {
		$this->CountryName = $CountryName;
	}
	
	/**
	 * @param unknown_type $CountryNameCode
	 */
	public function setCountryNameCode($CountryNameCode) {
		$this->CountryNameCode = $CountryNameCode;
	}
	
	/**
	 * @param unknown_type $Latitude
	 */
	public function setLatitude($Latitude) {
		$this->Latitude = $Latitude;
	}
	
	/**
	 * @param unknown_type $LocalityName
	 */
	public function setLocalityName($LocalityName) {
		$this->LocalityName = $LocalityName;
	}
	
	/**
	 * @param unknown_type $Longitude
	 */
	public function setLongitude($Longitude) {
		$this->Longitude = $Longitude;
	}
	
	/**
	 * @param unknown_type $Postalcode
	 */
	public function setPostalcode($Postalcode) {
		$this->Postalcode = $Postalcode;
	}
	
	/**
	 * @param unknown_type $ThoroughfareName
	 */
	public function setThoroughfareName($ThoroughfareName) {
		$this->ThoroughfareName = $ThoroughfareName;
	}
	
}