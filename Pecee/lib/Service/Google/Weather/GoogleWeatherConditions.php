<?php
namespace Pecee\Service\Google\Weather;
class GoogleWeatherConditions {
	private $condition;
	private $celcius;
	private $humidity;
	private $icon;
	private $wind;
	
	public function __construct($condition, $celcius, $humidity, $icon, $wind) {
		$this->condition = $condition;
		$this->celcius = $celcius;
		$this->humidity = $humidity;
		$this->icon = $icon;
		$this->wind = $wind;
	}
	
	/**
	 * @return int
	 */
	public function getCelcius() {
		return $this->celcius;
	}
	
	/**
	 * @return string
	 */
	public function getCondition() {
		return $this->condition;
	}
	
	/**
	 * @return string
	 */
	public function getHumidity() {
		return $this->humidity;
	}
	
	/**
	 * @return string
	 */
	public function getIcon() {
		return $this->icon;
	}
	
	/**
	 * @return string
	 */
	public function getWind() {
		return $this->wind;
	}

}