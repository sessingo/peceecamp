<?php
namespace Pecee\Service\Google\Weather;
class GoogleWeather {
	// Google Icons URL
	CONST GoogleIconsUrl = 'http://www.google.com';
	
	protected $data;
	protected $city;
	protected $country;
	protected $latitude;
	protected $longtitude;
	protected $includeGoogleIconUrl = TRUE;
	
	// Settings
	protected $language;
	
	public function __construct($language = 'en') {
		$this->language = $language;
	}
	
	private function getCityByCoordinates() {
		/**
		 * Url to Google Geocoding.
		 * Returns the city, areacode and country for given longtitude/latitude.
		 */
		$file = @file_get_contents('http://maps.google.com/maps/geo?output=csv&q=' . $this->longtitude . ',' .$this->latitude);
		if($file) {
			$temp = explode(',', $file);
			$this->city = $temp[2];
			$this->country = $temp[4];
		}
	}
	
	/**
	 * Gets Todays weather and the next 3 days
	 * @return GoogleWeatherForecast
	 */
	
	public function GetTodayAndNext3Days($day=NULL) {
		$list = array();
		$arr=$this->data;
		if(isset($arr[2]) && isset($arr[3]) && isset($arr[4]) && isset($arr[5])) {
			array_push($list, $arr[2]);
			array_push($list, $arr[3]);
			array_push($list, $arr[4]);
			array_push($list, $arr[5]);
			return ($day !== NULL && isset($list[$day])) ? $list[$day] : $list;
		}
		return NULL;
	}
	
	/**
	 * Get todays weather
	 * @return GoogleWeatherForecast
	 */
	
	public function GetTodaysWeather() {
		$arr=$this->data;
		if(isset($arr[5])) return $arr[5];
		return null;
	}
	
	public function hasWeather() {
		return (isset($this->data[2]) && isset($this->data[3]) && isset($this->data[4]) && isset($this->data[5]));
	}
	
	/**
	 * Gets todays current weather conditions
	 * @return GoogleWeatherConditions
	 */
	public function GetTodaysWeatherConditions() {
		$arr=$this->data;
		if(isset($arr[1])) return $arr[1];
		return null;
	}
	
	/**
	 * Returns the current weather forecast information.
	 * @return GoogleWeatherInfo
	 */
	public function GetWeatherForecastInformation() {
		$arr=$this->data;
		if(isset($arr[0])) return $arr[0];
		return null;
	}
	
	public function lookUp() {
		if(!$this->data) {
			if(!$this->city && !$this->country && $this->longtitude && $this->latitude) {
				$this->getCityByCoordinates();
			}
			
			/**
			 * Url to Google Weather Api.
			 * Parameters: @weather = City, Country
			 */
			$file = @file_get_contents('http://www.google.com/ig/api?weather='.(($this->city) ? $this->city . ',' : '').$this->country.'&hl='. $this->language);
			if(!$file) {
				throw new \ErrorException('ERROR: Couldt not connect to Google weather api.');
			}
			$file = utf8_encode($file);
			
			$doc = new \DOMDocument();
			$doc->preserveWhiteSpace=false;
			$doc->loadXML($file);
			
			$forecasts = array();
			
			foreach( $doc->getElementsByTagName('xml_api_reply') as $reply ){
				foreach( $doc->getElementsByTagName('weather') as $weather ){
					foreach ( $doc->getElementsByTagName('forecast_information') as $forecastInformation ) {
						array_push($forecasts, new GoogleWeatherInfo(
															$this->getDataValue($forecastInformation, 'current_date_time'),
															$this->getDataValue($forecastInformation, 'city'),
															$this->getDataValue($forecastInformation, 'forecast_date')));
					}
					foreach ( $doc->getElementsByTagName('current_conditions') as $currentConditions) {
						array_push($forecasts, new GoogleWeatherConditions(
												$this->getDataValue($currentConditions, 'condition'),
												$this->getDataValue($currentConditions, 'temp_c'),
												$this->getDataValue($currentConditions, 'humidity'),
												$this->getDataValue($currentConditions, 'icon'),
												$this->getDataValue($currentConditions, 'wind_condition')));
					}
					foreach( $doc->getElementsByTagName('forecast_conditions') as $forecast ){
						array_push( $forecasts, new GoogleWeatherForecast( 
							$this->getDataValue($forecast, 'condition' ),
							$this->getDataValue($forecast, 'icon' ), 
							$this->getDataValue($forecast, 'low' ), 
							$this->getDataValue($forecast, 'high' ),
							$this->getDataValue($forecast, 'day_of_week' )
						) ); 
					}
				}
			}
	
			$this->data = $forecasts;
		}
		return $this;
	}
	
	private function getDataValue( \DOMElement $forecast, $dataElementName ){
		if( $forecast != null ){
				foreach( $forecast->getElementsByTagName($dataElementName) as $dataElement ){
						$data = $dataElement->getAttributeNode('data');
			if( $data != null ){
				if($dataElementName == 'icon') {
					$matches=array();
					preg_match('/\/ig\/images\/weather\/(.*).gif$/', $data->value, $matches);
					return (($this->includeGoogleIconUrl && isset($matches[1])) ? self::GoogleIconsUrl.$data->value : $matches[1]);
				} else {
					return $data->value;
				}
			}
		}
		}
		return null;
	}
	
	/**
	 * @param string $city
	 */
	public function setCity($city) {
		$this->city = $city;
	}
	
	/**
	 * @param string $country
	 */
	public function setCountry($country) {
		$this->country = $country;
	}
	
	/**
	 * @param int $latitude
	 */
	public function setLatitude($latitude) {
		$this->latitude = $latitude;
	}
	
	/**
	 * @param int $longtitude
	 */
	public function setLongtitude($longtitude) {
		$this->longtitude = $longtitude;
	}
	
	/**
	 * @param bool $includeGoogleIconUrl
	 */
	public function setIncludeGoogleIconUrl($includeGoogleIconUrl) {
		$this->includeGoogleIconUrl = $includeGoogleIconUrl;
	}
	/**
	 * @return string $city
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * @return string $country
	 */
	public function getCountry() {
		return $this->country;
	}
}