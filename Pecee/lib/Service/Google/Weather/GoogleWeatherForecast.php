<?php
namespace Pecee\Service\Google\Weather;
class GoogleWeatherForecast {
	protected $condition;
	protected $low;
	protected $high;
	protected $icon;
	protected $dayOfWeek;
	public function __construct($condition, $icon, $low, $high, $dayOfWeek){
		$this->condition = $condition;
		$this->icon = $icon;
		$this->low = $low;
		$this->high = $high;
		$this->dayOfWeek = $dayOfWeek;
	}
	
	public function getCelcius() {
		return floor(($this->high/$this->low) + $this->low);
	}
	
	/**
	 * @return string
	 */
	public function getCondition() {
		return $this->condition;
	}
	
	/**
	 * @return string
	 */
	public function getDayOfWeek() {
		return $this->dayOfWeek;
	}

	/**
	 * @return int
	 */
	public function getHigh() {
		return $this->high;
	}
	
	/**
	 * @return string
	 */
	public function getIcon() {
		return $this->icon;
	}
	
	/**
	 * @return int
	 */
	public function getLow() {
		return $this->low;
	}
}