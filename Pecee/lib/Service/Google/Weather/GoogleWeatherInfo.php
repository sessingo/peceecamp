<?php
namespace Pecee\Service\Google\Weather;
class GoogleWeatherInfo {
	private $city;
	private $forecast_date;
	private $current_date_time;
	
	public function __construct($current_date_time, $city, $forecast_date) {
		$this->current_date_time = $current_date_time;
		$this->city = $city;
		$this->forecast_date = $forecast_date;
	}
	
	/**
	 * @return string
	 */
	public function getCurrent_date_time() {
		return $this->current_date_time;
	}
	
	/**
	 * @return string
	 */
	public function getCity() {
		return $this->city;
	}
	
	/**
	 * @return string
	 */
	public function getForecast_date() {
		return $this->forecast_date;
	}
}