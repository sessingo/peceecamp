<?php
namespace Pecee\Service\Google\Translate;
class GoogleTranslate {
	const translateUri = 'http://ajax.googleapis.com/ajax/services/language/translate?v=1.0&q=%1$s&langpair=%2$s%%7C%3$s&key=%4$s';
	private $fromLanguage;
	private $toLanguage;
	private $results = array();
	private $appKey;
	
	public function __construct($fromLanguage, $toLanguage) {
		if(strlen($fromLanguage) > 3)
			throw new \InvalidArgumentException('From language cannot be greater than 3 characters.');
		if(strlen($toLanguage) > 3)
			throw new \InvalidArgumentException('To language cannot be greater than 3 characters.');
		$this->fromLanguage = $fromLanguage;
		$this->toLanguage = $toLanguage;
		$this->appKey = \Pecee\Registry::GetInstance()->get(self::SETTINGS_KEY);
	}
	
	/**
	 * Creates new word array
	 * @param string $oldWord
	 * @param string $newWord
	 * @return array
	 */
	private function createWordArray($oldWord, $newWord) {
		$arr = array();
		$arr[$this->fromLanguage] = $oldWord;
		$arr[$this->toLanguage] = $newWord;
		return $arr;
	}
	
	/**
	 * Translates wordlist
	 * @param string $wordList
	 * @return string|array
	 */
	public function translateWordList($wordList, $returnArrayList = true) {
		$wordList = explode(' ', $wordList);
		$newWordList = null;
		foreach($wordList as $word) {
			if($returnArrayList)
				array_push($this->results, $this->createWordArray($word, $this->translateWord($word)));
			else
				$newWordList .= $this->translateWord( $word ) . ' ';
		}
		if($returnArrayList)
			return $this->results;
		return $newWordList;
	}
	
	/**
	 * Translates a single word
	 * @param string $word
	 * @return string
	 */
	public function translateWord($word) {
		$content = @file_get_contents(sprintf(self::translateUri,urlencode($word),$this->fromLanguage,$this->toLanguage, $this->appKey));
		if(!$content)
			throw new \ErrorException('Failed to connect to '.self::translateUri. '. Please try again later.');
		$json = json_decode($content);
		if(isset($json->responseStatus) && $json->responseStatus==400) {
			throw new \ErrorException('Invalid Google-key provided.');
		}
		return $json->responseData->translatedText;
	}
	public function getAppKey() {
		return $this->appKey;
	}
	public function setAppKey($appKey) {
		$this->appKey = $appKey;
	}
}