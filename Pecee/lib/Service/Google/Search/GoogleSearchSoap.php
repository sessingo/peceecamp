<?php
namespace Pecee\Service\Google\Search;
class GoogleSearchSoap {
	
	const ServiceUri = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&key=%3$s&start=%1$d&hl=da&q=%2$s';
	const MAX_RESULTS_ALLOWED_BY_GOOGLE = 64;
	// Settings
	private $query;
	private $resultsPerPage;
	private $pageIndex;
	
	private $result;
	private $siteUri;
	private $appKey;
	
	public function __construct() {
		$this->resultsPerPage = 10;
		$this->pageIndex = 0;
		$this->result = new GoogleSearchResult();
	}
	
	/**
	 * @return string
	 */
	public function getAppKey() {
		return $this->appKey;
	}
	
	/**
	 * @param string $appKey
	 */
	public function setAppKey($appKey) {
		$this->appKey = $appKey;
	}

	
	private function createResultItem( GoogleSearchResultItem $item ) {
		$this->result->items[] = $item;
	}
	
	public function performSearch() {
		if( !$this->query )
			throw new \InvalidArgumentException('No query has been entered.');
		$index = 0;
		$repeations = (($this->resultsPerPage > 0) ? ceil($this->resultsPerPage/4) : 0);
		$this->result->resultsPerPage = $this->resultsPerPage;
		$this->result->pageIndex = $this->pageIndex;
		if( $repeations > 0 ) {
			$q = ($this->siteUri) ? 'site:'.$this->siteUri . ' ' . $this->query : $this->query;
			$curl = new \Pecee\Curl();
			for( $i=0;$i<$repeations;$i++ ) {
				$url = sprintf( self::ServiceUri, (($this->resultsPerPage*$this->pageIndex) + ($i*4)), urlencode($q), $this->appKey);
				$curl->addUrl( $url );
				$curl->addOption( CURLOPT_AUTOREFERER, TRUE );
				$curl->addOption( CURLOPT_RETURNTRANSFER, TRUE );
			}
			
			$curl->execute();
			$responses = $curl->getData();
			
			if( $responses ) {
				foreach( $responses as $response ) {
					$response = json_decode($response);
					$this->result->estimatedResults = (isset($response->responseData->cursor->estimatedResultCount)) ? $response->responseData->cursor->estimatedResultCount : 0;
					if( isset($response->responseData->results) ) {
						foreach( $response->responseData->results as $result ) {
						
							if( $index >= $this->resultsPerPage || (($this->resultsPerPage*$this->pageIndex) + ($index+1)) > $this->result->estimatedResults ) 
								break;

							$item = new GoogleSearchResultItem();
							$item->index = (($this->resultsPerPage*$this->pageIndex) + ($index+1));
							$item->title = $result->title;
							$item->content = $result->content;
							$item->cacheUrl = $result->cacheUrl;
							$item->url = $result->url;
							$item->visibleUrl = $result->visibleUrl; 
							
							$this->createResultItem( $item );
							
							$index++;
						}
					}
				}
				$this->result->estimatedResults = ($this->result->estimatedResults > self::MAX_RESULTS_ALLOWED_BY_GOOGLE) ? self::MAX_RESULTS_ALLOWED_BY_GOOGLE : $this->result->estimatedResults;
				$this->result->estimatedResults = ($this->pageIndex > 0 && $this->result->estimatedResults == 0) ? self::MAX_RESULTS_ALLOWED_BY_GOOGLE : $this->result->estimatedResults;
				$this->result->maxNumberOfPages = ceil( $this->result->estimatedResults / $this->resultsPerPage );
				return $this->result;
			}
		}
		return null;
	}
	
	public function getSite() {
		return $this->siteUri;
	}
	
	public function setSite( $url ) {
		$this->siteUri = $url;
	}
	
	public function getQuery() {
		return $this->query;
	}
	
	public function setQuery($query) {
		$this->query = $query;
	}
	
	public function setPageIndex( $pageIndex ) {
		if( !\Pecee\Integer::is_int($pageIndex) )
			throw new \InvalidArgumentException('Unknown datatype for pageIndex. Must be INT or nummeric stirng.');
		$this->pageIndex = $pageIndex;
	}
	
	public function getResultsPerPage() {
		return $this->resultsPerPage;
	}
	
	public function setResultsPerPage( $number ) {
		if( !\Pecee\Integer::is_int($number) )
			throw new \InvalidArgumentException('Unknown datatype for number. Must be INT or nummeric stirng.');
		$this->resultsPerPage = $number;
	}
}