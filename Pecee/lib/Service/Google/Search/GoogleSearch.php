<?php
namespace Pecee\Service\Google\Search;
class GoogleSearch {
	
	/**
	 * Google Search Query URL
	 */
	const SearchUri = 'http://www.google.dk/search?hl=da&q=%1$s&start=%2$d&num=%3$d&meta=%4$s';
	
	private $keyWord;
	private $resultsCount;
	private $results = array();
	private $page = 0;
	
	// Search settings
	private $searchResults = 10;
	private $siteSearch;
	private $meta;
	
	public function __construct( $keyWord ) {
		$this->keyWord = $keyWord;
	}
	
	/**
	 * Receives a node and converts it to Dom document
	 *
	 * @param DOMNode $domNode
	 * @return DOMDocument
	 */
	private function convertNodeToDom( \DOMNode $domNode ) {
		$doc = new \DOMDocument();
		$doc->appendChild($doc->importNode($domNode,true));
		return $doc;
	}
	
	/**
	 * Creates new result array
	 *
	 * @param string $title
	 * @param string $url
	 * @param string $description
	 * @param array $subLinks
	 */
	private function createResult($title, $url, $description, array $subLinks) {
		$arr = array();
		$arr['Title'] = $title;
		$arr['Url'] = $url;
		$arr['Description'] = $description;
		foreach($subLinks as $key=>$link) {
			$arr[$key] = $link;
		}
		array_push($this->results, $arr);
	}
	
	/**
	 * Perform the Google search.
	 * @return array
	 */
	public function doSearch() {

		if(empty($this->keyWord))
			return false;
		$page = 0;
		if($this->page > 0)
			$page = $this->searchResults * $this->page;
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, sprintf( self::SearchUri, urlencode($this->keyWord), $page, $this->searchResults, $this->meta ));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Cookie: foo=bar'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$HTMLResult = curl_exec($ch);
		curl_close($ch);
		
		/*$HTMLResult = file_get_contents(  sprintf( self::SearchUri, urlencode($this->keyWord), $page, $this->searchResults, $this->meta ) );
			if (!$HTMLResult)
				return null;*/
				
				
		$resultsDom = new \DOMDocument();
		@$resultsDom->loadHTML($HTMLResult);
		$resultsDiv = $resultsDom->getElementsByTagName('div');
	    
	    $resultsCount = substr($resultsDiv->item(5)->nodeValue, (strpos($resultsDiv->item(5)->nodeValue, '.')+2));
		$this->resultsCount = substr($resultsCount, 0, (strpos($resultsCount, 'f')-1));
		
		$matches = array();
		preg_match_all('/<li class=g[^>]*?>(.)*?(?=(?:<li class=g[^>]*?>|<\/ol>|<\/li>))/is',$HTMLResult,$matches);
		
		$results = $matches[0];
		foreach($results as $result) {
			
			$dom = new \DOMDocument();
			@$dom->loadHTML($result);
			$title = $dom->getElementsByTagName('h3')->item(0)->nodeValue;
			$url = $dom->getElementsByTagName('a')->item(0)->getAttribute('href');
			//$de = array();
			$description = array();
			preg_match_all('/<div class=[^>]*?>(.*?)<cite>/is', $result, $description);
			//echo strip_tags($description).'<br>';
			
			$subLinks = array();
			if(!empty($dom->getElementsByTagName('span')->item(0)->nodeValue)) {
	   			$linkNodes = $dom->getElementsByTagName('span')->item(0)->getElementsByTagName('a');
	   			foreach($linkNodes as $linkNode)  {
	   				$subLinks[$linkNode->nodeValue] = $linkNode->getAttribute('href');
	   			}
	   		}
	   		
	   		$this->createResult(utf8_decode($title),
	   							$url,
	   							strip_tags(strip_tags($description[0][0])),
	   							$subLinks);
		}
		return $this->results;
	}
	
	/**
	 * @return int
	 */
	public function getResultsCount() {
		return $this->resultsCount;
	}
	
	/**
	 * @param int $page
	 */
	public function setPage($page) {
		if(!\Pecee\Integer::is_int($page)) throw new \InvalidArgumentException('Unknown datatype for page. Must be INT or nummeric string.');
		$this->page = $page;
	}
	
	/**
	 * @param int $searchResults
	 */
	public function setSearchResults($searchResults) {
		if(!\Pecee\Integer::is_int($searchResults)) throw new \InvalidArgumentException('Unknown datatype for page. Must be INT or nummeric string.');
		$this->searchResults = $searchResults;
	}
	
	/**
	 * Set the search to search a specific site
	 * @param string $siteDomain
	 */
	public function setSiteSearch($siteDomain) {
		$this->keyWord = $this->keyWord . ' site:'.$siteDomain;
	}
	
	/**
	 * Defines if the search should only contain results from certain country.
	 * For example 'da' for Denmark, 'en' for England and so on...
	 *
	 * @param string $searchByCountry
	 */
	public function setSearchByCountry($country) {
		if(strlen($country) > 2)
			throw new \InvalidArgumentException('Country may only be 2 digits long.');
		$this->meta = 'lr%3Dlang_' . $country;
	}
	
	/**
	 * Defines if the search should only contain results on certain language.
	 * For example 'dk' for danish, 'en' for english and so on...
	 *
	 * @param string $searchByCountry
	 */
	public function setSearchByLanguage($language) {
		if(strlen($language) > 3)
			throw new \InvalidArgumentException('Language may only be 3 digits long.');
		$this->meta = 'cr%3Dcountry' . strtoupper($language);
	}
	
	public function getPageIndex() {
		if($this->page && \Pecee\Integer::is_int($this->page))
			return $this->page;
		return 0;
	}
	
	public function getMaxNumberOfPages() {
		//echo 'hej '.$this->searchResults;
		if( strpos($this->resultsCount, '.') )
			$result =  @str_replace('.', '', $this->resultsCount);
		return floor ( (int)$result / (int)$this->searchResults );
	}
}