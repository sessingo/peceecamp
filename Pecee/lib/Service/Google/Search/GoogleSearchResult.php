<?php
namespace Pecee\Service\Google\Search;
class GoogleSearchResult {
	public $estimatedResults = 0;
	public $pageIndex;
	public $maxNumberOfPages = 0;
	public $resultsPerPage;
	public $items;
}