<?php
namespace Pecee\Service\Google\Search;
class GoogleSearchImages {
	
    private $imagesOf="Lindsay"; 
    private $numberOfImages;     
    private $googleUrlArray=array(); 
    private $page = 0;

    /** 
     * Function to download images from images.google 
     * 
     * @param string $imagesOf 
     * @param int $numberOfImages [OPTIONAL] 
     * @param int $sizeLimit [OPTIONAL] 
     */ 
    public function __construct($imagesOf, $numberOfImages=40, $pageIndex=0) { 
        $this->imagesOf=$imagesOf; 
        $this->numberOfImages=$numberOfImages;              
        $this->page = $pageIndex;
        $this->googleUrlArray=$this->createGoogleUrl();
    } 
     
    /** 
     * Function the create a list of google image urls 
     *  
     * @access private 
     * @return array 
     */ 
    private function createGoogleUrl() { 
        $imagesOf = $this->imagesOf; 
        $numberOfImages = $this->numberOfImages; 
        $numberOfPages = round(($numberOfImages/20)+1); 
        
        $j=0 ;
        
        if($this->page > 0)
			$j = $this->numberOfImages * $this->page;
			
		//echo $j;
         
        for($i=0; $i<$numberOfPages; $i++ ) { 
            $returnArray[] = "http://images.google.com/images?q=".rawurlencode($imagesOf)."&hl=da&start=".$j."&sa=N&gbv=1";             
            //$j += 20; 
        } 
        return $returnArray; 
    } 
     
    /** 
     * Function to download images from google 
     *  
     * @access public 
     * @return void 
     */ 
    public function downloadImages($sizeLimit) {         
        $imgUrlArray=$this->getImageUrls();         
        $dir=$this->imagesOf; 
        @mkdir($dir,0777); 
        for($i=0; $i<count($imgUrlArray); $i++) { 
            $imageName = basename($imgUrlArray[$i]); 
            $info = @getimagesize($imgUrlArray[$i]); 
            if(trim($sizeLimit) != "" && $sizeLimit > 0) { 
                if (count($info) > 0 && $info[0] >= $sizeLimit) { 
                    if(trim($imageName) != '' ) { 
                        copy($imgUrlArray[$i], $dir."/".$imageName); 
                    } 
                } 
            } 
            else { 
                if(trim($imageName) != '' ) { 
                    copy($imgUrlArray[$i], $dir."/".$imageName); 
                } 
            } 
        } 
    } 
     
    /** 
     * Function to return paths of all images to be downloaded 
     * 
     * @access public 
     * @return array 
     */ 
    public function getImageUrls() { 
        $urlArray=$this->googleUrlArray; 
        $returnArray=array(); 
        $returnArray1=array(); 
        $return=array(); 

        for ($j=0;$j<count($urlArray);$j++){ 
            $url=trim($urlArray[$j]); 
            $str=$this->getHtmlCode($url); 
            $pattern='/<img src=http:(.*)\<\/a>/Us'; 
            preg_match_all($pattern, $str, $returnArray, PREG_GREP_INVERT); 
            $returnArray1=$returnArray[1]; 
            $count=count($returnArray1); 
            for ($i=1;$i<$count;$i++) { 
               $str1=trim(strip_tags($returnArray[1][$i])); 
               $pos1=strrpos($str1,"http://"); 
               $pos2=strpos($str1," width"); 
               $link=trim(substr($str1,$pos1,$pos2-$pos1));
 
               if($this->checkHostName(urldecode($link)))
               		$return[]=urldecode($link); 

            } 
        } 
        return $return; 
    } 
    
    private function checkHostName($url) {
    	$image = @getimagesize($url);
    	if(isset($image['mime']))
    		return true;
    	return false;
    }
     
    /** 
     * Function to get source code of a url 
     * 
     * @param string $url 
     * @access private 
     * @return string 
     */ 
    private function getHtmlCode($url){ 
        $returnStr=""; 
        $fp=fopen($url, "r"); 
        while (!feof($fp)) { 
            $returnStr.=fgetc($fp); 
        } 
        fclose($fp); 
        return $returnStr; 
    } 
	
	public function getPageIndex() {
		if($this->page && \Pecee\Integer::is_int($this->page))
			return $this->page;
		return 0;
	}	
}