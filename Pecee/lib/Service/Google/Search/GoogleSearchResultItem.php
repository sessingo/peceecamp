<?php
namespace Pecee\Service\Google\Search;
class GoogleSearchResultItem {
	public $index;
	public $title;
	public $content;
	public $cacheUrl;
	public $visibleUrl;
	public $url;
}