<?php
namespace Pecee\Service\Google;
class SpellCheck{
	
	/**
	 * Google SpellChecker Request URI
	 */
	const RequestUri = 'https://www.google.com/tbproxy/spell?lang=%s&hl=en';
	private $language;
	private $encoding = 'UTF-8';
	public function __construct($language = 'en'){
		if(! extension_loaded('curl'))
			throw new \Exception('Curl extension is not loaded. Please check you\'re php.ini and try again.');
		
		$this->language = $language;
	}
	public function checkWords($wordsText){
		$words = array();
		$wordsArray = explode(' ', $wordsText);
		
		foreach($wordsArray as $word) {
			$temp = $this->getSuggestion($word);
			
			if(! empty($temp[0])) {
				$words[$word] = $temp;
			}
		}
		return $words;
	}
	private function getSuggestion($word){
		$suggestion = array();
		
		$matches = $this->getMatches($word);
		
		foreach($matches as $match) {
			$suggestion[] = $match;
		}
		
		return $suggestion;
	}
	private function getMatches($word){
		$url = sprintf(self::RequestUri, $this->language);
		$dom = new \DOMDocument('1.0', $this->encoding);
		$request = $dom->appendChild($dom->createElement('spellrequest', ''));
		$request->setAttribute('textalreadyclipped', '0');
		$request->setAttribute('ignoredups', '0');
		$request->setAttribute('ignoredigits', '1');
		$request->setAttribute('ignoreallcaps', '1');
		$request->appendChild($dom->createElement('text', utf8_encode($word)));
		$output = $dom->saveXML();
		
		$header = "POST " . $url . " HTTP/1.0 \r\n";
		$header .= "MIME-Version: 1.0 \r\n";
		$header .= "Content-type: application/html \r\n";
		$header .= "Content-length: " . strlen($output) . " \r\n";
		$header .= "Content-transfer-encoding: text \r\n";
		$header .= "Request-number: 1 \r\n";
		$header .= "Document-type: Request \r\n";
		$header .= "Connection: close \r\n\r\n";
		$header .= $output;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $header);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$xml = curl_exec($ch);
		curl_close($ch);
		
		$load = simplexml_load_string($xml);
		
		return explode("\t", $load->c);
	}
}