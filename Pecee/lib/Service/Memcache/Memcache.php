<?php
namespace Pecee\Service\Memcache;
class Memcache {
	const SETTINGS_HOST = 'MEMCACHE_HOST';
	const SETTINGS_PORT = 'MEMCACHE_PORT';
	private static $instance;
	protected $memcache;
	public static function GetInstance() {
		if(!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	public function __construct() {
		$this->memcache = new \Memcache();
		if(@$this->memcache->connect(\Pecee\Registry::GetInstance()->get(self::SETTINGS_HOST, '127.0.0.1'), 
								\Pecee\Registry::GetInstance()->get(self::SETTINGS_PORT, 11211)) === false) {
			throw new MemcacheException("Error connecting to memcache server: " . \Pecee\Registry::GetInstance()->get(self::SETTINGS_HOST, '127.0.0.1'));
		}
	}
	public function set($key, $value, $flag, $expire) {
		$this->memcache->set($key, $value, $flag, $expire);
	}
	public function get($key) {
		return $this->memcache->get($key);
	}
	public function clear($key=NULL) {
		if(!is_null($key)) {
			$this->memcache->delete($key);
		} else {
			$this->memcache->flush();
		}
	}
	public function getMemcache() {
		return $this->memcache;
	}
	public function close() {
		$this->memcache->close();
	}
	public function __destruct() {
		$this->close();
	}
}