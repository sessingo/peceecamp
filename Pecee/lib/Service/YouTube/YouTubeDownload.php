<?php
namespace Pecee\Service\YouTube;
class YouTubeDownload {
	
	private $YouTubeUri;
	private $outputMp4;
	
	public function __construct( $YouTubeUri ) {
		$this->YouTubeUri = $YouTubeUri;
	}
	
	public function download() {
		
		if( !$this->YouTubeUri )
			throw new \InvalidArgumentException('No YouTube video to process.');
		
		$v = $this->YouTubeUri;
		$v = preg_split ("/\\?v=/", $v);
		
		//youtube video-id
		$v = $v[1];
		
		//hardcoding URL here, couldn't figure how to make CURL follow 303 redirection
		//seems something wrong with libCurl or my knowledge...
		$url = 'http://www.youtube.com/watch?v=' . $v;
		
		//Start the Curl session
		$curl = new \Pecee\Curl();
		$curl->addUrl($url);
		$curl->addOption(CURLOPT_HEADER, TRUE);
		$curl->addOption(CURLOPT_FOLLOWLOCATION, TRUE);
		$curl->addOption(CURLOPT_RETURNTRANSFER, TRUE);
		$curl->execute();
		$response = $curl->getData();
		
		$matches = array();
		if (preg_match_all("/&t=[^&]*/", $response, $matches)) {
			$t = $matches[0][0];
			$t = preg_split("/=/", $t);
			
			//youtube t param
			$t = $t[1];
			
			//construct the flv-url
			$youtubeVideoPath = 'http://www.youtube.com/get_video.php?video_id=' . $v . '&t='. $t . $this->outputMp4;
			
			//redirect to flv -  you can replace this code with echo/print 
			//to return the path to client (browser/flash)
			return $youtubeVideoPath;
		}
		return null;		
	}
	
	function setOutputAsMp4( $option ) {
		if( $option )
			$this->outputMp4 = '&fmt=18';
	}
	
}