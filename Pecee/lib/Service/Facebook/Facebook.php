<?php
namespace Pecee\Service\Facebook;
class Facebook extends FacebookBase {
	protected static $kSupportedKeys = array('state', 'code', 'access_token', 'user_id');
	
	public function __construct($appId, $secret, $config=null) {
		if (!session_id()) {
			session_start();
	    }
	    $config=(is_null($config)) ? array() : $config;
	    $config=array_merge(array('appId' => $appId, 'secret' => $secret), $config);
	    
	    parent::__construct($config);
	}
	
	/**
	* Provides the implementations of the inherited abstract
	* methods.  The implementation uses PHP sessions to maintain
	* a store for authorization codes, user ids, CSRF states, and
	* access tokens.
	*/
	protected function setPersistentData($key, $value) {
		if (!in_array($key, self::$kSupportedKeys)) {
	      self::errorLog('Unsupported key passed to setPersistentData.');
	      return;
	    }
	
	    $session_var_name = $this->constructSessionVariableName($key);
	    $_SESSION[$session_var_name] = $value;
	}

	protected function getPersistentData($key, $default = false) {
		if (!in_array($key, self::$kSupportedKeys)) {
	      self::errorLog('Unsupported key passed to getPersistentData.');
	      return $default;
	    }
	
	    $session_var_name = $this->constructSessionVariableName($key);
	    return isset($_SESSION[$session_var_name]) ? $_SESSION[$session_var_name] : $default;
	}

	protected function clearPersistentData($key) {
		if (!in_array($key, self::$kSupportedKeys)) {
			self::errorLog('Unsupported key passed to clearPersistentData.');
			return;
	    }
	
	    $session_var_name = $this->constructSessionVariableName($key);
	    unset($_SESSION[$session_var_name]);
		if (!in_array($key, self::$kSupportedKeys)) {
			self::errorLog('Unsupported key passed to clearPersistentData.');
			return;
		}
	}

	protected function clearAllPersistentData() {
		foreach (self::$kSupportedKeys as $key) {
			$this->clearPersistentData($key);
	    }
	}

	protected function constructSessionVariableName($key) {
		return implode('_', array('fb', $this->getAppId(), $key));
	}
	
	public function __toString() {
		return '<div id="fb-root"></div> 
				<script type="text/javascript"> 
					window.fbAsyncInit = function() { 
						FB.init({	
									appId	: \''.$this->appId.'\', 
									status	: true, 
									cookie	: true, 
									xfbml	: true,
									oauth	: true
								}); 
						if (true) {
							FB.Canvas.setAutoGrow();
						}
			
						/* jQuery event */
						if (true && jQuery) {
							jQuery(document).trigger(\'fbAsyncInit\');
						}
					}; 
					(function() { 
						var e = document.createElement(\'script\'); e.async = true; 
						e.src = document.location.protocol + \'//connect.facebook.net/en_US/all.js\'; 
						document.getElementById(\'fb-root\').appendChild(e); }());
				</script>';
	}
}