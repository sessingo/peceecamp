<?php
namespace Pecee\Service\BreakMedia;
class BreakMedia {

	const SERVICE_URL = 'http://api.breakmedia.com/content/contentFeed/get?apiRequestJson={slug:%27Brk_Web_HP_Stream%27,responseType:%27rss%27}';

	protected $results;

	public function getItems() {
		$this->results=array();
		$data = new \SimpleXMLElement(file_get_contents(self::SERVICE_URL));

		if($data) {
			/* @var $item \SimpleXMLElement */
			foreach($data->xpath('channel/item') as $item) {
				$result = new BreakMediaItem();

				$result->title = html_entity_decode(trim(strip_tags((string)$item->title)));
				$result->description = html_entity_decode(trim(strip_tags((string)$item->description)));


				$result->identifierId = md5((string)$item->link);

				$content = $item->xpath('media:content');
				if(isset($content[0])) {
					$content = $content[0];
					$result->url = (string)$content['url'];
					$this->results[] = $result;
				}
			}

		}
		return $this->results;
	}

	/**
	 * @return array $results
	 */
	public function getResults() {
		return $this->results;
	}

	public static function Get() {
		$break = new self();
		return $break->getItems();
	}

}