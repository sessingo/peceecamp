<?php
namespace Pecee\Xml\Rss;
class RssItemEnclosure {
	public function __construct($url, $length, $type) {
		$this->url = $url;
		$this->length = $length;
		$this->type = $type;
	}
	protected $url;
	protected $length;
	protected $type;
	
	/**
	 * @return int
	 */
	public function getLength() {
		return $this->length;
	}
	
	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}
	
	/**
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}
	
	/**
	 * @param unknown_type $length
	 */
	public function setLength($length) {
		$this->length = $length;
	}
	
	/**
	 * @param unknown_type $type
	 */
	public function setType($type) {
		$this->type = $type;
	}
	
	/**
	 * @param unknown_type $url
	 */
	public function setUrl($url) {
		$this->url = $url;
	}

}