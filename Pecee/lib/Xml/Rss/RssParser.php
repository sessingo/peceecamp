<?php
namespace Pecee\Xml\Rss;
use Pecee\Str;

/**
 * ------------ RSS 2.0 PARSER ------------
 * Parses valid RSS-feeds according to: 
 * http://cyber.law.harvard.edu/rss/rss.html
 * ----------------------------------------
 * 
 * @version 1.0
 * @author Simon Sessingø v./ Pecee
 * @license The GNU General Public Licence
 */
class RssParser {
	private static $rssUri;

	/**
	 * Parses RSS-feed by given rss uri.
	 *
	 * @param string $rssUri
	 * @return \Pecee\Xml\Rss\Rss|null
	 */
	public static function parseRssUri($rssUri, $saveXml = false) {
		self::$rssUri = $rssUri;
		return self::doParse();
	}
	
	/**
	 * Parse RSS-feed by given XML.
	 *
	 * @param string $xml
	 * @return \Pecee\Xml\Rss\Rss|null
	 */
	public static function parseRss( $xml ) {
		return self::doParse( array($xml) );
	}
	
	/**
	 * Performs the parsing of the RSS-feed.
	 * @return \Pecee\Xml\Rss\Rss_Response|null
	 */
	private static function doParse( $data = NULL, $saveXml = false ) {
		if($data === NULL && \Pecee\Url::IsValid(self::$rssUri)) {
			$data=@file_get_contents(self::$rssUri);
			if(!$data) {
				throw new \ErrorException('Failed to connect to feed');
			}
			
		}
		if(!\Pecee\Util::is_valid_xml($data)) {
			$data=Str::CleanXML($data);
			if(!\Pecee\Util::is_valid_xml($data)) {
				throw new \ErrorException('Feed does not contain valid XML.');
			}
		}
		$dom = new \DOMDocument();
		$dom->preserveWhiteSpace=false;
		$dom->loadXML($data);
		
		// Parsing the required channel properties.
		$channel = $dom->getElementsByTagName('channel')->item(0);
		
		$title=NULL;
		$link = NULL;
		$description=NULL;
		$language = NULL;
		$copyright = NULL;
		$managingEditor = NULL;
		$webmaster = NULL;
		$pubDate = NULL;
		$lastBuildDate = NULL;
		$category = NULL;
		$generator = NULL;
		$docs = NULL;
		$cloud = NULL;
		$ttl = NULL;
		$image = NULL;
		$rating = NULL;
		$textInput = NULL;
		$skipHours = NULL;
		$skipDays = NULL;
		
		if($channel != null) {
			$title = self::GetValue($channel, 'title');
			$link = self::GetValue($channel, 'link');	
			$description = self::GetValue($channel, 'description');
		
			// Trying to parse the optional channel properties.
			$language = self::GetValue($channel, 'language');
			$copyright = self::GetValue($channel, 'copyright');
			$managingEditor = self::GetValue($channel, 'managingEditor');
			$webmaster = self::GetValue($channel, 'webMaster');
			$pubDate = self::GetValue($channel, 'pubDate');
			$lastBuildDate = self::GetValue($channel, 'lastBuildDate');
			$category = self::GetValue($channel, 'category');
			$generator = self::GetValue($channel, 'generator');
			$docs = self::GetValue($channel, 'docs');
			$cloud = self::GetValue($channel, 'cloud');
			$ttl = self::GetValue($channel, 'ttl');
			$image = self::GetValue($channel, 'image');
			$rating = self::GetValue($channel, 'rating');
			$textInput = self::GetValue($channel, 'textInput');
			$skipHours = self::GetValue($channel, 'skipHours');
			$skipDays = self::GetValue($channel, 'skipDays');
		}
		
		$items = $dom->getElementsByTagName('item');
		$items_array = array();
		if($items != null) {
			foreach($items as $item) {
				$item_title = self::GetValue($item, 'title');
				$item_link = self::GetValue($item, 'link');
				$item_description = self::GetValue($item, 'description');
				$item_author = self::GetValue($item, 'author');
				$item_category = self::GetValue($item, 'category');
				$item_comments = self::GetValue($item, 'comments');
				$item_enclosure = $item->getElementsByTagName('enclosure')->item(0);
				$enclosure_type = NULL;
				$enclosure_url = NULL;
				$enclosure_length = NULL;
				if($item_enclosure != null) {
					if($item_enclosure->hasAttribute('type')) {
						$enclosure_type = $item_enclosure->getAttribute('type');
					}
					if($item_enclosure->hasAttribute('url')) {
						$enclosure_url = $item_enclosure->getAttribute('url');
					}
					if($item_enclosure->hasAttribute('length')) {
						$enclosure_length = $item_enclosure->getAttribute('length');
					}
				}
				$item_guid = self::GetValue($item, 'guid');
				$item_pubDate = self::GetValue($item, 'pubDate');
				$item_source = self::GetValue($item, 'source');
				$item_embed = self::GetValue($item, 'embed');
				
				$rssItem = new RssItem();
				$rssItem->setTitle($item_title);
				$rssItem->setLink($item_link);
				$rssItem->setDescription($item_description);
				$rssItem->setAuthor($item_author);
				$rssItem->setCategory($item_category);
				$rssItem->setComments($item_comments);
				if($item_enclosure) {
					$rssItem->setEnclosure($enclosure_url, $enclosure_length, $enclosure_type);
				}
				$rssItem->setGuid($item_guid);
				$rssItem->setPubDate($item_pubDate);
				$rssItem->setSource($item_source);
				$rssItem->setEmbed($item_embed);
				$items_array[] = $rssItem;
			}
		}
		
		$rss = new \Pecee\Xml\Rss\Rss($title, $link, $description);
		$rss->setLink($link);
		$rss->setLanguage($language);
		$rss->setCopyright($copyright);
		$rss->setManagingEditor($managingEditor);
		$rss->setWebmaster($webmaster);
		$rss->setPubDate($pubDate);
		$rss->setLastBuildDate($lastBuildDate);
		$rss->setCategory($category);
		$rss->setGenerator($generator);
		$rss->setDocs($docs);
		$rss->setCloud($cloud);
		$rss->setTtl($ttl);
		$rss->setImage($image);
		$rss->setRating($rating);
		$rss->setTextInput($textInput);
		$rss->setSkipHours($skipHours);
		$rss->setSkipDays($skipDays);
		$rss->setItems($items_array);
		
		if($saveXml) {
			$rss->setXml($data);
		}
		return $rss;
	}
	
	public static function GetValue($item, $name) {
		return ($item->getElementsByTagName($name)->item(0) != null) ? $item->getElementsByTagName($name)->item(0)->nodeValue : NULL;
	}
	
}