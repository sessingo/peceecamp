<?php
namespace Pecee\Xml\Rss;

class RssItem {

	protected $title;
	protected $link;
	protected $description;
	protected $author;
	protected $category;
	protected $comments;
	protected $enclosure;
	protected $guid;
	protected $embed;
	protected $pubDate;
	protected $source;
	
	protected $cDataDescription = false;
	
	/*
	 * Properties of an item element.
	 * All elements of an item are optional, 
	 * however at least one of title or description must be present.
	 * 
	 * For more information, please refer to:
	 * http://cyber.law.harvard.edu/rss/rss.html
	 */
	
	/**
	 * @return string
	 */
	public function getAuthor() {
		return $this->author;
	}
	
	/**
	 * @return string
	 */
	public function getCategory() {
		return $this->category;
	}
	
	/**
	 * @return string
	 */
	public function getComments() {
		return $this->comments;
	}
	
	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * @return \Pecee\Xml\Rss\RssItemEnclosure
	 */
	public function getEnclosure() {
		return $this->enclosure;
	}
	
	/**
	 * @return string
	 */
	public function getGuid() {
		return $this->guid;
	}
	
	/**
	 * @return string
	 */
	public function getLink() {
		return $this->link;
	}
	
	/**
	 * @return string
	 */
	public function getPubDate() {
		return $this->pubDate;
	}
	
	/**
	 * @return string
	 */
	public function getSource() {
		return $this->source;
	}
	
	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}
	
	/**
	 * @param unknown_type $author
	 */
	public function setAuthor($author) {
		$this->author = $author;
	}
	
	/**
	 * @param unknown_type $category
	 */
	public function setCategory($category) {
		$this->category = $category;
	}
	
	/**
	 * @param unknown_type $comments
	 */
	public function setComments($comments) {
		$this->comments = $comments;
	}
	
	/**
	 * @param unknown_type $description
	 */
	public function setDescription($description, $cDataDescription = false) {
		$this->description = $description;
		$this->cDataDescription = $cDataDescription;
	}
	
	/**
	 * @param unknown_type $enclosure
	 */
	public function setEnclosure($url, $length, $mimeType) {
		$this->enclosure = new RssItemEnclosure($url, $length, $mimeType);
	}
	
	/**
	 * @param unknown_type $guid
	 */
	public function setGuid($guid) {
		$this->guid = $guid;
	}
	
	/**
	 * @param unknown_type $link
	 */
	public function setLink($link) {
		$this->link = $link;
	}
	
	/**
	 * @param unknown_type $pubDate
	 */
	public function setPubDate($pubDate) {
		$this->pubDate = $pubDate;
	}
	
	/**
	 * @param unknown_type $source
	 */
	public function setSource($source) {
		$this->source = $source;
	}
	
	/**
	 * @param unknown_type $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}
	
	/**
	 * @return bool
	 */
	public function getCDataDescription() {
		return $this->cDataDescription;
	}
	
	/**
	 * @return string $embed
	 */
	public function getEmbed() {
		return $this->embed;
	}

	/**
	 * @param string $embed
	 */
	public function setEmbed($embed) {
		$this->embed = $embed;
	}
}