<?php
namespace Pecee\Xml\Rss;
class Rss {
	
	protected $document;
	
	/*
	 * -- Feed properties --
	* For more information about valid channel properties,
	* please refer to: http://cyber.law.harvard.edu/rss/rss.html
	*/
	
	// Required channel properties
	/**
	 * The name of the channel.
	 * @var string
	 */
	protected $title;
	
	/**
	 * The URL to the HTML website corresponding to the channel.
	 * @var string
	 */
	protected $link;
	
	/**
	 * Phrase or sentence describing the channel.
	 * @var string
	 */
	protected $description;
	
	// Optional channel properties
	protected $language;
	protected $copyright;
	protected $managingEditor;
	protected $webmaster;
	protected $pubDate;
	protected $lastBuildDate;
	protected $category;
	protected $generator;
	protected $docs;
	protected $cloud;
	protected $ttl;
	protected $image;
	protected $rating;
	protected $textInput;
	protected $skipHours;
	protected $skipDays;
	protected $items = array();
	
	private $xml;
	
	public function __construct($title, $link, $description) {
		$this->title = $title;
		$this->link = $link;
		$this->description = $description;
	}
	
	/**
	 * @return unknown
	 */
	public function getCategory() {
		return $this->category;
	}
	
	/**
	 * @return unknown
	 */
	public function getCloud() {
		return $this->cloud;
	}
	
	/**
	 * @return unknown
	 */
	public function getCopyright() {
		return $this->copyright;
	}
	
	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * @return unknown
	 */
	public function getDocs() {
		return $this->docs;
	}
	
	/**
	 * @return unknown
	 */
	public function getGenerator() {
		return $this->generator;
	}
	
	/**
	 * @return unknown
	 */
	public function getImage() {
		return $this->image;
	}
	
	/**
	 * @return array
	 */
	public function getItems() {
		return $this->items;
	}
	
	/**
	 * @return unknown
	 */
	public function getLanguage() {
		return $this->language;
	}
	
	/**
	 * @return unknown
	 */
	public function getLastBuildDate() {
		return $this->lastBuildDate;
	}
	
	/**
	 * @return string
	 */
	public function getLink() {
		return $this->link;
	}
	
	/**
	 * @return unknown
	 */
	public function getManagingEditor() {
		return $this->managingEditor;
	}
	
	/**
	 * @return unknown
	 */
	public function getPubDate() {
		return $this->pubDate;
	}
	
	/**
	 * @return unknown
	 */
	public function getRating() {
		return $this->rating;
	}
	
	/**
	 * @return unknown
	 */
	public function getSkipDays() {
		return $this->skipDays;
	}
	
	/**
	 * @return unknown
	 */
	public function getSkipHours() {
		return $this->skipHours;
	}
	
	/**
	 * @return unknown
	 */
	public function getTextInput() {
		return $this->textInput;
	}
	
	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}
	
	/**
	 * @return unknown
	 */
	public function getTtl() {
		return $this->ttl;
	}
	
	/**
	 * @return unknown
	 */
	public function getWebmaster() {
		return $this->webmaster;
	}
	
	/**
	 * @param unknown_type $category
	 */
	public function setCategory($category) {
		$this->category = $category;
	}
	
	/**
	 * @param unknown_type $cloud
	 */
	public function setCloud($cloud) {
		$this->cloud = $cloud;
	}
	
	/**
	 * @param unknown_type $copyright
	 */
	public function setCopyright($copyright) {
		$this->copyright = $copyright;
	}
	
	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}
	
	/**
	 * @param unknown_type $docs
	 */
	public function setDocs($docs) {
		$this->docs = $docs;
	}
	
	/**
	 * @param unknown_type $generator
	 */
	public function setGenerator($generator) {
		$this->generator = $generator;
	}
	
	/**
	 * @param unknown_type $image
	 */
	public function setImage($image) {
		$this->image = $image;
	}
	
	/**
	 * @param array $items
	 */
	public function setItems($items) {
		$this->items = $items;
	}
	
	/**
	 * @param unknown_type $language
	 */
	public function setLanguage($language) {
		$this->language = $language;
	}
	
	/**
	 * @param unknown_type $lastBuildDate
	 */
	public function setLastBuildDate($lastBuildDate) {
		$this->lastBuildDate = $lastBuildDate;
	}
	
	/**
	 * @param string $link
	 */
	public function setLink($link) {
		$this->link = $link;
	}
	
	/**
	 * @param unknown_type $managingEditor
	 */
	public function setManagingEditor($managingEditor) {
		$this->managingEditor = $managingEditor;
	}
	
	/**
	 * @param unknown_type $pubDate
	 */
	public function setPubDate($pubDate) {
		$this->pubDate = $pubDate;
	}
	
	/**
	 * @param unknown_type $rating
	 */
	public function setRating($rating) {
		$this->rating = $rating;
	}
	
	/**
	 * @param unknown_type $skipDays
	 */
	public function setSkipDays($skipDays) {
		$this->skipDays = $skipDays;
	}
	
	/**
	 * @param unknown_type $skipHours
	 */
	public function setSkipHours($skipHours) {
		$this->skipHours = $skipHours;
	}
	
	/**
	 * @param unknown_type $textInput
	 */
	public function setTextInput($textInput) {
		$this->textInput = $textInput;
	}
	
	/**
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}
	
	/**
	 * @param unknown_type $ttl
	 */
	public function setTtl($ttl) {
		$this->ttl = $ttl;
	}
	
	/**
	 * @param unknown_type $webmaster
	 */
	public function setWebmaster($webmaster) {
		$this->webmaster = $webmaster;
	}
	
	/**
	 * @return string
	 */
	public function getXml() {
		return $this->xml;
	}
	
	/**
	 * @param string $xml
	 */
	public function setXml($xml) {
		$this->xml = $xml;
	}

	private function addAttribute( \DOMElement $element, $name, $value ) {
		$attr = $this->document->createAttribute($name);
		$attr->appendChild( $this->document->createTextNode($value) );
		$element->appendChild($attr);
	}
	
	private function createChild( \DOMElement $parent, \DOMElement $child, $removeIfNull = false ) {
		if($removeIfNull && !empty($child->nodeValue) || !$removeIfNull) {
			$parent->appendChild($child);
		}
	}
	
	public function write() {
		$this->document = new \DOMDocument('1.0', 'utf-8');
		$this->document->formatOutput = true;
		$rss = $this->document->createElement('rss');
		$this->addAttribute($rss, 'version', '2.0');
		$channel = $this->document->createElement('channel');
	
		/* Channel */
		$this->createChild($channel, $this->document->createElement('title', $this->title));
		$this->createChild($channel, $this->document->createElement('link', $this->link));
		$this->createChild($channel, $this->document->createElement('description', $this->description));
	
		/* Optional channel elements */
		$this->createChild($channel, $this->document->createElement('language', $this->language), TRUE);
		$this->createChild($channel, $this->document->createElement('copyright', $this->copyright), TRUE);
		$this->createChild($channel, $this->document->createElement('managingEditor', $this->managingEditor), TRUE);
		$this->createChild($channel, $this->document->createElement('webMaster', $this->webmaster), TRUE);
		$this->createChild($channel, $this->document->createElement('pubDate', (($this->pubDate) ? date('r', $this->pubDate) : $this->pubDate)), TRUE);
		$this->createChild($channel, $this->document->createElement('lastBuildDate', $this->lastBuildDate), TRUE);
		$this->createChild($channel, $this->document->createElement('category', $this->category), TRUE);
		$this->createChild($channel, $this->document->createElement('generator', $this->generator), TRUE);
		$this->createChild($channel, $this->document->createElement('docs', $this->docs), TRUE);
		$this->createChild($channel, $this->document->createElement('cloud', $this->cloud), TRUE);
		$this->createChild($channel, $this->document->createElement('ttl', $this->ttl), TRUE);
		$this->createChild($channel, $this->document->createElement('image', $this->image), TRUE);
		$this->createChild($channel, $this->document->createElement('rating', $this->rating), TRUE);
		$this->createChild($channel, $this->document->createElement('textInput', $this->textInput), TRUE);
		$this->createChild($channel, $this->document->createElement('skipHours', $this->skipHours), TRUE);
		$this->createChild($channel, $this->document->createElement('skipDays', $this->skipDays), TRUE);
	
		/* Items */
		if(!$this->items) {
			throw new \ErrorException('You need to add at least one item!');
		}
	
		foreach($this->items as $item) {
			/* @var $item RssItem */
			$itemElement = $this->document->createElement('item');
			@$this->createChild($itemElement, $this->document->createElement('title', $item->getTitle()));
			if($item->getCDataDescription()) {
				$description = $this->document->createElement('description');
				$description->appendChild($this->document->createCDATASection($item->getDescription()));
			} else {
				@$description = $this->document->createElement('description', $item->getDescription());
			}
			@$this->createChild($itemElement, $description);
			@$this->createChild($itemElement, $this->document->createElement('link', $item->getLink()), TRUE);
			@$this->createChild($itemElement, $this->document->createElement('author', $item->getAuthor()), TRUE);
			@$this->createChild($itemElement, $this->document->createElement('category', $item->getCategory()), TRUE);
			@$this->createChild($itemElement, $this->document->createElement('comments', $item->getComments()), TRUE);
			if($item->getEnclosure()) {
				$enclosure = $this->document->createElement('enclosure');
				$this->addAttribute($enclosure, 'url', $item->getEnclosure()->getUrl());
				$this->addAttribute($enclosure, 'length', $item->getEnclosure()->getLength());
				$this->addAttribute($enclosure, 'type', $item->getEnclosure()->getType());
				$this->createChild($itemElement, $enclosure);
			}
			$this->createChild($itemElement, $this->document->createElement('guid', $item->getGuid()), TRUE);
			$this->createChild($itemElement, $this->document->createElement('pubDate', (($item->getPubDate()) ? date('r', $item->getPubDate()) : $item->getPubDate())), TRUE);
			@$this->createChild($itemElement, $this->document->createElement('source', $item->getSource()), TRUE);
			$channel->appendChild($itemElement);
		}
		$rss->appendChild($channel);
		$this->document->appendChild($rss);
		return $this->document->saveXML();
	}
	
	public function addItem(RssItem $item ) {
		$this->items[] = $item;
	}
}