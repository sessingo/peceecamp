<?php
namespace Pecee\Xml\Feed;
class Feed {
	const M_POST = "POST";
	const M_GET = "GET";
	/**
	 * Protocol (http,https etc.)
	 *
	 * @var string
	 */
	private $protocol = 'http';
	/**
	 * Path
	 *
	 * @var string
	 */
	private $path;
	/**
	 * Host
	 *
	 * @var string
	 */
	private $host;
	/**
	 * Port
	 *
	 * @var integer
	 */
	private $port = 80;
	/**
	 * Query
	 *
	 * @var string
	 */
	private $query;
	/**
	 * Method
	 *
	 * @var Enum(GET,POST)
	 */
	private $method = self::M_GET;
	/**
	 * Response body
	 *
	 * @var string
	 */
	private $responseBody;
	/**
	 * Response header
	 *
	 * @var array
	 */
	private $responseHeader;
	/**
	 * Response mime type
	 *
	 * @var string
	 */
	private $responseMime;
	/**
	 * Response code
	 * e.g. 200 for OK
	 *
	 * @var int
	 */
	private $responseCode;
	
	/**
	 * Connection handle
	 * 
	 * @var resource
	 */
	private $connectionHandle;
	
	/**
	 * @return string
	 */
	public function getHost() {
		return $this->host;
	}
	
	/**
	 * @return Enum(GET,POST)
	 */
	public function getMethod() {
		return $this->method;
	}
	
	/**
	 * @return string
	 */
	public function getPath() {
		return $this->path;
	}
	
	/**
	 * @return integer
	 */
	public function getPort() {
		return $this->port;
	}
	
	/**
	 * @return string
	 */
	public function getQuery() {
		return $this->query;
	}
	
	/**
	 * @return string
	 */
	public function getResponseBody() {
		return $this->responseBody;
	}
	
	/**
	 * @return int
	 */
	public function getResponseCode() {
		return $this->responseCode;
	}
	
	/**
	 * @return array
	 */
	public function getResponseHeader() {
		return $this->responseHeader;
	}
	
	/**
	 * @return string
	 */
	public function getResponseMime() {
		return $this->responseMime;
	}
	
	/**
	 * @param string $host
	 */
	public function setHost($host) {
		$this->host = $host;
	}
	
	/**
	 * @param Enum(GET,POST) $method
	 */
	public function setMethod($method) {
		$this->method = $method;
	}
	
	/**
	 * @param string $path
	 */
	public function setPath($path) {
		$this->path = $path;
	}
	
	/**
	 * @param integer $port
	 */
	public function setPort($port) {
		$this->port = $port;
	}
	
	/**
	 * @param string $query
	 */
	public function setQuery($query) {
		$this->query = $query;
	}
	
	/**
	 * @param string $responseBody
	 */
	public function setResponseBody($responseBody) {
		$this->responseBody = $responseBody;
	}
	
	/**
	 * @param int $responseCode
	 */
	public function setResponseCode($responseCode) {
		$this->responseCode = $responseCode;
	}
	
	/**
	 * @param array $responseHeader
	 */
	public function setResponseHeader($responseHeader) {
		$this->responseHeader = $responseHeader;
	}
	
	/**
	 * @param string $responseMime
	 */
	public function setResponseMime($responseMime) {
		$this->responseMime = $responseMime;
	}
	
	/**
	 * @return string
	 */
	public function getProtocol() {
		return $this->protocol;
	}
	
	/**
	 * @param string $protocol
	 */
	public function setProtocol($protocol) {
		$this->protocol = $protocol;
	}
	
	/**
	 * @return resource
	 */
	public function &getConnectionHandle() {
		return $this->connectionHandle;
	}
	
	/**
	 * @param resource $connectionHandle
	 */
	public function setConnectionHandle(&$connectionHandle) {
		$this->connectionHandle = $connectionHandle;
	}

	
	/**
	 * Get URL
	 *
	 * @return string
	 */
	public function getUrl() {
		$url = sprintf('%s://%s:%s%s',
						$this->getProtocol(),
						$this->getHost(),
						$this->getPort(),
						'/'.ltrim($this->getPath(),'/'));
		if ($this->getMethod() == self::M_GET && $this->getQuery() != '')
			$url .= '?'.ltrim($this->getQuery(),'?');
		return $url;
	}

}