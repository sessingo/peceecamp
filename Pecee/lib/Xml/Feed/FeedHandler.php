<?php
namespace Pecee\Xml\Feed;
class FeedHandler {
	private static $instance;
	/**
	 * Connection timeout (MS)
	 *
	 * @var int
	 */
	private $connectTimeout = 2000;
	/**
	 * Timeout (MS)
	 *
	 * @var int
	 */
	private $timeout = 2000;
	/**
	 * HTTP User agent
	 *
	 * @var string
	 */
	private $useragent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)";
	/**
	 * Curl multi handle
	 *
	 * @var resource
	 */
	private $chm = null;
	/**
	 * Feeds
	 *
	 * @var array
	 */ 
	private $feeds = array();
	/**
	 * Add feed
	 *
	 * @param Feed $feed
	 */
	public function addFeed($feed) {
		$this->feeds[] = $feed;
	}
	
	public function getFeeds() {
		return $this->feeds;
	}
	
	public function resolveFeeds() {
		$this->setChm(curl_multi_init());
		for($i = 0; $i < count($this->feeds); $i++) {
			/* @var $feed Feed */
			if (!is_resource($this->feeds[$i]->getConnectionHandle())) {
				$hd = curl_init();
				curl_setopt($hd, CURLOPT_URL, $this->feeds[$i]->getUrl());
				curl_setopt($hd, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($hd, CURLOPT_NOSIGNAL,1);
				curl_setopt($hd, CURLOPT_CONNECTTIMEOUT, $this->getConnectTimeout());
				curl_setopt($hd, CURLOPT_TIMEOUT, $this->getTimeout());
				curl_setopt($hd, CURLOPT_USERAGENT, $this->getUseragent());
				if ($this->feeds[$i]->getMethod() == Feed::M_POST) {
					curl_setopt($hd, CURLOPT_POST, true);
					curl_setopt($hd,CURLOPT_POSTFIELDS,$this->feeds[$i]->getQuery());
				}
				$this->feeds[$i]->setConnectionHandle($hd);
				
			}
			curl_multi_add_handle($this->getChm(),$this->feeds[$i]->getConnectionHandle());
		}
		$running = null;
		do {
			curl_multi_exec($this->getChm(),$running);
			curl_multi_select($this->getChm(),0.01);
		} while($running > 0);

		for($i = 0; $i < count($this->feeds); $i++) {
			try {
				$BenchmarkName = $this->feeds[$i]->getHost();
				$this->feeds[$i]->setResponseHeader(curl_getinfo ($this->feeds[$i]->getConnectionHandle()));
				$this->feeds[$i]->setResponseCode(curl_getinfo ($this->feeds[$i]->getConnectionHandle(), CURLINFO_HTTP_CODE));
				$this->feeds[$i]->setResponseBody(curl_multi_getcontent($this->feeds[$i]->getConnectionHandle()));
				$this->feeds[$i]->setResponseMime(curl_getinfo ($this->feeds[$i]->getConnectionHandle(), CURLINFO_CONTENT_TYPE));			
				curl_multi_remove_handle($this->getChm(),$this->feeds[$i]->getConnectionHandle());
			} catch(\Exception $e) {
			}
		}
		curl_multi_close($this->getChm());
		$this->feeds = array();
	}
	
	/**
	 * @return resource
	 */
	protected function getChm() {
		return $this->chm;
	}
	
	/**
	 * @return int
	 */
	public function getConnectTimeout() {
		return $this->connectTimeout;
	}
	
	/**
	 * @return int
	 */
	public function getTimeout() {
		return $this->timeout;
	}
	
	/**
	 * @return string
	 */
	public function getUseragent() {
		return $this->useragent;
	}
	
	/**
	 * @param resource $chm
	 */
	protected function setChm($chm) {
		$this->chm = $chm;
	}
	
	/**
	 * @param int $connectTimeout
	 */
	public function setConnectTimeout($connectTimeout) {
		$this->connectTimeout = $connectTimeout;
	}
	
	/**
	 * @param int $timeout
	 */
	public function setTimeout($timeout) {
		$this->timeout = $timeout;
	}
	
	/**
	 * @param string $useragent
	 */
	public function setUseragent($useragent) {
		$this->useragent = $useragent;
	}

	
	/**
	 * Get feedhandler
	 *
	 * @return FeedHandler
	 */
	public static function getInstance() {
		if (!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
}