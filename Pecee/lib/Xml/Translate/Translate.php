<?php
namespace Pecee\Xml\Translate;
class Translate {
	protected static $instance;

	/**
	 * Get instance
	 * @return \Pecee\Xml\Translate\Translate
	 */
	public static function Instance() {
		if(is_null(self::$instance)) {
			self::$instance=new self();
		}
		return self::$instance;
	}
	
	protected $xml;
	public function __construct() {
		$this->setLanguageXml();
	}
	
	public function lookup($key) {
		if(!\Pecee\Registry::GetInstance()->get(\Pecee\Language::SETTINGS_XML_DIRECTORY, '../lang')) {
			throw new \Pecee\Xml\Translate\TranslateException('XML language directory must be specified: \Pecee\Language::SETTINGS_XML_DIRECTORY');
		}
		$xml=new \SimpleXmlElement($this->xml);		
		$node=NULL;
		if(strpos($key, '/') > -1) {
			$children=explode('/', $key);
			foreach($children as $i=>$child) {
				if($i==0) {
					$node=(isset($xml->$child) ? $xml->$child : NULL);
				} else {
					$node=(isset($node->$child) ? $node->$child : NULL);
				}
			}
		} else {
			$node=isset($xml->$key) ? $xml->$key : NULL;
		}
		if(!is_null($node)) {
			return $node;
		}
		throw new \Pecee\Xml\Translate\TranslateException(sprintf('Key "%s" does not exist for locale "%s"', $key, \Pecee\Locale::Instance()->getLocale()));
	}
	
	protected function setLanguageXml() {
		$path = sprintf('%s/%s.xml', \Pecee\Registry::GetInstance()->get(\Pecee\Language::SETTINGS_XML_DIRECTORY, '../lang'),
									\Pecee\Locale::Instance()->getLocale());
		if(!file_exists($path)) {
			throw new \Pecee\Xml\Translate\TranslateException(sprintf('Language file %s not found for locale %s', $path, \Pecee\Locale::Instance()->getLocale()));
		}
		$this->xml=file_get_contents($path);
	}
}