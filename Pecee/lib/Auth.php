<?php
namespace Pecee;
class Auth {
	private static $instance;
	protected $authLevel;
	protected $adminIPList;

	/**
	 * @return \Pecee\Auth
	 */
	public static function GetInstance() {
		if(is_null(self::$instance)) {
			self::$instance=new self();
		}
		return self::$instance;
	}

	public function __construct() {
		$this->adminIPList = array();
		$this->authLevel=NULL;
	}

	public function hasAdminIP($ip=NULL) {
		$ip = (is_null($ip)) ? \Pecee\Server::GetRemoteAddr() : $ip;
		return (in_array($ip, $this->adminIPList));
	}

	public function hasAccess() {
		return (is_null($this->authLevel) || \Pecee\Model\User\ModelUser::IsLoggedIn() && \Pecee\Model\User\ModelUser::Current()->getAdminLevel() >= $this->authLevel);
	}

	public function check() {
		if(!$this->hasAccess()) {
			throw new \Pecee\Auth\AuthException('You dont have the required permissions to view this page', 550);
		}
	}

	/**
	 * @return int $authLevel
	 */
	public function getAuthLevel(){
		return $this->authLevel;
	}

	/**
	 * @return array $adminIP
	 */
	public function getAdminIP(){
		return $this->adminIPList;
	}

	/**
	 * @param int $authLevel
	 */
	public function setAuthLevel($authLevel){
		$this->authLevel = $authLevel;
	}

	/**
	 * @param array $adminIP
	 */
	public function setAdminIP($adminIP){
		$this->adminIPList[] = $adminIP;
	}
}