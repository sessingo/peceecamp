<?php
namespace Pecee;
class Integer {
	
	public static $map = array('a','b','c','d','e','f','g','h','i','j','k','l','m',
			'n','o','p','q','r','s','t','u','v','w','x','y','z',
			'A','B','C','D','E','F','G','H','I','J','K','L','M',
			'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
			'#','_','+','-','=', '%',
			'0', '1','2','3','4','5','6','7','8','9');
	/**
	 * Encode integer to string from integer
	 * @param string $int
	 */
	public static function Encode($int) {
		$map=self::$map;
		krsort($map);
		$original=$int;
		foreach($map as $key=>$m) {			
			$int=str_replace($key, $m, $int);
			if(!str_replace($key, '', $original)) {
				break;
			}
		}
		return $int;
	}
	
	/**
	 * Decode integer from encoded string
	 * @param string $str
	 */
	public static function Decode($str) {
		$out='';
		for($i=0;$i<strlen($str);$i++) {
			$out.=array_search($str[$i], self::$map);
		}
		return $out;
	}
	
	public static function GetIntFromQueryString( $name, $defaultValue = NULL ){
		$value = self::GetValueFromQueryString($name, NULL);
		if($value !== NULL && self::is_int($value)){
			return $value;
		}
		return $defaultValue;
	}
	
	/**
	 * Check if a given value is a counting type or 
	 * if the value of the string has numbers in it.
	 * @return bool
	 */
	public static function is_int($val) {
		return (is_int(filter_var($val, FILTER_VALIDATE_INT)) || is_int($val));
	}
	
	public static function IsNummeric($val) {
		return (self::is_int($val) || is_numeric($val));
	}
}