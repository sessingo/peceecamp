<?php
namespace Pecee\Widget;
use Pecee\Str;

abstract class Widget  {
	const MSG_ERROR='error';
	protected $jsWrapRoute;
	protected $cssWrapRoute;
	protected $_site;
	protected $_messages;
	protected $_template;
	protected $_contentTemplate;
	protected $_contentHtml;
	protected $data;
	protected $request;
	protected $files;
	protected $form;

	public function __construct() {
		\Pecee\Debug::Instance()->add('START ' . get_class($this));
		\Pecee\Auth::GetInstance()->check();
		$this->_site = \Pecee\UI\Site::GetInstance();
		$this->_messages = \Pecee\Session\SessionMessage::Instance();
		$this->data = new \Pecee\UI\ResponseData\ResponseDataPost();
		$this->request = new \Pecee\UI\ResponseData\ResponseDataGet();
		$this->files = new \Pecee\UI\ResponseData\ResponseDataFile();
		$this->form = new \Pecee\UI\Form\Form($this->data);
		$this->setTemplate('Default.php');
		$this->setContentTemplate(\Pecee\Router::GetTemplatePath(get_class($this)));
		$this->jsWrapRoute = \Pecee\Router::GetRoute('js','wrap');
		$this->cssWrapRoute = \Pecee\Router::GetRoute('css','wrap');
	}

	public function isAjaxRequest() {
		return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
	}

	public function showMessages($type, $formName = NULL) {
		if($this->hasMessages($type)) {
			$output = array();
			$output[] = sprintf('<ul class="msg %s">', $type);
			/* @var $error \Pecee\UI\Form\FormMessage */
			foreach($this->getMessages($type) as $error) {
				 $output[] = sprintf('<li>%s</li>', $error->getMessage());
			}
			$output[] = '</ul>';
			return join($output, '');
		}
		return '';
	}

	/**
	 * Get form message
	 * @param string $type
	 * @return \Pecee\UI\Form\FormMessage|NULL
	 */
	public function getMessage($type){
		$errors=$this->getMessages($type);
		if(count($errors) > 0) {
			return $errors[0];
		}
		return NULL;
	}

	/**
	 * Get form messages
	 * @param string $type
	 * @return \Pecee\UI\Form\FormMessage|NULL
	 */
	public function getMessages($type) {
		return $this->_messages->get($type);
	}

	public function hasMessages($type) {
		return $this->_messages->hasMessages($type);
	}

	/**
	 * Set message
	 * @param $string $message
	 * @param string $type
	 * @param string|null $input
	 * @param string|null $place Key to use if you want the message to be displayed an unique place
	 */
	protected function setMessage($message, $type, $form=NULL, $placement=NULL, $index = NULL) {
		$msg=new \Pecee\UI\Form\FormMessage();
		$msg->setForm($form);
		$msg->setMessage($message);
		$msg->setPlacement($placement);
		$msg->setIndex($index);
		$this->_messages->set($msg, $type);
	}

	public function showErrors($formName=NULL) {
		return $this->showMessages(self::MSG_ERROR, $formName);
	}

	public function hasErrors() {
		return $this->hasMessages(self::MSG_ERROR);
	}

	/**
	 * Set error
	 * @param string $message
	 */
	protected function setError($message) {
		return $this->setMessage($message, self::MSG_ERROR);
	}

	/**
	 * Get error messages
	 * @return \Pecee\UI\Form\FormMessage|NULL
	 */
	public function getErrors() {
		return $this->getMessages(self::MSG_ERROR);
	}

	public function getFormName($post=TRUE) {
		if($this->isPostBack() && $post) {
			return \Pecee\UI\ResponseData\ResponseDataPost::GetFormName();
		} else {
			return \Pecee\UI\ResponseData\ResponseDataGet::GetFormName();
		}
		return NULL;
	}

	/**
	 * @return string
	 */
	public function printHeader($includeCss=TRUE, $includeJs=TRUE) {

		$enc=new \Pecee\UI\Html\HtmlMeta('text/html; charset='.$this->_site->getCharset());
		$enc->addAttribute('http-equiv', 'Content-Type');
		$o=array($enc);

		if($this->_site->getTitle())  {
			$o[]='<title>' . $this->_site->getTitle() . '</title>';
		}

		if($this->_site->getDescription()) {
			$this->_site->addMeta('description', $this->_site->getDescription());
		}
		if(count($this->_site->getKeywords()) > 0) {
			$this->_site->addMeta('keywords', join(', ', $this->_site->getKeywords()));
		}

		$get=NULL;
		if(\Pecee\Auth::GetInstance()->hasAdminIP()) {
			$get=array();
			if($this->_site->getDebug() || $this->hasParam('clearcache')) {
				$get['clearcache']='true';
			}
			if($this->_site->getDebug() || $this->hasParam('debug')) {
				$get['debug']='true';
			}
		}

		if($includeCss) {
			$o[] = $this->printCss();
		}
		if($includeJs) {
			$o[] = $this->printJs();
		}
		if(count($this->_site->getHeader()) > 0) {
			$header = $this->_site->getHeader();
			$o[]=join(chr(10), $header);
		}
		return join('', $o);
	}

	protected function printCss() {
		$o = array();
		if($this->_site->getCssFilesWrapped()) {
			$get=NULL;
			if(\Pecee\Auth::GetInstance()->hasAdminIP()) {
				$get=array();
				if($this->_site->getDebug() || $this->hasParam('clearcache')) {
					$get['clearcache']='true';
				}
				if($this->_site->getDebug() || $this->hasParam('debug')) {
					$get['debug']='true';
				}
			}

			$p = $this->cssWrapRoute . join($this->_site->getCssFilesWrapped(),',') . \Pecee\Url::GetQueryStringSeperator($this->cssWrapRoute) . \Pecee\Url::QueryStringToString($get);
			$o[] = new \Pecee\UI\Html\HtmlLink($p);
		}

		$css = $this->_site->getCss();
		if(count($css) > 0) {
			foreach($css as $c) {
				$o[] = $c;
			}
		}
		return join('',$o);
	}

	protected function printJs() {
		$o = array();
		if($this->_site->getJsFilesWrapped()) {
			$get=NULL;
			if(\Pecee\Auth::GetInstance()->hasAdminIP()) {
				$get=array();
				if($this->_site->getDebug() || $this->hasParam('clearcache')) {
					$get['clearcache']='true';
				}
				if($this->_site->getDebug() || $this->hasParam('debug')) {
					$get['debug']='true';
				}
			}

			$p = $this->jsWrapRoute . join($this->_site->getJsFilesWrapped(),',') . \Pecee\Url::GetQueryStringSeperator($this->jsWrapRoute) . \Pecee\Url::QueryStringToString($get);
			$o[] = new \Pecee\UI\Html\HtmlScript($p);
		}

		$js = $this->_site->getJs();
		if(count($js) > 0) {
			foreach($js as $j) {
				$o[] = $j;
			}
		}
		return join('', $o);
	}

	protected function appendSiteTitle($title, $seperator='-') {
		$seperator=is_null($seperator) ? '': sprintf(' %s ', $seperator);
		$this->_site->setTitle(($this->_site->getTitle() . $seperator . $title));
	}

	protected function prependSiteTitle($title, $seperator='') {
		$this->_site->setTitle(($title . $seperator .$this->_site->getTitle()));
	}

	protected function getTemplate() {
		return $this->_template;
	}

	protected function setTemplate($path,$relative=TRUE) {
		$this->_template = (($relative && !empty($path)) ? 'Template' . DIRECTORY_SEPARATOR : '') . $path;
	}

	protected function setContentTemplate($template) {
		$this->_contentTemplate = $template;
	}

	protected function getContentTemplate() {
		return $this->_contentTemplate;
	}

	protected function setContentHtml($html) {
		$this->_contentHtml = $html;
	}

	protected function getContentHtml() {
		return $this->_contentHtml;
	}

	/**
	 * Creates form element
	 * @return \Pecee\UI\Form\Form
	 */
	public function form() {
		return $this->form;
	}

	/**
	 * Adds input validation
	 *
	 * @param string $name
	 * @param string $index
	 * @param \Pecee\UI\Form\Validate\ValidateInput|array $type
	 */
	protected function addInputValidation($name, $index, $type) {
		if(\Pecee\Util::GetTypeOf($type) == 'Pecee\\UI\\Form\\Validate\\ValidateFile') {
			$this->files->addInputValidation($name, $index, $type);
		} else {
			$this->data->addInputValidation($name, $index, $type);
		}
	}

	/**
	 * Include snippet from the content/snippet directory
	 * by filling the path to the desired snippet.
	 *
	 * @param string $file
	 */
	public function snippet($file) {
		require('Template'.DIRECTORY_SEPARATOR.'Snippet'.DIRECTORY_SEPARATOR.$file);
	}

	/**
	 * Include widget on page.
	 * @param \Pecee\Widget\Widget $widget
	 */
	public function widget(\Pecee\Widget\Widget $widget) {
		if($widget->getTemplate() == 'Template\Default.php') {
			$widget->setTemplate(NULL);
		}
		echo $widget;
	}

	/**
	 * Get request
	 * @param string $formName
	 * @param string $elementName
	 * @return \Pecee\UI\ResponseData\ResponseDataGet
	 */
	public function request($elementName, $formName = NULL) {
		$element = $this->request->__get( (($formName) ? $formName . '_' : null) . $elementName);
		return ($element) ? $element : NULL;
	}

	/**
	 * Translates message
	 * @param string $messageID
	 * @param array $args
	 * @return Language
	 */
	public function _($key, $args = NULL) {
		if (!is_array($args)) {
			$args = func_get_args();
			$args = array_slice($args, 1);
		}
		return \Pecee\Language::Instance()->_($key, $args);
	}

	public function __toString() {
		try {
			return $this->render();
		}catch(\Exception $e) {
			$this->setError($e->getMessage());
		}
	}

	/**
	 * Checks if there has been a form post-back
	 * @return bool
	 */
	public function isPostBack() {
		return \Pecee\UI\ResponseData\ResponseDataPost::IsPostBack();
	}

	protected function getKey($name) {
		$key=$this->getFormName(FALSE);
		if(isset($_GET[$key.'_'.$name])) {
			return $key.'_'.$name;
		}
		if(isset($_GET[$name])) {
			return $name;
		}
		return NULL;
	}

	/**
	 * Check if a certain param has been set
	 * @param string $name
	 * @return bool
	 */
	public function hasParam($name) {
		return isset($_GET[$this->getKey($name)]);
	}

	/**
	 * Get param
	 * @param string $name
	 * @return string|null
	 */
	public function getParam($name, $default=NULL) {
		return ($this->hasParam($name) ? Str::GetFirstOrValue($_GET[$this->getKey($name)],$default) : $default);
	}

	/**
	 * Set user admin level.
	 * @param int $adminLevel
	 */
	protected function setAuthLevel($authLevel) {
		\Pecee\Auth::GetInstance()->setAuthLevel($authLevel);
	}

	public function render()  {
		$this->renderContent();
		$this->renderTemplate();
		$this->_messages->clear();
		$output = Str::GetFirstOrValue($this->_contentHtml, '');
		\Pecee\Debug::Instance()->add('END ' . get_class($this));
		// Output debug info
		if(Str::GetFirstOrValue($this->_template, FALSE) && \Pecee\Auth::GetInstance()->hasAdminIP() && strtolower($this->getParam('debug')) == 'true') {
			$output .= \Pecee\Debug::Instance()->__toString();
		}
		return $output;
	}

	protected function renderContent() {
		if(is_null($this->_contentHtml) && !is_null($this->_contentTemplate)) {
			ob_start();
			include $this->_contentTemplate;
			$this->_contentHtml = ob_get_contents();
			ob_end_clean();
		}
	}

	protected function renderTemplate() {
		if(Str::GetFirstOrValue($this->_template, FALSE)) {
			ob_start();
			include $this->_template;
			$this->_contentHtml = ob_get_contents();
			ob_end_clean();
		}
	}

	protected function setJsWrapRoute($route) {
		$this->jsWrapRoute = $route;
	}

	protected function setCssWrapRoute($route) {
		$this->cssWrapRoute = $route;
	}

	/**
	 * Get site
	 * @return \Pecee\UI\Site
	 */
	public function getSite() {
		return $this->_site;
	}
}