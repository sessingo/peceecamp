<?php
namespace Pecee\Widget;
use Pecee\Str;

abstract class WidgetTaglib extends Widget {
	public function __construct() {
		parent::__construct();
	}
	
	protected function render()  {
		$this->renderContent();
		$this->renderTemplate();
		$this->_messages->clear();
		return Str::GetFirstOrValue($this->_contentHtml, '');
	}
	
	public function renderContent() {
		parent::renderContent();
		$phtml=new \Pecee\UI\Phtml\Phtml();
		$this->_contentHtml = $phtml->read($this->_contentHtml)->toPHP();	
	}
}