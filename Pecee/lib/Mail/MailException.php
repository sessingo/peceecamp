<?php
namespace Pecee\Mail;
class MailException extends \Exception{
	public function errorMessage(){
		$errorMsg = '<strong>' . $this->getMessage() . "</strong><br />\n";
		return $errorMsg;
	}
}