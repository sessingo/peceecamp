<?php
namespace Pecee;
class Registry {
	protected static $instance;
	protected static $registry;
	/**
	 * Get instance
	 * @return \Pecee\Registry
	 */
	public static function GetInstance() {
		if(!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	/**
	 * Get key from registry
	 * @param string $Key
	 * @return string|null
	 */
	public function get($Key, $default=NULL) {
		return (isset(self::$registry[$Key]) ? self::$registry[$Key] : $default);
	}
	/**
	 * Set registry key
	 * @param string $Key
	 * @param string $Value
	 */
	public function set($Key, $Value) {
		self::$registry[$Key] = $Value;
	}
}