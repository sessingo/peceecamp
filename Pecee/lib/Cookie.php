<?php
namespace Pecee;
class Cookie {
	public static function Create($id, $value, $cookieTime = NULL, $domain = NULL, $secure = NULL) {
		$cookieTime=(is_null($cookieTime)) ? time()+60*60*24*6004 : $cookieTime;
		if(is_null($domain)) {
			$sub = explode('.',$_SERVER['HTTP_HOST']);
			$domain = (count($sub) > 2) ? $_SERVER['HTTP_HOST'] : '.' . $_SERVER['HTTP_HOST'];
		}
		return (@setCookie($id, $value, (($cookieTime > 0) ? $cookieTime : null), '/', $domain, $secure));
	}
	public static function Get($id,$defaultValue=NULL) {
		return isset($_COOKIE[$id]) ? $_COOKIE[$id] : $defaultValue;
	}
	public static function Delete($id) {
		self::Create($id, '', time()-999999999999);
	}
	public static function Exists($id) {
		return (isset($_COOKIE[$id]));
	}
}