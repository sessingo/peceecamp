<?php
namespace Pecee;
class Debug {
	private static $instance;
	protected $enabled;
	protected $lastTime;
	protected $stack;
	protected $startTime;

	/**
	 * Get instance of Debug class
	 * @return Debug
	 */
	public static function Instance() {
		if(self::$instance === NULL) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	public function __construct(){
		$this->enabled = \Pecee\UI\Site::GetInstance()->getDebug();
		$this->startTime = microtime(TRUE);
		$this->add('Debugger initialized.');
	}

	public function __destruct() {
		$this->add('Debugger destructed.');
	}

	protected function getTime() {
		return number_format(microtime(TRUE)-$this->startTime, 10);
	}

	protected function addObject($text) {
		$trace=debug_backtrace();
		$this->stack[]=array('text' => $text, 'time' => $this->getTime(), 'file' => (isset($trace[1]['file']) ? $trace[1]['file'] : ''), 'line' => (isset($trace[1]['line']) ? $trace[1]['line'] : ''), 'method' => (isset($trace[2]['function']) ? $trace[2]['function'] : ''));
		$this->lastTime = microtime(TRUE);
	}

	public function add($text) {
		if($this->getEnabled()) {
			$this->addObject($text);
		}
	}

	public function getEnabled() {
		return $this->enabled;
	}

	public function setEnabled($bool) {
		$this->enabled = $bool;
	}

	public function __toString() {
		if(count($this->stack) > 0) {
			$output[] = '<h1 style="font-family:Arial;font-size:18px;margin:10px 0px;border-bottom:1px solid #CCC;padding-bottom:5px;">Debug information</h1>
			<table cellspacing="0" cellpadding="0" style="width:100%;font-size:12px;font-family:Arial;">
			<thead style="background-color:#EEE;">
				<tr>
					<th align="left" style="padding:5px;">Execution time</th>
					<th align="left" style="padding:5px;">Message</th>
					<th align="left" style="padding:5px;">Method</th>
					<th align="left" style="padding:5px;">File</th>
					<th align="center" style="padding:5px;">Line</th>
				</tr>
			</thead>
			<tbody style="background-color:#FFF;">';
			foreach($this->stack as $log) {
				$output[] = sprintf('<tr style="border-bottom:1px solid #CCC;">
				<td style="padding:5px;">%s</td>
				<td style="padding:5px;">%s</td>
				<td style="padding:5px;">%s</td>
				<td style="padding:5px;">%s</td>
				<td style="padding:5px;" align="center">%s</td>
			</tr>', $log['time'], $log['text'], $log['method'], $log['file'], $log['line']);
			}
			$output[] = '</tbody></table>';
			return join('', $output);
		}
		return '';
	}

}