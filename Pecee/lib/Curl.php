<?php
namespace Pecee;
class Curl{
	protected $callback = false;
	protected $secure = false;
	protected $conn = false;
	protected $cookiefile = false;
	protected $header = false;
	protected $cookie = false;
	protected $follow = true;
	protected $options;
	public function __construct($u = false){
		$this->conn = curl_init();
		$this->options=array();
	}
	public function setCallback($func_name){
		$this->callback = $func_name;
	}
	public function close(){
		curl_close($this->conn);
		if(is_file($this->cookiefile)) {
			unlink($this->cookiefile);
		}
	}
	public function addOption($name, $value) {
		$this->options[$name] = $value;
	}
	public function doRequest($method, $url, $vars){
		$ch = $this->conn;
		
		$user_agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
		curl_setopt($ch, CURLOPT_URL, $url);
		if($this->header) {
			curl_setopt($ch, CURLOPT_HEADER, 1);
		} else {
			curl_setopt($ch, CURLOPT_HEADER, 0);
		}
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		
		if($this->secure) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		}
		
		if($this->cookie) {
			curl_setopt($ch, CURLOPT_COOKIE, $this->cookie);
		}
		
		if($this->follow) {
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		} else {
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
		}
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiefile);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiefile);
		
		if($method == 'POST') {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect: ')); // lighttpd fix
		}
		
		foreach($this->options as $option=>$value) {
			curl_setopt($ch, $option, value);
		}
		
		$data = curl_exec($ch);
		
		if($data) {
			if($this->callback) {
				$callback = $this->callback;
				$this->callback = false;
				return call_user_func($callback, $data);
			} else {
				return $data;
			}
		} else {
			return FALSE;
		}
	}
	public function get($url){
		return $this->doRequest('GET', $url, 'NULL');
	}
	public function getError(){
		return curl_error($this->conn);
	}
	public function post($url, $params = false){
		$post_data = '';
		
		if(is_array($params)) {
			
			foreach($params as $var=>$val) {
				if(! empty($post_data))
					$post_data .= '&';
				$post_data .= $var . '=' . urlencode($val);
			}
		} else {
			$post_data = $params;
		}
		
		return $this->doRequest('POST', $url, $post_data);
	}
	public static function Download($url){
		$conn = new self();
		return $conn->get($url);
	}
}