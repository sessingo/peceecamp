<?php
namespace Pecee;
class Language {
	const SETTINGS_CACHE_ENABLE = 'LANGUAGE_CACHE_ENABLE';
	const SETTINGS_XML_DIRECTORY = 'LANGUAGE_XML_DIRECTORY';
	const SETTINGS_AUTO_TRANSLATE = 'AUTO_TRANSLATE';
	
	const TYPE_DATABASE='LANG_DB';
	const TYPE_XML='LANG_XML';
	
	protected static $instance;
	public static $TYPES=array(self::TYPE_DATABASE,self::TYPE_XML);
	
	protected $type;
	
	public static function Instance() {
		if(is_null(self::$instance)) {
			self::$instance=new self();
		}
		return self::$instance;
	}
	
	public function __construct() {
		$this->type = self::TYPE_DATABASE;
	}
	
	/**
	 * Translate message.
	 * @param string $text
	 * @param array $args
	 * @return string
	 */	
	public function _($key, $args = null) {
		if (!is_array($args)) {
			$args = func_get_args();
			$args = array_slice($args, 1);
		}
		return vsprintf($this->lookup($key), $args);
	}
	
	protected function lookup($key) {
		switch($this->type) {
			case self::TYPE_DATABASE:
				return \Pecee\Model\Translate\ModelTranslate::GetInstance()->lookup($key);
			case self::TYPE_XML:
				return \Pecee\Xml\Translate\Translate::Instance()->lookup($key);
		}
	}
	
	public function setType($languageType) {
		if(!in_array($languageType,self::$TYPES)) {
			throw new \InvalidArgumentException('Invalid language type defined');
		}
		$this->type=$languageType;
	}
	
	public function getType() {
		return $this->type;
	}
}