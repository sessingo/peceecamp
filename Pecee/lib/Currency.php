<?php
namespace Pecee;
class Currency {
	public static $CURRENCIES=array('Afghanistan' => 'AFN',
									'Albania' => 'ALL',
									'Algeria' => 'DZD',
									'Andorra' => 'EUR',
									'Angola' => 'AOA',
									'Anguilla' => 'XCD',
									'Antigua and Barbuda' => 'XCD',
									'Argentina' => 'ARS',
									'Armenia' => 'AMD',
									'Aruba' => 'AWG',
									'Australia' => 'AUD',
									'Austria' => 'EUR',
									'Azerbaijan' => 'AZN',
									'Bahamas' => 'BSD',
									'Bahrain' => 'BHD',
									'Bangladesh' => 'BDT',
									'Barbados' => 'BBD',
									'Belarus' => 'BYR',
									'Belgium' => 'EUR',
									'Belize' => 'BZD',
									'Benin' => 'XOF',
									'Bhutan' => 'BTN',
									'Bolivia' => 'BOB',
									'Bosnia-Herzegovina' => 'BAM',
									'Botswana' => 'BWP',
									'Brazil' => 'BRL',
									'Brunei' => 'BND',
									'Bulgaria' => 'BGN',
									'Burkina Faso' => 'XOF',
									'Burundi' => 'BIF',
									'Cambodia' => 'KHR',
									'Cameroon' => 'XAF',
									'Canada' => 'CAD',
									'Cape Verde' => 'CVE',
									'Cayman Islands' => 'KYD',
									'Central African Republic' => 'XAF',
									'Chad' => 'XAF',
									'Chile' => 'CLP',
									'China' => 'CNY',
									'Colombia' => 'COP',
									'Comoros' => 'KMF',
									'Congo' => 'XAF',
									'Congo, Democratic Republic' => 'CDF',
									'Costa Rica' => 'CRC',
									'Côte d\'Ivoire' => 'XOF',
									'Croatia' => 'HRK',
									'Cuba' => 'CUC',
									'Cyprus' => 'EUR',
									'Czech Republic' => 'CZK',
									'Denmark' => 'DKK',
									'Djibouti' => 'DJF',
									'Dominica' => 'XCD',
									'Dominican Republic' => 'DOP',
									'East Timor' => 'USD',
									'Ecuador' => 'USD',
									'Egypt' => 'EGP',
									'El Salvador' => 'USD',
									'Equatorial Guinea' => 'GQE',
									'Eritrea' => 'ERN',
									'Estonia' => 'EEK',
									'Ethiopia' => 'ETB',
									'Falkland Islands' => 'FKP',
									'Fiji' => 'FJD',
									'Finland' => 'EUR',
									'France' => 'EUR',
									'French Polynesia' => 'XPF',
									'Gabon' => 'XAF',
									'Gambia' => 'GMD',
									'Georgia' => 'GEL',
									'Germany' => 'EUR',
									'Ghana' => 'GHS',
									'Gibraltar' => 'GIP',
									'Greece' => 'EUR',
									'Grenada' => 'XCD',
									'Guatemala' => 'GTQ',
									'Guinea' => 'GNF',
									'Guinea-Bissau' => 'XOF',
									'Guyana' => 'GYD',
									'Haiti' => 'HTG',
									'Honduras' => 'HNL',
									'Hong Kong' => 'HKD',
									'Hungary' => 'HUF',
									'Iceland' => 'ISK',
									'India' => 'INR',
									'Indonesia' => 'IDR',
									'International Monetary Fund' => 'XDR',
									'Iran' => 'IRR',
									'Iraq' => 'IQD',
									'Ireland' => 'EUR',
									'Israel' => 'ILS',
									'Italy' => 'EUR',
									'Jamaica' => 'JMD',
									'Japan' => 'JPY',
									'Jordan' => 'JOD',
									'Kazakhstan' => 'KZT',
									'Kenya' => 'KES',
									'Kiribati' => 'AUD',
									'Korea North' => 'KPW',
									'Korea South' => 'KRW',
									'Kuwait' => 'KWD',
									'Kyrgyzstan' => 'KGS',
									'Laos' => 'LAK',
									'Latvia' => 'LVL',
									'Lebanon' => 'LBP',
									'Lesotho' => 'LSL',
									'Liberia' => 'LRD',
									'Libya' => 'LYD',
									'Liechtenstein' => 'CHF',
									'Lithuania' => 'LTL',
									'Luxembourg' => 'EUR',
									'Macau' => 'MOP',
									'Macedonia (Former Yug. Rep.)' => 'MKD',
									'Madagascar' => 'MGA',
									'Malawi' => 'MWK',
									'Malaysia' => 'MYR',
									'Maldives' => 'MVR',
									'Mali' => 'XOF',
									'Malta' => 'EUR',
									'Mauritania' => 'MRO',
									'Mauritius' => 'MUR',
									'Mexico' => 'MXN',
									'Micronesia' => 'USD',
									'Moldova' => 'MDL',
									'Monaco' => 'EUR',
									'Mongolia' => 'MNT',
									'Montenegro' => 'EUR',
									'Montserrat' => 'XCD',
									'Morocco' => 'MAD',
									'Mozambique' => 'MZM',
									'Myanmar' => 'MMK',
									'Namibia' => 'NAD',
									'Nauru' => 'AUD',
									'Nepal' => 'NPR',
									'Netherlands Antilles' => 'ANG',
									'Netherlands' => 'EUR',
									'New Caledonia' => 'XPF',
									'New Zealand' => 'NZD',
									'Nicaragua' => 'NIO',
									'Niger' => 'XOF',
									'Nigeria' => 'NGN',
									'Norway' => 'NOK',
									'Oman' => 'OMR',
									'Pakistan' => 'PKR',
									'Palau' => 'USD',
									'Panama' => 'PAB',
									'Panama Canal Zone' => 'USD',
									'Papua New Guinea' => 'PGK',
									'Paraguay' => 'PYG',
									'Peru' => 'PEN',
									'Philippines' => 'PHP',
									'Poland' => 'PLN',
									'Portugal' => 'EUR',
									'Puerto Rico' => 'USD',
									'Qatar' => 'QAR',
									'Romania' => 'RON',
									'Russia' => 'RUB',
									'Rwanda' => 'RWF',
									'Saint Helena' => 'SHP',
									'Saint Kitts and Nevis' => 'XCD',
									'Saint Lucia' => 'XCD',
									'Saint Vincent and the Grenadines' => 'XCD',
									'Samoa (Western)' => 'WST',
									'San Marino' => 'EUR',
									'Sao Tome and Principe' => 'STD',
									'Saudi Arabia' => 'SAR',
									'Senegal' => 'XOF',
									'Serbia' => 'RSD',
									'Seychelles' => 'SCR',
									'Sierra Leone' => 'SLL',
									'Singapore' => 'SGD',
									'Slovakia' => 'SKK',
									'Slovenia' => 'EUR',
									'Solomon Islands' => 'SBD',
									'Somalia' => 'SOS',
									'South Africa' => 'ZAR',
									'South Sudan' => 'SDG',
									'Spain' => 'EUR',
									'Sri Lanka' => 'LKR',
									'Sudan' => 'SDG',
									'Suriname' => 'SRD',
									'Swaziland' => 'SZL',
									'Sweden' => 'SEK',
									'Switzerland' => 'CHF',
									'Syria' => 'SYP',
									'Taiwan' => 'TWD',
									'Tajikistan' => 'TJS',
									'Tanzania' => 'TZS',
									'Thailand' => 'THB',
									'Togo' => 'XOF',
									'Tonga' => 'TOP',
									'Trinidad and Tobago' => 'TTD',
									'Tunisia' => 'TND',
									'Turkey' => 'TRY',
									'Turkmenistan' => 'TMM',
									'Tuvalu' => 'AUD',
									'Uganda' => 'UGX',
									'Ukraine' => 'UAH',
									'United Arab Emirates' => 'AED',
									'United Kingdom' => 'GBP',
									'United States of America' => 'USD',
									'Uruguay' => 'UYU',
									'Uzbekistan' => 'UZS',
									'Vanuatu' => 'VUV',
									'Vatican' => 'EUR',
									'Venezuela' => 'VEB',
									'Vietnam' => 'VND',
									'Wallis and Futuna Islands' => 'XPF',
									'Yemen' => 'YER',
									'Zambia' => 'ZMK',
									'Zimbabwe' => 'ZWD');
	
	public static function GetCountries() {
		return array_keys(self::$CURRENCIES);
	}
	
	public static function GetCurrencyByCountry($country) {
		return (isset(self::$CURRENCIES[$country])) ? self::$CURRENCIES[$country] : NULL;
	}
	
	public static function FormatMoney($format, $number) {
		$regex  = '/%((?:[\^!\-]|\+|\(|\=.)*)([0-9]+)?(?:#([0-9]+))?(?:\.([0-9]+))?([in%])/';
		if(setlocale(LC_MONETARY, 0)=='C') {
			setlocale(LC_MONETARY, '');
		}
		$locale = localeconv();
		preg_match_all($regex, $format, $matches, PREG_SET_ORDER);
		foreach ($matches as $fmatch) {
			$value = floatval($number);
			$flags = array(
					'fillchar'  => preg_match('/\=(.)/', $fmatch[1], $match) ?
					$match[1] : ' ',
					'nogroup'   => preg_match('/\^/', $fmatch[1]) > 0,
					'usesignal' => preg_match('/\+|\(/', $fmatch[1], $match) ?
					$match[0] : '+',
					'nosimbol'  => preg_match('/\!/', $fmatch[1]) > 0,
					'isleft'    => preg_match('/\-/', $fmatch[1]) > 0
			);
			$width      = trim($fmatch[2]) ? (int)$fmatch[2] : 0;
			$left       = trim($fmatch[3]) ? (int)$fmatch[3] : 0;
			$right      = trim($fmatch[4]) ? (int)$fmatch[4] : $locale['int_frac_digits'];
			$conversion = $fmatch[5];
			$positive = true;
			
			if ($value < 0) {
				$positive = false;
				$value  *= -1;
			}
			
			$letter = $positive ? 'p' : 'n';
			$prefix = $suffix = $cprefix = $csuffix = $signal = '';
			$signal = $positive ? $locale['positive_sign'] : $locale['negative_sign'];
			
			switch (true) {
				case $locale["{$letter}_sign_posn"] == 1 && $flags['usesignal'] == '+':
					$prefix = $signal;
					break;
				case $locale["{$letter}_sign_posn"] == 2 && $flags['usesignal'] == '+':
					$suffix = $signal;
					break;
				case $locale["{$letter}_sign_posn"] == 3 && $flags['usesignal'] == '+':
					$cprefix = $signal;
					break;
				case $locale["{$letter}_sign_posn"] == 4 && $flags['usesignal'] == '+':
					$csuffix = $signal;
					break;
				case $flags['usesignal'] == '(':
				case $locale["{$letter}_sign_posn"] == 0:
					$prefix = '(';
					$suffix = ')';
					break;
			}
			if (!$flags['nosimbol']) {
				$currency = $cprefix . ($conversion == 'i' ? $locale['int_curr_symbol'] : $locale['currency_symbol']).$csuffix;
			} else {
				$currency = '';
			}
			$space  = $locale["{$letter}_sep_by_space"] ? ' ' : '';
			$value = number_format($value, $right, $locale['mon_decimal_point'], $flags['nogroup'] ? '' : $locale['mon_thousands_sep']);
			$value = @explode($locale['mon_decimal_point'], $value);
	
			$n = strlen($prefix) + strlen($currency) + strlen($value[0]);
			if ($left > 0 && $left > $n) {
				$value[0] = str_repeat($flags['fillchar'], $left - $n) . $value[0];
			}
			$value = implode($locale['mon_decimal_point'], $value);
			if ($locale["{$letter}_cs_precedes"]) {
				$value = $prefix . $currency . $space . $value . $suffix;
			} else {
				$value = $prefix . $value . $space . $currency . $suffix;
			}
			if ($width > 0) {
				$value = str_pad($value, $width, $flags['fillchar'], $flags['isleft'] ?
						STR_PAD_RIGHT : STR_PAD_LEFT);
			}
			$format = str_replace($fmatch[0], $value, $format);
		}
		return $format;
	}
}