<?php
namespace Pecee;
class Mcrypt {
	public static function Encrypt($data_input, $key){     
	    $td = mcrypt_module_open(MCRYPT_CAST_256, '', 'ecb', '');
	    $iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	    mcrypt_generic_init($td, $key, $iv);
	    $encrypted_data = mcrypt_generic($td, $data_input);
	    mcrypt_generic_deinit($td);
	    mcrypt_module_close($td);
	    $encoded_64 = base64_encode($encrypted_data);
	    return $encoded_64;
	}   

	public static function Decrypt($encoded_64, $key){
	    $decoded_64 = base64_decode($encoded_64);
	    $td = mcrypt_module_open(MCRYPT_CAST_256, '', 'ecb', '');
	    $iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	    mcrypt_generic_init($td, $key, $iv);
	    $decrypted_data = mdecrypt_generic($td, $decoded_64);
	    mcrypt_generic_deinit($td);
	    mcrypt_module_close($td);
	    return $decrypted_data;
	}
	
	/**
	 * Creates an random password, with a given length.
	 *
	 * @param int $length
	 * @return string
	 */
	public static function createRandomPassword($length) {
		if(!\Pecee\Integer::is_int($length)) throw new \Exception("Unknown datatype for length. Must be INT or nummeric string.");
		$chars = "ABCDEFGHIJKLMNOPQRSTUVXYXW023456789";
		srand((double)microtime()*1000000);
		$i = 0;
		$pass = '' ;
			while ($i <= $length) {
				$num = rand() % 33;
				$tmp = substr($chars, $num, 1);
				$pass = $pass . $tmp;
				$i++;
			}
		return $pass;
	}
	    
}