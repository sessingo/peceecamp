<?php
/* Defines include paths */
function abspath() {
	return substr(__FILE__,0,strlen(__FILE__) - strlen('config/routes.php'));
}

function __autoload($class){
	$file = NULL;
	if(strpos($class, 'Pecee\\') !== FALSE) {
		$file = str_replace('Pecee\\', '', $class).'.php';
		$file = abspath() . 'lib'. DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $file);
	} else {
		$file=abspath() . 'lib'. DIRECTORY_SEPARATOR . str_replace('_','/',substr($class, (strpos($class, '_')+1))).'.php';
		$file=str_replace( '_', DIRECTORY_SEPARATOR, $class ).'.php';
	}
	if($file) {
		$exists = stream_resolve_include_path($file);
		if($exists !== FALSE) {
			include_once $file;
		}
	}
}

set_include_path(get_include_path() . PATH_SEPARATOR . abspath() . 'lib' . DIRECTORY_SEPARATOR . PATH_SEPARATOR . abspath());
$modules = \Pecee\Module::GetInstance()->getModules();
if($modules) {
	foreach($modules as $module) {
		set_include_path(get_include_path().PATH_SEPARATOR.$module . DIRECTORY_SEPARATOR);
		set_include_path(get_include_path().PATH_SEPARATOR.$module . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR);
	}
}